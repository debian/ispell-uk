SFX V Y 100
SFX V ий ого [^ц]ий # відісланий відісланого (Р.З.) @ adj:m:v_rod/v_zna//n:v_rod
SFX V ий ому [^ц]ий # відісланий відісланому (Д.М.) @ adj:m:v_dav/v_mis//n:v_dav/v_mis
SFX V ий им ий # відісланий відісланим (О. Мн:Д.) @ adj:m:v_oru//n:v_oru//p:v_dav
SFX V ий ім ий # відісланий відісланім (М.) @ adj:m:v_mis//n:v_mis
SFX V ий а [^ц]ий # відісланий відіслана (Н.) @ adj:f:v_naz
SFX V ий ої [^ц]ий # відісланий відісланої (Р.) @ adj:f:v_rod
SFX V ий ій ий # відісланий відісланій (Д.) @ adj:f:v_dav/v_mis
SFX V ий у [^ц]ий # відісланий відіслану (З.) @ adj:f:v_zna
SFX V ий ою [^ц]ий # відісланий відісланою (О.) @ adj:f:v_oru
SFX V ий е ий # відісланий відіслане (Н.) @ adj:n:v_naz/v_zna
SFX V ий і ий # відісланий відіслані (Н.) @ adj:p:v_naz/v_zna
SFX V ий их ий # відісланий відісланих (Р.) @ adj:p:v_rod/v_zna/v_mis
SFX V ий ими ий # відісланий відісланими (О.) @ adj:p:v_oru
SFX V е ого е # дизпальне дизпального (Р.З.) @ adj:n:v_rod
SFX V е ому е # дизпальне дизпальному (Д.М.) @ adj:n:v_dav/v_mis
SFX V е им е # дизпальне дизпальним (О.) @ adj:n:v_oru
SFX V я ьої я # незаміжня незаміжньої (Р.) @ adj:f:v_rod
SFX V я ій я # незаміжня незаміжній (Д.) @ adj:f:v_dav/v_mis
SFX V я ю я # незаміжня незаміжню (З.) @ adj:f:v_zna
SFX V я ьою я # незаміжня незаміжньою (О.) @ adj:f:v_oru
SFX V а ої а # суперновя супернової (Р.) @ adj:f:v_rod
SFX V а ій а # суперновя суперновій (Д.) @ adj:f:v_dav/v_mis
SFX V а у а # суперновя супернову (З.) @ adj:f:v_zna
SFX V а ою а # суперновя суперновою (О.) @ adj:f:v_oru
SFX V ий ього лиций # білолиций білолицього (Р.З.) @ adj:m:v_rod/v_zna//n:v_rod/v_zna
SFX V ий ьому лиций # білолиций білолицьому (Д.М.) @ adj:m:v_dav/v_mis//n:v_rod/v_zna
SFX V ий я лиций # білолиций білолиця (Н.) @ adj:f:v_naz
SFX V ий ьої лиций # білолиций білолицьої (Р.) @ adj:f:v_rod
SFX V ий ю лиций # білолиций білолицю (З.) @ adj:f:v_zna
SFX V ий ьою лиций # білолиций білолицьою (О.) @ adj:f:v_oru
SFX V ий ого [^л]иций # ниций ницого (Р.З.) @ adj:m:v_rod/v_zna//n:v_rod/v_zna
SFX V ий ому [^л]иций # ниций ницому (Д.М.) @ adj:m:v_dav/v_mis//n:v_rod/v_zna
SFX V ий а [^л]иций # ниций ница (Н.) @ adj:f:v_naz
SFX V ий ої [^л]иций # ниций ницої (Р.) @ adj:f:v_rod
SFX V ий у [^л]иций # ниций ницу (З.) @ adj:f:v_zna
SFX V ий ою [^л]иций # ниций ницою (О.) @ adj:f:v_oru
SFX V ий ого [^и]ций # куций куцого (Р.З.) @ adj:m:v_rod/v_zna//n:v_rod/v_zna
SFX V ий ому [^и]ций # куций куцому (Д.М.) @ adj:m:v_dav/v_mis//n:v_dav/v_mis
SFX V ий а [^и]ций # куций куца (Н.) @ adj:f:v_naz
SFX V ий ої [^и]ций # куций куцої (Р.) @ adj:f:v_rod
SFX V ий у [^и]ций # куций куцу (З.) @ adj:f:v_zna
SFX V ий ою [^и]ций # куций куцою (О.) @ adj:f:v_oru
SFX V ій ього ій # синій синього (Р.) @ adj:m:v_rod/v_zna//n:v_rod/v_zna
SFX V ій ьому ій # синій синьому (Д.) @ adj:m:v_dav/v_mis//n:v_dav/v_mis
SFX V ій ім ій # синій синім (О. Мн.:Д.) @ adj:m:v_oru/v_mis//n:v_oru/v_mis//p:v_dav
SFX V ій я ій # синій синя (Н.) @ adj:f:v_naz
SFX V ій ьої ій # синій синьої (Р.) @ adj:f:v_rod
SFX V ій ю ій # синій синю (З.) @ adj:f:v_zna
SFX V ій ьою ій # синій синьою (О.) @ adj:f:v_oru
SFX V ій є ій # синій синє (Н.) @ adj:n:v_naz/v_zna
SFX V й 0 [їі]й # синій сині (Н.) @ adj:p:v_naz/v_zna
SFX V й х [їі]й # синій синіх (Р.З.М.) @ adj:p:v_rod/v_zna/v_mis
SFX V й ми [їі]й # синій синіми (О.) @ adj:p:v_oru
SFX V їй його їй # безкраїй безкрайого (Р.З.) @ adj:m:v_rod/v_zna//n:v_rod/v_zna
SFX V їй йому їй # безкраїй безкрайому (Д.М.) @ adj:m:v_dav/v_mis//n:v_dav/v_mis
SFX V їй їм їй # безкраїй безкраїм (О.М. Мн.:Д.) @ adj:m:v_oru/v_zna//n:v_oru/v_zna//p:v_dav
SFX V їй я їй # безкраїй безкрая (Н.) @ adj:f:v_naz
SFX V їй йої їй # безкраїй безкрайої (Р.) @ adj:f:v_rod
SFX V їй ю їй # безкраїй безкраю (З.) @ adj:f:v_zna
SFX V їй йою їй # безкраїй безкрайою (О.) @ adj:f:v_oru
SFX V їй є їй # безкраїй безкрає (Н.) @ adj:n:v_naz/v_zna
SFX V 0 ого [їи]н # сестрин сестриного (Р.) @ adj:m:v_rod/v_zna//n:v_rod
SFX V 0 ому [їи]н # сестрин сестриному (Д.М.) @ adj:m:v_dav/v_mis//n:v_dav/v_mis
SFX V 0 им [їи]н # сестрин сестриним (О. Мн:Д.) @ adj:m:v_oru//n:v_oru//p:v_dav
SFX V 0 ім [їи]н # сестрин сестринім (М.) @ adj:m:v_mis//n:v_mis
SFX V 0 а [їи]н # сестрин сестрина (Н.) @ adj:f:v_naz
SFX V 0 ої [їи]н # сестрин сестриної (Р.) @ adj:f:v_rod
SFX V 0 ій [їи]н # сестрин сестриній (Д.М.) @ adj:f:v_dav/v_mis
SFX V 0 у [їи]н # сестрин сестрину (З.) @ adj:f:v_zna
SFX V 0 ою [їи]н # сестрин сестриною (О.) @ adj:f:v_oru
SFX V 0 е [їи]н # сестрин сестрине (Н.) @ adj:n:v_naz/v_zna
SFX V 0 і [їи]н # сестрин сестрині (Н.) @ adj:p:v_naz/v_zna
SFX V 0 их [їи]н # сестрин сестриних (Р.З.) @ adj:p:v_rod/v_zna
SFX V 0 ими [їи]н # сестрин сестриними (О.) @ adj:p:v_oru
SFX V ів ового ів # братів братового (Р.) @ adj:m:v_rod/v_zna//n:v_rod
SFX V ів овому ів # братів братовому (Д.) @ adj:m:v_dav/v_mis//n:v_dav/v_mis
SFX V ів овим ів # братів братовим (О. Мн:Д.) @ adj:m:v_oru//n:v_oru//p:v_dav
SFX V ів овім ів # братів братовім (М.) @ adj:m:v_mis//n:v_mis
SFX V ів ова ів # братів братова (Н.) @ adj:f:v_naz
SFX V ів ової ів # братів братової (Р.) @ adj:f:v_rod
SFX V ів овій ів # братів братовій (Д.М.) @ adj:f:v_dav/v_mis
SFX V ів ову ів # братів братову (З.) @ adj:f:v_zna
SFX V ів овою ів # братів братовою (О.) @ adj:f:v_oru
SFX V ів ове ів # братів братове (Н.) @ adj:n:v_naz/v_zna
SFX V ів ові ів # братів братові (Н.) @ adj:p:v_naz/v_zna
SFX V ів ових ів # братів братових (Р.З.) @ adj:p:v_rod/v_zna/v_mis
SFX V ів овими ів # братів братовими (О.) @ adj:p:v_oru
SFX V їв євого їв # Сергіїв Сергієвого (Р.) @ adj:m:v_rod/v_zna//n:v_rod
SFX V їв євому їв # Сергіїв Сергієвому (Д.) @ adj:m:v_dav/v_mis//n:v_dav/v_mis
SFX V їв євим їв # Сергіїв Сергієвим (О. Мн:Д.) @ adj:m:v_oru//n:v_oru//p:v_dav
SFX V їв євім їв # Сергіїв Сергієвім (М.) @ adj:m:v_mis//n:v_mis
SFX V їв єва їв # Сергіїв Сергієва (Н.) @ adj:f:v_naz
SFX V їв євої їв # Сергіїв Сергієвої (Р.) @ adj:f:v_rod
SFX V їв євій їв # Сергіїв Сергієвій (Д.М.) @ adj:f:v_dav/v_mis
SFX V їв єву їв # Сергіїв Сергієву (З.) @ adj:f:v_zna
SFX V їв євою їв # Сергіїв Сергієвою (О.) @ adj:f:v_rod
SFX V їв єве їв # Сергіїв Сергієве (Н.) @ adj:n:v_naz/v_zna
SFX V їв єві їв # Сергіїв Сергієві (Н.) @ adj:p:v_naz
SFX V їв євих їв # Сергіїв Сергієвих (Р.З.) @ adj:p:v_rod/v_mis
SFX V їв євими їв # Сергіїв Сергієвими (О.) @ adj:p:v_oru
SFX U Y 33
SFX U ів евого ів # лікарів лікаревого (Р.) @ adj:m:v_rod/v_zna//n:v_rod
SFX U ів евому ів # лікарів лікаревому (Д.) @ adj:m:v_dav/v_mis//n:v_dav/v_mis
SFX U ів евим ів # лікарів лікаревим (О. Мн:Д.) @ adj:m:v_oru//n:v_oru//p:v_dav
SFX U ів евім ів # лікарів лікаревім (М.) @ adj:m:v_mis//n:v_mis
SFX U ів ева ів # лікарів лікарева (Н.) @ adj:f:v_naz
SFX U ів евої ів # лікарів лікаревої (Р.) @ adj:f:v_rod
SFX U ів евій ів # лікарів лікаревій (Д.М.) @ adj:f:v_dav/v_mis
SFX U ів еву ів # лікарів лікареву (З.) @ adj:f:v_zna
SFX U ів евою ів # лікарів лікаревою (О.) @ adj:f:v_oru
SFX U ів еве ів # лікарів лікареве (Н.) @ adj:n:v_naz/v_zna
SFX U ів еві ів # лікарів лікареві (Н.З.) @ adj:p:v_naz/v_zna
SFX U ів евих ів # лікарів лікаревих (Р.З.М. @ adj:p:v_rod/v_zna/v_mis
SFX U ів евими ів # лікарів лікаревими (О.) @ adj:p:v_oru
SFX U в ва [оеє]в # Іванов Іванова (Р.Зн. Ж:Н.) @ noun:m:v_rod/v_zna//f:v_naz
SFX U в ву [оеє]в # Іванов Іванову (Д.М. Ж:З.) @ noun:m:v_dav/v_mis//f:v_zna
SFX U в вим [оеє]в # Іванов Івановим (О. Мн:Д.) @ noun:m:v_oru//p:v_dav
SFX U в ві [оеє]в # Іванов Іванові (М.) @ noun:m:v_mis
SFX U в вої [оеє]в # Іванов Іванової (Ж:Р.) @ noun:f:v_rod
SFX U в вій [оеє]в # Іванов Івановій (Ж:Д.М.) @ noun:f:v_dav/v_mis
SFX U в вою [оеє]в # Іванов Івановою (Ж:О.) @ noun:f:v_oru
SFX U ін іна ін # Іванін Іваніна (Р.Зн. Ж:Н.) @ noun:m:v_rod/v_zna//f:v_naz
SFX U ін іну ін # Іванін Іваніну (Д.М. Ж:З.) @ noun:m:v_dav/v_zna//f:v_zna
SFX U ін іним ін # Іванін Іваніним (О. Мн:Д.) @ noun:m:v_oru//p:v_dav
SFX U ін іні ін # Іванін Іваніні (М.) @ noun:m:v_mis
SFX U ін іної ін # Іванін Іваніної (Ж:Р.) @ noun:f:v_rod
SFX U ін іній ін # Іванін Іваніній (Ж:Д.М.) @ noun:f:v_dav/v_mis
SFX U ін іною ін # Іванін Іваніною (Ж:О.) @ noun:f:v_oru
SFX U в ви [оеє]в # Іванов Іванови (Мн:Н.) @ noun:p:v_naz
SFX U в вих [оеє]в # Іванов Іванових (Мн:Р.З.М.) @ noun:p:v_rod/v_zna/v_mis
SFX U в вими [оеє]в # Іванов Івановими (Мн:О.) @ noun:p:v_oru
SFX U ін іни ін # Іванін Іваніни (Мн:Н.) @ noun:p:v_naz
SFX U ін іних ін # Іванін Іваніних (Мн:Р.З.М.) @ noun:p:v_rod/v_zna/v_mis
SFX U ін іними ін # Іванін Іваніними (Мн:О.) @ noun:p:v_oru
SFX W Y 4
SFX W ий о [^жшщ]ий # швидкий швидко @ adv
SFX W ій ьо ій # синій синьо @ adv
SFX W їй йо їй # безкраїй безкрайо @ adv
SFX W ти но ти # казати казано @ impers
SFX 0 Y 23
SFX 0 увати овано увати # фарбувати фарбовано @ impers
SFX 0 ювати йовано [іоу]ювати # емулювати емульовано @ impers
SFX 0 ювати ьовано [^іоу]ювати # емулювати емульовано @ impers
SFX 0 ити жено [^з]дити # знешкодити знешкоджено @ impers
SFX 0 здити жджено здити # знешкоджено @ impers
SFX 0 зити жено зити # знешкодити знешкоджено @ impers
SFX 0 вити влено вити # заправити заправлено @ impers
SFX 0 стити щено стити # вмістити вміщено @ impers
SFX 0 тити чено [^с]тити # конопатити конопачено @ impers
SFX 0 сити шено сити # вгасити вгашено @ impers
SFX 0 бити блено бити # заліпити заліплено @ impers
SFX 0 мити млено мити # заліпити заліплено @ impers
SFX 0 пити плено пити # заліпити заліплено @ impers
SFX 0 ити ено [жлнрчшщ]ити # завантажити завантажено @ impers
SFX 0 іти жено [^з]діти # відсидіти відсиджено @ impers
SFX 0 ути ено нути # вдягнути вдягнено @ impers
SFX 0 їти єно їти # вдостоїти вдостоєно @ impers
SFX 0 оти ено оти # заколоти заколено @ impers
SFX 0 ти ено [зс]ти # привнести привнесено @ impers
SFX 0 гти жено гти # запрягти запряжено @ impers
SFX 0 кти чено кти # запекти запечено @ impers
SFX 0 ти дено йти # винайти винайдено @ impers
SFX 0 ти лено бти # вискубти вискублено @ impers
SFX 1 Y 2
SFX 1 сти тено сти # мести метено @ impers
SFX 1 гнути жено гнути # вивергнути вивержено @ impers
SFX 2 Y 1
SFX 2 ти то ти # вимити вимито @ impers
SFX 3 Y 1
SFX 3 сти дено сти # ввести введено @ impers
SFX a Y 241
SFX a а и [^жчшщ]а # хата хати (Р.) @ noun:f:v_rod//p:v_naz
SFX a а і [^гґкхжчшщ]а # хата хаті (Д.М.) @ noun:f:v_dav/v_mis
SFX a а і [жчшщ]а # їжа їжі (Р.Д.М.) @ noun:f:v_rod/v_dav/v_mis//p:v_naz
SFX a а у а # хата хату (З.) @ noun:f:v_zna
SFX a а ою [^жчшщ]а # хата хатою (О.) @ noun:f:v_oru
SFX a а ею [жчшщ]а # душа душею (О.) @ noun:f:v_oru
SFX a га зі га # допомога допомозі (Д.М.) @ noun:f:v_dav/v_mis
SFX a ґа зі ґа # дзиґа дзизі (Д.М.) @ noun:f:v_dav/v_mis
SFX a ка ці [^к]ка # ріка ріці (Д.М.) @ noun:f:v_dav/v_mis
SFX a кка цці кка # мекка мецці (Д.М.) @ noun:f:v_dav/v_mis
SFX a ха сі ха # свекруха свекрусі (Д.М.) @ noun:f:v_dav/v_mis
SFX a я і [^іеиаоу'ь]я # вишня вишні (Р.Д.М.) @ noun:f:v_rod/v_dav/v_mis//p:v_naz
SFX a я ю я # вишня вишню (З.) @ noun:f:v_zna
SFX a я ею [^іеиаоу'ь]я # вишня вишнею (О.) @ noun:f:v_oru
SFX a я ї ['ьіеиаоу]я # сім'я сім'ї (Р.Д.М.) @ noun:f:v_rod/v_dav/v_mis//p:v_naz
SFX a я єю ['ьіеиаоу]я # сім'я сім'єю (О.) @ noun:f:v_oru
SFX a ір ору [^л]ір # вибір вибору (Р.) @ noun:m:v_dav
SFX a ір орові [^л]ір # вибір виборові (Д.) @ noun:m:v_dav/v_mis
SFX a ір ором [^л]ір # вибір вибором (О.) @ noun:m:v_oru
SFX a ір орі [^л]ір # вибір виборі (М.) @ noun:m:v_mis
SFX a ір ьору лір # колір кольору (Р.) @ noun:m:v_dav
SFX a ір ьорові лір # колір кольорові (Д.) @ noun:m:v_dav/v_mis
SFX a ір ьором лір # колір кольором (О.) @ noun:m:v_oru
SFX a ір ьорі лір # колір кольорі (М.) @ noun:m:v_mis
SFX a ін ону ін # загін загону (Д.Р.) @ noun:m:v_dav
SFX a ін онові ін # загін загонові (Д.) @ noun:m:v_dav
SFX a ін оном ін # загін загоном (О.) @ noun:m:v_oru
SFX a ін оні ін # загін загоні (М.) @ noun:m:v_mis
SFX a іг огу іг # батіг батогу (Д.) @ noun:m:v_dav
SFX a іг огові іг # батіг батогові (Д.М.) @ noun:m:v_dav
SFX a іг огом іг # батіг батогом (О.) @ noun:m:v_oru
SFX a іг озі іг # батіг батозі (М.) @ noun:m:v_mis
SFX a ід оду [^л]ід # провід проводу (Д.Р.) @ noun:m:v_dav
SFX a ід одові [^л]ід # провід проводові (Д.) @ noun:m:v_dav
SFX a ід одом [^л]ід # провід проводом (О.) @ noun:m:v_oru
SFX a ід оді [^л]ід # провід проводі (М.) @ noun:m:v_mis
SFX a ід ьоду [^пг]лід # провід проводу (Д.Р.) @ noun:m:v_dav
SFX a ід ьодові [^пг]лід # провід проводові (Д.) @ noun:m:v_dav
SFX a ід ьодом [^пг]лід # провід проводом (О.) @ noun:m:v_oru
SFX a ід ьоді [^пг]лід # провід проводі (М.) @ noun:m:v_mis
SFX a ід оду [пг]лід # приплід приплоду (Д.Р.) @ noun:m:v_dav
SFX a ід одові [пг]лід # приплід приплодові (Д.) @ noun:m:v_dav
SFX a ід одом [пг]лід # приплід приплодом (О.) @ noun:m:v_oru
SFX a ід оді [пг]лід # приплід приплоді (М.) @ noun:m:v_mis
SFX a іб обу іб # засіб засобу (Д.Р.) @ noun:m:v_dav
SFX a іб обові іб # засіб засобові (Д.) @ noun:m:v_dav
SFX a іб обом іб # засіб засобом (О.) @ noun:m:v_oru
SFX a іб обі іб # засіб засобі (М.) @ noun:m:v_mis
SFX a іп опу іп # ЧіП ЧоПу (Д.Р.) @ noun:m:v_dav
SFX a іп опові іп # ЧіП ЧоПові (Д.) @ noun:m:v_dav/v_mis
SFX a іп опом іп # ЧіП ЧоПом (О.) @ noun:m:v_oru
SFX a іп опі іп # ЧіП ЧоПі (М.) @ noun:m:v_mis
SFX a івш овшу івш # ківш ковшу (Д.Р.) @ noun:m:v_dav/v_mis
SFX a івш овшеві івш # ківш ковшеві (Д.) @ noun:m:v_dav/v_mis
SFX a івш овшем івш # ківш ковшем (О.) @ noun:m:v_oru
SFX a івш овші івш # ківш ковші (М.) @ noun:m:v_mis//p:v_naz
SFX a ізд озду ізд # дрізд дрозду (Д.Р.) @ noun:m:v_dav
SFX a ізд оздові ізд # дрізд дроздові (Д.) @ noun:m:v_dav/v_mis
SFX a ізд оздом ізд # дрізд дроздом (О.) @ noun:m:v_oru
SFX a ізд озді ізд # дрізд дрозді (М.) @ noun:m:v_mis
SFX a іл олу іл # дозвіл дозволу (Д.Р.) @ noun:m:v_dav
SFX a іл олові іл # дозвіл дозволові (Д.) @ noun:m:v_dav
SFX a іл олом іл # дозвіл дозволом (О.) @ noun:m:v_oru
SFX a іл олі іл # дозвіл дозволі (М.) @ noun:m:v_mis
SFX a ів ову ів # острову (Д.Р.) @ noun:m:v_dav/v_mis
SFX a ів овом ів # островом (О.) @ noun:m:v_oru
SFX a ів ові ів # острові (М.) @ noun:m:v_mis
SFX a їв єву їв # Києву (Д.Р.) @ noun:m:v_dav/v_mis
SFX a їв євом їв # Києвом (О.) @ noun:m:v_oru
SFX a їв єві їв # Києві (М.) @ noun:m:v_mis
SFX a ік оку ік # року (Д.Р.) @ noun:m:v_dav/v_oru
SFX a ік оком ік # роком (О.) @ noun:m:v_oru
SFX a ік оці ік # році (М.) @ noun:m:v_mis
SFX a іск оску іск # воску (Д.Р.) @ noun:m:v_dav
SFX a іск оскові іск # воскові (Д.) @ noun:m:v_dav
SFX a іск оском іск # воском (О.) @ noun:m:v_oru
SFX a іст осту іст # росту (Д.Р.) @ noun:m:v_dav
SFX a іст остові іст # ростові (Д.) @ noun:m:v_dav
SFX a іст остом іст # ростом (О.) @ noun:m:v_oru
SFX a іст ості іст # рості (М.) @ noun:m:v_mis
SFX a іс осу [кнч]іс # носу (Д.Р.) @ noun:m:v_dav
SFX a іс осові [кнч]іс # носові (Д.М.) @ noun:m:v_dav
SFX a іс осом [кнч]іс # носом (О.) @ noun:m:v_oru
SFX a іс осі [кнч]іс # носі (М.) @ noun:m:v_mis
SFX a іт оту [^л]іт # гуркоту (Д.Р.) @ noun:m:v_dav
SFX a іт отові [^л]іт # гуркотові (Д.) @ noun:m:v_dav
SFX a іт отом [^л]іт # гуркотом (О.) @ noun:m:v_oru
SFX a іт оті [^л]іт # гуркоті (М.) @ noun:m:v_mis
SFX a іт ьоту [^п]літ # польоту (Д.Р.) @ noun:m:v_dav
SFX a іт ьотові [^п]літ # польотові (Д.) @ noun:m:v_dav
SFX a іт ьотом [^п]літ # польотом (О.) @ noun:m:v_oru
SFX a іт ьоті [^п]літ # польоті (М.) @ noun:m:v_mis
SFX a іт оту [п]літ # плоту (Д.Р.) @ noun:m:v_dav
SFX a іт отові [п]літ # плотові (Д.) @ noun:m:v_dav
SFX a іт отом [п]літ # плотом (О.) @ noun:m:v_oru
SFX a іт оті [п]літ # плоті (М.) @ noun:m:v_mis
SFX a із озу із # возу (Д.Р.) @ noun:m:v_dav
SFX a із озові із # возові (Д.) @ noun:m:v_dav
SFX a із озом із # возом (О.) @ noun:m:v_oru
SFX a із озі із # возі (М.) @ noun:m:v_mis
SFX a іш ошу іш # грошу (Д.Р.) @ noun:m:v_dav/v_mis
SFX a іш ошеві іш # грошеві (Д.) @ noun:m:v_dav
SFX a іш ошем іш # грошем (О.) @ noun:m:v_oru
SFX a іш оші іш # гроші (М.) @ noun:m:v_mis//p:v_naz
SFX a іж ожу [^тбд]іж # ножу (Д.Р.) @ noun:m:v_dav
SFX a іж ожеві [^тбд]іж # ножеві (Д.) @ noun:m:v_dav
SFX a іж ожем [^тбд]іж # ножем (О.) @ noun:m:v_oru
SFX a іж ожі [^тбд]іж # ножі (М.) @ noun:m:v_mis//p:v_naz
SFX a іж ожу е[тб]іж # небожу (Д.Р.) @ noun:m:v_dav
SFX a іж ожеві е[тб]іж # небожеві (Д.) @ noun:m:v_dav
SFX a іж ожем е[тб]іж # небожем (О.) @ noun:m:v_oru
SFX a іж ожі е[тб]іж # небожі (М.) @ noun:m:v_mis//p:v_naz
SFX a іж ежу [^е][тбд]іж # рубежу (Д.Р.) @ noun:m:v_dav
SFX a іж ежеві [^е][тбд]іж # рубежеві (Д.) @ noun:m:v_dav
SFX a іж ежем [^е][тбд]іж # рубежем (О.) @ noun:m:v_oru
SFX a іж ежі [^е][тбд]іж # рубежі (М.) @ noun:m:v_mis//p:v_naz
SFX a ен ну ен # човен човну (Д.Р.) @ noun:m:v_dav
SFX a ен ном ен # човен човном (О.) @ noun:m:v_oru
SFX a ен ні ен # човен човні (М.) @ noun:m:v_mis
SFX a інь оню [к]інь # кінь коню (Д.) @ noun:m:v_dav
SFX a інь оневі [к]інь # кінь коневі (Д.М.) @ noun:m:v_dav
SFX a інь онем [к]інь # кінь конем (О.) @ noun:m:v_oru
SFX a інь оні [к]інь # кінь коні (М.) @ noun:m:v_mis//p:v_naz
SFX a інь еню [^ксв]інь # корінь кореню (Д.) @ noun:m:v_dav/v_mis
SFX a інь еневі [^ксв]інь # корінь кореневі (Д.) @ noun:m:v_dav
SFX a інь енем [^ксв]інь # корінь коренем (О.) @ noun:m:v_oru
SFX a інь ені [^ксв]інь # корінь корені (М.) @ noun:m:v_mis//p:v_naz
SFX a інь еню [^о][св]інь # ревінь ревеню (Д.) @ noun:m:v_dav/v_mis
SFX a інь еневі [^о][св]інь # ревінь ревеневі (Д.) @ noun:m:v_dav
SFX a інь енем [^о][св]інь # ревінь ревенем (О.) @ noun:m:v_dav/v_oru
SFX a інь ені [^о][св]інь # ревінь ревені (М.) @ noun:m:v_mis//p:v_naz
SFX a ень ню ень # день дню (Д.) @ noun:m:v_dav/v_mis
SFX a ень неві ень # день дневі (З.) @ noun:m:v_dav
SFX a ень нем ень # день днем (О.) @ noun:m:v_oru
SFX a ень ні ень # день дні (M.) @ noun:m:v_mis//p:v_naz
SFX a онь ню онь # вогонь вогню (Д.) @ noun:m:v_dav/v_mis
SFX a онь неві онь # вогонь вогневі (З.) @ noun:m:v_dav
SFX a онь нем онь # вогонь вогнем (О.) @ noun:m:v_oru
SFX a онь ні онь # вогонь вогні (M.) @ noun:m:v_mis//p:v_naz
SFX a оль лю оль # кухоль кухлю (Д.) @ noun:m:v_dav/v_mis
SFX a оль леві оль # кухоль кухлеві (З.) @ noun:m:v_dav
SFX a оль лем оль # кухоль кухлем (О.) @ noun:m:v_oru
SFX a оль лі оль # кухоль кухлі (M.) @ noun:m:v_mis//p:v_naz
SFX a оть тю оть # ніготь нігтю (Д.) @ noun:m:v_dav/v_mis
SFX a оть теві оть # ніготь нігтеві (Д.) @ noun:m:v_dav
SFX a оть тем оть # ніготь нігтем (О.) @ noun:m:v_oru
SFX a оть ті оть # ніготь нігті (M.) @ noun:m:v_mis//p:v_naz
SFX a ій ою ій # рій рою (Д.Р.) @ noun:m:v_dav/v_mis
SFX a ій оєві ій # рій роєві (Д.) @ noun:m:v_dav
SFX a ій оєм ій # рій роєм (О.) @ noun:m:v_oru
SFX a ій ої ій # рій рої (М.) @ noun:m:v_mis//p:v_naz
SFX a ідь едю ідь # ведмідь ведмедю (Д.) @ noun:m:v_dav/v_mis
SFX a ідь едеві ідь # ведмідь ведмедеві (Д.) @ noun:m:v_dav/v_mis
SFX a ідь едем ідь # ведмідь ведмедем (О.) @ noun:m:v_oru
SFX a ідь еді ідь # ведмідь ведмеді (М.) @ noun:m:v_mis//p:v_naz
SFX a іль елю іль # важіль важелю (Д.) @ noun:m:v_dav
SFX a іль елеві іль # важіль важелеві (Д.) @ noun:m:v_dav
SFX a іль елем іль # важіль важелем (О.) @ noun:m:v_oru
SFX a іль елі іль # важіль важелі (М.) @ noun:m:v_mis//p:v_naz
SFX a ість остю ість # гість гостю (Д.) @ noun:m:v_dav/v_mis
SFX a ість остеві ість # гість гостеві (Д.) @ noun:m:v_dav/v_mis
SFX a ість остем ість # гість гостем (О.) @ noun:m:v_oru
SFX a ість ості ість # гість гості (М.) @ noun:m:v_mis//p:v_naz
SFX a ок ку ок # будиночок будиночку (З.) @ noun:m:v_dav/v_mis
SFX a ок кові ок # будиночок будиночкові (Д.) @ noun:m:v_dav/v_mis
SFX a ок ком ок # будиночок будиночком (О.) @ noun:m:v_oru
SFX a ол лу ол # вузол вузлу (З.) @ noun:m:v_dav
SFX a ол лові ол # вузол вузлові (Д.) @ noun:m:v_dav
SFX a ол лі ол # вузол вузлі (M.) @ noun:m:v_mis
SFX a ол лом ол # вузол вузлом (О.) @ noun:m:v_oru
SFX a ор ру ор # свекор свекру (Д.) @ noun:m:v_dav
SFX a ор ром ор # свекор свекром (О.) @ noun:m:v_oru
SFX a ор рі ор # свекор свекрі (М.) @ noun:m:v_mis
SFX a ор рові ор # свекор свекрові (М.) @ noun:m:v_dav/v_mis
SFX a ер ру ер # вітер вітру (Д.) @ noun:m:v_dav/v_mis
SFX a ер ром ер # вітер вітром (О.) @ noun:m:v_oru
SFX a ер рі ер # вітер вітрі (М.) @ noun:m:v_mis
SFX a ер рові ер # вітер вітрові (М.) @ noun:m:v_dav/v_mis
SFX a ел лу ел # осел ослу (Д.) @ noun:m:v_dav
SFX a ел лом ел # осел ослом (О.) @ noun:m:v_oru
SFX a ел лі ел # осел ослі (М.) @ noun:m:v_mis
SFX a ел лові ел # осел ослові (М.) @ noun:m:v_dav/v_mis
SFX a ет ту ет # оцет оцту (Д.) @ noun:m:v_dav
SFX a ет том ет # оцет оцтом (О.) @ noun:m:v_oru
SFX a ет ті ет # оцет оцті (М.) @ noun:m:v_mis
SFX a ет тові ет # оцет оцтові (М.) @ noun:m:v_dav/v_mis
SFX a ес су ес # пес псу (Д.) @ noun:m:v_dav
SFX a ес сом ес # пес псом (О.) @ noun:m:v_oru
SFX a ес сі ес # пес псі (М.) @ noun:m:v_mis
SFX a ес сові ес # пес псові (М.) @ noun:m:v_dav/v_mis
SFX a ель лю ель # журавель журавлю (Д.) @ noun:m:v_dav/v_mis
SFX a ель лем ель # журавель журавлем (О.) @ noun:m:v_oru
SFX a ель лі ель # журавель журавлі (М.) @ noun:m:v_mis//p:v_naz
SFX a ель леві ель # журавель журавлеві (М.Д.) @ noun:m:v_dav/v_mis
SFX a єць йцю [^о]єць # англієць англійцю (Д.) @ noun:m:v_dav/v_mis
SFX a єць йцеві [^о]єць # англієць англійцеві (М.Д.) @ noun:m:v_dav/v_mis
SFX a єць йці [^о]єць # англієць англійці (M.) @ noun:m:v_mis//p:v_naz
SFX a єць йцем [^о]єць # англієць англійцем (О.) @ noun:m:v_oru
SFX a оєць ійцю оєць # боєць бійцю (Д.) @ noun:m:v_dav/v_mis
SFX a оєць ійцеві оєць # боєць бійцеві (Д.М.) @ noun:m:v_dav/v_mis
SFX a оєць ійці оєць # боєць бійці (M.) @ noun:m:v_mis//p:v_naz
SFX a оєць ійцем оєць # боєць бійцем (О.) @ noun:m:v_oru
SFX a ець цю [^лрнв]ець # горобець горобцю (Д.) @ noun:m:v_dav/v_mis
SFX a ець цеві [^лрнв]ець # горобець горобцеві (З.) @ noun:m:v_dav/v_mis
SFX a ець цем [^лрнв]ець # горобець горобцем (О.) @ noun:m:v_oru
SFX a ець ці [^лрнв]ець # горобець горобці (M.) @ noun:m:v_mis//p:v_naz
SFX a лець льцю лець # бразилець бразильцю (Д.) @ noun:m:v_dav/v_mis
SFX a лець льцеві лець # бразилець бразильцеві (З.) @ noun:m:v_dav/v_mis
SFX a лець льцем лець # бразилець бразильцем (О.) @ noun:m:v_oru
SFX a лець льці лець # бразилець бразильці (M.) @ noun:m:v_mis//p:v_naz
SFX a ець цю [аеєиіїоуюя]рець # борець борцю (Д.) @ noun:m:v_dav/v_mis
SFX a ець цеві [аеєиіїоуюя]рець # борець борцеві (Д.М.) @ noun:m:v_dav/v_mis
SFX a ець цем [аеєиіїоуюя]рець # борець борцем (О.) @ noun:m:v_oru
SFX a ець ці [аеєиіїоуюя]рець # борець борці (М.) @ noun:m:v_mis//p:v_naz
SFX a рець ерцю [^аеєиіїоуюя]рець # жрець жерцю (Д.) @ noun:m:v_dav/v_mis
SFX a рець ерцеві [^аеєиіїоуюя]рець # жрець жерцеві (З.) @ noun:m:v_dav/v_mis
SFX a рець ерці [^аеєиіїоуюя]рець # жрець жерці (M.) @ noun:m:v_mis//p:v_naz
SFX a рець ерцем [^аеєиіїоуюя]рець # жрець жерцем (О.) @ noun:m:v_oru
SFX a ець цю [аеєиіїуюяго][нв]ець # американець американцю (Д.) @ noun:m:v_dav/v_mis
SFX a ець цеві [аеєиіїуюяго][нв]ець # американець американцеві (З.) @ noun:m:v_dav/v_mis
SFX a ець цем [аеєиіїуюяго][нв]ець # американець американцем (О.) @ noun:m:v_oru
SFX a ець ці [аеєиіїуюяго][нв]ець # американець американці (M.) @ noun:m:v_mis//p:v_naz
SFX a нець енцю [^аеєиіїоуюяг]нець # жнець женцю (Д.) @ noun:m:v_dav/v_mis
SFX a нець енцеві [^аеєиіїоуюяг]нець # жнець женцеві (З.) @ noun:m:v_dav/v_mis
SFX a нець енцем [^аеєиіїоуюяг]нець # жнець женцем (О.) @ noun:m:v_oru
SFX a нець енці [^аеєиіїоуюяг]нець # жнець женці (M.) @ noun:m:v_mis//p:v_naz
SFX a вець евцю [^аеєиіїоуюя]вець # швець шевцю (Д.) @ noun:m:v_dav/v_mis
SFX a вець евцеві [^аеєиіїоуюя]вець # швець шевцеві (З.) @ noun:m:v_dav/v_mis
SFX a вець евцем [^аеєиіїоуюя]вець # швець шевцем (О.) @ noun:m:v_oru
SFX a вець евці [^аеєиіїоуюя]вець # швець шевцІ (M.) @ noun:m:v_mis//p:v_naz
SFX a ь и ять # двадцять двадцяти (Р.) @ numr:v_rod/v_dav/v_mis
SFX a ь ьом ять # двадцять двадцятьом (Д.) @ numr:v_dav
SFX a ь ьма ять # двадцять двадцятьма (О.) @ numr:v_oru
SFX a ь ьома ять # двадцять двадцятоьма (О.) @ numr:v_oru
SFX a ь ьох ять # двадцять двадцятох (М.) @ numr:v_rod/v_zna/v_mis
SFX a 0 и сят # п'ятдесят п'ятдесяти (Р.) @ numr:v_rod/v_dav/v_mis
SFX a 0 ьом сят # п'ятдесят п'ятдесятьом (Д.) @ numr:v_dav
SFX a 0 ьма сят # п'ятдесят п'ятдесятьма (О.) @ numr:v_oru
SFX a 0 ьома сят # п'ятдесят п'ятдесятьома (О.) @ numr:v_oru
SFX a 0 ьох сят # п'ятдесят п'ятдесятох (М.) @ numr:v_rod/v_zna/v_mis
SFX a о а сто # сто ста (Р.) @ numr:v_rod/v_dav/v_oru/v_mis
SFX b Y 307
SFX b а 0 [^клн]а # хата хат (Р.) @ noun:p:v_rod
SFX b а 0 [^ст]ла # щогла щогл (Р.) @ noun:p:v_rod
SFX b ла ел [ст]ла # мітла мітел (Р.) @ noun:p:v_rod
SFX b а 0 [^сжзвм]на # батьківщина батьківщин (Р.) @ noun:p:v_rod
SFX b на ен [сжвм]на # сосна сосен (Р.) @ noun:p:v_rod
SFX b на н изна # тризна тризн (Р.) @ noun:p:v_rod
SFX b на ен озна # борозна борозен (Р.) @ noun:p:v_rod
SFX b а 0 [аеєоуиіїяю]ка # автоматика автоматик (Р.) @ noun:p:v_rod
SFX b ка ок [^аеєоуиіїяю]ка # відпустка відпусток (Р.) @ noun:p:v_rod
SFX b я ь [^іеуиаон'ьлтдр]я # Вася Вась (Р.) @ noun:p:v_rod
SFX b я 0 [^ео]ря # буря бур (Р.) @ noun:p:v_rod
SFX b я ь еря # вечеря вечерь (Р.) @ noun:p:v_rod
SFX b оря ір оря # зоря зір (Р.) @ noun:p:v_rod
SFX b я ь [іеиаоюуїяє]ня # богиня богинь (Р.) @ noun:p:v_rod
SFX b ня онь [кх]ня # кухня кухонь (Р.) @ noun:p:v_rod
SFX b йня єнь йня # бойня боєнь (Р.) @ noun:p:v_rod
SFX b ьня ень ьня # вітальня віталень (Р.) @ noun:p:v_rod
SFX b ня ень [^іеиаоюуїяєнкхйь]ня # вишня вишень (Р.) @ noun:p:v_rod
SFX b ля ель [^лоеуаієюяїиск]ля # будівля будівель (Р.) @ noun:p:v_rod
SFX b я ь [лоеуаієюяїиск]ля # Валя Валь (Р.) @ noun:p:v_rod
SFX b я ей адя # попадя попадей (Р.) @ noun:p:v_rod
SFX b я ів [^да]дя # дядя дядів (Р.) @ noun:p:v_rod
SFX b я ь [^тс]тя # Катя Кать (Р.) @ noun:p:v_rod
SFX b ля ей лля # рілля рілей (Р.) @ noun:p:v_rod
SFX b ья ей ья # ескадрилья ескадрилей (Р.) @ noun:p:v_rod
SFX b я ів уддя # суддя суддів (Р.) @ noun:p:v_rod
SFX b дя ей аддя # баддя бадей (Р.) @ noun:p:v_rod
SFX b тя ей ття # стаття статей (Р.) @ noun:p:v_rod
SFX b я ь [^о]стя # причастя причасть (Р.) @ noun:p:v_rod
SFX b я ей остя # гостя гостей (Р.) @ noun:p:v_rod
SFX b 'я ей [^р]'я # сім'я сімей (Р.) @ noun:p:v_rod
SFX b я й [іеиаоу]я # мрія мрій (Р.) @ noun:p:v_rod
SFX b 0 м [ая] # хата хатам (Д.) @ noun:p:v_dav
SFX b 0 ми [ая] # хати хатами (О.) @ noun:p:v_oru
SFX b 0 х [ая] # хата хатах (М.) @ noun:p:v_mis
SFX b ір ори [^л]ір # вибір вибори (Н.) @ noun:p:v_naz
SFX b ір орів [^л]ір # вибір виборів (Р.) @ noun:p:v_rod
SFX b ір орам [^л]ір # вибір виборам (Д.) @ noun:p:v_dav
SFX b ір орами [^л]ір # вибір виборами (О.) @ noun:p:v_oru
SFX b ір орах [^л]ір # вибір виборах (М.) @ noun:p:v_mis
SFX b ір ьори лір # колір кольори (Н.) @ noun:p:v_naz
SFX b ір ьорів лір # колір кольорів (Р.) @ noun:p:v_rod
SFX b ір ьорам лір # колір кольорам (Д.) @ noun:p:v_dav
SFX b ір ьорами лір # колір кольорами (О.) @ noun:p:v_oru
SFX b ір ьорах лір # колір кольорах (М.) @ noun:p:v_mis
SFX b ін они ін # загін загони (Н.) @ noun:p:v_naz
SFX b ін онів ін # загін загонів (Р.) @ noun:p:v_rod
SFX b ін онам ін # загін загонам (Д.) @ noun:p:v_dav
SFX b ін онами ін # загін загонами (О.) @ noun:p:v_oru
SFX b ін онах ін # загін загонах (М.) @ noun:p:v_mis
SFX b іп опи іп # ЧіП ЧоПи (Н.) @ noun:p:v_naz
SFX b іп опів іп # ЧіП ЧоПів (З.) @ noun:p:v_rod
SFX b іп опам іп # ЧіП ЧоПам (Д.) @ noun:p:v_dav
SFX b іп опами іп # ЧіП ЧоПами (О.) @ noun:p:v_oru
SFX b іп опах іп # ЧіП ЧоПах (М.) @ noun:p:v_mis
SFX b івш овшів івш # ківш ковшів (З.) @ noun:p:v_rod
SFX b івш овшам івш # ківш ковшам (Д.) @ noun:p:v_dav
SFX b івш овшами івш # ківш ковшами (О.) @ noun:p:v_oru
SFX b івш овшах івш # ківш ковшах (М.) @ noun:p:v_mis
SFX b ізд озди ізд # дрізд дрозди (Н.) @ noun:p:v_naz
SFX b ізд оздів ізд # дрізд дроздів (Р.) @ noun:p:v_rod
SFX b ізд оздам ізд # дрізд дроздам (Д.) @ noun:p:v_dav
SFX b ізд оздами ізд # дрізд дроздами (О.) @ noun:p:v_oru
SFX b ізд оздах ізд # дрізд дроздах (М.) @ noun:p:v_mis
SFX b іг оги іг # батіг батоги (Н.) @ noun:p:v_naz
SFX b іг огів іг # батіг батогів (З.) @ noun:p:v_rod
SFX b іг огам іг # батіг батогам (Д.) @ noun:p:v_dav
SFX b іг огами іг # батіг батогами (О.) @ noun:p:v_oru
SFX b іг огах іг # батіг батогах (М.) @ noun:p:v_mis
SFX b ід оди [^л]ід # провід проводи (Н.) @ noun:p:v_naz
SFX b ід одів [^л]ід # провід проводів (З.) @ noun:p:v_rod
SFX b ід одам [^л]ід # провід проводам (Д.) @ noun:p:v_dav
SFX b ід одами [^л]ід # провід проводами (О.) @ noun:p:v_oru
SFX b ід одах [^л]ід # провід проводах (М.) @ noun:p:v_mis
SFX b ід оди [пг]лід # (Н.) @ noun:p:v_naz
SFX b ід одів [пг]лід # (З.) @ noun:p:v_rod
SFX b ід одам [пг]лід # (Д.) @ noun:p:v_dav
SFX b ід одами [пг]лід # (О.) @ noun:p:v_oru
SFX b ід одах [пг]лід # (М.) @ noun:p:v_mis
SFX b ід ьоди [^пг]лід # (Н.) @ noun:p:v_naz
SFX b ід ьодів [^пг]лід # (З.) @ noun:p:v_rod
SFX b ід ьодам [^пг]лід # (Д.) @ noun:p:v_dav
SFX b ід ьодами [^пг]лід # (О.) @ noun:p:v_oru
SFX b ід ьодах [^пг]лід # (М.) @ noun:p:v_mis
SFX b іб оби іб # спосіб способи (Н.) @ noun:p:v_naz
SFX b іб обів іб # спосіб способів (З.) @ noun:p:v_rod
SFX b іб обам іб # спосіб способам (Д.) @ noun:p:v_dav
SFX b іб обами іб # спосіб способами (О.) @ noun:p:v_oru
SFX b іб обах іб # спосіб способах (М.) @ noun:p:v_mis
SFX b іл оли іл # дозвіл дозволи (Н.) @ noun:p:v_naz
SFX b іл олів іл # дозвіл дозволів (З.) @ noun:p:v_rod
SFX b іл олам іл # дозвіл дозволам (Д.) @ noun:p:v_dav
SFX b іл олами іл # дозвіл дозволами (О.) @ noun:p:v_oru
SFX b іл олах іл # дозвіл дозволах (М.) @ noun:p:v_mis
SFX b ів ови ів # острови (Н.) @ noun:p:v_naz
SFX b ів овів ів # островів (Р.) @ noun:p:v_rod
SFX b ів овам ів # островам (Д.) @ noun:p:v_dav
SFX b ів овами ів # островами (О.) @ noun:p:v_oru
SFX b ів овах ів # островах (М.) @ noun:p:v_mis
SFX b їв єви їв # Києви :-) (Н.) @ noun:p:v_naz
SFX b їв євів їв # (Р.) @ noun:p:v_rod
SFX b їв євам їв # (Д.) @ noun:p:v_dav
SFX b їв євами їв # (О.) @ noun:p:v_oru
SFX b їв євах їв # (М.) @ noun:p:v_mis
SFX b ік оки ік # рік роки (Н.) @ noun:p:v_naz
SFX b ік оків ік # рік років (З.) @ noun:p:v_rod
SFX b ік окам ік # рік рокам (Д.) @ noun:p:v_dav
SFX b ік оками ік # рік роками (О.) @ noun:p:v_oru
SFX b ік оках ік # рік роках (М.) @ noun:p:v_mis
SFX b іск оски іск # віск воски (Н.) @ noun:p:v_naz
SFX b іск осків іск # віск восків (З.) @ noun:p:v_rod
SFX b іск оскам іск # віск воскам (Д.) @ noun:p:v_dav
SFX b іск осками іск # віск восками (О.) @ noun:p:v_oru
SFX b іск осках іск # віск восках (М.) @ noun:p:v_mis
SFX b іст ости іст # наріст нарости (Н.) @ noun:p:v_naz
SFX b іст остів іст # наріст наростів (З.) @ noun:p:v_rod
SFX b іст остам іст # наріст наростам (Д.) @ noun:p:v_dav
SFX b іст остами іст # наріст наростами (О.) @ noun:p:v_oru
SFX b іст остах іст # наріст наростах (М.) @ noun:p:v_mis
SFX b іс оси [кнч]іс # ніс носи (Н.) @ noun:p:v_naz
SFX b іс осів [кнч]іс # ніс носів (Р.) @ noun:p:v_rod
SFX b іс осам [кнч]іс # ніс носам (Д.) @ noun:p:v_dav
SFX b іс осами [кнч]іс # ніс носами (О.) @ noun:p:v_oru
SFX b іс осах [кнч]іс # ніс носах (М.) @ noun:p:v_mis
SFX b іт оти [^л]іт # гніт гноти (Н.) @ noun:p:v_naz
SFX b іт отів [^л]іт # гніт гнотів (З.) @ noun:p:v_rod
SFX b іт отам [^л]іт # гніт гнотам (Д.) @ noun:p:v_dav
SFX b іт отами [^л]іт # гніт гнотами (О.) @ noun:p:v_oru
SFX b іт отах [^л]іт # гніт гнотах (М.) @ noun:p:v_mis
SFX b іт оти [п]літ # пліт плоти (Н.) @ noun:p:v_naz
SFX b іт отів [п]літ # пліт плотів (З.) @ noun:p:v_rod
SFX b іт отам [п]літ # пліт плотам (Д.) @ noun:p:v_dav
SFX b іт отами [п]літ # пліт плотами (О.) @ noun:p:v_oru
SFX b іт отах [п]літ # пліт плотах (М.) @ noun:p:v_mis
SFX b іт ьоти [^п]літ # політ польоти (Н.) @ noun:p:v_naz
SFX b іт ьотів [^п]літ # політ польотів (З.) @ noun:p:v_rod
SFX b іт ьотам [^п]літ # політ польотам (Д.) @ noun:p:v_dav
SFX b іт ьотами [^п]літ # політ польотами (О.) @ noun:p:v_oru
SFX b іт ьотах [^п]літ # політ польотах (М.) @ noun:p:v_mis
SFX b із ози із # віз вози (Н.) @ noun:p:v_naz
SFX b із озів із # віз возів (З.) @ noun:p:v_rod
SFX b із озам із # віз возам (Д.) @ noun:p:v_dav
SFX b із озами із # віз возами (О.) @ noun:p:v_oru
SFX b із озах із # віз возах (М.) @ noun:p:v_mis
SFX b іж ожів [^тбд]іж # ніж ножів (З.) @ noun:p:v_rod
SFX b іж ожам [^тбд]іж # ніж ножам (Д.) @ noun:p:v_dav
SFX b іж ожами [^тбд]іж # ніж ножами (О.) @ noun:p:v_oru
SFX b іж ожах [^тбд]іж # ніж ножах (М.) @ noun:p:v_mis
SFX b іж ожів е[тб]іж # небіж небожів (З.) @ noun:p:v_rod
SFX b іж ожам е[тб]іж # небіж небожам (Д.) @ noun:p:v_dav
SFX b іж ожами е[тб]іж # небіж небожами (О.) @ noun:p:v_oru
SFX b іж ожах е[тб]іж # небіж небожах (М.) @ noun:p:v_mis
SFX b іж ежів [^е][тбд]іж # ніж рубежів (З.) @ noun:p:v_rod
SFX b іж ежам [^е][тбд]іж # ніж рубежам (Д.) @ noun:p:v_dav
SFX b іж ежами [^е][тбд]іж # ніж рубежами (О.) @ noun:p:v_oru
SFX b іж ежах [^е][тбд]іж # ніж рубежах (М.) @ noun:p:v_mis
SFX b ен ни ен # човен човни (Н.) @ noun:p:v_naz
SFX b ен нів ен # човен човнів (Р.) @ noun:p:v_rod
SFX b ен нам ен # човен човнам (Д.) @ noun:p:v_dav
SFX b ен нами ен # човен човнами (О.) @ noun:p:v_oru
SFX b ен нах ен # човен човнах (М.) @ noun:p:v_mis
SFX b ет ти ет # оцет оцти (мн.) @ noun:p:v_naz
SFX b ет тів ет # оцет оцтів (Р.З.) @ noun:p:v_rod
SFX b ет там ет # оцет оцтам (Д.) @ noun:p:v_dav
SFX b ет тами ет # оцет оцтами (О.) @ noun:p:v_oru
SFX b ет тах ет # оцет оцтах (М.) @ noun:p:v_mis
SFX b ес си ес # пес пси (мн.) @ noun:p:v_naz
SFX b ес сів ес # пес псів (Р.З.) @ noun:p:v_rod
SFX b ес сам ес # пес псам (Д.) @ noun:p:v_dav
SFX b ес сами ес # пес псами (О.) @ noun:p:v_oru
SFX b ес сах ес # пес псах (М.) @ noun:p:v_mis
SFX b інь оней [к]інь # кінь коней (Р.) @ noun:p:v_rod
SFX b інь оням [к]інь # кінь коням (Д.) @ noun:p:v_dav
SFX b інь онями [к]інь # кінь конями (О.) @ noun:p:v_oru
SFX b 0 ми [к]інь # кінь кіньми (О.) @ noun:p:v_oru
SFX b інь онях [к]інь # кінь конях (М.) @ noun:p:v_mis
SFX b інь енів [^ксв]інь # корінь коренів (Р.) @ noun:p:v_rod
SFX b інь еням [^ксв]інь # корінь кореням (Д.) @ noun:p:v_dav
SFX b інь енями [^ксв]інь # корінь коренями (О.) @ noun:p:v_oru
SFX b інь енях [^ксв]інь # корінь коренях (М.) @ noun:p:v_mis
SFX b інь енів [^о][св]інь # ревінь ревенів (Р.) @ noun:p:v_rod
SFX b інь еням [^о][св]інь # ревінь ревеням (Д.) @ noun:p:v_dav
SFX b інь енями [^о][св]інь # ревінь ревенями (О.) @ noun:p:v_oru
SFX b інь енях [^о][св]інь # ревінь ревенях (М.) @ noun:p:v_mis
SFX b ій оїв ій # рій роїв (З.) @ noun:p:v_rod
SFX b ій оям ій # рій роям (Д.) @ noun:p:v_dav
SFX b ій оями ій # рій роями (О.) @ noun:p:v_oru
SFX b ій оях ій # рій роях (М.) @ noun:p:v_mis
SFX b ідь едів ідь # ведмідь ведмедів (Д.) @ noun:p:v_rod
SFX b ідь едям ідь # ведмідь ведмедям (Д.) @ noun:p:v_dav
SFX b ідь едями ідь # ведмідь ведмедями (Д.) @ noun:p:v_oru
SFX b ідь едях ідь # ведмідь ведмедях (Д.) @ noun:p:v_mis
SFX b іль елів іль # важіль важелів (Д.) @ noun:p:v_rod
SFX b іль елям іль # важіль важелям (Д.) @ noun:p:v_dav
SFX b іль елями іль # важіль важелями (Д.) @ noun:p:v_oru
SFX b іль елях іль # важіль важелях (Д.) @ noun:p:v_mis
SFX b ість остів ість # гість гостів (Д.) @ noun:p:v_rod
SFX b ість остям ість # гість гостям (Д.) @ noun:p:v_dav
SFX b ість остями ість # гість гостями (Д.) @ noun:p:v_oru
SFX b ість остях ість # гість гостях (Д.) @ noun:p:v_mis
SFX b ок ки ок # будиночок будиночки (Н.) @ noun:p:v_naz
SFX b ок ків ок # будиночок будиночків (Р.) @ noun:p:v_rod
SFX b ок кам ок # будиночок будиночкам (Д.) @ noun:p:v_dav
SFX b ок ками ок # будиночок будиночками (О.) @ noun:p:v_oru
SFX b ок ках ок # будиночок будиночках (М.) @ noun:p:v_mis
SFX b ол ли ол # вузол вузли (Н.) @ noun:p:v_naz
SFX b ол лів ол # вузол вузлів (Р.) @ noun:p:v_rod
SFX b ол лам ол # вузол вузлам (Д.) @ noun:p:v_dav
SFX b ол лами ол # вузол вузлами (О.) @ noun:p:v_oru
SFX b ол лах ол # вузол вузлах (М.) @ noun:p:v_mis
SFX b ор ри ор # свекор свекри (Н.) @ noun:p:v_naz
SFX b ор рів ор # свекор свекрів (Р.) @ noun:p:v_rod
SFX b ор рам ор # свекор свекрам (Д.) @ noun:p:v_dav
SFX b ор рами ор # свекор свекрами (О.) @ noun:p:v_oru
SFX b ор рах ор # свекор свекрах (М.) @ noun:p:v_mis
SFX b ер ри ер # вітер вітри (Н.) @ noun:p:v_naz
SFX b ер рів ер # вітер вітрів (Р.) @ noun:p:v_rod
SFX b ер рам ер # вітер вітрам (Д.) @ noun:p:v_dav
SFX b ер рами ер # вітер вітрами (О.) @ noun:p:v_oru
SFX b ер рах ер # вітер вітрах (М.) @ noun:p:v_mis
SFX b ел ли ел # осел осли (Н.) @ noun:p:v_naz
SFX b ел лів ел # осел ослів (Р.) @ noun:p:v_rod
SFX b ел лам ел # осел ослам (Д.) @ noun:p:v_dav
SFX b ел лами ел # осел ослами (О.) @ noun:p:v_oru
SFX b ел лах ел # осел ослах (М.) @ noun:p:v_mis
SFX b ель лів ель # журавель журавлів (Р.З.) @ noun:p:v_rod
SFX b ель лям ель # журавель журавлям (Д.) @ noun:p:v_dav
SFX b ель лями ель # журавель журавлями (О.) @ noun:p:v_oru
SFX b ель лях ель # журавель журавлях (М.) @ noun:p:v_mis
SFX b єць йців [^о]єць # англієць англійців (Р.) @ noun:p:v_rod
SFX b єць йцям [^о]єць # англієць англійцям (Д.) @ noun:p:v_dav
SFX b єць йцями [^о]єць # англієць англійцями (О.) @ noun:p:v_oru
SFX b єць йцях [^о]єць # англієць англійцях (М.) @ noun:p:v_mis
SFX b оєць ійців оєць # боєць бійців (Р.) @ noun:p:v_rod
SFX b оєць ійцям оєць # боєць бійцям (Д.) @ noun:p:v_dav
SFX b оєць ійцями оєць # боєць бійцями (О.) @ noun:p:v_oru
SFX b оєць ійцях оєць # боєць бійцях (М.) @ noun:p:v_mis
SFX b ець ців [^лрнв]ець # сіамець сіамців (Р.) @ noun:p:v_rod
SFX b ець цям [^лрнв]ець # сіамець сіамцям (Д.) @ noun:p:v_dav
SFX b ець цями [^лрнв]ець # сіамець сіамцями (О.) @ noun:p:v_oru
SFX b ець цях [^лрнв]ець # сіамець сіамцях (М.) @ noun:p:v_mis
SFX b лець льців лець # бразилець бразильців (Р.) @ noun:p:v_rod
SFX b лець льцям лець # бразилець бразильцям (Д.) @ noun:p:v_dav
SFX b лець льцями лець # бразилець бразильцями (О.) @ noun:p:v_oru
SFX b лець льцях лець # бразилець бразильцях (М.) @ noun:p:v_mis
SFX b рець ерців [^аеєиіїоуюя]рець # жрець жерців (Р.) @ noun:p:v_rod
SFX b рець ерцям [^аеєиіїоуюя]рець # жрець жерцям (Д.) @ noun:p:v_dav
SFX b рець ерцями [^аеєиіїоуюя]рець # жрець жерцями (О.) @ noun:p:v_oru
SFX b рець ерцях [^аеєиіїоуюя]рець # жрець жерцях (М.) @ noun:p:v_mis
SFX b рець рців [аеєиіїоуюя]рець # борець борців (Р.) @ noun:p:v_rod
SFX b рець рцям [аеєиіїоуюя]рець # борець борцям (Д.) @ noun:p:v_dav
SFX b рець рцями [аеєиіїоуюя]рець # борець борцями (О.) @ noun:p:v_oru
SFX b рець рцях [аеєиіїоуюя]рець # борець борцях (М.) @ noun:p:v_mis
SFX b ець ців [аеєиіїуюяго][нв]ець # американець американців (Р.) @ noun:p:v_rod
SFX b ець цям [аеєиіїуюяго][нв]ець # американець американцям (Д.) @ noun:p:v_dav
SFX b ець цями [аеєиіїуюяго][нв]ець # американець американцями (О.) @ noun:p:v_oru
SFX b ець цях [аеєиіїуюяго][нв]ець # американець американцях (М.) @ noun:p:v_mis
SFX b нець енців [^аеєиіїоуюяг]нець # жнець женців (Р.) @ noun:p:v_rod
SFX b нець енцям [^аеєиіїоуюяг]нець # жнець женцям (Д.) @ noun:p:v_dav
SFX b нець енцями [^аеєиіїоуюяг]нець # жнець женцями (О.) @ noun:p:v_oru
SFX b нець енцях [^аеєиіїоуюяг]нець # жнець женцях (М.) @ noun:p:v_mis
SFX b вець евців [^аеєиіїоуюя]вець # швець шевців (Р.) @ noun:p:v_rod
SFX b вець евцям [^аеєиіїоуюя]вець # швець шевцям (Д.) @ noun:p:v_dav
SFX b вець евцями [^аеєиіїоуюя]вець # швець шевцями (О.) @ noun:p:v_oru
SFX b вець евцях [^аеєиіїоуюя]вець # швець шевцях (М.) @ noun:p:v_mis
SFX b ень нів ень # день днів (Р.) @ noun:p:v_rod
SFX b ень ням ень # день дням (Д.) @ noun:p:v_dav
SFX b ень нями ень # день днями (О.) @ noun:p:v_oru
SFX b ень нях ень # день днях (М.) @ noun:p:v_mis
SFX b онь нів онь # вогонь вогнів (Р.) @ noun:p:v_rod
SFX b онь ням онь # вогонь вогням (Д.) @ noun:p:v_dav
SFX b онь нями онь # вогонь вогнями (О.) @ noun:p:v_oru
SFX b онь нях онь # вогонь вогнях (М.) @ noun:p:v_mis
SFX b оль лів оль # кухоль кухлів (Р.) @ noun:p:v_rod
SFX b оль лям оль # кухоль кухлям (Д.) @ noun:p:v_dav
SFX b оль лями оль # кухоль кухлями (О.) @ noun:p:v_oru
SFX b оль лях оль # кухоль кухлях (М.) @ noun:p:v_mis
SFX b оть тів оть # ніготь нігтів (Р.) @ noun:p:v_rod
SFX b оть тям оть # ніготь нігтям (Д.) @ noun:p:v_dav
SFX b оть тями оть # ніготь нігтями (О.) @ noun:p:v_oru
SFX b оть тях оть # ніготь нігтях (М.) @ noun:p:v_mis
SFX b ки ок ки # лапки лапок (Р.) @ noun:p:v_rod
SFX b и ей [днст]и # люди людей (Р.) @ noun:p:v_rod
SFX b и ам ки # шаровари шароварам (Д.) @ noun:p:v_dav
SFX b и ям [днст]и # люди людям (Д.) @ noun:p:v_dav
SFX b и ами ки # шаровари шароварами (О.) @ noun:p:v_oru
SFX b и ьми [днст]и # люди людьми (О.) @ noun:p:v_oru
SFX b и ах ки # шаровари шароварах (М.) @ noun:p:v_mis
SFX b и ях [днст]и # люди людях (М.) @ noun:p:v_mis
SFX b і ей ші # гроші грошей (Р.) @ noun:p:v_rod
SFX b і ам [^лзнцр]і # гроші грошам (Д.) @ noun:p:v_dav
SFX b і ами [^лзнцр]і # гроші грошами (О.) @ noun:p:v_oru
SFX b і ах [^лзнцр]і # гроші грошах (М.) @ noun:p:v_mis
SFX b і ь иці # вечорниці вечорниць (Р.) @ noun:p:v_rod
SFX b ці ець рці # дверці дверець (Р.) @ noun:p:v_rod
SFX b і ей [аяеєиії]рі # двері дверей (Р.) @ noun:p:v_rod
SFX b і ям [лзнцр]і # штанці штанцям (Д.) @ noun:p:v_dav
SFX b і ями [лзнц]і # штанці штанцями (О.) @ noun:p:v_oru
SFX b і ями [^е]рі # нетрі нетрями (О.) @ noun:p:v_oru
SFX b і ми ері # двері дверми (О.) @ noun:p:v_oru
SFX b і има ері # двері дверима (О.) @ noun:p:v_oru
SFX b і ях [лзнцр]і # штанці штанцях (М.) @ noun:p:v_mis
SFX b ї й [иі]ї # геніталії геніталій (Р.) @ noun:p:v_rod
SFX b ї їв [^иі]ї # хазяї хазяїв (Р.) @ noun:p:v_rod
SFX b ї ям ї # геніталії геніталіям (Д.) @ noun:p:v_dav
SFX b ї ями ї # геніталії геніталіями (О.) @ noun:p:v_oru
SFX b ї ях ї # геніталії геніталіях (М.) @ noun:p:v_mis
SFX c Y 53
SFX c ір ора [^л]ір # вечір вечора (Р.) @ noun:m:v_rod
SFX c ір ьора лір # колір кольора (Р.) @ noun:m:v_rod
SFX c ін она ін # загін загона (Р.) @ noun:m:v_rod
SFX c іг ога іг # батіг батога (Р.) @ noun:m:v_rod
SFX c ід ода [^л]ід # провід провода (Р.) @ noun:m:v_rod
SFX c ід ода [^пг]лід # провід провода (Р.) @ noun:m:v_rod
SFX c ід ода [пг]лід # провід провода (Р.) @ noun:m:v_rod
SFX c іп опа іп # ЧіП ЧоПа (Р.) @ noun:m:v_rod 
SFX c івш овша івш # ківш ковша (Р.) @ noun:m:v_rod 
SFX c ізд озда ізд # дрізд дрозда (Р.) @ noun:m:v_rod 
SFX c іб оба іб # засіб засоба (Р.) @ noun:m:v_rod
SFX c іл ола іл # дозвіл дозвола (Р.) @ noun:m:v_rod
SFX c ів ова ів # острів острова (Р.) @ noun:m:v_rod
SFX c їв єва їв # Київ Києва (Р.) @ noun:m:v_rod
SFX c ік ока ік # (Р.) @ noun:m:v_rod
SFX c іск оска іск # (Р.) @ noun:m:v_rod
SFX c іст оста іст # (Р.) @ noun:m:v_rod
SFX c іс оса [кнч]іс # ніс носа (Р.) @ noun:m:v_rod
SFX c іт ота [^л]іт # гніт гнота (Р.) @ noun:m:v_rod
SFX c іт ьота [^п]літ # політ польота (Р.) @ noun:m:v_rod
SFX c іт ота [п]літ # пліт плота (Р.) @ noun:m:v_rod
SFX c із оза із # віз воза (Р.) @ noun:m:v_rod
SFX c іш оша іш # гріш гроша (Р.) @ noun:m:v_rod
SFX c іж ожа [^тб]іж # ніж ножа (Р.) @ noun:m:v_rod
SFX c іж ожа е[тб]іж # небіж небожа (Р.) @ noun:m:v_rod
SFX c ій оя ій # рій роя (Р.) @ noun:m:v_rod
SFX c ен на ен # рожен рожна (Р.) @ noun:m:v_rod
SFX c ет та ет # оцет оцта (Р.) @ noun:m:v_rod
SFX c ес са ес # пес пса (Р.) @ noun:m:v_rod
SFX c інь оня [к]інь # кінь коня (Р.) @ noun:m:v_rod
SFX c інь еня [^к]інь # корінь кореня (Р.) @ noun:m:v_rod
SFX c ідь едя ідь # ведмідь ведмедя (Р.) @ noun:m:v_rod
SFX c іль еля іль # важіль важеля (Р.) @ noun:m:v_rod
SFX c ість остя ість # гість гостя (Р.) @ noun:m:v_rod
SFX c ок ка ок # будиночок будиночка (Р.) @ noun:m:v_rod
SFX c ол ла ол # вузол вузла (Р.) @ noun:m:v_rod
SFX c ор ра ор # вугор вугра (Р.) @ noun:m:v_rod
SFX c ер ра ер # вітер вітра (Р.) @ noun:m:v_rod
SFX c ел ла ел # орел орла (Р.) @ noun:m:v_rod
SFX c ель ля ель # журавель журавля (Р.) @ noun:m:v_rod
SFX c єць йця [^о]єць # англієць англійця (Р.) @ noun:m:v_rod
SFX c оєць ійця оєць # боєць бійця (Р.) @ noun:m:v_rod
SFX c ець ця [^лрвн]ець # американець американця (Р.) @ noun:m:v_rod
SFX c лець льця лець # бразилець бразильця (Р.) @ noun:m:v_rod
SFX c рець ерця [^аеєиіїоуюя]рець # жрець жерця (Р.) @ noun:m:v_rod
SFX c рець рця [аеєиіїоуюя]рець # борець борця (Р.) @ noun:m:v_rod
SFX c ець ця [аеєиіїуюяго][нв]ець # американець американця @ noun:m:v_rod
SFX c нець енця [^аеєиіїоуюяг]нець # жнець женці @ noun:m:v_rod
SFX c вець евця [^аеєиіїоуюя]вець # швець шевці @ noun:m:v_rod
SFX c ень ня ень # день дня (Р.) @ noun:m:v_rod
SFX c онь ня онь # вогонь вогня (Р.) @ noun:m:v_rod
SFX c оль ля оль # кухоль кухля (Р.) @ noun:m:v_rod
SFX c оть тя оть # ніготь нігтя (Р.) @ noun:m:v_rod
SFX o Y 58
SFX o а и а # школа школи (Н.) @ noun:p:v_naz
SFX o ола іл ола # школа шкіл (Р.) @ noun:p:v_rod
SFX o оба іб оба # доБа діБ (Р.) @ noun:p:v_rod
SFX o ода ід ода # бороДа боріД (Р.) @ noun:p:v_rod
SFX o ога іг ога # ноГа ніГ (Р.) @ noun:p:v_rod
SFX o га іг рга # кочерГа кочеріГ (Р.) @ noun:p:v_rod
SFX o оха іх оха # блоХа бліХ (Р.) @ noun:p:v_rod
SFX o ока ік ока # щоКа щіК (Р.) @ noun:p:v_rod
SFX o она ін она # бороНа боріН (Р.) @ noun:p:v_rod
SFX o опа іп опа # коПа кіП (Р.) @ noun:p:v_rod
SFX o ора ір ора # гоРа гіР (Р.) @ noun:p:v_rod
SFX o ра ер тра # сестРа сестеР (Р.) @ noun:p:v_rod
SFX o ра ор [^от]ра # іскРа іскоР (Р.) @ noun:p:v_rod
SFX o оса іс оса # коСа кіС (Р.) @ noun:p:v_rod
SFX o ота іт ота # сироТа сиріТ (Р.) @ noun:p:v_rod
SFX o ва ов [^о]ва # молитва молитов (Р.) @ noun:p:v_rod
SFX o ова ів [о]ва # удоВа удіВ (Р.) @ noun:p:v_rod
SFX o оза із [^ь]оза # коЗа кіЗ (Р.) @ noun:p:v_rod
SFX o ьоза із ьоза # сльоЗа сліЗ (Р.) @ noun:p:v_rod
SFX o еза із еза # береЗа беріЗ (Р.) @ noun:p:v_rod
SFX o а ів жа # магарадЖа магараджів (Р.) @ noun:p:v_rod
SFX o а ів жа # магарадЖа магараджів (Р.) @ noun:p:v_rod
SFX o а ей ша # миша мишей (Р.) @ noun:p:v_rod
SFX o ійня оєнь ійня # бійня боєнь (Р.) @ noun:p:v_rod
SFX o 0 м [ая] # школа школам (Д.) @ noun:p:v_dav
SFX o 0 ми [ая] # школа школами (О.) @ noun:p:v_oru
SFX o 0 х [ая] # школа школах (М.) @ noun:p:v_mis
SFX o о а о # слово слова (Н.З.) @ noun:p:v_naz
SFX o ово ів ово # слово слів (Р.) @ noun:p:v_rod
SFX o ото іт ото # болото боліт (Р.) @ noun:p:v_rod
SFX o ето іт ето # решето решіт (Р.) @ noun:p:v_rod
SFX o оло іл оло # коло кіл (Р.) @ noun:p:v_rod
SFX o ело іл ело # село сіл (Р.) @ noun:p:v_rod
SFX o есо іс есо # колесо коліс (Р.) @ noun:p:v_rod
SFX o осо іс осо # просо пріс (Р.) @ noun:p:v_rod
SFX o но он [кг]но # вікно вікон (Р.) @ noun:p:v_rod
SFX o ло ол [кз]ло # ікло ікол (Р.) @ noun:p:v_rod
SFX o ко ок [ь]ко # серденько серденьок (Р.) @ noun:p:v_rod
SFX o ло ел [пбдт]ло # житло жител (Р.) @ noun:p:v_rod
SFX o но ен [дтрв]но # зерно зерен (Р.) @ noun:p:v_rod
SFX o ро ер [дб]ро # ребро ребер (Р.) @ noun:p:v_rod
SFX o мо ем рмо # ярмо ярем (Р.) @ noun:p:v_rod
SFX o мо ом смо # пасмо пасом (Р.) @ noun:p:v_rod
SFX o о ам о # слово словам (Д.) @ noun:p:v_dav
SFX o о ами о # слово словами (О.) @ noun:p:v_oru
SFX o о ах о # слово словах (М.) @ noun:p:v_mis
SFX o г зі г # протяг протязі (М.) @ noun:m:v_mis
SFX o ґ зі ґ # луґ лузі (М.) @ noun:m:v_mis
SFX o к ці к # крок кроці (М.) @ noun:m:v_mis
SFX o х сі х # реп'ях реп'ясі (М.) @ noun:m:v_mis
SFX o и 0 и # Карпати Карпат (Р.) @ noun:p:v_rod
SFX o и ам и # Карпати Карпатам (Д.) @ noun:p:v_dav
SFX o и ами и # Карпати Карпатами (О.) @ noun:p:v_oru
SFX o и ах и # Карпати Карпатах (М.) @ noun:p:v_mis
SFX o і 0 і # ноші нош (Р.) @ noun:p:v_rod
SFX o і ам і # ноші ношам (Д.) @ noun:p:v_dav
SFX o і ами і # ноші ношами (О.) @ noun:p:v_oru
SFX o і ах і # ноші ношах (М.) @ noun:p:v_mis
SFX d Y 37
SFX d а о [^жчшщ]а # хата хато (К.) @ noun:f:v_kly
SFX d а е [жчщ]а # вежа веже (К.) @ noun:f:v_kly
SFX d я е [лнц]я # вишня вишне (К.) @ noun:f:v_kly
SFX d я є ['ьіеиаоу]я # сім'я сім'є (К.) @ noun:f:v_kly
SFX d ір оре [^л]ір # вибір виборе @ noun:m:v_kly
SFX d ір ьоре лір # колір кольоре @ noun:m:v_kly
SFX d ін оне ін # загін загоне @ noun:m:v_kly
SFX d іг оже [^р]іг # батіг батоже @ noun:m:v_kly
SFX d іг еже ріг # оберіг обереже @ noun:m:v_kly
SFX d ід оде [^л]ід # провід проводе @ noun:m:v_kly
SFX d ід ьоде [^пг]лід # @ noun:m:v_kly
SFX d ід оде [пг]лід # @ noun:m:v_kly
SFX d іб обе іб # засіб засобе @ noun:m:v_kly
SFX d іп опе іп # ЧіП ЧоПЕ @ noun:m:v_kly
SFX d івш овше івш # ківш ковшЕ @ noun:m:v_kly
SFX d ізд озде ізд # дрізд дроздЕ @ noun:m:v_kly
SFX d іл оле іл # дозвіл дозвзоле @ noun:m:v_kly
SFX d ів ове ів # острове @ noun:m:v_kly
SFX d їв єве їв # Києве @ noun:m:v_kly
SFX d ік оче ік # роче @ noun:m:v_kly
SFX d іск осче іск # @ noun:m:v_kly
SFX d іст осте іст # @ noun:m:v_kly
SFX d іт оте [^л]іт # гноте @ noun:m:v_kly
SFX d іт ьоте [^п]літ # польоте @ noun:m:v_kly
SFX d іт оте [п]літ # поте @ noun:m:v_kly
SFX d із озе із # возе @ noun:m:v_kly
SFX d іж оже [^тб]іж # ноже @ noun:m:v_kly
SFX d іж оже е[тб]іж # ноже @ noun:m:v_kly
SFX d іж еже [^е][тб]іж # рубеже @ noun:m:v_kly
SFX d ет те ет # Єгипте @ noun:m:v_kly
SFX d інь оне [к]інь # кінь коне (К.) @ noun:m:v_kly
SFX d інь ене [^ксв]інь # корінь корене (К.) @ noun:m:v_kly
SFX d інь ене [^о][св]інь # корінь корене (К.) @ noun:m:v_kly
SFX d ол ле ол # вузол вузле @ noun:m:v_kly
SFX d ор ре ор # свекор свекре @ noun:m:v_kly
SFX d ер ре ер # вітер вітре @ noun:m:v_kly
SFX d ел ле ел # орел орле @ noun:m:v_kly
SFX e Y 19
SFX e о а о # батько батька (Р.) @ noun:m:v_rod
SFX e о у о # батько батьку (Д.З.К.) @ noun:m:v_dav
SFX e о ові о # батько батькові (Д.) @ noun:m:v_dav/v_mis
SFX e о ом о # батько батьком (О.) @ noun:m:v_oru
SFX e 0 у [^аєиійоуьюя] # завод заводу (Р.) @ noun:m:v_dav/v_mis
SFX e 0 ові [^аєиійоуьюяжчшщв] # завод заводові (Д.М.) @ noun:m:v_dav/v_mis
SFX e 0 ові [^о]в # спів співові (Д.М.) @ noun:m:v_dav/v_mis
SFX e 0 ом [^аєиійоуьюяжчшщ] # завод заводом (О.) @ noun:m:v_oru
SFX e 0 і [^аєиійоуьюягґкх] # завод заводі (М.) @ noun:m:v_mis
SFX e 0 еві [жчшщ] # товариш товаришеві (Д.М.) @ noun:m:v_dav/v_mis
SFX e 0 ем [жчшщ] # товариш товаришем (О.) @ noun:m:v_oru
SFX e й ю й # край краю (Р.) @ noun:m:v_dav/v_mis
SFX e й єві й # край краєві (ДМ.) @ noun:m:v_dav/v_mis
SFX e й єм й # край краєм (О.) @ noun:m:v_oru
SFX e й ї й # край краї (М.) @ noun:m:v_mis
SFX e ь ю ь # автомобіль автомобілю (Д.) @ noun:m:v_dav/v_mis
SFX e ь еві ь # автомобіль автомобілеві (З.) @ noun:m:v_dav/v_mis
SFX e ь ем ь # автомобіль автомобілем (О.) @ noun:m:v_oru
SFX e ь і ь # автомобіль автомобілі (М.) @ noun:m:v_mis
SFX f Y 39
SFX f 0 и [^аєиіїйоуьюяжчшщ] # завод заводи (Н.) @ noun:p:v_naz
SFX f 0 ів [^аєиіїйоуьюя] # завод заводів (Р.) @ noun:p:v_rod
SFX f 0 ам [^аєиіїйоуьюя] # завод заводам (Д.) @ noun:p:v_dav
SFX f 0 ами [^аєиіїйоуьюя] # завод заводами (О.) @ noun:p:v_oru
SFX f 0 ах [^аєиіїйоуьюя] # завод заводах (М.) @ noun:p:v_mis
SFX f 0 і [жчшщ] # товариш товариші (Н.) @ noun:p:v_naz
SFX f о и о # батько батьки (Н.) @ noun:p:v_naz
SFX f о ів о # батько батьків (Р.) @ noun:p:v_rod
SFX f о ам о # батько батькам (Д.) @ noun:p:v_dav
SFX f о ами о # батько батьками (О.) @ noun:p:v_oru
SFX f о ах о # батько батьках (М.) @ noun:p:v_mis
SFX f й ї й # край краї (Н.) @ noun:p:v_naz
SFX f й їв й # край країв (Р.) @ noun:p:v_rod
SFX f й ям й # край краям (Д.) @ noun:p:v_dav
SFX f й ями й # край краями (О.) @ noun:p:v_oru
SFX f й ях й # край краях (М.) @ noun:p:v_mis
SFX f ь і ь # автомобіль автомобілі (Н.) @ noun:p:v_naz
SFX f ь ів ь # автомобіль автомобілів (Р.) @ noun:p:v_rod
SFX f ь ям ь # автомобіль автомобілям (Д.) @ noun:p:v_dav
SFX f ь ями ь # автомобіль автомобілями (О.) @ noun:p:v_oru
SFX f ь ях ь # автомобіль автомобілях (М.) @ noun:p:v_mis
SFX f и ів и # шаровари шароварів (Р.) @ noun:p:v_rod
SFX f і ів і # шаровари шароварів (Р.) @ noun:p:v_rod
SFX f и ам и # кайдани кайданам (Д.) @ noun:p:v_dav
SFX f і ам [^злнрц]і # кайдани кайданам (Д.) @ noun:p:v_dav
SFX f і ям [злнрц]і # кайдани кайданам (Д.) @ noun:p:v_dav
SFX f и ами и # кайдани кайданами (О.) @ noun:p:v_oru
SFX f і ами [^злнрц]і # кайдани кайданами (О.) @ noun:p:v_oru
SFX f і ями [злнрц]і # кайдани кайданами (О.) @ noun:p:v_oru
SFX f и ах и # кайдани кайданах (М.) @ noun:p:v_mis
SFX f і ах [^злнрц]і # кайдани кайданах (М.) @ noun:p:v_mis
SFX f і ях [злнрц]і # кайдани кайданах (М.) @ noun:p:v_mis
SFX f ї їв ї # помиї помиїв (Р.) @ noun:p:v_rod
SFX f ї ям ї # помиї помиям (Д.) @ noun:p:v_dav
SFX f ї ями ї # помиї помиями (О.) @ noun:p:v_oru
SFX f ї ях ї # помиї помиях (М.) @ noun:p:v_mis
SFX f я ів ття # життя життів (Р.) @ noun:p:v_rod
SFX f я ями я # життя життями (О.) @ noun:p:v_oru
SFX f я ях я # життя життях (М.) @ noun:p:v_mis
SFX g Y 3
SFX g 0 а [^аєиійоуьюя] # ягуар ягуара (Р.) @ noun:m:v_rod
SFX g й я й # багатій багатія (Р.) @ noun:m:v_rod
SFX g ь я ь # автомобіль автомобіля (Р.) @ noun:m:v_rod
SFX h Y 4
SFX h 0 е [^аєиійоуьюякгжчшщ] # завод заводе (К.) @ noun:m:v_kly
SFX h к че [уая]к # козак козаче (К.) @ noun:m:v_kly
SFX h г же [уо]г # луг луже (К.) @ noun:m:v_kly
SFX h о е [рл]о # Петро Петре (К.) @ noun:m:v_kly
SFX i Y 47
SFX i ь і ь # міць міці (Р.Д.М.) @ noun:f:v_rod/v_dav/v_zna/v_mis
SFX i 0 і [^і][вф] # кров крові (Р.Д.М.) @ noun:f:v_rod/v_dav/v_zna/v_mis
SFX i ь ю [^аоуеиіяюєї].ь # смерть смертю (О.) @ noun:f:v_oru
SFX i ь тю [аоуеиіяюєї]ть # благодать благодаттю (О.) @ noun:f:v_oru
SFX i ь ню [аоуеиіяюєї]нь # тінь тінню (О.) @ noun:f:v_oru
SFX i ь дю [аоуеиіяюєї]дь # мідь міддю (О.) @ noun:f:v_oru
SFX i ь лю [аоуеиіяюєї]ль # сіль сіллю (О.) @ noun:f:v_oru
SFX i ь зю [аоуеиіяюєї]зь # галузь галуззю (О.) @ noun:f:v_oru
SFX i ь сю [аоуеиіяюєї]сь # Русь Руссю (О.) @ noun:f:v_oru
SFX i 0 'ю [^і][вф] # кров кров'ю (О.) @ noun:f:v_oru
SFX i 0 і [жчшщр] # зустріч зустрічі (Р.Д.М.) @ noun:f:v_rod/v_dav/v_mis//p:v_naz
SFX i 0 'ю [ау]р # глазур глазур'ю (О.) @ noun:f:v_oru
SFX i 0 ю [^аоуеиіяюєї][жчшщ] # фальш фальшю (О.) @ noun:f:v_oru
SFX i 0 чю [аоуеиіяюєї]ч # зустріч зустріччю (О.) @ noun:f:v_oru
SFX i 0 жю [аоуеиіяюєї]ж # подорож подорожжю (О.) @ noun:f:v_oru
SFX i 0 шю [аоуеиіяюєї]ш # розкіш розкішшю (О.) @ noun:f:v_oru
SFX i о а о # озеро озера (Р.) @ noun:n:v_rod//p:v_naz
SFX i о у о # озеро озеру (Д.) @ noun:n:v_dav/v_mis
SFX i о ом о # озеро озером (О.) @ noun:n:v_oru
SFX i о і [^кх]о # озеро озері (М.) @ noun:n:v_mis
SFX i хо сі хо # вухо вусі (М.) @ noun:n:v_mis
SFX i ко ці око # молоко молоці (М.) @ noun:n:v_mis
SFX i е я [^жчшщ]е # море моря (Р.) @ noun:n:v_rod//p:v_naz
SFX i е ю [^жчшщ]е # море морю (Д.) @ noun:n:v_dav/v_mis
SFX i е ем [^жчшщ]е # море морем (О.) @ noun:n:v_oru
SFX i е і [^жчшщ]е # море морі (М.) @ noun:n:v_mis
SFX i е а [жчшщ]е # прізвище прізвища (Р.) @ noun:n:v_rod//p:v_naz
SFX i е у [жчшщ]е # прізвище прізвищу (Д.) @ noun:n:v_dav/v_mis
SFX i е ем [жчшщ]е # прізвище прізвищем (О.) @ noun:n:v_oru
SFX i е і [жчшщ]е # прізвище прізвищі (М.) @ noun:n:v_mis
SFX i я ю я # завдання завданню (Д.) @ noun:n:v_dav/v_mis
SFX i я ям я # завдання завданням (О.) @ noun:n:v_oru//p:v_dav
SFX i я і [^'ь]я # завдання завданні (М.) @ noun:n:v_mis
SFX i я ї ['ь]я # бездощів'я бездощів'ї (М.) @ noun:n:v_mis
SFX i ий ого ий # хорунжий хорунжого (Р.З.) @ noun:m:v_rod/v_zna
SFX i ий ому ий # хорунжий хорунжому (Д.М.) @ noun:m:v_dav/v_mis
SFX i ий им ий # хорунжий хорунжим (О.) @ noun:m:v_oru
SFX i ій ього ій # Всевишній Всевишнього (Р.З.) @ noun:m:v_rod/v_zna
SFX i ій ьому ій # Всевишній Всевишньому (Д.М.) @ noun:m:v_dav/v_mis
SFX i ій ім ій # Всевишній Всевишнім (О.) @ noun:m:v_oru
SFX i а ої [втнк]а # чебуречна чебуречної (Р.) @ noun:f:v_rod
SFX i а ій [втнк]а # чебуречна чебуречній (Д.М.) @ noun:f:v_dav/v_mis
SFX i а у [втнк]а # чебуречна чебуречну (З.) @ noun:f:v_zna
SFX i а ою [втнк]а # чебуречна чебуречною (О.) @ noun:f:v_oru
SFX i е ого [нк]е # неподільне неподільного (Р.З.) @ noun:n:v_rod/v_zna
SFX i е ому [нк]е # неподільне неподільному (Д.М.) @ noun:n:v_dav/v_mis
SFX i е им [нк]е # неподільне неподільним (О.) @ noun:n:v_oru
SFX j Y 64
SFX j ь ей ь # тінь тіней (Р.) @ noun:p:v_rod
SFX j ь ям ь # тінь тіням (Д.) @ noun:p:v_dav
SFX j ь ями ь # тінь тінями (О.) @ noun:p:v_oru
SFX j ь ях ь # тінь тінях (М.) @ noun:p:v_mis
SFX j 0 ей [жчшщ] # зустріч зустрічей (Р.) @ noun:p:v_rod
SFX j 0 ам [жчшщ] # зустріч зустрічам (Д.) @ noun:p:v_dav
SFX j 0 ами [жчшщ] # зустріч зустрічами (О.) @ noun:p:v_oru
SFX j 0 ах [жчшщ] # зустріч зустрічах (М.) @ noun:p:v_mis
SFX j 0 ей [ау]р # глазурей (Р.) @ noun:p:v_rod
SFX j 0 ям [ау]р # глазурям (Д.) @ noun:p:v_dav
SFX j 0 ями [ау]р # глазурями (О.) @ noun:p:v_oru
SFX j 0 ях [ау]р # глазурях (М.) @ noun:p:v_mis
SFX j 0 ей ф # верфей (Р.) @ noun:p:v_rod
SFX j 0 ям ф # верфям (Д.) @ noun:p:v_dav
SFX j 0 ями ф # верфями (О.) @ noun:p:v_oru
SFX j 0 ях ф # верфях (М.) @ noun:p:v_mis
SFX j ло ел сло # гасло гасел (Р.) @ noun:p:v_rod
SFX j о 0 [^с]ло # тіло тіл (Р.) @ noun:p:v_rod
SFX j ко ок [^ьоаеиіу]ко # коліщатко коліщаток (Р.) @ noun:p:v_rod
SFX j о 0 [ьоаеиіу]ко # лико лик (Р.) @ noun:p:v_rod
SFX j о 0 [^лк]о # озеро озер (Р.) @ noun:p:v_rod
SFX j о ам о # озеро озерам (Д.) @ noun:p:v_dav
SFX j о ами о # озеро озерами (О.) @ noun:p:v_oru
SFX j о ах о # озеро озерах (М.) @ noun:p:v_mis
SFX j е ів [^жчшщц]е # море морів (Р.) @ noun:p:v_rod
SFX j е ям [^жчшщ]е # море морям (Д.) @ noun:p:v_dav
SFX j е ями [^жчшщ]е # море морями (О.) @ noun:p:v_oru
SFX j е ях [^жчшщ]е # море морях (М.) @ noun:p:v_mis
SFX j це дець серце # серце сердець (Р.) @ noun:p:v_rod
SFX j це ець [^с]ерце # озерце озерець (Р.) @ noun:p:v_rod
SFX j е ь ісце # місце місць (Р.) @ noun:p:v_rod
SFX j йце єць йце # яйце яєць (Р.) @ noun:p:v_rod
SFX j ьце ець ьце # бильце билець (Р.) @ noun:p:v_rod
SFX j це ець [^йрсь]це # віконце віконець (Р.) @ noun:p:v_rod
SFX j е 0 [жшщ]е # прізвище прізвищ (Р.) @ noun:p:v_rod
SFX j 0 й че # плече плечей (Р.) @ noun:p:v_rod
SFX j е ам [жчшщ]е # прізвище прізвищам (Д.) @ noun:p:v_dav
SFX j е ами [жчшщ]е # прізвище прізвищами (О.) @ noun:p:v_oru
SFX j е ах [жчшщ]е # прізвище прізвищах (М.) @ noun:p:v_mis
SFX j я їв 'я # бездощів'я бездощів'їв (Р.) @ noun:p:v_rod
SFX j ня ь ння # завдання завдань (Р.) @ noun:p:v_rod
SFX j я ів [у]жжя # подружжя подружжів (Р.) @ noun:p:v_rod
SFX j жя 0 [^у]жжя # прибережжя прибереж (Р.) @ noun:p:v_rod
SFX j тя ь ття # заняття занять (Р.) @ noun:p:v_rod
SFX j я ь [^т]тя # заняття занять (Р.) @ noun:p:v_rod
SFX j дя ь ддя # безвладдя безвладь (Р.) @ noun:p:v_rod
SFX j я ь [^д]дя # безглуздя безглуздь (Р.) @ noun:p:v_rod
SFX j ля ь лля # зусилля зусиль (Р.) @ noun:p:v_rod
SFX j чя 0 ччя # десятиріччя десятиріч (Р.) @ noun:p:v_rod
SFX j шя 0 шшя # затишшя затиш (Р.) @ noun:p:v_rod
SFX j ся ь сся # дрібнолісся дрібнолісь (Р.) @ noun:p:v_rod
SFX j я ями я # завдання завданнями (О.) @ noun:p:v_oru
SFX j я ях я # завдання завданнях (М.) @ noun:p:v_mis
SFX j ий і ий # хорунжий хорунжі (Н.) @ noun:p:v_naz
SFX j ий их ий # хорунжий хорунжих (Р.) @ noun:p:v_rod/v_mis
SFX j ий ими ий # хорунжий хорунжими (О.) @ noun:p:v_oru
SFX j і их і # Коморські Коморських (Р.З.М.) @ noun:p:v_rod/v_mis
SFX j і им і # Коморські Коморським (Д.) @ noun:p:v_dav
SFX j і ими і # Коморські Коморськими (О.) @ noun:p:v_oru
SFX j н 0 ин # болгарин болгари (Н.) @ noun:p:v_naz
SFX j ин 0 ин # болгарин болгар (Р.) @ noun:p:v_rod
SFX j ин ам ин # болгарин болгарам (Д.) @ noun:p:v_dav
SFX j ин ами ин # болгарин болгарами (О.) @ noun:p:v_oru
SFX j ин ах ин # болгарин болгарах (М.) @ noun:p:v_mis
SFX l Y 46
SFX l ість ості ість # ніжність ніжності (Р.Д.М.) @ noun:f:v_rod/v_dav/v_zna/v_mis
SFX l їсть йості їсть # безкраїсть безкрайості (Р.Д.М.) @ noun:f:v_rod/v_dav/v_zna/v_mis
SFX l ь і [^їі]сть # водорость водорості (Р.Д.М.) @ noun:f:v_rod/v_dav/v_zna/v_mis
SFX l ь ю сть # ніжність ніжністю (О.) @ noun:f:v_oru
SFX l інь ені о[св]інь # осінь осені (Р.Д.З.М.) @ noun:f:v_rod/v_dav/v_zna/v_mis
SFX l ь ню о[св]інь # осінь осінню (О.) @ noun:f:v_oru
SFX l іль олі іль # сіль солі (Р.Д.М.) @ noun:f:v_rod/v_dav/v_zna/v_mis
SFX l ь лю іль # сіль сіллю (О.) @ noun:f:v_oru
SFX l іць оці іць # міць моці (Р.Д.М.) @ noun:f:v_rod/v_dav/v_zna/v_mis
SFX l ь цю іць # сіль міццю (О.) @ noun:f:v_oru
SFX l іч ечі [^н]іч # піч печі (Р.) @ noun:f:v_rod/v_dav/v_zna/v_mis
SFX l іч очі ніч # ніч ночі (Р.) @ noun:f:v_rod/v_dav/v_zna/v_mis
SFX l іш оші іш # розкіш розкоші (Р.) @ noun:f:v_rod/v_dav/v_zna/v_mis
SFX l 0 чю іч # піч піччю (О.) @ noun:f:v_oru
SFX l 0 шю іш # розкіш розкішшю (О.) @ noun:f:v_oru
SFX l 0 ти [^'][ая] # цуценя цуценяти (Р.) @ noun:n:v_rod
SFX l 0 ті [^'][ая] # цуценя цуценяті (Д.М.) @ noun:n:v_dav/v_mis
SFX l 0 м [ая] # цуценя цуценям (О.) @ noun:n:v_oru
SFX l 0 ти [^м]'я # хлоп'я хлоп'яти (Р.) @ noun:n:v_rod
SFX l 0 ті [^м]'я # хлоп'я хлоп'яті (Д.М.) @ noun:n:v_dav/v_mis
SFX l 'я ені м'я # ім'я імені (Р.) @ noun:n:v_rod/v_dav/v_mis
SFX l 'я енем м'я # ім'я іменем (О.) @ noun:n:v_oru
SFX l 'я 'ю м'я # ім'я ім'ю (Д.М.) @ noun:n:v_dav/v_mis
SFX l одець ідця одець # виходець вихідця (Р.З.) @ noun:m:v_rod/v_zna
SFX l одець ідцю одець # виходець вихідцю (Д.) @ noun:m:v_dav
SFX l одець ідцеві одець # виходець вихідцеві (Д.) @ noun:m:v_dav
SFX l одець ідцем одець # виходець вихідцем (О.) @ noun:m:v_oru
SFX l одець ідці одець # виходець вихідці (M.) @ noun:m:v_mis//p:v_naz
SFX l овець івця овець # (Р.З.) @ noun:m:v_rod/v_zna
SFX l овець івцю овець # (Д.) @ noun:m:v_dav
SFX l овець івцеві овець # (Д.) @ noun:m:v_dav
SFX l овець івцем овець # (О.) @ noun:m:v_oru
SFX l овець івці овець # (M.) @ noun:m:v_mis//p:v_naz
SFX l 0 у яр # газетяр газетяру (Д.) @ noun:m:v_dav
SFX l 0 ю [аиуе]р # кобзар кобзарю (Д.М.) @ noun:m:v_dav/v_mis
SFX l 0 еві [аиуяе]р # кобзар кобзареві (Д.М.) @ noun:m:v_dav/v_mis
SFX l 0 ем [аиуяе]р # кобзар кобзарем (О.) @ noun:m:v_oru
SFX l 0 і [аиуяе]р # кобзар кобзарі (М.) @ noun:m:v_mis//p:v_naz
SFX l день ню день # тижню (Д.) @ noun:m:v_dav/v_mis
SFX l день неві день # тижневі (З.) @ noun:m:v_dav
SFX l день ні день # тижні (M.) @ noun:m:v_mis//p:v_naz
SFX l день нем день # тижнем (О.) @ noun:m:v_oru
SFX l тень ню тень # персню (Д.) @ noun:m:v_dav/v_mis
SFX l тень неві тень # персневі (З.) @ noun:m:v_dav
SFX l тень ні тень # персні (M.) @ noun:m:v_mis//p:v_naz
SFX l тень нем тень # перснем (О.) @ noun:m:v_oru
SFX m Y 76
SFX m ість остей ість # ніжність ніжностей (Р.) @ noun:p:v_rod
SFX m ість остям ість # ніжність ніжностям (Д.) @ noun:p:v_dav
SFX m ість остями ість # ніжність ніжностями(О.) @ noun:p:v_oru
SFX m ість остях ість # ніжність ніжностях (М.) @ noun:p:v_mis
SFX m їсть йостей їсть # безкраїсть безкрайостей (Р.) @ noun:p:v_rod
SFX m їсть йостям їсть # безкраїсть безкрайостям (Д.) @ noun:p:v_dav
SFX m їсть йостями їсть # безкраїсть безкрайостями (О.) @ noun:p:v_oru
SFX m їсть йостях їсть # безкраїсть безкрайостях (М.) @ noun:p:v_mis
SFX m інь еней о[св]інь # осінь осеней (Р.) @ noun:p:v_rod
SFX m інь еням о[св]інь # осінь осеням (Д.) @ noun:p:v_dav
SFX m інь енями о[св]інь # осінь осенями (О.) @ noun:p:v_oru
SFX m інь енях о[св]інь # осінь осенях (М.) @ noun:p:v_mis
SFX m іль олей іль # сіль солей (Р.) @ noun:p:v_rod
SFX m іль олям іль # сіль солям (Д.) @ noun:p:v_dav
SFX m іль олями іль # сіль солями (О.) @ noun:p:v_oru
SFX m іль олях іль # сіль солях (М.) @ noun:p:v_mis
SFX m іч ечей [^н]іч # піч печей (Р.) @ noun:p:v_rod
SFX m іч ечам [^н]іч # піч печам (Д.) @ noun:p:v_dav
SFX m іч ечами [^н]іч # піч печами (О.) @ noun:p:v_oru
SFX m іч ечах [^н]іч # піч печах (М.) @ noun:p:v_mis
SFX m іч очей ніч # ніч ночей (Р.) @ noun:p:v_rod
SFX m іч очам ніч # ніч ночам (Д.) @ noun:p:v_dav
SFX m іч очами ніч # ніч ночами (О.) @ noun:p:v_oru
SFX m іч очах ніч # ніч ночах (М.) @ noun:p:v_mis
SFX m іш ошів іш # розкіш розкошів (Р.) @ noun:p:v_rod
SFX m іш ошам іш # розкіш розкошам (Д.) @ noun:p:v_dav
SFX m іш ошами іш # розкіш розкошами (О.) @ noun:p:v_oru
SFX m іш ошах іш # розкіш розкошах (М.) @ noun:p:v_mis
SFX m ь ей [^їі]сть # водорость водоростей (Р.) @ noun:p:v_rod
SFX m ь ям [^їі]сть # водорость водоростям (Д.) @ noun:p:v_dav
SFX m ь ями [^їі]сть # водорость водоростями (О.) @ noun:p:v_oru
SFX m ь ях [^їі]сть # водорость водоростях (М.) @ noun:p:v_mis
SFX m 0 та [^'][яа] # цуценя цуценята (Н.) @ noun:p:v_naz
SFX m 0 т [^'][яа] # цуценя цуценят (Р.) @ noun:p:v_rod
SFX m 0 там [^'][яа] # цуценя цуценятам (Д.) @ noun:p:v_dav
SFX m 0 тами [^'][яа] # цуценя цуценятами (О.) @ noun:p:v_oru
SFX m 0 тах [^'][яа] # цуценя цуценятах (М.) @ noun:p:v_mis
SFX m 0 та п'я # хлоп'я хлоп'ята (Н.) @ noun:p:v_naz
SFX m 0 т п'я # хлоп'я хлоп'ят (Р.) @ noun:p:v_rod
SFX m 0 там п'я # хлоп'я хлоп'ятам (Д.) @ noun:p:v_dav
SFX m 0 тами п'я # хлоп'я хлоп'ятами (О.) @ noun:p:v_oru
SFX m 0 тах п'я # хлоп'я хлоп'ятах (Д.) @ noun:p:v_dav
SFX m 'я ена [^п]'я # ім'я імена (Н.) @ noun:p:v_naz
SFX m 'я ен [^п]'я # ім'я імен (Р.) @ noun:p:v_rod
SFX m 'я енам [^п]'я # ім'я іменам (Д.) @ noun:p:v_dav
SFX m 'я енами [^п]'я # ім'я іменами (О.) @ noun:p:v_oru
SFX m 'я енах [^п]'я # ім'я іменах (М.) @ noun:p:v_mis
SFX m одець ідці одець # виходець вихідці (Mн.) @ noun:p:v_naz
SFX m одець ідців одець # виходець вихідців (Р.З.) @ noun:m:v_rod
SFX m одець ідцям одець # виходець вихідцям (Д.) @ noun:m:v_dav
SFX m одець ідцями одець # виходець вихідцями (Д.) @ noun:m:v_oru
SFX m одець ідцях одець # виходець вихідцях (М.) @ noun:m:v_mis
SFX m овець івці овець # (Mн.) @ noun:p:v_naz
SFX m овець івців овець # (Р.З.) @ noun:p:v_rod
SFX m овець івцям овець # (Д.) @ noun:p:v_dav
SFX m овець івцями овець # (О.) @ noun:p:v_oru
SFX m овець івцях овець # (М.) @ noun:p:v_mis
SFX m 0 і яр # газетяр газетярі (Н.) @ noun:p:v_naz
SFX m 0 ів яр # газетяр газетярів (Р.) @ noun:p:v_rod
SFX m 0 ам яр # газетяр газетярам (Д.) @ noun:p:v_dav
SFX m 0 ами яр # газетяр газетярами (О.) @ noun:p:v_oru
SFX m 0 ах яр # газетяр газетярах (М.) @ noun:p:v_mis
SFX m 0 ів [аиуе]р # кобзар кобзарів (Р.) @ noun:p:v_rod
SFX m 0 ям [аиуе]р # кобзар кобзарям (Д.) @ noun:p:v_dav
SFX m 0 ями [аиуе]р # кобзар кобзарями (О.) @ noun:p:v_oru
SFX m 0 ях [аиуе]р # кобзар кобзарях (М.) @ noun:p:v_mis
SFX m день ні день # тижні @ noun:p:v_naz
SFX m день нів день # тижнів @ noun:p:v_rod
SFX m день ням день # тижням @ noun:p:v_dav
SFX m день нями день # тижнями @ noun:p:v_oru
SFX m день нях день # тижнях @ noun:p:v_mis
SFX m тень ні тень # персні @ noun:p:v_naz
SFX m тень нів тень # перснів @ noun:p:v_rod
SFX m тень ням тень # персням @ noun:p:v_dav
SFX m тень нями тень # перснями @ noun:p:v_oru
SFX m тень нях тень # перснях @ noun:p:v_mis
SFX n Y 9
SFX n інь ене о[св]інь # осінь осене (К.) @ noun:f:v_kly
SFX n іль оле іль # сіль соле (К.) @ noun:f:v_kly
SFX n іч ече [^н]іч # піч пече (К.) @ noun:f:v_kly
SFX n іч оче ніч # ніч ноче (К.) @ noun:f:v_kly
SFX n іш оше іш # розкіш розкоше (К.) @ noun:f:v_kly
SFX n ість осте ість # ніжність ніжносте (К.) @ noun:f:v_kly
SFX n їсть йосте їсть # безкраїсть безкрайосте (К.) @ noun:f:v_kly
SFX n ь те [^їі]сть # водорость водоросте (К.) @ noun:f:v_kly
SFX n 0 е [аиуя]р # газетяр газетяре (К.) @ noun:m:v_kly
SFX q Y 4
SFX q 0 а яр # газетяр газетяра (Р.З.) @ noun:m:v_rod/v_zna
SFX q 0 я [аиуе]р # кобзар кобзаря (Р.З.) @ noun:m:v_rod/v_zna
SFX q день ня день # тиждень тижня (Д.) @ noun:m:v_rod
SFX q тень ня тень # перстень персня (Д.) @ noun:m:v_rod
SFX p Y 182
SFX p я йович ія # Єремія Єремійович (Н.ч.о.) @ noun:m:v_naz
SFX p я ївна ія # Єремія Єреміївна (Н.ж.о.) @ noun:f:v_naz
SFX p я йовича ія # Єремія Єремійовича (Р.З.ч.о.) @ noun:m:v_rod/v_zna
SFX p я ївни ія # Єремія Єреміївни (Р.ж.о.Н.ж.м) @ noun:f:v_rod
SFX p я йовичу ія # Єремія Єремійовичу (Д.М.К.ч.о.) @ noun:m:v_dav/v_mis/v_kly
SFX p я йовичеві ія #Єремія Єремійовичеві (Д.М.ч.о.) @ noun:m:v_dav/v_mis
SFX p я ївні ія # Єремія Єреміївні (Д.М.ж.о.) @ noun:f:v_dav/v_mis
SFX p я ївну ія # Єремія Єреміївну (З.ж.о.) @ noun:f:v_zna
SFX p я йовичем ія # Єремія Єремійовичем (О.ч.о.) @ noun:m:v_oru
SFX p я ївною ія # Єремія Єреміївною (О.ж.о.) @ noun:f:v_oru
SFX p я йовичі ія # Єремія Єремійовичі (М.ч.о.Н.ч.м) @ noun:p:v_naz
SFX p я ївно ія # Єремія Єреміївно (К.ж.о.) @ noun:f:v_kly
SFX p я йовичів ія # Єремія Єремійовичів (Р.З.ч.м.)
SFX p я йовичам ія # Єремія Єремійовичам (Д.ч.м.)
SFX p я йовичами ія # Єремія Єремійовичами (О.ч.м.)
SFX p я йовичах ія # Єремія Єремійовичах (М.ч.м.)
SFX p я іч [^і]я # Ілля Ілліч (Н.ч.о.) @ noun:m:v_naz
SFX p я івна [^і]я # Ілля Іллівна (Н.ж.о.) @ noun:f:v_naz
SFX p я іча [^і]я # Ілля Ілліча (Р.З.ч.о.) @ noun:m:v_rod/v_zna
SFX p я івни [^і]я # Ілля Іллівни (Р.ж.о.Н.ж.м) @ noun:f:v_rod
SFX p я ічу [^і]я # Ілля Іллічу (Д.М.К.ч.о.) @ noun:m:v_dav/v_mis/v_kly
SFX p я ічеві [^і]я # Ілля Іллічеві (Д.М.ч.о.) @ noun:m:v_dav/v_mis
SFX p я івні [^і]я # Ілля Іллівні (Д.М.ж.о.) @ noun:f:v_dav
SFX p я івну [^і]я # Ілля Іллівну (З.ж.о.) @ noun:f:v_zna
SFX p я ічем [^і]я # Ілля Іллічем (О.ч.о.) @ noun:m:v_oru
SFX p я івною [^і]я # Ілля Іллівною (О.ж.о.) @ noun:f:v_oru
SFX p я ічі [^і]я # Ілля Іллічі (М.ч.о.Н.ч.м) @ noun:m:v_mis//p:v_naz
SFX p я івно [^і]я # Ілля Іллівно (К.ж.о.) @ noun:f:v_kly
SFX p я ічів [^і]я # Ілля Іллічів (Р.З.ч.м.)
SFX p я ічам [^і]я # Ілля Іллічам (Д.ч.м.)
SFX p я ічами [^і]я # Ілля Іллічами (О.ч.м.)
SFX p я ічах [^і]я # Ілля Іллічах (М.ч.м.)
SFX p а ич [^тс]а # Кузьма Кузьмич (Н.ч.о.) @ noun:m:v_naz
SFX p а ича [^тс]а # Кузьма Кузьмича (Р.З.ч.о.) @ noun:m:v_rod/v_zna
SFX p а ичу [^тс]а # Кузьма Кузьмичу (Д.М.К.ч.о.) @ noun:m:v_dav/v_mis
SFX p а ичеві [^тс]а # Кузьма Кузьмичеві (Д.М.ч.о.) @ noun:m:v_dav/v_mis
SFX p а ичем [^тс]а # Кузьма Кузьмичем (О.Д.ч.о.) @ noun:m:v_oru
SFX p а ичі [^тс]а # Кузьма Кузьмичі (М.ч.о.Н.ч.м) @ noun:m:v_mis
SFX p а ович [тс]а # Олекса Олексович (Н.ч.о.) @ noun:m:v_naz
SFX p а овича [тс]а # Олекса Олексовича (Р.З.ч.о.) @ noun:m:v_rod/v_zna
SFX p а овичу [тс]а # Олекса Олексовичу (Д.М.К.ч.о.) @ noun:m:v_dav/v_mis/v_kly
SFX p а овичеві [тс]а # Олекса Олексовичеві (Д.М.ч.о.) @ noun:m:v_dav/v_mis
SFX p а овичем [тс]а # Олекса Олексовичем (О.Д.ч.о.) @ noun:m:v_oru
SFX p а овичі [тс]а # Олекса Олексовичі (М.ч.о.Н.ч.м) @ noun:m:v_mis
SFX p а івна а # Кузьма Кузьмівна (Н.ж.о.) @ noun:f:v_naz
SFX p а івни а # Кузьма Кузьмівни (Р.ж.о.Н.ж.м) @ noun:f:v_rod
SFX p а івні а # Кузьма Кузьмівні (Д.М.ж.о.) @ noun:f:v_dav/v_mis
SFX p а івну а # Кузьма Кузьмівну (З.ж.о.) @ noun:f:v_zna
SFX p а івною а # Кузьма Кузьмівною (О.ж.о.) @ noun:f:v_oru
SFX p а івно а # Кузьма Кузьмівно (К.ж.о.) @ noun:f:v_kly
SFX p а ичів [^тс]а # Кузьма Кузьмичів (Р.З.ч.м.)
SFX p а ичам [^тс]а # Кузьма Кузьмичам (Д.ч.м.)
SFX p а ичами [^тс]а # Кузьма Кузьмичами (О.ч.м.)
SFX p а ичах [^тс]а # Кузьма Кузьмичах (М.ч.м.)
SFX p а овичів [тс]а # Олекса ів (Р.З.ч.м.)
SFX p а овичам [тс]а # Олекса ам (Д.ч.м.)
SFX p а овичами [тс]а # Олекса ами (О.ч.м.)
SFX p а овичах [тс]а # Олекса ах (М.ч.м.)
SFX p ір орович ір # Федір Федорович (Н.ч.о.) @ noun:m:v_naz
SFX p ір орівна ір # Федір Федорівна (Н.ж.о.) @ noun:f:v_naz
SFX p ір оровича ір # Федір Федоровича (Р.З.ч.о.) @ noun:m:v_rod/v_zna
SFX p ір орівни ір # Федір Федорівни (Р.ж.о.Н.ж.м) @ noun:f:v_rod
SFX p ір оровичу ір # Федір Федоровичу (Д.М.К.ч.о.) @ noun:m:v_dav/v_mis/v_kly
SFX p ір оровичеві ір # Федір Федоровичеві (Д.М.ч.о.) @ noun:m:v_dav/v_mis
SFX p ір орівні ір # Федір Федорівні (Д.М.ж.о.) @ noun:f:v_dav/v_mis
SFX p ір орівну ір # Федір Федорівну (З.ж.о.) @ noun:f:v_zna
SFX p ір оровичем ір # Федір Федоровичем (О.ч.о.) @ noun:m:v_oru
SFX p ір орівною ір # Федір Федорівною (О.ж.о.) @ noun:f:v_oru
SFX p ір оровичі ір # Федір Федоровичі (М.ч.о.Н.ч.м) @ noun:m:v_mis
SFX p ір орівно ір # Федір Федорівно (К.ж.о.) @ noun:f:v_kly
SFX p ір оровичів ір # Федір Федоровичів (Р.З.ч.м.)
SFX p ір оровичам ір # Федір Федоровичам (Д.ч.м.)
SFX p ір оровичами ір # Федір Федоровичами (О.ч.м.)
SFX p ір оровичах ір # Федір Федоровичах (М.ч.м.)
SFX p ін онович и[мх]ін # Пимін Пимонович (Н.ч.о.) @ noun:m:v_naz
SFX p ін онівна и[мх]ін # Пимін Пимонівна (Н.ж.о.) @ noun:f:v_naz
SFX p ін оновича и[мх]ін # Пимін Пимоновича (Р.Д.ч.о.) @ noun:m:v_rod/v_zna
SFX p ін онівни и[мх]ін # Пимін Пимонівни (Р.ж.о.Н.ж.м) @ noun:f:v_rod
SFX p ін оновичу и[мх]ін # Пимін Пимоновичу (Д.М.К.ч.о.) @ noun:m:v_dav/v_mis/v_kly
SFX p ін оновичеві и[мх]ін # Пимін Пимоновичеві (Д.М.ч.о.) @ noun:m:v_dav/v_mis
SFX p ін онівні и[мх]ін # Пимін Пимонівні (Д.М.ж.о.) @ noun:f:v_dav/v_mis
SFX p ін онівну и[мх]ін # Пимін Пимонівну (З.ж.о.) @ noun:f:v_zna
SFX p ін оновичем и[мх]ін # Пимін Пимоновичем (О.Д.ч.о.) @ noun:m:v_oru
SFX p ін онівною и[мх]ін # Пимін Пимонівною (О.ж.о.) @ noun:f:v_oru
SFX p ін оновичі и[мх]ін # Пимін Пимоновичі (М.ч.о.Н.ч.м) @ noun:m:v_mis
SFX p ін онівно и[мх]ін # Пимін Пимонівно (К.ж.о.) @ noun:f:v_kly
SFX p ін оновичів и[мх]ін # Пимін Пимоновичів (Р.З.ч.м.)
SFX p ін оновичам и[мх]ін # Пимін Пимоновичам (Д.ч.м.)
SFX p ін оновичами и[мх]ін # Пимін Пимоновичами (О.ч.м.)
SFX p ін оновичах и[мх]ін # Пимін Пимоновичах (М.ч.м.)
SFX p ів ович ів # Яків Якович (Н.ч.о.) @ noun:m:v_naz
SFX p ів івна ів # Яків Яківна (Н.ж.о.) @ noun:f:v_naz
SFX p ів овича ів # Яків Яковича (Р.Д.ч.о.) @ noun:m:v_rod/v_zna
SFX p ів івни ів # Яків Яківни (Р.ж.о.Н.ж.м) @ noun:f:v_rod
SFX p ів овичу ів # Яків Яковичу (Д.М.К.ч.о.) @ noun:m:v_dav/v_mis/v_kly
SFX p ів овичеві ів # Яків Яковичеві (Д.М.ч.о.) @ noun:m:v_dav/v_mis
SFX p ів івні ів # Яків Яківні (Д.М.ж.о.) @ noun:f:v_dav/v_mis
SFX p ів івну ів # Яків Яківну (З.ж.о.) @ noun:f:v_zna
SFX p ів овичем ів # Яків Яковичем (О.Д.ч.о.) @ noun:m:v_oru
SFX p ів івною ів # Яків Яківною (О.ж.о.) @ noun:f:v_oru
SFX p ів овичі ів # Яків Яковичі (М.ч.о.Н.ч.м) @ noun:m:v_mis
SFX p ів івно ів # Яків Яківно (К.ж.о.) @ noun:f:v_kly
SFX p ів овичів ів # Яків Яковичів (Р.З.ч.м.)
SFX p ів овичам ів # Яків Яковичам (Д.ч.м.)
SFX p ів овичами ів # Яків Яковичами (О.ч.м.)
SFX p ів овичах ів # Яків Яковичах (М.ч.м.)
SFX p о ович о # Павло Павлович (Н.ч.о.) @ noun:m:v_naz
SFX p о івна о # Павло Павлівна (Н.ж.о.) @ noun:f:v_naz
SFX p о овича о # Павло Павловича (Р.З.ч.о.) @ noun:m:v_rod/v_zna
SFX p о івни о # Павло Павлівни (Р.ж.о.Н.ж.м) @ noun:f:v_rod
SFX p о овичу о # Павло Павловичу (Д.М.К.ч.о.) @ noun:m:v_dav/v_mis/v_kly
SFX p о овичеві о # Павло Павловичеві (Д.М.ч.о.) @ noun:m:v_dav/v_mis
SFX p о івні о # Павло Павлівні (Д.М.ж.о.) @ noun:f:v_dav/v_mis
SFX p о івну о # Павло Павлівну (З.ж.о.) @ noun:f:v_zna
SFX p о овичем о # Павло Павловичем (О.Д.ч.о.) @ noun:m:v_oru
SFX p о івною о # Павло Павлівною (О.ж.о.) @ noun:f:v_oru
SFX p о овичі о # Павло Павловичі (М.ч.о.Н.ч.м) @ noun:m:v_mis//p:v_naz
SFX p о івно о # Павло Павлівно (К.ж.о.) @ noun:f:v_kly
SFX p о овичів о # Павло Павловичів (Р.З.ч.м.) @ noun:p:v_rod/v_zna
SFX p о овичам о # Павло Павловичам (Д.ч.м.) @ noun:p:v_dav
SFX p о овичами о # Павло Павловичами (О.ч.м.) @ noun:p:v_oru
SFX p о овичах о # Павло Павловичах (М.ч.м.) @ noun:p:v_mis
SFX p 0 ович [^врнуеаоіиїєяю] # Антон Антонович (Н.ч.о.) @ noun:m:v_naz
SFX p 0 івна [^врнйуеаоіиїєяюь] # Антон Антонівна (Н.ж.о.) @ noun:f:v_naz
SFX p 0 овича [^врнуеаоіиїєяю] # Антон Антоновича (Р.З.ч.о.) @ noun:m:v_rod/v_zna
SFX p 0 івни [^врнйуеаоіиїєяюь] # Антон Антонівни (Р.ж.о.Н.ж.м) @ noun:f:v_rod
SFX p 0 овичу [^врнуеаоіиїєяю] # Антон Антоновичу (Д.М.К.ч.о.) @ noun:m:v_dav/v_mis/v_kly
SFX p 0 овичеві [^врнуеаоіиїєяю] # Антон Антоновичеві (Д.М.ч.о.) @ noun:m:v_dav/v_mis
SFX p 0 івні [^врнйуеаоіиїєяюь] # Антон Антонівні (Д.М.ж.о.) @ noun:f:v_dav/v_mis
SFX p 0 івну [^врнйуеаоіиїєяюь] # Антон Антонівну (З.ж.о.) @ noun:f:v_zna
SFX p 0 овичем [^врнуеаоіиїєяю] # Антон Антоновичем (О.Д.ч.о.) @ noun:m:v_oru
SFX p 0 івною [^врнйуеаоіиїєяюь] # Антон Антонівною (О.ж.о.) @ noun:f:v_oru
SFX p 0 овичі [^врнуеаоіиїєяю] # Антон Антоновичі (М.ч.о.Н.ч.м) @ noun:m:v_mis
SFX p 0 івно [^врнйуеаоіиїєяюь] # Антон Антонівно (К.ж.о.) @ noun:f:v_kly
SFX p й ївна й # Анатолій Анатоліївна (Н.ж.о.) @ noun:f:v_naz
SFX p й ївни й # Анатолій Анатоліївни (Р.ж.о.Н.ж.м) @ noun:f:v_rod
SFX p й ївні й # Анатолій Анатоліївні (Д.М.ж.о.) @ noun:f:v_dav/v_mis
SFX p й ївну й # Анатолій Анатоліївну (З.ж.о.) @ noun:f:v_zna
SFX p й ївною й # Анатолій Анатоліївною (О.ж.о.) @ noun:f:v_oru
SFX p й ївно й # Анатолій Анатоліївно (К.ж.о.) @ noun:f:v_kly
SFX p 0 овичів [^врнуеаоіиїєяю] # Антон Антоновичів (Р.З.ч.м.)
SFX p 0 овичам [^врнуеаоіиїєяю] # Антон Антоновичам (Д.ч.м.)
SFX p 0 овичами [^врнуеаоіиїєяю] # Антон Антоновичами (О.ч.м.)
SFX p 0 овичах [^врнуеаоіиїєяю] # Антон Антоновичах (М.ч.м.)
SFX p ь івна ь # Василь Василівна (Н.ж.о.) @ noun:f:v_naz
SFX p ь івни ь # Василь Василівни (Р.ж.о.Н.ж.м) @ noun:f:v_rod/v_zna
SFX p ь івні ь # Василь Василівні (Д.М.ж.о.) @ noun:f:v_dav/v_mis
SFX p ь івну ь # Василь Василівну (З.ж.о.) @ noun:f:v_zna
SFX p ь івною ь # Василь Василівною (О.ж.о.) @ noun:f:v_oru
SFX p ь івно ь # Василь Василівно (К.ж.о.) @ noun:f:v_kly
SFX p 0 ович [^і][врн] # Єгор Єгорович (Н.ч.о.) @ noun:m:v_naz
SFX p 0 івна [^і][врн] # Єгор Єгорівна (Н.ж.о.) @ noun:f:v_naz
SFX p 0 овича [^і][врн] # Єгор Єгоровича (Р.Д.ч.о.) @ noun:m:v_rod/v_zna
SFX p 0 івни [^і][врн] # Єгор Єгорівни (Р.ж.о.Н.ж.м) @ noun:f:v_rod
SFX p 0 овичу [^і][врн] # Єгор Єгоровичу (Д.М.К.ч.о.) @ noun:m:v_dav/v_mis/v_kly
SFX p 0 овичеві [^і][врн] # Єгор Єгоровичеві (Д.М.ч.о.) @ noun:m:v_dav/v_mis
SFX p 0 івні [^і][врн] # Єгор Єгорівні (Д.М.ж.о.) @ noun:f:v_dav/v_mis
SFX p 0 івну [^і][врн] # Єгор Єгорівну (З.ж.о.) @ noun:f:v_zna
SFX p 0 овичем [^і][врн] # Єгор Єгоровичем (О.Д.ч.о.) @ noun:m:v_oru
SFX p 0 івною [^і][врн] # Єгор Єгорівною (О.ж.о.) @ noun:f:v_oru
SFX p 0 овичі [^і][врн] # Єгор Єгоровичі (М.ч.о.Н.ч.м) @ noun:m:v_mis
SFX p 0 івно [^і][врн] # Єгор Єгорівно (К.ж.о.) @ noun:f:v_kly
SFX p 0 овичів [^і][врн] # Єгор Єгоровичів (Р.З.ч.м.)
SFX p 0 овичам [^і][врн] # Єгор Єгоровичам (Д.ч.м.)
SFX p 0 овичами [^і][врн] # Єгор Єгоровичами (О.ч.м.)
SFX p 0 овичах [^і][врн] # Єгор Єгоровичах (М.ч.м.)
SFX p 0 ович [^и]мін # Веніамін Веніамінович (Н.ч.о.) @ noun:m:v_naz
SFX p 0 івна [^и]мін # Веніамін Веніамінівна (Н.ж.о.) @ noun:f:v_naz
SFX p 0 овича [^и]мін # Веніамін Веніаміновича (Р.Д.ч.о.) @ noun:m:v_rod/v_zna
SFX p 0 івни [^и]мін # Веніамін Веніамінівни (Р.ж.о.Н.ж.м) @ noun:f:v_rod
SFX p 0 овичу [^и]мін # Веніамін Веніаміновичу (Д.М.К.ч.о.) @ noun:m:v_dav/v_mis/v_kly
SFX p 0 овичеві [^и]мін # Веніамін Веніаміновичеві (Д.М.ч.о.) @ noun:m:v_dav/v_mis
SFX p 0 івні [^и]мін # Веніамін Веніамінівні (Д.М.ж.о.) @ noun:f:v_dav/v_mis
SFX p 0 івну [^и]мін # Веніамін Веніамінівну (З.ж.о.) @ noun:f:v_zna
SFX p 0 овичем [^и]мін # Веніамін Веніаміновичем (О.ч.о.) @ noun:m:v_oru
SFX p 0 івною [^и]мін # Веніамін Веніамінівною (О.ж.о.) @ noun:f:v_oru
SFX p 0 овичі [^и]мін # Веніамін Веніаміновичі (М.ч.о.Н.ч.м) @ noun:m:v_mis
SFX p 0 івно [^и]мін # Веніамін Веніамінівно (К.ж.о.) @ noun:f:v_kly
SFX p 0 овичів [^и]мін # Веніамін Веніаміновичів (Р.З.ч.м.)
SFX p 0 овичам [^и]мін # Веніамін Веніаміновичам (Д.ч.м.)
SFX p 0 овичами [^и]мін # Веніамін Веніаміновичами (О.ч.м.)
SFX p 0 овичах [^и]мін # Веніамін Веніаміновичах (М.ч.м.)
SFX A Y 413
SFX A ти ла [^с]ти # абонувати абонувала (Вона) @ verb:past:f
SFX A ти ло [^с]ти # абонувати абонувало (Воно) @ verb:past:n
SFX A ти ли [^с]ти # абонувати абонували (Ми, Ви, Вони) @ verb:past:p
SFX A ти в [аеиіїоуя]ти # абонувати абонував (Я, Ти, Він) @ verb:past:m
SFX A ти вши [аеиіїоуя]ти # абонувати абонувавши @ adp:perf
SFX A вати ю [ауюя]вати # абонувати абоную (Я) @ verb:pres:s:1
SFX A вати єш [ауюя]вати # абонувати абонуєш (Ти) @ verb:pres:s:2
SFX A вати є [ауюя]вати # абонувати абонує (Він) @ verb:pres:s:3
SFX A вати ємо [ауюя]вати # абонувати абонуємо (Ми) @ verb:pres:p:1
SFX A вати єте [ауюя]вати # абонувати абонуєте (Ви) @ verb:pres:p:2
SFX A вати ють [ауюя]вати # абонувати абонують (Вони) @ verb:pres:p:3
SFX A вати й [ую]вати # абонувати абонуй (Ти) @ verb:impr:s:2
SFX A вати ймо [ую]вати # абонувати абонуймо (Ми) @ verb:impr:p:1
SFX A вати йте [ую]вати # абонувати абонуйте (Ви) @ verb:impr:p:2
SFX A ти й [ая]вати # ставати ставай (Ти) @ verb:impr:s:2
SFX A ти ймо [ая]вати # ставати ставаймо (Ми) @ verb:impr:p:1
SFX A ти йте [ая]вати # ставати ставайте (Ви) @ verb:impr:p:2
SFX A ати у [рз]вати # рвати рву (Я) @ verb:pres:s:1
SFX A ати еш [рз]вати # рвати рвеш (Ти) @ verb:pres:s:2
SFX A ати е [рз]вати # рвати рве (Він) @ verb:pres:s:3
SFX A ати емо [рз]вати # рвати рвемо (Ми) @ verb:pres:p:1
SFX A ати ете [рз]вати # рвати рвете (Ви) @ verb:pres:p:2
SFX A ати уть [рз]вати # рвати рвуть (Вони) @ verb:pres:p:3
SFX A ати и [рз]вати # рвати рви (Ти) @ verb:impr:s:2
SFX A ати імо [рз]вати # рвати рвімо (Ми) @ verb:impr:p:1
SFX A ати іть [рз]вати # рвати рвіть (Ви) @ verb:impr:p:2
SFX A зати жу зати # казати кажу (Я) @ verb:pres:s:1
SFX A зати жеш зати # казати кажеш (Ти) @ verb:pres:s:2
SFX A зати же зати # казати каже (Він) @ verb:pres:s:3
SFX A зати жемо зати # казати кажемо (Ми) @ verb:pres:p:1
SFX A зати жете зати # казати кажете (Ви) @ verb:pres:p:2
SFX A зати жуть зати # казати кажуть (Вони) @ verb:pres:p:3
SFX A зати ж ізати # різати ріж (Ти) @ verb:impr:s:2
SFX A зати ж мазати # мазати маж (Ти) @ verb:impr:s:2
SFX A зати жи казати # казати кажи (Ти) @ verb:impr:s:2
SFX A зати жи [еия]зати # лизати лижи (Ти) @ verb:impr:s:2
SFX A зати жмо ізати # відрізати відріжмо (Ми) @ verb:impr:p:1
SFX A зати жмо мазати # мазати мажмо (Ми) @ verb:impr:p:1
SFX A зати жімо казати # казати кажімо (Ми) @ verb:impr:p:1
SFX A зати жімо [еия]зати # лизати лижімо (Ми) @ verb:impr:p:1
SFX A зати жте ізати # відрізати відріжте (Ви) @ verb:impr:p:2
SFX A зати жте мазати # мазати мажте (Ви) @ verb:impr:p:2
SFX A зати жіть казати # казати кажіть (Ви) @ verb:impr:p:2
SFX A зати жіть [еия]зати # лизати лижіть (Ви) @ verb:impr:p:2
SFX A ати у [днжщ]ати # блищати блищу (Я) @ verb:pres:s:1
SFX A ати у [^ао]чати # деренчати деренчу (Я) @ verb:pres:s:1
SFX A ати ну [ао]чати # зачати зачну (Я) @ verb:pres:s:1
SFX A тати чу [^с]тати # шептати шепчу (Я) @ verb:pres:s:1
SFX A кати чу [^с]кати # плакати плачу (Я) @ verb:pres:s:1
SFX A сати шу сати # писати пишу (Я) @ verb:pres:s:1
SFX A хати шу хати # брехати брешу (Я) @ verb:pres:s:1
SFX A стати щу стати # свистати свищу (Я) @ verb:pres:s:1
SFX A скати щу скати # плескати плещу (Я) @ verb:pres:s:1
SFX A слати шлю слати # послати пошлю (Я) @ verb:pres:s:1
SFX A ати лю пати # сипати сиплю (Я) @ verb:pres:s:1
SFX A ати ю орати # орати орю (Я) @ verb:pres:s:1
SFX A рати еру [бдп]рати # брати беру (Я) @ verb:pres:s:1
SFX A ати еш [дн]ати # стогнати стогнеш (Ти) @ verb:pres:s:2
SFX A ати иш [жщ]ати # блищати блищиш (Ти) @ verb:pres:s:2
SFX A ати иш [^оа]чати # бряжчати бряжчиш (Ти) @ verb:pres:s:2
SFX A ати неш [ао]чати # зачати зачнеш (Ти) @ verb:pres:s:2
SFX A тати чеш [^с]тати # шептати шепчеш (Ти) @ verb:pres:s:2
SFX A кати чеш [^с]кати # плакати плачеш (Ти) @ verb:pres:s:2
SFX A сати шеш сати # писати пишеш (Ти) @ verb:pres:s:2
SFX A хати шеш хати # брехати брешеш (Ти) @ verb:pres:s:2
SFX A стати щеш стати # свистати свищеш (Ти) @ verb:pres:s:2
SFX A скати щеш скати # плескати плещеш (Ти) @ verb:pres:s:2
SFX A слати шлеш слати # послати пошлеш (Ти) @ verb:pres:s:2
SFX A ати леш ипати # сипати сиплеш (Ти) @ verb:pres:s:2
SFX A ати иш спати # спати спиш (Ти) @ verb:pres:s:2
SFX A ати еш орати # орати ореш (Ти) @ verb:pres:s:2
SFX A рати ереш [бдп]рати # брати береш (Ти) @ verb:pres:s:2
SFX A ати е [дн]ати # стогнати стогне (Він) @ verb:pres:s:3
SFX A ати ить [жщ]ати # блищати блищить (Він) @ verb:pres:s:3
SFX A ати ить [^оа]чати # бряжчати бряжчить (Він) @ verb:pres:s:3
SFX A ати не [ао]чати # зачати зачне (Він) @ verb:pres:s:3
SFX A тати че [^с]тати # шептати шепче (Він) @ verb:pres:s:3
SFX A кати че [^с]кати # плакати плаче (Він) @ verb:pres:s:3
SFX A сати ше сати # писати пише (Він) @ verb:pres:s:3
SFX A хати ше хати # брехати бреше (Він) @ verb:pres:s:3
SFX A стати ще стати # свистати свище (Він) @ verb:pres:s:3
SFX A скати ще скати # плескати плеще (Він) @ verb:pres:s:3
SFX A слати шле слати # послати пошле (Він) @ verb:pres:s:3
SFX A ати ле ипати # сипати сипле (Він) @ verb:pres:s:3
SFX A ати ить спати # спати спить (Він) @ verb:pres:s:3
SFX A ати е орати # орати оре (Він) @ verb:pres:s:3
SFX A рати ере [бдп]рати # брати бере (Він) @ verb:pres:s:3
SFX A ати емо [дн]ати # стогнати стогнемо (Ми) @ verb:pres:p:1
SFX A ати имо [жщ]ати # блищати блищимо (Ми) @ verb:pres:p:1
SFX A ати немо [ао]чати # зачати зачнемо (Ми) @ verb:pres:p:1
SFX A чати чимо [^оа]чати # бряжчати бряжчимо (Ми) @ verb:pres:p:1
SFX A тати чемо [^с]тати # шептати шепчемо (Ми) @ verb:pres:p:1
SFX A кати чемо [^с]кати # плакати плачемо (Ми) @ verb:pres:p:1
SFX A сати шемо сати # писати пишемо (Ми) @ verb:pres:p:1
SFX A хати шемо хати # брехати брешемо (Ми) @ verb:pres:p:1
SFX A стати щемо стати # свистати свищемо (Ми) @ verb:pres:p:1
SFX A скати щемо скати # плескати плещемо (Ми) @ verb:pres:p:1
SFX A слати шлемо слати # послати пошлемо (Ми) @ verb:pres:p:1
SFX A ати лемо ипати # сипати сиплемо (Ми) @ verb:pres:p:1
SFX A ати имо спати # спати спимо (Ми) @ verb:pres:p:1
SFX A ати емо орати # орати оремо (Ми) @ verb:pres:p:1
SFX A рати еремо [бдп]рати # брати беремо (Ми) @ verb:pres:p:1
SFX A ати ете [дн]ати # стогнати стогнете (Ви) @ verb:pres:p:2
SFX A ати ите [жщ]ати # блищати блищиш (Ви) @ verb:pres:p:2
SFX A ати нете [ао]чати # зачати зачнете (Ви) @ verb:pres:p:2
SFX A чати чите [^оа]чати # бряжчати бряжчиш (Ви) @ verb:pres:p:2
SFX A тати чете [^с]тати # шептати шепчете (Ви) @ verb:pres:p:2
SFX A кати чете [^с]кати # плакати плачете (Ви) @ verb:pres:p:2
SFX A сати шете сати # писати пишете (Ви) @ verb:pres:p:2
SFX A хати шете хати # брехати брешете (Ви) @ verb:pres:p:2
SFX A стати щете стати # свистати свищете (Ви) @ verb:pres:p:2
SFX A скати щете скати # плескати плещете (Ви) @ verb:pres:p:2
SFX A слати шлете слати # послати пошлете (Ви) @ verb:pres:p:2
SFX A ати лете ипати # сипати сиплете (Ви) @ verb:pres:p:2
SFX A ати ите спати # спати спите (Ви) @ verb:pres:p:2
SFX A ати ете орати # орати орете (Ви) @ verb:pres:p:2
SFX A рати ерете [бдп]рати # брати берете (Ви) @ verb:pres:p:2
SFX A ати уть [дн]ати # стогнати стогнуть (Вони) @ verb:pres:p:3
SFX A ати ать [жщ]ати # блищати блищать (Вони) @ verb:pres:p:3
SFX A ати нуть [ао]чати # зачати зачнуть (Вони) @ verb:pres:p:3
SFX A ати ать [^ао]чати # бряжчати бряжчать (Вони) @ verb:pres:p:3
SFX A тати чуть [^с]тати # шептати шепчуть (Вони) @ verb:pres:p:3
SFX A кати чуть [^с]кати # плакати плачуть (Вони) @ verb:pres:p:3
SFX A сати шуть сати # писати пишуть (Вони) @ verb:pres:p:3
SFX A хати шуть хати # брехати брешуть (Вони) @ verb:pres:p:3
SFX A стати щуть стати # свистати свищуть (Вони) @ verb:pres:p:3
SFX A скати щуть скати # плескати плещуть (Вони) @ verb:pres:p:3
SFX A слати шлють слати # послати пошлють (Вони) @ verb:pres:p:3
SFX A ати лють ипати # сипати сиплють (Вони) @ verb:pres:p:3
SFX A ати лять спати # спати сплять (Вони) @ verb:pres:p:3
SFX A ати ють орати # орати орють (Вони) @ verb:pres:p:3
SFX A рати еруть [бдп]рати # брати беруть (Вони) @ verb:pres:p:3
SFX A ати и [днжщ]ати # блищати блищи (Ти) @ verb:impr:s:2
SFX A ати ни [ао]чати # зачати зачни (Ти) @ verb:impr:s:2
SFX A ати и [^ао]чати # деренчати деренчи (Ти) @ verb:impr:s:2
SFX A тати чи [^с]тати # шептати шепчи (Ти) @ verb:impr:s:2
SFX A сати ши сати # писати пиши (Ти) @ verb:impr:s:2
SFX A хати ши хати # брехати бреши (Ти) @ verb:impr:s:2
SFX A кати ч лакати # плакати плач (Ти) @ verb:impr:s:2
SFX A кати чи какати # скакати скачи (Ти) @ verb:impr:s:2
SFX A кати чи ткати # ткати тчи (Ти) @ verb:impr:s:2
SFX A кати ч икати # кликати клич (Ти) @ verb:impr:s:2
SFX A скати щи скати # плескати плещи (Ти) @ verb:impr:s:2
SFX A стати щи стати # свистати свищи (Ти) @ verb:impr:s:2
SFX A слати шли слати # послати пошли (Ти) @ verb:impr:s:2
SFX A пати пи спати # сипати спи (Ти) @ verb:impr:s:2
SFX A пати п ипати # сипати сип (Ти) @ verb:impr:s:2
SFX A ати и орати # орати ори (Ти) @ verb:impr:s:2
SFX A рати ери [бдп]рати # брати бери (Ти) @ verb:impr:s:2
SFX A ати імо [днжщ]ати # блищати блищімо (Ми) @ verb:impr:p:1
SFX A ати німо [ао]чати # зачати зачнімо (Ми) @ verb:impr:p:1
SFX A ати імо [^оа]чати # бряжчати бряжчімо (Ми) @ verb:impr:p:1
SFX A тати чімо [^с]тати # шептати шепчімо (Ми) @ verb:impr:p:1
SFX A сати шімо сати # писати пишімо (Ми) @ verb:impr:p:1
SFX A хати шімо хати # писати пишімо (Ми) @ verb:impr:p:1
SFX A кати чмо лакати # плакати плачмо (Ми) @ verb:impr:p:1
SFX A кати чімо какати # ткати тчімо (Ми) @ verb:impr:p:1
SFX A кати чімо ткати # ткати тчімо (Ми) @ verb:impr:p:1
SFX A кати чмо икати # кликати кличмо (Ми) @ verb:impr:p:1
SFX A скати щімо скати # плескати плещімо (Ми) @ verb:impr:p:1
SFX A стати щімо стати # свистати свищімо (Ми) @ verb:impr:p:1
SFX A слати шлімо слати # послати пошлімо (Ми) @ verb:impr:p:1
SFX A ати мо ипати # сипати сипмо (Ми) @ verb:impr:p:1
SFX A ати імо спати # спати спімо (Ми) @ verb:impr:p:1
SFX A ати імо орати # орати орімо (Ми) @ verb:impr:p:1
SFX A рати ерімо [бдп]рати # брати берімо (Ми) @ verb:impr:p:1
SFX A ати іть [джщн]ати # блищати блищіть (Ви) @ verb:impr:p:2
SFX A ати ніть [ао]чати # зачати зачніть (Ви) @ verb:impr:p:2
SFX A чати чіть [^оа]чати # бряжчати бряжчіть (Ви) @ verb:impr:p:2
SFX A тати чіть [^с]тати # шептати шепчіть (Ви) @ verb:impr:p:2
SFX A сати шіть сати # писати пишіть (Ви) @ verb:impr:p:2
SFX A хати шіть хати # писати пишіть (Ви) @ verb:impr:p:2
SFX A кати чте лакати # плакати плачте (Ви) @ verb:impr:p:2
SFX A кати чіть какати # скакати скачіть (Ви) @ verb:impr:p:2
SFX A кати чіть ткати # ткати тчіть (Ви) @ verb:impr:p:2
SFX A стати щіть стати # свистати свищіть (Ви) @ verb:impr:p:2
SFX A скати щіть скати # плескати плещіть (Ви) @ verb:impr:p:2
SFX A слати шліть слати # послати пошліть (Ви) @ verb:impr:p:2
SFX A кати чте [^ста]кати # плакати плачте (Ви) @ verb:impr:p:2
SFX A ати те ипати # сипати сипте (Ви) @ verb:impr:p:2
SFX A ати іть спати # спати спіть (Ви) @ verb:impr:p:2
SFX A ати іть орати # орати оріть (Ви) @ verb:impr:p:2
SFX A рати еріть [бдп]рати # брати беріть (Ви) @ verb:impr:p:2
SFX A ити жу [^з]дити # входити входжу (Я) @ verb:pres:s:1
SFX A здити жджу здити # їздити їжджу (Я) @ verb:pres:s:1
SFX A зити жу зити # возити вожу (Я) @ verb:pres:s:1
SFX A ити у [жчшщ]ити # бентежити бентежу (Я) @ verb:pres:s:1
SFX A сити шу сити # місити мішу (Я) @ verb:pres:s:1
SFX A тити чу [^с]тити # тратити трачу (Я) @ verb:pres:s:1
SFX A стити щу стити # мостити мощу (Я) @ verb:pres:s:1
SFX A ити лю [бвмпф]ити # вимовити вимовлю (Я) @ verb:pres:s:1
SFX A ити ю [лнр]ити # творити творю (Я) @ verb:pres:s:1
SFX A ти ш ити # бентежити бентежиш (Ти) @ verb:pres:s:2
SFX A и ь ити # бентежити бентежить (Він) @ verb:pres:s:3
SFX A ти мо ити # бентежити бентежимо (Ми) @ verb:pres:p:1
SFX A ити ите ити # бентежити бентежите (Ви) @ verb:pres:p:2
SFX A ити ать [жчшщ]ити # бентежити бентежать (Вони) @ verb:pres:p:3
SFX A ити лять [бвмпф]ити # вимовити вимовлять (Вони) @ verb:pres:p:3
SFX A ити ять [дзлнрст]ити # входити входять (Вони) @ verb:pres:p:3
SFX A іти жу діти # смердіти смерджу (Я) @ verb:pres:s:1
SFX A іти у [шж]іти # кишіти кишу (Я) @ verb:pres:s:1
SFX A сіти шу сіти # висіти вишу (Я) @ verb:pres:s:1
SFX A тіти чу [^с]тіти # летіти лечу (Я) @ verb:pres:s:1
SFX A стіти щу стіти # шелестіти шелещу (Я) @ verb:pres:s:1
SFX A іти лю [бвмп]іти # шуміти шумлю (Я) @ verb:pres:s:1
SFX A іти ю [нлр]іти # веліти велю (Я) @ verb:pres:s:1
SFX A іти иш іти # шепотіти шепотиш (Ти) @ verb:pres:s:2
SFX A іти ить іти # шепотіти шепотить (Він) @ verb:pres:s:3
SFX A іти имо іти # шепотіти шепотимо (Ми) @ verb:pres:p:1
SFX A іти ите іти # шепотіти шепотите (Ви) @ verb:pres:p:2
SFX A іти ать шіти # кишіти кишать (Вони) @ verb:pres:p:3
SFX A іти лять [бвмп]іти # шуміти шумлять (Вони) @ verb:pres:p:3
SFX A іти ять [^бвмпш]іти # входити входять (Вони) @ verb:pres:p:3
SFX A іти и [^д]іти # скрипіти скрипи (Ти) @ verb:impr:s:2
SFX A іти імо [^д]іти # скрипіти скрипімо (Ми) @ verb:impr:p:1
SFX A іти іть [^д]іти # скрипіти скрипіть (Ви) @ verb:impr:s:2
SFX A ути у нути # тягнути тягну (Я) @ verb:pres:s:1
SFX A ути еш нути # тягнути тягнеш (Ти) @ verb:pres:s:2
SFX A ути е нути # тягнути тягне (Він) @ verb:pres:s:3
SFX A ути емо нути # тягнути тягнемо (Ми) @ verb:pres:p:1
SFX A ути ете нути # тягнути тягнете (Ви) @ verb:pres:p:2
SFX A ути уть нути # тягнути тягнуть (Вони) @ verb:pres:p:3
SFX A ти ду бути # забути забуду (Я) @ verb:pres:s:1
SFX A ти деш бути # забути забудеш (Ти) @ verb:pres:s:2
SFX A ти де бути # забути забуде (Він) @ verb:pres:s:3
SFX A ти демо бути # забути забудемо (Ми) @ verb:pres:p:1
SFX A ти дете бути # забути забудете (Ви) @ verb:pres:p:2
SFX A ти дуть бути # забути забудуть (Вони) @ verb:pres:p:3
SFX A ти дь бути # забути забудь (Ти) @ verb:impr:s:2
SFX A ти дьмо бути # забути забудьмо (Ми) @ verb:impr:p:1
SFX A ти дьте бути # забути забудьте (Ви) @ verb:impr:p:2
SFX A оти ю оти # бороти борю (Я) @ verb:pres:s:1
SFX A оти еш оти # бороти бореш (Ти) @ verb:pres:s:2
SFX A оти е оти # бороти боре (Він) @ verb:pres:s:3
SFX A оти емо оти # бороти боремо (Ми) @ verb:pres:p:1
SFX A оти ете оти # бороти борете (Ви) @ verb:pres:p:2
SFX A оти ють оти # бороти борють (Вони) @ verb:pres:p:3
SFX A оти и оти # бороти бори (Ти) @ verb:impr:s:2
SFX A оти імо оти # бороти борімо (Ми) @ verb:impr:p:1
SFX A оти іть оти # бороти боріть (Ви) @ verb:impr:p:2
SFX A їти ю їти # клеїти клею (Я) @ verb:pres:s:1
SFX A ти ш їти # клеїти клеїш (Ти) @ verb:pres:s:2
SFX A ти ть їти # клеїти клеїть (Він) @ verb:pres:s:3
SFX A ти мо їти # клеїти клеїмо (Ми) @ verb:pres:p:1
SFX A ти те їти # клеїти клеїте (Ви) @ verb:pres:p:2
SFX A їти ять їти # клеїти клеять (Вони) @ verb:pres:p:3
SFX A ти у [збв]ти # везти везу (Я) @ verb:pres:s:1
SFX A ти еш [збв]ти # везти везеш (Ти) @ verb:pres:s:2
SFX A ти е [збв]ти # везти везе (Він) @ verb:pres:s:3
SFX A ти емо [збв]ти # везти веземо (Ми) @ verb:pres:p:1
SFX A ти ете [збв]ти # везти везете (Ви) @ verb:pres:p:2
SFX A ти уть [збв]ти # везти везуть (Вони) @ verb:pres:p:3
SFX A ебти іб ебти # гребти гріб (Я, Ти, Він) @ verb:past:m
SFX A езти із езти # везти віз (Я, Ти, Він) @ verb:past:m
SFX A зти з [^е]зти # гризти гриз (Я, Ти, Він) @ verb:past:m
SFX A ти ів евти # ревти ревів (Я, Ти, Він) @ verb:past:m
SFX A вти в [^е]вти # пливти плив (Я, Ти, Він) @ verb:past:m
SFX A бти б убти # скубти скуб (Я, Ти, Він) @ verb:past:m
SFX A ти ь ізти # лізти лізь (Ти) @ verb:impr:s:2
SFX A ти и [^і]зти # везти вези (Ти) @ verb:impr:s:2
SFX A ти и [бв]ти # везти вези (Ти) @ verb:impr:s:2
SFX A ти ьмо ізти # лізти лізьмо (Ми) @ verb:impr:p:1
SFX A ти імо [^і]зти # везти везімо (Ми) @ verb:impr:p:1
SFX A ти імо [бв]ти # везти везімо (Ми) @ verb:impr:p:1
SFX A ти ьте ізти # лізти лізьте (Ви) @ verb:impr:p:2
SFX A ти іть [^і]зти # везти везіть (Ви) @ verb:impr:p:2
SFX A ти іть [бв]ти # везти везіть (Ви) @ verb:impr:p:2
SFX A сти ла [^о]сти # плести плела (Вона) @ verb:past:f
SFX A сти ло [^о]сти # плести плело (Воно) @ verb:past:n
SFX A сти ли [^о]сти # плести плели (Вони) @ verb:past:p:3
SFX A сти сла ости # рости росла (Вона) @ verb:past:f
SFX A сти сло ости # рости росло (Воно) @ verb:past:n
SFX A сти сли ости # рости росли (Вони) @ verb:past:p:3
SFX A ти ту ости # рости росту (Я) @ verb:pres:s:1
SFX A ти теш ости # рости ростеш (Ти) @ verb:pres:s:2
SFX A ти те ости # рости росте (Він) @ verb:pres:s:3
SFX A ти темо ости # рости ростемо (Ми) @ verb:pres:p:1
SFX A ти тете ости # рости ростете (Ви) @ verb:pres:p:2
SFX A ти туть ости # рости ростуть (Вони) @ verb:pres:p:3
SFX A сти ту [еі]сти # цвісти цвіту (Я) @ verb:pres:s:1
SFX A сти теш [еі]сти # цвісти цвітеш (Ти) @ verb:pres:s:2
SFX A сти те [еі]сти # цвісти цвіте (Він) @ verb:pres:s:3
SFX A сти темо [еі]сти # цвісти цвітемо (Ми) @ verb:pres:p:1
SFX A сти тете [еі]сти # цвісти цвітете (Ви) @ verb:pres:p:2
SFX A сти туть [еі]сти # цвісти цвітуть (Вони) @ verb:pres:p:3
SFX A сти ну лясти # клясти кляну (Я) @ verb:pres:s:1
SFX A сти неш лясти # клясти клянеш (Ти) @ verb:pres:s:2
SFX A сти не лясти # клясти кляне (Він) @ verb:pres:s:3
SFX A сти немо лясти # клясти клянемо (Ми) @ verb:pres:p:1
SFX A сти нете лясти # клясти клянете (Ви) @ verb:pres:p:2
SFX A сти нуть лясти # клясти клянуть (Вони) @ verb:pres:p:3
SFX A сти в [іяа]сти # прясти пряв (Я, Ти, Він) @ verb:past:m
SFX A ести ів ести # плести плів (Я, Ти, Він) @ verb:past:m
SFX A ости іс ости # рости ріс (Я, Ти, Він) @ verb:past:m
SFX A сти вши [ія]сти # прясти прявши @ adp:perf
SFX A сти ти [еі]сти # плести плети (Ти) @ verb:impr:s:2
SFX A сти тімо [еі]сти # плести плетімо (Ми) @ verb:impr:p:1
SFX A сти тіть [еі]сти # плести плетіть (Ви) @ verb:impr:p:2
SFX A сти ни лясти # клясти кляни (Ти) @ verb:impr:s:2
SFX A сти німо лясти # клясти клянімо (Ми) @ verb:impr:p:1
SFX A сти ніть лясти # клясти кляніть (Ви) @ verb:impr:p:2
SFX A и імо ости # рости ростімо (Ми) @ verb:impr:p:1
SFX A и іть ости # рости ростіть (Ви) @ verb:impr:p:2
SFX A кти чу кти # текти течу (Я) @ verb:pres:s:1
SFX A кти чеш кти # текти течеш (Ти) @ verb:pres:s:2
SFX A кти че кти # текти тече (Він) @ verb:pres:s:3
SFX A кти чемо кти # текти течемо (Ми) @ verb:pres:p:1
SFX A кти чете кти # текти течете (Ви) @ verb:pres:p:2
SFX A кти чуть кти # текти течуть (Вони) @ verb:pres:p:3
SFX A екти ік екти # текти тік (Я, Ти, Він) @ verb:past:m
SFX A ікти ік ікти # одсікти одсік (Я, Ти, Він) @ verb:past:m
SFX A окти ік окти # волокти волік (Я, Ти, Він) @ verb:past:m
SFX A вкти вк вкти # товкти товк (Я, Ти, Він) @ verb:past:m
SFX A екти ікши екти # текти тік @ adp:perf
SFX A ікти ікши ікти # одсікти одсік @ adp:perf
SFX A окти ікши окти # волокти волік @ adp:perf
SFX A вкти вкши вкти # товкти товк @ adp:perf
SFX A кти чи кти # текти течи (Ти) @ verb:impr:s:2
SFX A кти чімо кти # текти течімо (Ми) @ verb:impr:p:1
SFX A кти чіть кти # текти течіть (Ви) @ verb:impr:p:2
SFX A гти жу [еоиія]гти # допомогти допоможу (Я) @ verb:pres:s:1
SFX A гти жиш ігти # бігти біжиш (Ти) @ verb:pres:s:2
SFX A гти жеш [еоия]гти # допомогти допоможеш (Ти) @ verb:pres:s:2
SFX A гти жить ігти # бігти біжить (Він) @ verb:pres:s:3
SFX A гти же [еоия]гти # допомогти допоможе (Він) @ verb:pres:s:3
SFX A гти жимо ігти # бігти біжимо (Ми) @ verb:pres:p:1
SFX A гти жемо [еоия]гти # допомогти допоможемо (Ми) @ verb:pres:p:1
SFX A гти жите ігти # бігти біжите (Ви) @ verb:pres:p:2
SFX A гти жете [еоия]гти # допомогти допоможете (Ви) @ verb:pres:p:2
SFX A гти жать ігти # бігти біжать (Вони) @ verb:pres:p:3
SFX A гти жуть [еоия]гти # допомогти допоможуть (Вони) @ verb:pres:p:3
SFX A егти іг егти # зберегти зберіг (Я, Ти, Він) @ verb:past:m
SFX A огти іг огти # допомогти допоміг (Я, Ти, Він) @ verb:past:m
SFX A ягти іг ягти # лягти ліг (Я, Ти, Він) @ verb:past:m
SFX A гти г [иі]гти # стригти стриг біг (Я, Ти, Він) @ verb:past:m
SFX A егти ігши егти # зберегти зберігши @ adp:perf
SFX A огти ігши огти # допомогти допомігши @ adp:perf
SFX A ягти ігши ягти # лягти лігши @ adp:perf
SFX A гти гши [иі]гти # стригти стригши @ adp:perf
SFX A гти ж лягти # лягти ляж (Ти) @ verb:impr:s:2
SFX A гти жи рягти # запрягти запряжи (Ти) @ verb:impr:s:2
SFX A гти жи [еоіи]гти # допомогти допоможи (Ти) @ verb:impr:s:2
SFX A гти жмо лягти # лягти ляжмо (Ми) @ verb:impr:p:1
SFX A гти жімо рягти # запрягти запряжімо (Ми) @ verb:impr:p:1
SFX A гти жімо [еоіи]гти # допомогти допоможімо (Ми) @ verb:impr:p:1
SFX A гти жте лягти # лягти ляжте (Ви) @ verb:impr:p:2
SFX A гти жіть рягти # запрягти запряжіть (Ви) @ verb:impr:p:2
SFX A гти жіть [еоіи]гти # допомогти допоможіть (Ви) @ verb:impr:p:2
SFX A ерти ру [^дж]ерти # терти тру (Я) @ verb:pres:s:1
SFX A ерти реш [^дж]ерти # терти треш (Ти) @ verb:pres:s:2
SFX A ерти ре [^дж]ерти # терти тре (Він) @ verb:pres:s:3
SFX A ерти ремо [^дж]ерти # терти тремо (Ми) @ verb:pres:p:1
SFX A ерти рете [^дж]ерти # терти трете (Ви) @ verb:pres:p:2
SFX A ерти руть [^дж]ерти # терти труть (Вони) @ verb:pres:p:3
SFX A рти р рти # терти тер (Я, Ти, Він) @ verb:past:m
SFX A рти рши рти # терти терши @ adp:perf
SFX A ерти ри [^дж]ерти # терти три (Ти) @ verb:impr:s:2
SFX A ерти рімо [^дж]ерти # терти трімо (Ми) @ verb:impr:p:1
SFX A ерти ріть [^дж]ерти # терти тріть (Ви) @ verb:impr:p:2
SFX A рти ру [дж]ерти # жерти жеру (Я) @ verb:pres:s:1
SFX A рти реш [дж]ерти # жерти жереш (Ти) @ verb:pres:s:2
SFX A рти ре [дж]ерти # жерти жере (Він) @ verb:pres:s:3
SFX A рти ремо [дж]ерти # жерти жеремо (Ми) @ verb:pres:p:1
SFX A рти рете [дж]ерти # жерти жерете (Ви) @ verb:pres:p:2
SFX A рти руть [дж]ерти # жерти жеруть (Вони) @ verb:pres:p:3
SFX A ти и [дж]ерти # жерти жери (Ти) @ verb:impr:s:2
SFX A ти імо [дж]ерти # жерти жерімо (Ми) @ verb:impr:p:1
SFX A ти іть [дж]ерти # жерти жеріть (Ви) @ verb:impr:p:2
SFX A ти ю [аі]яти # паяти паяю (Я) @ verb:pres:s:1
SFX A ти єш [аі]яти # паяти паяєш (Ти) @ verb:pres:s:2
SFX A ти є [аі]яти # паяти паяє (Він) @ verb:pres:s:3
SFX A ти ємо [аі]яти # паяти паяємо (Ми) @ verb:pres:p:1
SFX A ти єте [аі]яти # паяти паяєте (Ви) @ verb:pres:p:2
SFX A ти ють [аі]яти # паяти паяють (Вони) @ verb:pres:p:3
SFX A ти й [аі]яти # паяти паяй (Ти) @ verb:impr:s:2
SFX A ти ймо [аі]яти # паяти паяймо (Ми) @ verb:impr:p:1
SFX A ти йте [аі]яти # паяти паяйте (Ви) @ verb:impr:p:2
SFX A зяти ізьму взяти # взяти візьму (Я) @ verb:futr:s:1
SFX A зяти ізьмеш взяти # взяти візьмеш (Ти) @ verb:futr:s:2
SFX A зяти ізьме взяти # взяти візьме (Він) @ verb:futr:s:3
SFX A зяти ізьмемо взяти # взяти візьмемо (Ми) @ verb:futr:p:1
SFX A зяти ізьмете взяти # взяти візьмете (Ви) @ verb:futr:p:2
SFX A зяти ізьмуть взяти # взяти візьмуть (Вони) @ verb:futr:p:3
SFX A зяти ізьми взяти # взяти візьми (Ти) @ verb:impr:s:2
SFX A зяти ізьмімо взяти # взяти візьмімо (Ми) @ verb:impr:p:1
SFX A зяти ізьміть взяти # взяти візьміть (Ви) @ verb:impr:p:2
SFX A няти му йняти # зайняти займу (Я) @ verb:futr:s:1
SFX A няти меш йняти # зайняти займеш (Ти) @ verb:futr:s:2
SFX A няти ме йняти # зайняти займе (Він) @ verb:futr:s:3
SFX A няти мемо йняти # зайняти займемо (Ми) @ verb:futr:p:1
SFX A няти мете йняти # зайняти займете (Ви) @ verb:futr:p:2
SFX A няти муть йняти # зайняти займуть (Вони) @ verb:futr:p:3
SFX A няти ми йняти # зайняти займи (Ти) @ verb:impr:s:2
SFX A няти мімо йняти # зайняти займімо (Ми) @ verb:impr:p:1
SFX A няти міть йняти # зайняти займіть (Ви) @ verb:impr:p:2
SFX A яти іму [здб]няти # підняти підніму (Я) @ verb:futr:s:1
SFX A яти імеш [здб]няти # підняти піднімеш (Ти) @ verb:futr:s:2
SFX A яти іме [здб]няти # підняти підніме (Він) @ verb:futr:s:3
SFX A яти імемо [здб]няти # підняти піднімемо (Ми) @ verb:futr:p:1
SFX A яти імете [здб]няти # підняти піднімете (Ви) @ verb:futr:p:2
SFX A яти імуть [здб]няти # підняти піднімуть (Вони) @ verb:futr:p:3
SFX A яти іми [здб]няти # підняти підніми (Ти) @ verb:impr:s:2
SFX A яти імімо [здб]няти # підняти піднімімо (Ми) @ verb:impr:p:1
SFX A яти іміть [здб]няти # підняти підніміть (Ви) @ verb:impr:p:2
SFX A 'яти ну 'яти # зім'яти зімну (Я) @ verb:futr:s:1
SFX A 'яти неш 'яти # зім'яти зімнеш (Ти) @ verb:futr:s:2
SFX A 'яти не 'яти # зім'яти зімне (Він) @ verb:futr:s:3
SFX A 'яти немо 'яти # зім'яти зімнемо (Ми) @ verb:futr:p:1
SFX A 'яти нете 'яти # зім'яти зімнете (Ви) @ verb:futr:p:2
SFX A 'яти нуть 'яти # зім'яти зімнуть (Вони) @ verb:futr:p:3
SFX A 'яти ни 'яти # зім'яти зімни (Ти) @ verb:impr:s:2
SFX A 'яти німо 'яти # зім'яти зімнімо (Ми) @ verb:impr:p:1
SFX A 'яти ніть 'яти # зім'яти зімніть (Ви) @ verb:impr:p:2
SFX C Y 15
SFX C ити 0 [вжчшщбмпр]ити # бентежити бентеж (Ти) @ verb:impr:s:2
SFX C ити ь [дтзснл]ити # заходити заходь (Ти) @ verb:impr:s:2
SFX C ити мо [вжчшщбмпр]ити # бентежити бентежмо (Ми) @ verb:impr:p:1
SFX C ити ьмо [дтзснл]ити # заходити заходьмо (Ми) @ verb:impr:p:1
SFX C ити те [вжчшщбмпр]ити # бентежити бентежте (Ви) @ verb:impr:p:2
SFX C ити ьте [дтзснл]ити # проводити проводьте (Ви) @ verb:impr:p:2
SFX C іти ь діти # посидіти посидь (Ти) @ verb:impr:s:2
SFX C іти ьмо діти # посидіти посидьмо (Ми) @ verb:impr:p:1
SFX C іти ьте діти # посидіти посидьте (Ви) @ verb:impr:p:2
SFX C ути ь нути # кинути кинь (Ти) @ verb:impr:s:2
SFX C ути ьмо нути # кинути киньмо (Ми) @ verb:impr:p:1
SFX C ути ьте нути # кинути киньте (Ви) @ verb:impr:p:2
SFX C їти й їти # клеїти клей (Ти) @ verb:impr:s:2
SFX C їти ймо їти # клеїти клеймо (Ми) @ verb:impr:p:1
SFX C їти йте їти # клеїти клейте (Ви) @ verb:impr:p:2
SFX E Y 12
SFX E ити и ити # учити учи (Ти) @ verb:impr:s:2
SFX E ити імо ити # учити учімо (Ми) @ verb:impr:p:1
SFX E ити іть ити # учити учіть (Ви) @ verb:impr:p:2
SFX E іти и діти # сидіти сиди (Ти) @ verb:impr:s:2
SFX E іти імо діти # сидіти сидімо (Ми) @ verb:impr:p:1
SFX E іти іть діти # сидіти сидіть (Ви) @ verb:impr:p:2
SFX E ути и нути # кашлянути кашляни (Ти) @ verb:impr:s:2
SFX E ути імо нути # кашлянути кашлянімо (Ми) @ verb:impr:p:1
SFX E ути іть нути # кашлянути кашляніть (Ви) @ verb:impr:p:2
SFX E їти ї їти # напоїти напої (Ти) @ verb:impr:s:2
SFX E їти їмо їти # напоїти напоїмо (Ми) @ verb:impr:p:1
SFX E їти їть їти # напоїти напоїть (Ви) @ verb:impr:p:2
SFX G Y 6
SFX G 0 му ти # абонувати абонуватиму (Я) @ verb:futr:s:1
SFX G 0 меш ти # абонувати абонуватимеш (Ти) @ verb:futr:s:2
SFX G 0 ме ти # абонувати абонуватиме (Він) @ verb:futr:s:3
SFX G 0 мемо ти # абонувати абонуватимемо (Ми) @ verb:futr:p:1
SFX G 0 мете ти # абонувати абонуватимете (Ви) @ verb:futr:p:2
SFX G 0 муть ти # абонувати абонуватимуть (Вони) @ verb:futr:p:3
SFX I Y 83
SFX I ти ла ти # вбивати вбивала (Вона) @ verb:past:f
SFX I ти ло ти # вбивати вбивало (Воно) @ verb:past:n
SFX I ти ли ти # вбивати вбивали (Вони) @ verb:past:p
SFX I ти в [аиіуя]ти # вбивати вбивав (Я, Ти, Він) @ verb:past:m
SFX I ти вши [аиіуя]ти # вбивати вбивавши @ adp:perf
SFX I яти ю [аяіо]яти # віяти вію (Я) @ verb:pres:s:1
SFX I ти ю [илнрцд]яти # ганяти ганяю (Я) @ verb:pres:s:1
SFX I ти ю [аіу]ти # вбивати вбиваю (Я) @ verb:pres:s:1
SFX I яти їш ояти # стояти стоїш (Ти) @ verb:pres:s:2
SFX I яти єш [аяі]яти # віяти вієш (Ти) @ verb:pres:s:2
SFX I ти єш [илнрцд]яти # ганяти ганяєш (Ти) @ verb:pres:s:2
SFX I ти єш [аіу]ти # вбивати вбиваєш (Ти) @ verb:pres:s:2
SFX I яти їть ояти # стояти стоїть (Він) @ verb:pres:s:3
SFX I яти є [аяі]яти # віяти віє (Він) @ verb:pres:s:3
SFX I ти є [илнрцд]яти # ганяти ганяє (Він) @ verb:pres:s:3
SFX I ти є [аіу]ти # вбивати вбиває (Він) @ verb:pres:s:3
SFX I яти їмо ояти # стояти стоїмо (Ми) @ verb:pres:p:1
SFX I яти ємо [аяі]яти # віяти віємо (Ми) @ verb:pres:p:1
SFX I ти ємо [илнрцд]яти # ганяти ганяємо (Ми) @ verb:pres:p:1
SFX I ти ємо [аіу]ти # вбивати вбиваємо (Ми) @ verb:pres:p:1
SFX I яти їте ояти # стояти стоїте (Ви) @ verb:pres:p:2
SFX I яти єте [аяі]яти # віяти вієте (Ви) @ verb:pres:p:2
SFX I ти єте [илнрцд]яти # ганяти ганяєте (Ви) @ verb:pres:p:2
SFX I ти єте [аіу]ти # вбивати вбиваєте (Ви) @ verb:pres:p:2
SFX I яти ять ояти # стояти стоять (Вони) @ verb:pres:p:3
SFX I яти ють [аяі]яти # віяти віють (Вони) @ verb:pres:p:3
SFX I ти ють [илнрцд]яти # ганяти ганяють (Вони) @ verb:pres:p:3
SFX I ти ють [аіу]ти # вбивати вбивають (Вони) @ verb:pres:p:3
SFX I ояти ій ояти # стояти стій (Ти) @ verb:impr:s:2
SFX I яти й [аяі]яти # віяти вій (Ти) @ verb:impr:s:2
SFX I ти й [илнрцд]яти # ганяти ганяй (Ти) @ verb:impr:s:2
SFX I ти й [аіу]ти # вбивати вбивай (Ти) @ verb:impr:s:2
SFX I ояти іймо ояти # стояти стіймо (Ми) @ verb:impr:p:1
SFX I яти ймо [аяі]яти # віяти віймо (Ми) @ verb:impr:p:1
SFX I ти ймо [илнрцд]яти # ганяти ганяймо (Ми) @ verb:impr:p:1
SFX I ти ймо [аіу]ти # вбивати вбиваймо (Ми) @ verb:impr:p:1
SFX I ояти ійте ояти # стояти стійте (Ви) @ verb:impr:p:2
SFX I яти йте [аяі]яти # віяти війте (Ви) @ verb:impr:p:2
SFX I ти йте [илнрцд]яти # ганяти ганяйте (Ви) @ verb:impr:p:2
SFX I ти йте [аіу]ти # вбивати вбивайте (Ви) @ verb:impr:p:2
SFX I ити 'ю [бвп]ити # бити б'ю (Я) @ verb:pres:s:1
SFX I ити 'єш [бвп]ити # бити б'єш (Ти) @ verb:pres:s:2
SFX I ити 'є [бвп]ити # бити б'є (Він) @ verb:pres:s:3
SFX I ити 'ємо [бвп]ити # бити б'ємо (Ми) @ verb:pres:p:1
SFX I ити 'єте [бвп]ити # бити б'єте (Ви) @ verb:pres:p:2
SFX I ити 'ють [бвп]ити # бити б'ють (Вони) @ verb:pres:p:3
SFX I ити ию [врмнш]ити # рити рию (Я) @ verb:pres:s:1
SFX I ити иєш [врмнш]ити # рити риєш (Ти) @ verb:pres:s:2
SFX I ити иє [врмнш]ити # рити риє (Він) @ verb:pres:s:3
SFX I ити иємо [врмнш]ити # рити риємо (Ми) @ verb:pres:p:1
SFX I ити иєте [врмнш]ити # рити риєте (Ви) @ verb:pres:p:2
SFX I ити иють [врмнш]ити # рити риють (Вони) @ verb:pres:p:3
SFX I ити лю лити # лити ллю (Я) @ verb:pres:s:1
SFX I ити лєш лити # лити ллєш (Ти) @ verb:pres:s:2
SFX I ити лє лити # лити ллє (Він) @ verb:pres:s:3
SFX I ити лємо лити # лити ллємо (Ми) @ verb:pres:p:1
SFX I ити лєте лити # лити ллєте (Ви) @ verb:pres:p:2
SFX I ити лють лити # лити ллють (Вони) @ verb:pres:p:3
SFX I ти ву жити # жити живу (Я) @ verb:pres:s:1
SFX I ти веш жити # жити живеш (Ти) @ verb:pres:s:2
SFX I ти ве жити # жити живе (Він) @ verb:pres:s:3
SFX I ти вемо жити # жити живемо (Ми) @ verb:pres:p:1
SFX I ти вете жити # жити живете (Ви) @ verb:pres:p:2
SFX I ти вуть жити # жити живуть (Вони) @ verb:pres:p:3
SFX I ти й [^ж]ити # бити бий (Ти) @ verb:impr:s:2
SFX I ти ймо [^ж]ити # бити биймо (Ми) @ verb:impr:p:1
SFX I ти йте [^ж]ити # бити бийте (Ви) @ verb:impr:p:2
SFX I ти ви [ж]ити # жити живи (Ти) @ verb:impr:s:2
SFX I ти вімо [ж]ити # жити живімо (Ми) @ verb:impr:p:1
SFX I ти віть [ж]ити # жити живіть (Ви) @ verb:impr:p:2
SFX I ти у сти # пасти пасу (Я) @ verb:pres:s:1
SFX I ти еш сти # пасти пасеш (Ти) @ verb:pres:s:2
SFX I ти е сти # пасти пасе (Він) @ verb:pres:s:3
SFX I ти емо сти # пасти пасемо (Ми) @ verb:pres:p:1
SFX I ти ете сти # пасти пасете (Ви) @ verb:pres:p:2
SFX I ти уть сти # пасти пасуть (Вони) @ verb:pres:p:3
SFX I сти с [ая]сти # пасти пас (Я, Ти, Він) @ verb:past:m
SFX I ести іс ести # нести ніс (Я, Ти, Він) @ verb:past:m
SFX I сти сши [ая]сти # пасти пасши @ adp:perf
SFX I ести ісши ести # нести нісши @ adp:perf
SFX I ти и сти # пасти паси (Ти) @ verb:impr:s:2
SFX I ти імо сти # пасти пасімо (Ми) @ verb:impr:p:1
SFX I ти іть сти # пасти пасіте (Ви) @ verb:impr:p:2
SFX K Y 163
SFX K ти ла [^ус]ти # гнати гнала (Вона) @ verb:past:s:f
SFX K ти ло [^ус]ти # гнати гнало (Воно) @ verb:past:s:n
SFX K ти ли [^ус]ти # гнати гнали (Вони) @ verb:past:p
SFX K ти в [аеиіоя]ти # зігнати зігнав (Я, Ти, Він) @ verb:past:m
SFX K ти вши [аеиіоя]ти # зігнати зігнавши @ adp:perf
SFX K нути ла нути # змерзнути змерзла (Вона) @ verb:past:f
SFX K нути ло нути # змерзнути змерзло (Воно) @ verb:past:n
SFX K нути ли нути # змерзнути змерзли (Вони) @ verb:past:p
SFX K нути 0 [^о]нути # змерзнути змерз (Я, Ти, Він) @ verb:past:m
SFX K нути в онути # прохолонути прохолов (Я, Ти, Він) @ verb:past:m
SFX K нути нув онути # прохолонути прохолонув (Я, Ти, Він) @ verb:past:m
SFX K нути ши [^о]нути # змерзнути змерзши @ adp:perf
SFX K нути вши онути # охолонути охоловши @ adp:perf
SFX K нути нувши онути # охолонути охолонувши @ adp:perf
SFX K нути нула нути # змерзнути змерзнула (Вона) @ verb:past:f
SFX K нути нуло нути # змерзнути змерзнуло (Воно) @ verb:past:n
SFX K нути нули нути # змерзнути змерзнули (Вони) @ verb:past:p
SFX K нути нув нути # змерзнути змерзнув (Я, Ти, Він) @ verb:past:m
SFX K нути нувши нути # змерзнути змерзнувши @ adp:perf
SFX K ути у нути # змерзнути змерзну (Я) @ verb:futr:s:1
SFX K ути еш нути # змерзнути змерзнеш (Ти) @ verb:futr:s:2
SFX K ути е нути # змерзнути змерзне (Він) @ verb:futr:s:3
SFX K ути емо нути # змерзнути змерзнемо (Ми) @ verb:futr:p:1
SFX K ути ете нути # змерзнути змерзнете (Ви) @ verb:futr:p:2
SFX K ути уть нути # змерзнути змерзнуть (Вони) @ verb:futr:p:3
SFX K ти ну чити # відпочити відпочину (Я) @ verb:futr:s:1
SFX K ти неш чити # відпочити відпочинеш (Ти) @ verb:futr:s:2
SFX K ти не чити # відпочити відпочине (Він) @ verb:futr:s:3
SFX K ти немо чити # відпочити відпочинемо (Ми) @ verb:futr:p:1
SFX K ти нете чити # відпочити відпочинете (Ви) @ verb:futr:p:2
SFX K ти нуть чити # відпочити відпочинуть (Вони) @ verb:futr:p:3
SFX K ігнати жену ігнати # відігнати віджену (Я) @ verb:futr:s:1
SFX K ігнати женеш ігнати # відігнати відженеш (Ти) @ verb:futr:s:2
SFX K ігнати жене ігнати # відігнати віджене (Він) @ verb:futr:s:3
SFX K ігнати женемо ігнати # відігнати відженемо (Ми) @ verb:futr:p:1
SFX K ігнати женете ігнати # відігнати відженете (Ви) @ verb:futr:p:2
SFX K ігнати женуть ігнати # відігнати відженуть (Вони) @ verb:futr:p:3
SFX K ігнати жени ігнати # відігнати віджени (Ти) @ verb:impr:s:2
SFX K ігнати женімо ігнати # відігнати відженімо (Ми) @ verb:impr:p:1
SFX K ігнати женіть ігнати # відігнати відженіть (Ви) @ verb:impr:p:2
SFX K іпрати перу іпрати # відіпрати відперу (Я) @ verb:futr:s:1
SFX K іпрати переш іпрати # відіпрати відпереш (Ти) @ verb:futr:s:2
SFX K іпрати пере іпрати # відіпрати відпере (Він) @ verb:futr:s:3
SFX K іпрати перемо іпрати # відіпрати відперемо (Ми) @ verb:futr:p:1
SFX K іпрати перете іпрати # відіпрати відперете (Ви) @ verb:futr:p:2
SFX K іпрати перуть іпрати # відіпрати відперуть (Вони) @ verb:futr:p:3
SFX K іпрати пери іпрати # відіпрати відпери (Ти) @ verb:impr:s:2
SFX K іпрати перімо іпрати # відіпрати відперімо (Ми) @ verb:impr:p:1
SFX K іпрати періть іпрати # відіпрати відперіть (Ви) @ verb:impr:p:2
SFX K ібрати беру ібрати # відібрати відберу (Я) @ verb:futr:s:1
SFX K ібрати береш ібрати # відібрати відбереш (Ти) @ verb:futr:s:2
SFX K ібрати бере ібрати # відібрати відбере (Він) @ verb:futr:s:3
SFX K ібрати беремо ібрати # відібрати відберемо (Ми) @ verb:futr:p:1
SFX K ібрати берете ібрати # відібрати відберете (Ви) @ verb:futr:p:2
SFX K ібрати беруть ібрати # відібрати відберуть (Вони) @ verb:futr:p:3
SFX K ібрати бери ібрати # відібрати відбери (Ти) @ verb:impr:s:2
SFX K ібрати берімо ібрати # відібрати відберімо (Ми) @ verb:impr:p:1
SFX K ібрати беріть ібрати # відібрати відберіть (Ви) @ verb:impr:p:2
SFX K ідрати деру ідрати # відідрати віддеру (Я) @ verb:futr:s:1
SFX K ідрати дереш ідрати # відідрати віддереш (Ти) @ verb:futr:s:2
SFX K ідрати дере ідрати # відідрати віддере (Він) @ verb:futr:s:3
SFX K ідрати деремо ідрати # відідрати віддеремо (Ми) @ verb:futr:p:1
SFX K ідрати дерете ідрати # відідрати віддерете (Ви) @ verb:futr:p:2
SFX K ідрати деруть ідрати # відідрати віддеруть (Вони) @ verb:futr:p:3
SFX K ідрати дери ідрати # відідрати віддери (Ти) @ verb:impr:s:2
SFX K ідрати дерімо ідрати # відідрати віддерімо (Ми) @ verb:impr:p:1
SFX K ідрати деріть ідрати # відідрати віддеріть (Ви) @ verb:impr:p:2
SFX K бити іб'ю [бдз]бити # надбити надіб'ю (Я) @ verb:futr:s:1
SFX K бити іб'єш [бдз]бити # надбити надіб'єш (Ти) @ verb:futr:s:2
SFX K бити іб'є [бдз]бити # надбити надіб'є (Він) @ verb:futr:s:3
SFX K бити іб'ємо [бдз]бити # надбити надіб'ємо (Ми) @ verb:futr:p:1
SFX K бити іб'єте [бдз]бити # надбити надіб'єте (Ви) @ verb:futr:p:2
SFX K бити іб'ють [бдз]бити # надбити надіб'ють (Вони) @ verb:futr:p:3
SFX K вити ів'ю [бдз]вити # надвити надів'ю (Я) @ verb:futr:s:1
SFX K вити ів'єш [бдз]вити # надвити надів'єш (Ти) @ verb:futr:s:2
SFX K вити ів'є [бдз]вити # надвити надів'є (Він) @ verb:futr:s:3
SFX K вити ів'ємо [бдз]вити # надвити надів'ємо (Ми) @ verb:futr:p:1
SFX K вити ів'єте [бдз]вити # надвити надів'єте (Ви) @ verb:futr:p:2
SFX K вити ів'ють [бдз]вити # надвити надів'ють (Вони) @ verb:futr:p:3
SFX K пити іп'ю [бдз]пити # надпити надіп'ю (Я) @ verb:futr:s:1
SFX K пити іп'єш [бдз]пити # надпити надіп'єш (Ти) @ verb:futr:s:2
SFX K пити іп'є [бдз]пити # надпити надіп'є (Він) @ verb:futr:s:3
SFX K пити іп'ємо [бдз]пити # надпити надіп'ємо (Ми) @ verb:futr:p:1
SFX K пити іп'єте [бдз]пити # надпити надіп'єте (Ви) @ verb:futr:p:2
SFX K пити іп'ють [бдз]пити # надпити надіп'ють (Вони) @ verb:futr:p:3
SFX K лити іллю [бвдз]лити # надлити наділлю (Я) @ verb:futr:s:1
SFX K лити іллєш [бвдз]лити # надлити наділлєш (Ти) @ verb:futr:s:2
SFX K лити іллє [бвдз]лити # надлити наділлє (Він) @ verb:futr:s:3
SFX K лити іллємо [бвдз]лити # надлити наділлємо (Ми) @ verb:futr:p:1
SFX K лити іллєте [бвдз]лити # надлити наділлєте (Ви) @ verb:futr:p:2
SFX K лити іллють [бвдз]лити # надлити наділлють (Вони) @ verb:futr:p:3
SFX K ти й [бвлп]ити # надбити надбий (Ти) @ verb:impr:s:2
SFX K ти ймо [бвлп]ити # надбити надбиймо (Ми) @ verb:impr:p:1
SFX K ти йте [бвлп]ити # надбити надбийте (Ви) @ verb:impr:p:2
SFX K ти м дати # дати дам (Я) @ verb:futr:s:1
SFX K ти си дати # дати даси (Ти) @ verb:futr:s:2
SFX K ти сть дати # дати дасть (Він) @ verb:futr:s:3
SFX K ти мо дати # дати дамо (Ми) @ verb:futr:p:1
SFX K ти сте дати # дати дасте (Ви) @ verb:futr:p:2
SFX K ти дуть дати # дати дадуть (Вони) @ verb:futr:p:3
SFX K ти й дати # дати дай (Ти) @ verb:impr:s:2
SFX K ти ймо дати # дати даймо (Ми) @ verb:impr:p:1
SFX K ти йте дати # дати дайте (Ви) @ verb:impr:p:2
SFX K ати му жати # жати жму (Я) @ verb:pres:s:1
SFX K ати меш жати # жати жмеш (Ти) @ verb:pres:s:2
SFX K ати ме жати # жати жме (Він) @ verb:pres:s:3
SFX K ати мемо жати # жати жмемо (Ми) @ verb:pres:p:1
SFX K ати мете жати # жати жмете (Ви) @ verb:pres:p:2
SFX K ати муть жати # жати жмуть (Вони) @ verb:pres:p:3
SFX K ати ми жати # жати жми (Ти) @ verb:impr:s:2
SFX K ати мімо жати # жати жмімо (Ми) @ verb:impr:p:1
SFX K ати міть жати # жати жміть (Ви) @ verb:impr:p:2
SFX K ти ну діти # подіти подіну (Я) @ verb:futr:s:1
SFX K ти неш діти # подіти подінеш (Ти) @ verb:futr:s:2
SFX K ти не діти # подіти подіне (Він) @ verb:futr:s:3
SFX K ти немо діти # подіти подінемо (Ми) @ verb:futr:p:1
SFX K ти нете діти # подіти подінете (Ви) @ verb:futr:p:2
SFX K ти нуть діти # подіти подінуть (Вони) @ verb:futr:p:3
SFX K ти нь .діти # одіти одінь (Ти) @ verb:impr:s:2
SFX K ти ньмо .діти # одіти одіньмо (Ми) @ verb:impr:p:1
SFX K ти ньте .діти # одіти одіньте (Ви) @ verb:impr:p:2
SFX K ти ну стати # постати постану (Я) @ verb:futr:s:1
SFX K ти неш стати # постати постанеш (Ти) @ verb:futr:s:2
SFX K ти не стати # постати постане (Він) @ verb:futr:s:3
SFX K ти немо стати # постати постанемо (Ми) @ verb:futr:p:1
SFX K ти нете стати # постати постанете (Ви) @ verb:futr:p:2
SFX K ти нуть стати # постати постануть (Вони) @ verb:futr:p:3
SFX K ти нь стати # постати постань (Ти) @ verb:impr:s:2
SFX K ти ньмо стати # постати постаньмо (Ми) @ verb:impr:p:1
SFX K ти ньте стати # постати постаньте (Ви) @ verb:impr:p:2
SFX K ати у ссати # поссати поссу (Я) @ verb:futr:s:1
SFX K ати еш ссати # поссати поссеш (Ти) @ verb:futr:s:2
SFX K ати е ссати # поссати поссе (Він) @ verb:futr:s:3
SFX K ати емо ссати # поссати поссемо (Ми) @ verb:futr:p:1
SFX K ати ете ссати # поссати поссете (Ви) @ verb:futr:p:2
SFX K ати уть ссати # поссати поссуть (Вони) @ verb:futr:p:3
SFX K ати и ссати # поссати посси (Ти) @ verb:impr:s:2
SFX K ати імо ссати # поссати поссімо (Ми) @ verb:impr:p:1
SFX K ати ім ссати # поссати поссім (Ми) @ verb:impr:p:1
SFX K ати іть ссати # поссати поссіть (Ви) @ verb:impr:p:2
SFX K олоти елю олоти # молоти мелю (Я) @ verb:pres:s:1
SFX K олоти елеш олоти # молоти мелеш (Ти) @ verb:pres:s:2
SFX K олоти еле олоти # молоти меле (Він) @ verb:pres:s:3
SFX K олоти елемо олоти # молоти мелемо (Ми) @ verb:pres:p:1
SFX K олоти елете олоти # молоти мелете (Ви) @ verb:pres:p:2
SFX K олоти елють олоти # молоти мелють (Вони) @ verb:pres:p:3
SFX K олоти ели олоти # молоти мели (Ти) @ verb:impr:s:2
SFX K олоти елімо олоти # молоти мелімо (Ми) @ verb:impr:p:1
SFX K олоти еліть олоти # молоти меліть (Ви) @ verb:impr:p:2
SFX K істи яду істи # сісти сяду (Я) @ verb:futr:s:1
SFX K істи ядеш істи # сісти сядеш (Ти) @ verb:futr:s:2
SFX K істи яде істи # сісти сяде (Він) @ verb:futr:s:3
SFX K істи ядемо істи # сісти сядемо (Ми) @ verb:futr:p:1
SFX K істи ядете істи # сісти сядете (Ви) @ verb:futr:p:2
SFX K істи ядуть істи # сісти сядуть (Вони) @ verb:futr:p:3
SFX K сти ла істи # сісти сіла (Вона) @ verb:past:p:f
SFX K сти ло істи # сісти сіло (Воно) @ verb:past:p:n
SFX K сти ли істи # сісти сіли (Вони) @ verb:past:p:p
SFX K сти в істи # сісти сів (Він) @ verb:past:p:m
SFX K сти вши істи # сісти сівши @ adp:perf
SFX K істи ядь істи # сісти сядь (Ти) @ verb:impr:s:2
SFX K істи ядьмо істи # сісти сядьмо (Ми) @ verb:impr:p:1
SFX K істи ядьте істи # сісти сядьте (Ви) @ verb:impr:p:2
SFX M Y 91
SFX M ти ла [^йіс]ти # ржати ржала (Вона) @ verb:past:f
SFX M ти ло [^йіс]ти # ржати ржало (Воно) @ verb:past:n
SFX M ти ли [^йіс]ти # ржати ржали (Вони) @ verb:past:m
SFX M ти в [аеиоу]ти # ржати ржав (Я, Ти, Він) @ verb:past:m
SFX M ти вши [аеиоу]ти # ржати ржавши @ adp:perf
SFX M ати у ржати # ржати ржу (Я) @ verb:pres:s:1
SFX M ати еш ржати # ржати ржеш (Ти) @ verb:pres:s:2
SFX M ати е ржати # ржати рже (Він) @ verb:pres:s:3
SFX M ати емо ржати # ржати ржемо (Ми) @ verb:pres:p:1
SFX M ати ете ржати # ржати ржете (Ви) @ verb:pres:p:2
SFX M ати уть ржати # ржати ржуть (Вони) @ verb:pres:p:3
SFX M ати и ржати # ржати ржи (Ти) @ verb:impr:s:2
SFX M ати імо ржати # ржати ржімо (Ми) @ verb:impr:p:1
SFX M ати іть ржати # ржати ржіть (Ви) @ verb:impr:p:2
SFX M жати іжму [^р]жати # жати жму (Я) @ verb:futr:s:1
SFX M жати іжмеш [^р]жати # жати жмеш (Ти) @ verb:futr:s:2
SFX M жати іжме [^р]жати # жати жме (Він) @ verb:futr:s:3
SFX M жати іжмемо [^р]жати # жати жмемо (Ми) @ verb:futr:p:1
SFX M жати іжмете [^р]жати # жати жмете (Ви) @ verb:futr:p:2
SFX M жати іжмуть [^р]жати # жати жмуть (Вони) @ verb:futr:p:3
SFX M жати іжми [^р]жати # жати жми (Ти) @ verb:impr:s:2
SFX M жати іжмімо [^р]жати # жати жмімо (Ми) @ verb:impr:p:1
SFX M жати іжміть [^р]жати # жати жміть (Ви) @ verb:impr:p:2
SFX M ти ду [йі]ти # йти йду (Я) @ verb:pres:s:1
SFX M ти деш [йі]ти # йти йдеш (Ти) @ verb:pres:s:2
SFX M ти де [йі]ти # йти йде (Він) @ verb:pres:s:3
SFX M ти демо [йі]ти # йти йдемо (Ми) @ verb:pres:p:1
SFX M ти дете [йі]ти # йти йдете (Ви) @ verb:pres:p:2
SFX M ти дуть [йі]ти # йти йдуть (Вони) @ verb:pres:p:3
SFX M ти шов [йі]ти # йти йшов (Я, Ти, Він) @ verb:past:m
SFX M ти шла [йі]ти # йти йшла (Вона) @ verb:past:f
SFX M ти шло [йі]ти # йти йшло (Воно) @ verb:past:n
SFX M ти шли [йі]ти # йти йшли (Вони) @ verb:past:p
SFX M ти шовши [йі]ти # йти йшовши @ adp:perf
SFX M ти ди [йі]ти # йти йди (Ти) @ verb:impr:s:2
SFX M ти дімо [йі]ти # йти йдімо (Ми) @ verb:impr:p:1
SFX M ти діть [йі]ти # йти йдіть (Ви) @ verb:impr:p:2
SFX M хати ду хати # їхати їду (Я) @ verb:pres:s:1
SFX M хати деш хати # їхати їдеш (Ти) @ verb:pres:s:2
SFX M хати де хати # їхати їде (Він) @ verb:pres:s:3
SFX M хати демо хати # їхати їдемо (Ми) @ verb:pres:p:1
SFX M хати дете хати # їхати їдете (Ви) @ verb:pres:p:2
SFX M хати дуть хати # їхати їдуть (Вони) @ verb:pres:p:3
SFX M хати дь хати # їхати їдь (Ти) @ verb:impr:s:2
SFX M хати дьмо хати # їхати їдьмо (Ми) @ verb:impr:p:1
SFX M хати дьте хати # їхати їдьте (Ви) @ verb:impr:p:2
SFX M гнати жену гнати # гнати жену (Я) @ verb:pres:s:1
SFX M гнати женеш гнати # гнати женеш (Ти) @ verb:pres:s:2
SFX M гнати жене гнати # гнати жене (Він) @ verb:pres:s:3
SFX M гнати женемо гнати # гнати женемо (Ми) @ verb:pres:p:1
SFX M гнати женете гнати # гнати женете (Ви) @ verb:pres:p:2
SFX M гнати женуть гнати # гнати женуть (Вони) @ verb:pres:p:3
SFX M гнати жени гнати # гнати жени (Ти) @ verb:impr:s:2
SFX M гнати женімо гнати # гнати женімо (Ми) @ verb:impr:p:1
SFX M гнати женіть гнати # гнати женіть (Ви) @ verb:impr:p:2
SFX M ти ну [ия]гти # одягти одягну (Я) @ verb:futr:s:1
SFX M ти неш [ия]гти # одягти одягнеш (Ти) @ verb:futr:s:2
SFX M ти не [ия]гти # одягти одягне (Він) @ verb:futr:s:3
SFX M ти немо [ия]гти # одягти одягнемо (Ми) @ verb:futr:p:1
SFX M ти нете [ия]гти # одягти одягнете (Ви) @ verb:futr:p:2
SFX M ти нуть [ия]гти # одягти одягнуть (Вони) @ verb:futr:p:3
SFX M гти г [ия]гти # одягти одяг (Я, Ти, Він) @ verb:past:s:m
SFX M гти гши [ия]гти # одягти одягши @ adp:perf
SFX M ти ни ягти # одягти одягни (Ти) @ verb:impr:s:2
SFX M ти німо ягти # одягти одягнімо (Ми) @ verb:impr:p:1
SFX M ти ніть ягти # одягти одягніть (Ви) @ verb:impr:p:2
SFX M сти ду [еая]сти # вести веду (Я) @ verb:pres:s:1
SFX M сти деш [еая]сти # вести ведеш (Ти) @ verb:pres:s:2
SFX M сти де [еая]сти # вести веде (Він) @ verb:pres:s:3
SFX M сти демо [еая]сти # вести ведемо (Ми) @ verb:pres:p:1
SFX M сти дете [еая]сти # вести ведете (Ви) @ verb:pres:p:2
SFX M сти дуть [еая]сти # вести ведуть (Вони) @ verb:pres:p:3
SFX M сти ла сти # їсти їла (Вона) @ verb:past:s:f
SFX M сти ло сти # їсти їло (Воно) @ verb:past:s:n
SFX M сти ли сти # їсти їли (Вони) @ verb:past:s:p
SFX M сти в [^е]сти # їсти їв (Він) @ verb:past:s:m
SFX M ести ів ести # вести вів (Я, Ти, Він) @ verb:past:s:m
SFX M ести івши ести # вести вівши @ adp:perf
SFX M сти вши [^е]сти # їсти ївши @ adp:perf
SFX M сти ди [еая]сти # вести веди (Ти) @ verb:impr:s:2
SFX M сти дімо [еая]сти # вести веімо (Ми) @ verb:impr:p:1
SFX M сти діть [еая]сти # вести ведіть (Ви) @ verb:impr:p:2
SFX M сти м [ії]сти # їсти їм (Я) @ verb:pres:s:1
SFX M ти и [ії]сти # їсти їси (Ти) @ verb:pres:s:2
SFX M ти ть [ії]сти # їсти їсть (Він) @ verb:pres:s:3
SFX M сти мо [ії]сти # їсти їмо (Ми) @ verb:pres:p:1
SFX M ти те [ії]сти # їсти їсте (Ви) @ verb:pres:p:2
SFX M сти дять їсти # їсти їдять (Вони) @ verb:pres:p:3
SFX M сти ж їсти # їсти їж (Ти) @ verb:impr:s:2
SFX M сти жмо їсти # їсти їжмо (Ми) @ verb:impr:p:1
SFX M сти жте їсти # їсти їжте (Ви) @ verb:impr:p:2
SFX B Y 1287
SFX B 0 ся ти # ~ти ~тися @ verb:inf:rev
SFX B 0 сь ти # ~ти ~ись @ verb:inf:rev
SFX B ся сь тися # ~тися ~ись @ verb:inf:rev
SFX B ти лась [^с]ти # абонувати абонувалась (Вона) @ verb:rev:past:f
SFX B тися лась [^с]тися # абонуватися абонувалась (Вона) @ verb:rev:past:f
SFX B ти лася [^с]ти # абонувати абонувалася (Вона) @ verb:rev:past:f
SFX B тися лася [^с]тися # абонуватися абонувалася (Вона) @ verb:rev:past:f
SFX B ти лось [^с]ти # абонувати абонувалось (Воно) @ verb:rev:past:n
SFX B тися лось [^с]тися # абонуватися абонувалось (Воно) @ verb:rev:past:n
SFX B ти лося [^с]ти # абонувати абонувалося (Воно) @ verb:rev:past:n
SFX B тися лося [^с]тися # абонуватися абонувалося (Воно) @ verb:rev:past:n
SFX B ти лись [^с]ти # абонувати абонувались (Ми, Ви, Вони) @ verb:rev:past:p
SFX B тися лись [^с]тися # абонуватися абонувались (Ми, Ви, Вони) @ verb:rev:past:p
SFX B ти лися [^с]ти # абонувати абонувалися (Ми, Ви, Вони) @ verb:rev:past:p
SFX B тися лися [^с]тися # абонуватися абонувалися (Ми, Ви, Вони) @ verb:rev:past:p
SFX B ти всь [аеиіїоуя]ти # абонувати абонувавсь (Я, Ти, Він) @ verb:rev:past:m
SFX B тися всь [аеиіїоуя]тися # абонуватися абонувавсь (Я, Ти, Він) @ verb:rev:past:m
SFX B ти вся [аеиіїоуя]ти # абонувати абонувався (Я, Ти, Він) @ verb:rev:past:m
SFX B тися вся [аеиіїоуя]тися # абонуватися абонувався (Я, Ти, Він) @ verb:rev:past:m
SFX B ти вшись [аеиіїоуя]ти # абонувати абонувавшись @ adp:rev:perf
SFX B тися вшись [аеиіїоуя]тися # абонуватися абонувавшись @ adp:rev:perf
SFX B вати юсь [ауюя]вати # абонувати абонуюсь (Я) @ verb:rev:pres:s:1
SFX B ватися юсь [ауюя]ватися # абонуватися абонуюсь (Я) @ verb:rev:pres:s:1
SFX B вати юся [ауюя]вати # абонувати абонуюся (Я) @ verb:rev:pres:s:1
SFX B ватися юся [ауюя]ватися # абонуватися абонуюся (Я) @ verb:rev:pres:s:1
SFX B вати єшся [ауюя]вати # абонувати абонуєшся (Ти) @ verb:rev:pres:s:2
SFX B ватися єшся [ауюя]ватися # абонуватися абонуєшся (Ти) @ verb:rev:pres:s:2
SFX B вати ється [ауюя]вати # абонувати абонується (Він) @ verb:rev:pres:s:3
SFX B ватися ється [ауюя]ватися # абонуватися абонується (Він) @ verb:rev:pres:s:3
SFX B вати ємось [ауюя]вати # абонувати абонуємось (Ми) @ verb:rev:pres:p:1
SFX B ватися ємось [ауюя]ватися # абонуватися абонуємось (Ми) @ verb:rev:pres:p:1
SFX B вати ємося [ауюя]вати # абонувати абонуємося (Ми) @ verb:rev:pres:p:1
SFX B ватися ємося [ауюя]ватися # абонуватися абонуємося (Ми) @ verb:rev:pres:p:1
SFX B вати єтесь [ауюя]вати # абонувати абонуєтесь (Ви) @ verb:rev:pres:p:2
SFX B ватися єтесь [ауюя]ватися # абонуватися абонуєтесь (Ви) @ verb:rev:pres:p:2
SFX B вати єтеся [ауюя]вати # абонувати абонуєтеся (Ви) @ verb:rev:pres:p:2
SFX B ватися єтеся [ауюя]ватися # абонуватися абонуєтеся (Ви) @ verb:rev:pres:p:2
SFX B вати ються [ауюя]вати # абонувати абонуються (Вони) @ verb:rev:pres:p:3
SFX B ватися ються [ауюя]ватися # абонуватися абонуються (Вони) @ verb:rev:pres:p:3
SFX B вати йсь [ую]вати # абонувати абонуйсь (Ти) @ verb:rev:impr:s:2
SFX B ватися йсь [ую]ватися # абонуватися абонуйсь (Ти) @ verb:rev:impr:s:2
SFX B вати йся [ую]вати # абонувати абонуйся (Ти) @ verb:rev:impr:s:2
SFX B ватися йся [ую]ватися # абонуватися абонуйся (Ти) @ verb:rev:impr:s:2
SFX B вати ймось [ую]вати # абонувати абонуймось (Ми) @ verb:rev:impr:p:1
SFX B ватися ймось [ую]ватися # абонуватися абонуймось (Ми) @ verb:rev:impr:p:1
SFX B вати ймося [ую]вати # абонувати абонуймося (Ми) @ verb:rev:impr:p:1
SFX B ватися ймося [ую]ватися # абонуватися абонуймося (Ми) @ verb:rev:impr:p:1
SFX B вати йтесь [ую]вати # абонувати абонуйтесь (Ви) @ verb:rev:impr:p:2
SFX B ватися йтесь [ую]ватися # абонуватися абонуйтесь (Ви) @ verb:rev:impr:p:2
SFX B вати йтеся [ую]вати # абонувати абонуйтеся (Ви) @ verb:rev:impr:p:2
SFX B ватися йтеся [ую]ватися # абонуватися абонуйтеся (Ви) @ verb:rev:impr:p:2
SFX B ти йсь [ая]вати # ставати ставайсь (Ти) @ verb:rev:impr:s:2
SFX B тися йсь [ая]ватися # ставатися ставайсь (Ти) @ verb:rev:impr:s:2
SFX B ти йся [ая]вати # ставати ставайся (Ти) @ verb:rev:impr:s:2
SFX B тися йся [ая]ватися # ставатися ставайся (Ти) @ verb:rev:impr:s:2
SFX B ти ймось [ая]вати # ставати ставаймось (Ми) @ verb:rev:impr:p:1
SFX B тися ймось [ая]ватися # ставатися ставаймось (Ми) @ verb:rev:impr:p:1
SFX B ти ймося [ая]вати # ставати ставаймося (Ми) @ verb:rev:impr:p:1
SFX B тися ймося [ая]ватися # ставатися ставаймося (Ми) @ verb:rev:impr:p:1
SFX B ти йтесь [ая]вати # ставати ставайтесь (Ви) @ verb:rev:impr:p:2
SFX B тися йтесь [ая]ватися # ставатися ставайтесь (Ви) @ verb:rev:impr:p:2
SFX B ти йтеся [ая]вати # ставати ставайтеся (Ви) @ verb:rev:impr:p:2
SFX B тися йтеся [ая]ватися # ставатися ставайтеся (Ви) @ verb:rev:impr:p:2
SFX B ати усь [рз]вати # рвати рвусь (Я) @ verb:rev:pres:s:1
SFX B атися усь [рз]ватися # рватися рвусь (Я) @ verb:rev:pres:s:1
SFX B ати уся [рз]вати # рвати рвуся (Я) @ verb:rev:pres:s:1
SFX B атися уся [рз]ватися # рватися рвуся (Я) @ verb:rev:pres:s:1
SFX B ати ешся [рз]вати # рвати рвешся (Ти) @ verb:rev:pres:s:2
SFX B атися ешся [рз]ватися # рватися рвешся (Ти) @ verb:rev:pres:s:2
SFX B ати еться [рз]вати # рвати рветься (Він) @ verb:rev:pres:s:3
SFX B атися еться [рз]ватися # рватися рветься (Він) @ verb:rev:pres:s:3
SFX B ати емось [рз]вати # рвати рвемось (Ми) @ verb:rev:pres:p:1
SFX B атися емось [рз]ватися # рватися рвемось (Ми) @ verb:rev:pres:p:1
SFX B ати емося [рз]вати # рвати рвемося (Ми) @ verb:rev:pres:p:1
SFX B атися емося [рз]ватися # рватися рвемося (Ми) @ verb:rev:pres:p:1
SFX B ати етесь [рз]вати # рвати рветесь (Ви) @ verb:rev:pres:p:2
SFX B атися етесь [рз]ватися # рватися рветесь (Ви) @ verb:rev:pres:p:2
SFX B ати етеся [рз]вати # рвати рветеся (Ви) @ verb:rev:pres:p:2
SFX B атися етеся [рз]ватися # рватися рветеся (Ви) @ verb:rev:pres:p:2
SFX B ати уться [рз]вати # рвати рвуться (Вони) @ verb:rev:pres:p:3
SFX B атися уться [рз]ватися # рватися рвуться (Вони) @ verb:rev:pres:p:3
SFX B ати ись [рз]вати # рвати рвись (Ти) @ verb:rev:impr:s:2
SFX B атися ись [рз]ватися # рватися рвись (Ти) @ verb:rev:impr:s:2
SFX B ати ися [рз]вати # рвати рвися (Ти) @ verb:rev:impr:s:2
SFX B атися ися [рз]ватися # рватися рвися (Ти) @ verb:rev:impr:s:2
SFX B ати імось [рз]вати # рвати рвімось (Ми) @ verb:rev:impr:p:1
SFX B атися імось [рз]ватися # рватися рвімось (Ми) @ verb:rev:impr:p:1
SFX B ати імося [рз]вати # рвати рвімося (Ми) @ verb:rev:impr:p:1
SFX B атися імося [рз]ватися # рватися рвімося (Ми) @ verb:rev:impr:p:1
SFX B ати іться [рз]вати # рвати рвіться (Ви) @ verb:rev:impr:p:2
SFX B атися іться [рз]ватися # рватися рвіться (Ви) @ verb:rev:impr:p:2
SFX B зати жусь зати # казати кажусь (Я) @ verb:rev:pres:s:1
SFX B затися жусь затися # казатися кажусь (Я) @ verb:rev:pres:s:1
SFX B зати жуся зати # казати кажуся (Я) @ verb:rev:pres:s:1
SFX B затися жуся затися # казатися кажуся (Я) @ verb:rev:pres:s:1
SFX B зати жешся зати # казати кажешся (Ти) @ verb:rev:pres:s:2
SFX B затися жешся затися # казатися кажешся (Ти) @ verb:rev:pres:s:2
SFX B зати жеться зати # казати кажеться (Він) @ verb:rev:pres:s:3
SFX B затися жеться затися # казатися кажеться (Він) @ verb:rev:pres:s:3
SFX B зати жемось зати # казати кажемось (Ми) @ verb:rev:pres:p:1
SFX B затися жемось затися # казатися кажемось (Ми) @ verb:rev:pres:p:1
SFX B зати жемося зати # казати кажемося (Ми) @ verb:rev:pres:p:1
SFX B затися жемося затися # казатися кажемося (Ми) @ verb:rev:pres:p:1
SFX B зати жетесь зати # казати кажетесь (Ви) @ verb:rev:pres:p:2
SFX B затися жетесь затися # казатися кажетесь (Ви) @ verb:rev:pres:p:2
SFX B зати жетеся зати # казати кажетеся (Ви) @ verb:rev:pres:p:2
SFX B затися жетеся затися # казатися кажетеся (Ви) @ verb:rev:pres:p:2
SFX B зати жуться зати # казати кажуться (Вони) @ verb:rev:pres:p:3
SFX B затися жуться затися # казатися кажуться (Вони) @ verb:rev:pres:p:3
SFX B зати жся ізати # різати ріжся (Ти) @ verb:rev:impr:s:2
SFX B затися жся ізатися # різатися ріжся (Ти) @ verb:rev:impr:s:2
SFX B зати жся мазати # мазати мажся (Ти) @ verb:rev:impr:s:2
SFX B затися жся мазатися # мазатися мажся (Ти) @ verb:rev:impr:s:2
SFX B зати жись казати # казати кажись (Ти) @ verb:rev:impr:s:2
SFX B затися жись казатися # казатися кажись (Ти) @ verb:rev:impr:s:2
SFX B зати жися казати # казати кажися (Ти) @ verb:rev:impr:s:2
SFX B затися жися казатися # казатися кажися (Ти) @ verb:rev:impr:s:2
SFX B зати жись [еия]зати # лизати лижись (Ти) @ verb:rev:impr:s:2
SFX B затися жись [еия]затися # лизатися лижись (Ти) @ verb:rev:impr:s:2
SFX B зати жися [еия]зати # лизати лижися (Ти) @ verb:rev:impr:s:2
SFX B затися жися [еия]затися # лизатися лижися (Ти) @ verb:rev:impr:s:2
SFX B зати жмось ізати # відрізати відріжмось (Ми) @ verb:rev:impr:p:1
SFX B затися жмось ізатися # відрізатися відріжмось (Ми) @ verb:rev:impr:p:1
SFX B зати жмося ізати # відрізати відріжмося (Ми) @ verb:rev:impr:p:1
SFX B затися жмося ізатися # відрізатися відріжмося (Ми) @ verb:rev:impr:p:1
SFX B зати жмось мазати # мазати мажмось (Ми) @ verb:rev:impr:p:1
SFX B затися жмось мазатися # мазатися мажмось (Ми) @ verb:rev:impr:p:1
SFX B зати жмося мазати # мазати мажмося (Ми) @ verb:rev:impr:p:1
SFX B затися жмося мазатися # мазатися мажмося (Ми) @ verb:rev:impr:p:1
SFX B зати жімось казати # казати кажімось (Ми) @ verb:rev:impr:p:1
SFX B затися жімось казатися # казатися кажімось (Ми) @ verb:rev:impr:p:1
SFX B зати жімося казати # казати кажімося (Ми) @ verb:rev:impr:p:1
SFX B затися жімося казатися # казатися кажімося (Ми) @ verb:rev:impr:p:1
SFX B зати жімось [еия]зати # лизати лижімось (Ми) @ verb:rev:impr:p:1
SFX B затися жімось [еия]затися # лизатися лижімось (Ми) @ verb:rev:impr:p:1
SFX B зати жімося [еия]зати # лизати лижімося (Ми) @ verb:rev:impr:p:1
SFX B затися жімося [еия]затися # лизатися лижімося (Ми) @ verb:rev:impr:p:1
SFX B зати жтесь ізати # відрізати відріжтесь (Ви) @ verb:rev:impr:p:2
SFX B затися жтесь ізатися # відрізатися відріжтесь (Ви) @ verb:rev:impr:p:2
SFX B зати жтеся ізати # відрізати відріжтеся (Ви) @ verb:rev:impr:p:2
SFX B затися жтеся ізатися # відрізатися відріжтеся (Ви) @ verb:rev:impr:p:2
SFX B зати жтесь мазати # мазати мажтесь (Ви) @ verb:rev:impr:p:2
SFX B затися жтесь мазатися # мазатися мажтесь (Ви) @ verb:rev:impr:p:2
SFX B зати жтеся мазати # мазати мажтеся (Ви) @ verb:rev:impr:p:2
SFX B затися жтеся мазатися # мазатися мажтеся (Ви) @ verb:rev:impr:p:2
SFX B зати жіться казати # казати кажіться (Ви) @ verb:rev:impr:p:2
SFX B затися жіться казатися # казатися кажіться (Ви) @ verb:rev:impr:p:2
SFX B зати жіться [еия]зати # лизати лижіться (Ви) @ verb:rev:impr:p:2
SFX B затися жіться [еия]затися # лизатися лижіться (Ви) @ verb:rev:impr:p:2
SFX B ати усь [днжщ]ати # блищати блищусь (Я) @ verb:rev:pres:s:1
SFX B атися усь [днжщ]атися # блищатися блищусь (Я) @ verb:rev:pres:s:1
SFX B ати уся [днжщ]ати # блищати блищуся (Я) @ verb:rev:pres:s:1
SFX B атися уся [днжщ]атися # блищатися блищуся (Я) @ verb:rev:pres:s:1
SFX B ати усь [^ао]чати # деренчати деренчусь (Я) @ verb:rev:pres:s:1
SFX B атися усь [^ао]чатися # деренчатися деренчусь (Я) @ verb:rev:pres:s:1
SFX B ати уся [^ао]чати # деренчати деренчуся (Я) @ verb:rev:pres:s:1
SFX B атися уся [^ао]чатися # деренчатися деренчуся (Я) @ verb:rev:pres:s:1
SFX B ати нусь [ао]чати # зачати зачнусь (Я) @ verb:rev:pres:s:1
SFX B атися нусь [ао]чатися # зачатися зачнусь (Я) @ verb:rev:pres:s:1
SFX B ати нуся [ао]чати # зачати зачнуся (Я) @ verb:rev:pres:s:1
SFX B атися нуся [ао]чатися # зачатися зачнуся (Я) @ verb:rev:pres:s:1
SFX B тати чусь [^с]тати # шептати шепчусь (Я) @ verb:rev:pres:s:1
SFX B татися чусь [^с]татися # шептатися шепчусь (Я) @ verb:rev:pres:s:1
SFX B тати чуся [^с]тати # шептати шепчуся (Я) @ verb:rev:pres:s:1
SFX B татися чуся [^с]татися # шептатися шепчуся (Я) @ verb:rev:pres:s:1
SFX B кати чусь [^с]кати # плакати плачусь (Я) @ verb:rev:pres:s:1
SFX B катися чусь [^с]катися # плакатися плачусь (Я) @ verb:rev:pres:s:1
SFX B кати чуся [^с]кати # плакати плачуся (Я) @ verb:rev:pres:s:1
SFX B катися чуся [^с]катися # плакатися плачуся (Я) @ verb:rev:pres:s:1
SFX B сати шусь сати # писати пишусь (Я) @ verb:rev:pres:s:1
SFX B сатися шусь сатися # писатися пишусь (Я) @ verb:rev:pres:s:1
SFX B сати шуся сати # писати пишуся (Я) @ verb:rev:pres:s:1
SFX B сатися шуся сатися # писатися пишуся (Я) @ verb:rev:pres:s:1
SFX B хати шусь хати # брехати брешусь (Я) @ verb:rev:pres:s:1
SFX B хатися шусь хатися # брехатися брешусь (Я) @ verb:rev:pres:s:1
SFX B хати шуся хати # брехати брешуся (Я) @ verb:rev:pres:s:1
SFX B хатися шуся хатися # брехатися брешуся (Я) @ verb:rev:pres:s:1
SFX B стати щусь стати # свистати свищусь (Я) @ verb:rev:pres:s:1
SFX B статися щусь статися # свистатися свищусь (Я) @ verb:rev:pres:s:1
SFX B стати щуся стати # свистати свищуся (Я) @ verb:rev:pres:s:1
SFX B статися щуся статися # свистатися свищуся (Я) @ verb:rev:pres:s:1
SFX B скати щусь скати # плескати плещусь (Я) @ verb:rev:pres:s:1
SFX B скатися щусь скатися # плескатися плещусь (Я) @ verb:rev:pres:s:1
SFX B скати щуся скати # плескати плещуся (Я) @ verb:rev:pres:s:1
SFX B скатися щуся скатися # плескатися плещуся (Я) @ verb:rev:pres:s:1
SFX B слати шлюсь слати # послати пошлюсь (Я) @ verb:rev:pres:s:1
SFX B слатися шлюсь слатися # послатися пошлюсь (Я) @ verb:rev:pres:s:1
SFX B слати шлюся слати # послати пошлюся (Я) @ verb:rev:pres:s:1
SFX B слатися шлюся слатися # послатися пошлюся (Я) @ verb:rev:pres:s:1
SFX B ати люсь пати # сипати сиплюсь (Я) @ verb:rev:pres:s:1
SFX B атися люсь патися # сипатися сиплюсь (Я) @ verb:rev:pres:s:1
SFX B ати люся пати # сипати сиплюся (Я) @ verb:rev:pres:s:1
SFX B атися люся патися # сипатися сиплюся (Я) @ verb:rev:pres:s:1
SFX B ати юсь орати # орати орюсь (Я) @ verb:rev:pres:s:1
SFX B атися юсь оратися # оратися орюсь (Я) @ verb:rev:pres:s:1
SFX B ати юся орати # орати орюся (Я) @ verb:rev:pres:s:1
SFX B атися юся оратися # оратися орюся (Я) @ verb:rev:pres:s:1
SFX B рати ерусь [бдп]рати # брати берусь (Я) @ verb:rev:pres:s:1
SFX B ратися ерусь [бдп]ратися # братися берусь (Я) @ verb:rev:pres:s:1
SFX B рати еруся [бдп]рати # брати беруся (Я) @ verb:rev:pres:s:1
SFX B ратися еруся [бдп]ратися # братися беруся (Я) @ verb:rev:pres:s:1
SFX B ати ешся [дн]ати # стогнати стогнешся (Ти) @ verb:rev:pres:s:2
SFX B атися ешся [дн]атися # стогнатися стогнешся (Ти) @ verb:rev:pres:s:2
SFX B ати ишся [жщ]ати # блищати блищишся (Ти) @ verb:rev:pres:s:2
SFX B атися ишся [жщ]атися # блищатися блищишся (Ти) @ verb:rev:pres:s:2
SFX B ати ишся [^оа]чати # бряжчати бряжчишся (Ти) @ verb:rev:pres:s:2
SFX B атися ишся [^оа]чатися # бряжчатися бряжчишся (Ти) @ verb:rev:pres:s:2
SFX B ати нешся [ао]чати # зачати зачнешся (Ти) @ verb:rev:pres:s:2
SFX B атися нешся [ао]чатися # зачатися зачнешся (Ти) @ verb:rev:pres:s:2
SFX B тати чешся [^с]тати # шептати шепчешся (Ти) @ verb:rev:pres:s:2
SFX B татися чешся [^с]татися # шептатися шепчешся (Ти) @ verb:rev:pres:s:2
SFX B кати чешся [^с]кати # плакати плачешся (Ти) @ verb:rev:pres:s:2
SFX B катися чешся [^с]катися # плакатися плачешся (Ти) @ verb:rev:pres:s:2
SFX B сати шешся сати # писати пишешся (Ти) @ verb:rev:pres:s:2
SFX B сатися шешся сатися # писатися пишешся (Ти) @ verb:rev:pres:s:2
SFX B хати шешся хати # брехати брешешся (Ти) @ verb:rev:pres:s:2
SFX B хатися шешся хатися # брехатися брешешся (Ти) @ verb:rev:pres:s:2
SFX B стати щешся стати # свистати свищешся (Ти) @ verb:rev:pres:s:2
SFX B статися щешся статися # свистатися свищешся (Ти) @ verb:rev:pres:s:2
SFX B скати щешся скати # плескати плещешся (Ти) @ verb:rev:pres:s:2
SFX B скатися щешся скатися # плескатися плещешся (Ти) @ verb:rev:pres:s:2
SFX B слати шлешся слати # послати пошлешся (Ти) @ verb:rev:pres:s:2
SFX B слатися шлешся слатися # послатися пошлешся (Ти) @ verb:rev:pres:s:2
SFX B ати лешся ипати # сипати сиплешся (Ти) @ verb:rev:pres:s:2
SFX B атися лешся ипатися # сипатися сиплешся (Ти) @ verb:rev:pres:s:2
SFX B ати ишся спати # спати спишся (Ти) @ verb:rev:pres:s:2
SFX B атися ишся спатися # спатися спишся (Ти) @ verb:rev:pres:s:2
SFX B ати ешся орати # орати орешся (Ти) @ verb:rev:pres:s:2
SFX B атися ешся оратися # оратися орешся (Ти) @ verb:rev:pres:s:2
SFX B рати ерешся [бдп]рати # брати берешся (Ти) @ verb:rev:pres:s:2
SFX B ратися ерешся [бдп]ратися # братися берешся (Ти) @ verb:rev:pres:s:2
SFX B ати еться [дн]ати # стогнати стогнеться (Він) @ verb:rev:pres:s:3
SFX B атися еться [дн]атися # стогнатися стогнеться (Він) @ verb:rev:pres:s:3
SFX B ати иться [жщ]ати # блищати блищиться (Він) @ verb:rev:pres:s:3
SFX B атися иться [жщ]атися # блищатися блищиться (Він) @ verb:rev:pres:s:3
SFX B ати иться [^оа]чати # бряжчати бряжчиться (Він) @ verb:rev:pres:s:3
SFX B атися иться [^оа]чатися # бряжчатися бряжчиться (Він) @ verb:rev:pres:s:3
SFX B ати неться [ао]чати # зачати зачнеться (Він) @ verb:rev:pres:s:3
SFX B атися неться [ао]чатися # зачатися зачнеться (Він) @ verb:rev:pres:s:3
SFX B тати четься [^с]тати # шептати шепчеться (Він) @ verb:rev:pres:s:3
SFX B татися четься [^с]татися # шептатися шепчеться (Він) @ verb:rev:pres:s:3
SFX B кати четься [^с]кати # плакати плачеться (Він) @ verb:rev:pres:s:3
SFX B катися четься [^с]катися # плакатися плачеться (Він) @ verb:rev:pres:s:3
SFX B сати шеться сати # писати пишеться (Він) @ verb:rev:pres:s:3
SFX B сатися шеться сатися # писатися пишеться (Він) @ verb:rev:pres:s:3
SFX B хати шеться хати # брехати брешеться (Він) @ verb:rev:pres:s:3
SFX B хатися шеться хатися # брехатися брешеться (Він) @ verb:rev:pres:s:3
SFX B стати щеться стати # свистати свищеться (Він) @ verb:rev:pres:s:3
SFX B статися щеться статися # свистатися свищеться (Він) @ verb:rev:pres:s:3
SFX B скати щеться скати # плескати плещеться (Він) @ verb:rev:pres:s:3
SFX B скатися щеться скатися # плескатися плещеться (Він) @ verb:rev:pres:s:3
SFX B слати шлеться слати # послати пошлеться (Він) @ verb:rev:pres:s:3
SFX B слатися шлеться слатися # послатися пошлеться (Він) @ verb:rev:pres:s:3
SFX B ати леться ипати # сипати сиплеться (Він) @ verb:rev:pres:s:3
SFX B атися леться ипатися # сипатися сиплеться (Він) @ verb:rev:pres:s:3
SFX B ати иться спати # спати спиться (Він) @ verb:rev:pres:s:3
SFX B атися иться спатися # спатися спиться (Він) @ verb:rev:pres:s:3
SFX B ати еться орати # орати ореться (Він) @ verb:rev:pres:s:3
SFX B атися еться оратися # оратися ореться (Він) @ verb:rev:pres:s:3
SFX B рати ереться [бдп]рати # брати береться (Він) @ verb:rev:pres:s:3
SFX B ратися ереться [бдп]ратися # братися береться (Він) @ verb:rev:pres:s:3
SFX B ати емось [дн]ати # стогнати стогнемось (Ми) @ verb:rev:pres:p:1
SFX B атися емось [дн]атися # стогнатися стогнемось (Ми) @ verb:rev:pres:p:1
SFX B ати емося [дн]ати # стогнати стогнемося (Ми) @ verb:rev:pres:p:1
SFX B атися емося [дн]атися # стогнатися стогнемося (Ми) @ verb:rev:pres:p:1
SFX B ати имось [жщ]ати # блищати блищимось (Ми) @ verb:rev:pres:p:1
SFX B атися имось [жщ]атися # блищатися блищимось (Ми) @ verb:rev:pres:p:1
SFX B ати имося [жщ]ати # блищати блищимося (Ми) @ verb:rev:pres:p:1
SFX B атися имося [жщ]атися # блищатися блищимося (Ми) @ verb:rev:pres:p:1
SFX B ати немось [ао]чати # зачати зачнемось (Ми) @ verb:rev:pres:p:1
SFX B атися немось [ао]чатися # зачатися зачнемось (Ми) @ verb:rev:pres:p:1
SFX B ати немося [ао]чати # зачати зачнемося (Ми) @ verb:rev:pres:p:1
SFX B атися немося [ао]чатися # зачатися зачнемося (Ми) @ verb:rev:pres:p:1
SFX B чати чимось [^оа]чати # бряжчати бряжчимось (Ми) @ verb:rev:pres:p:1
SFX B чатися чимось [^оа]чатися # бряжчатися бряжчимось (Ми) @ verb:rev:pres:p:1
SFX B чати чимося [^оа]чати # бряжчати бряжчимося (Ми) @ verb:rev:pres:p:1
SFX B чатися чимося [^оа]чатися # бряжчатися бряжчимося (Ми) @ verb:rev:pres:p:1
SFX B тати чемось [^с]тати # шептати шепчемось (Ми) @ verb:rev:pres:p:1
SFX B татися чемось [^с]татися # шептатися шепчемось (Ми) @ verb:rev:pres:p:1
SFX B тати чемося [^с]тати # шептати шепчемося (Ми) @ verb:rev:pres:p:1
SFX B татися чемося [^с]татися # шептатися шепчемося (Ми) @ verb:rev:pres:p:1
SFX B кати чемось [^с]кати # плакати плачемось (Ми) @ verb:rev:pres:p:1
SFX B катися чемось [^с]катися # плакатися плачемось (Ми) @ verb:rev:pres:p:1
SFX B кати чемося [^с]кати # плакати плачемося (Ми) @ verb:rev:pres:p:1
SFX B катися чемося [^с]катися # плакатися плачемося (Ми) @ verb:rev:pres:p:1
SFX B сати шемось сати # писати пишемось (Ми) @ verb:rev:pres:p:1
SFX B сатися шемось сатися # писатися пишемось (Ми) @ verb:rev:pres:p:1
SFX B сати шемося сати # писати пишемося (Ми) @ verb:rev:pres:p:1
SFX B сатися шемося сатися # писатися пишемося (Ми) @ verb:rev:pres:p:1
SFX B хати шемось хати # брехати брешемось (Ми) @ verb:rev:pres:p:1
SFX B хатися шемось хатися # брехатися брешемось (Ми) @ verb:rev:pres:p:1
SFX B хати шемося хати # брехати брешемося (Ми) @ verb:rev:pres:p:1
SFX B хатися шемося хатися # брехатися брешемося (Ми) @ verb:rev:pres:p:1
SFX B стати щемось стати # свистати свищемось (Ми) @ verb:rev:pres:p:1
SFX B статися щемось статися # свистатися свищемось (Ми) @ verb:rev:pres:p:1
SFX B стати щемося стати # свистати свищемося (Ми) @ verb:rev:pres:p:1
SFX B статися щемося статися # свистатися свищемося (Ми) @ verb:rev:pres:p:1
SFX B скати щемось скати # плескати плещемось (Ми) @ verb:rev:pres:p:1
SFX B скатися щемось скатися # плескатися плещемось (Ми) @ verb:rev:pres:p:1
SFX B скати щемося скати # плескати плещемося (Ми) @ verb:rev:pres:p:1
SFX B скатися щемося скатися # плескатися плещемося (Ми) @ verb:rev:pres:p:1
SFX B слати шлемось слати # послати пошлемось (Ми) @ verb:rev:pres:p:1
SFX B слатися шлемось слатися # послатися пошлемось (Ми) @ verb:rev:pres:p:1
SFX B слати шлемося слати # послати пошлемося (Ми) @ verb:rev:pres:p:1
SFX B слатися шлемося слатися # послатися пошлемося (Ми) @ verb:rev:pres:p:1
SFX B ати лемось ипати # сипати сиплемось (Ми) @ verb:rev:pres:p:1
SFX B атися лемось ипатися # сипатися сиплемось (Ми) @ verb:rev:pres:p:1
SFX B ати лемося ипати # сипати сиплемося (Ми) @ verb:rev:pres:p:1
SFX B атися лемося ипатися # сипатися сиплемося (Ми) @ verb:rev:pres:p:1
SFX B ати имось спати # спати спимось (Ми) @ verb:rev:pres:p:1
SFX B атися имось спатися # спатися спимось (Ми) @ verb:rev:pres:p:1
SFX B ати имося спати # спати спимося (Ми) @ verb:rev:pres:p:1
SFX B атися имося спатися # спатися спимося (Ми) @ verb:rev:pres:p:1
SFX B ати емось орати # орати оремось (Ми) @ verb:rev:pres:p:1
SFX B атися емось оратися # оратися оремось (Ми) @ verb:rev:pres:p:1
SFX B ати емося орати # орати оремося (Ми) @ verb:rev:pres:p:1
SFX B атися емося оратися # оратися оремося (Ми) @ verb:rev:pres:p:1
SFX B рати еремось [бдп]рати # брати беремось (Ми) @ verb:rev:pres:p:1
SFX B ратися еремось [бдп]ратися # братися беремось (Ми) @ verb:rev:pres:p:1
SFX B рати еремося [бдп]рати # брати беремося (Ми) @ verb:rev:pres:p:1
SFX B ратися еремося [бдп]ратися # братися беремося (Ми) @ verb:rev:pres:p:1
SFX B ати етесь [дн]ати # стогнати стогнетесь (Ви) @ verb:rev:pres:p:2
SFX B атися етесь [дн]атися # стогнатися стогнетесь (Ви) @ verb:rev:pres:p:2
SFX B ати етеся [дн]ати # стогнати стогнетеся (Ви) @ verb:rev:pres:p:2
SFX B атися етеся [дн]атися # стогнатися стогнетеся (Ви) @ verb:rev:pres:p:2
SFX B ати итесь [жщ]ати # блищати блищиш (Ви) @ verb:rev:pres:p:2
SFX B атися итесь [жщ]атися # блищатися блищиш (Ви) @ verb:rev:pres:p:2
SFX B ати итеся [жщ]ати # блищати блищиш (Ви) @ verb:rev:pres:p:2
SFX B атися итеся [жщ]атися # блищатися блищиш (Ви) @ verb:rev:pres:p:2
SFX B ати нетесь [ао]чати # зачати зачнетесь (Ви) @ verb:rev:pres:p:2
SFX B атися нетесь [ао]чатися # зачатися зачнетесь (Ви) @ verb:rev:pres:p:2
SFX B ати нетеся [ао]чати # зачати зачнетеся (Ви) @ verb:rev:pres:p:2
SFX B атися нетеся [ао]чатися # зачатися зачнетеся (Ви) @ verb:rev:pres:p:2
SFX B чати читесь [^оа]чати # бряжчати бряжчиш (Ви) @ verb:rev:pres:p:2
SFX B чатися читесь [^оа]чатися # бряжчатися бряжчиш (Ви) @ verb:rev:pres:p:2
SFX B чати читеся [^оа]чати # бряжчати бряжчиш (Ви) @ verb:rev:pres:p:2
SFX B чатися читеся [^оа]чатися # бряжчатися бряжчиш (Ви) @ verb:rev:pres:p:2
SFX B тати четесь [^с]тати # шептати шепчетесь (Ви) @ verb:rev:pres:p:2
SFX B татися четесь [^с]татися # шептатися шепчетесь (Ви) @ verb:rev:pres:p:2
SFX B тати четеся [^с]тати # шептати шепчетеся (Ви) @ verb:rev:pres:p:2
SFX B татися четеся [^с]татися # шептатися шепчетеся (Ви) @ verb:rev:pres:p:2
SFX B кати четесь [^с]кати # плакати плачетесь (Ви) @ verb:rev:pres:p:2
SFX B катися четесь [^с]катися # плакатися плачетесь (Ви) @ verb:rev:pres:p:2
SFX B кати четеся [^с]кати # плакати плачетеся (Ви) @ verb:rev:pres:p:2
SFX B катися четеся [^с]катися # плакатися плачетеся (Ви) @ verb:rev:pres:p:2
SFX B сати шетесь сати # писати пишетесь (Ви) @ verb:rev:pres:p:2
SFX B сатися шетесь сатися # писатися пишетесь (Ви) @ verb:rev:pres:p:2
SFX B сати шетеся сати # писати пишетеся (Ви) @ verb:rev:pres:p:2
SFX B сатися шетеся сатися # писатися пишетеся (Ви) @ verb:rev:pres:p:2
SFX B хати шетесь хати # брехати брешетесь (Ви) @ verb:rev:pres:p:2
SFX B хатися шетесь хатися # брехатися брешетесь (Ви) @ verb:rev:pres:p:2
SFX B хати шетеся хати # брехати брешетеся (Ви) @ verb:rev:pres:p:2
SFX B хатися шетеся хатися # брехатися брешетеся (Ви) @ verb:rev:pres:p:2
SFX B стати щетесь стати # свистати свищетесь (Ви) @ verb:rev:pres:p:2
SFX B статися щетесь статися # свистатися свищетесь (Ви) @ verb:rev:pres:p:2
SFX B стати щетеся стати # свистати свищетеся (Ви) @ verb:rev:pres:p:2
SFX B статися щетеся статися # свистатися свищетеся (Ви) @ verb:rev:pres:p:2
SFX B скати щетесь скати # плескати плещетесь (Ви) @ verb:rev:pres:p:2
SFX B скатися щетесь скатися # плескатися плещетесь (Ви) @ verb:rev:pres:p:2
SFX B скати щетеся скати # плескати плещетеся (Ви) @ verb:rev:pres:p:2
SFX B скатися щетеся скатися # плескатися плещетеся (Ви) @ verb:rev:pres:p:2
SFX B слати шлетесь слати # послати пошлетесь (Ви) @ verb:rev:pres:p:2
SFX B слатися шлетесь слатися # послатися пошлетесь (Ви) @ verb:rev:pres:p:2
SFX B слати шлетеся слати # послати пошлетеся (Ви) @ verb:rev:pres:p:2
SFX B слатися шлетеся слатися # послатися пошлетеся (Ви) @ verb:rev:pres:p:2
SFX B ати летесь ипати # сипати сиплетесь (Ви) @ verb:rev:pres:p:2
SFX B атися летесь ипатися # сипатися сиплетесь (Ви) @ verb:rev:pres:p:2
SFX B ати летеся ипати # сипати сиплетеся (Ви) @ verb:rev:pres:p:2
SFX B атися летеся ипатися # сипатися сиплетеся (Ви) @ verb:rev:pres:p:2
SFX B ати итесь спати # спати спитесь (Ви) @ verb:rev:pres:p:2
SFX B атися итесь спатися # спатися спитесь (Ви) @ verb:rev:pres:p:2
SFX B ати итеся спати # спати спитеся (Ви) @ verb:rev:pres:p:2
SFX B атися итеся спатися # спатися спитеся (Ви) @ verb:rev:pres:p:2
SFX B ати етесь орати # орати оретесь (Ви) @ verb:rev:pres:p:2
SFX B атися етесь оратися # оратися оретесь (Ви) @ verb:rev:pres:p:2
SFX B ати етеся орати # орати оретеся (Ви) @ verb:rev:pres:p:2
SFX B атися етеся оратися # оратися оретеся (Ви) @ verb:rev:pres:p:2
SFX B рати еретесь [бдп]рати # брати беретесь (Ви) @ verb:rev:pres:p:2
SFX B ратися еретесь [бдп]ратися # братися беретесь (Ви) @ verb:rev:pres:p:2
SFX B рати еретеся [бдп]рати # брати беретеся (Ви) @ verb:rev:pres:p:2
SFX B ратися еретеся [бдп]ратися # братися беретеся (Ви) @ verb:rev:pres:p:2
SFX B ати уться [дн]ати # стогнати стогнуться (Вони) @ verb:rev:pres:p:3
SFX B атися уться [дн]атися # стогнатися стогнуться (Вони) @ verb:rev:pres:p:3
SFX B ати аться [жщ]ати # блищати блищаться (Вони) @ verb:rev:pres:p:3
SFX B атися аться [жщ]атися # блищатися блищаться (Вони) @ verb:rev:pres:p:3
SFX B ати нуться [ао]чати # зачати зачнуться (Вони) @ verb:rev:pres:p:3
SFX B атися нуться [ао]чатися # зачатися зачнуться (Вони) @ verb:rev:pres:p:3
SFX B ати аться [^ао]чати # бряжчати бряжчаться (Вони) @ verb:rev:pres:p:3
SFX B атися аться [^ао]чатися # бряжчатися бряжчаться (Вони) @ verb:rev:pres:p:3
SFX B тати чуться [^с]тати # шептати шепчуться (Вони) @ verb:rev:pres:p:3
SFX B татися чуться [^с]татися # шептатися шепчуться (Вони) @ verb:rev:pres:p:3
SFX B кати чуться [^с]кати # плакати плачуться (Вони) @ verb:rev:pres:p:3
SFX B катися чуться [^с]катися # плакатися плачуться (Вони) @ verb:rev:pres:p:3
SFX B сати шуться сати # писати пишуться (Вони) @ verb:rev:pres:p:3
SFX B сатися шуться сатися # писатися пишуться (Вони) @ verb:rev:pres:p:3
SFX B хати шуться хати # брехати брешуться (Вони) @ verb:rev:pres:p:3
SFX B хатися шуться хатися # брехатися брешуться (Вони) @ verb:rev:pres:p:3
SFX B стати щуться стати # свистати свищуться (Вони) @ verb:rev:pres:p:3
SFX B статися щуться статися # свистатися свищуться (Вони) @ verb:rev:pres:p:3
SFX B скати щуться скати # плескати плещуться (Вони) @ verb:rev:pres:p:3
SFX B скатися щуться скатися # плескатися плещуться (Вони) @ verb:rev:pres:p:3
SFX B слати шлються слати # послати пошлються (Вони) @ verb:rev:pres:p:3
SFX B слатися шлються слатися # послатися пошлються (Вони) @ verb:rev:pres:p:3
SFX B ати лються ипати # сипати сиплються (Вони) @ verb:rev:pres:p:3
SFX B атися лються ипатися # сипатися сиплються (Вони) @ verb:rev:pres:p:3
SFX B ати ляться спати # спати спляться (Вони) @ verb:rev:pres:p:3
SFX B атися ляться спатися # спатися спляться (Вони) @ verb:rev:pres:p:3
SFX B ати ються орати # орати орються (Вони) @ verb:rev:pres:p:3
SFX B атися ються оратися # оратися орються (Вони) @ verb:rev:pres:p:3
SFX B рати еруться [бдп]рати # брати беруться (Вони) @ verb:rev:pres:p:3
SFX B ратися еруться [бдп]ратися # братися беруться (Вони) @ verb:rev:pres:p:3
SFX B ати ись [днжщ]ати # блищати блищись (Ти) @ verb:rev:impr:s:2
SFX B атися ись [днжщ]атися # блищатися блищись (Ти) @ verb:rev:impr:s:2
SFX B ати ися [днжщ]ати # блищати блищися (Ти) @ verb:rev:impr:s:2
SFX B атися ися [днжщ]атися # блищатися блищися (Ти) @ verb:rev:impr:s:2
SFX B ати нись [ао]чати # зачати зачнись (Ти) @ verb:rev:impr:s:2
SFX B атися нись [ао]чатися # зачатися зачнись (Ти) @ verb:rev:impr:s:2
SFX B ати нися [ао]чати # зачати зачнися (Ти) @ verb:rev:impr:s:2
SFX B атися нися [ао]чатися # зачатися зачнися (Ти) @ verb:rev:impr:s:2
SFX B ати ись [^ао]чати # деренчати деренчись (Ти) @ verb:rev:impr:s:2
SFX B атися ись [^ао]чатися # деренчатися деренчись (Ти) @ verb:rev:impr:s:2
SFX B ати ися [^ао]чати # деренчати деренчися (Ти) @ verb:rev:impr:s:2
SFX B атися ися [^ао]чатися # деренчатися деренчися (Ти) @ verb:rev:impr:s:2
SFX B тати чись [^с]тати # шептати шепчись (Ти) @ verb:rev:impr:s:2
SFX B татися чись [^с]татися # шептатися шепчись (Ти) @ verb:rev:impr:s:2
SFX B тати чися [^с]тати # шептати шепчися (Ти) @ verb:rev:impr:s:2
SFX B татися чися [^с]татися # шептатися шепчися (Ти) @ verb:rev:impr:s:2
SFX B сати шись сати # писати пишись (Ти) @ verb:rev:impr:s:2
SFX B сатися шись сатися # писатися пишись (Ти) @ verb:rev:impr:s:2
SFX B сати шися сати # писати пишися (Ти) @ verb:rev:impr:s:2
SFX B сатися шися сатися # писатися пишися (Ти) @ verb:rev:impr:s:2
SFX B хати шись хати # брехати брешись (Ти) @ verb:rev:impr:s:2
SFX B хатися шись хатися # брехатися брешись (Ти) @ verb:rev:impr:s:2
SFX B хати шися хати # брехати брешися (Ти) @ verb:rev:impr:s:2
SFX B хатися шися хатися # брехатися брешися (Ти) @ verb:rev:impr:s:2
SFX B кати чся лакати # плакати плачся (Ти) @ verb:rev:impr:s:2
SFX B катися чся лакатися # плакатися плачся (Ти) @ verb:rev:impr:s:2
SFX B кати чись какати # скакати скачись (Ти) @ verb:rev:impr:s:2
SFX B катися чись какатися # скакатися скачись (Ти) @ verb:rev:impr:s:2
SFX B кати чися какати # скакати скачися (Ти) @ verb:rev:impr:s:2
SFX B катися чися какатися # скакатися скачися (Ти) @ verb:rev:impr:s:2
SFX B кати чись ткати # ткати тчись (Ти) @ verb:rev:impr:s:2
SFX B катися чись ткатися # ткатися тчись (Ти) @ verb:rev:impr:s:2
SFX B кати чися ткати # ткати тчися (Ти) @ verb:rev:impr:s:2
SFX B катися чися ткатися # ткатися тчися (Ти) @ verb:rev:impr:s:2
SFX B кати чся икати # кликати кличся (Ти) @ verb:rev:impr:s:2
SFX B катися чся икатися # кликатися кличся (Ти) @ verb:rev:impr:s:2
SFX B скати щись скати # плескати плещись (Ти) @ verb:rev:impr:s:2
SFX B скатися щись скатися # плескатися плещись (Ти) @ verb:rev:impr:s:2
SFX B скати щися скати # плескати плещися (Ти) @ verb:rev:impr:s:2
SFX B скатися щися скатися # плескатися плещися (Ти) @ verb:rev:impr:s:2
SFX B стати щись стати # свистати свищись (Ти) @ verb:rev:impr:s:2
SFX B статися щись статися # свистатися свищись (Ти) @ verb:rev:impr:s:2
SFX B стати щися стати # свистати свищися (Ти) @ verb:rev:impr:s:2
SFX B статися щися статися # свистатися свищися (Ти) @ verb:rev:impr:s:2
SFX B слати шлись слати # послати пошлись (Ти) @ verb:rev:impr:s:2
SFX B слатися шлись слатися # послатися пошлись (Ти) @ verb:rev:impr:s:2
SFX B слати шлися слати # послати пошлися (Ти) @ verb:rev:impr:s:2
SFX B слатися шлися слатися # послатися пошлися (Ти) @ verb:rev:impr:s:2
SFX B пати пись спати # сипати спись (Ти) @ verb:rev:impr:s:2
SFX B патися пись спатися # сипатися спись (Ти) @ verb:rev:impr:s:2
SFX B пати пися спати # сипати спися (Ти) @ verb:rev:impr:s:2
SFX B патися пися спатися # сипатися спися (Ти) @ verb:rev:impr:s:2
SFX B пати пся ипати # сипати сипся (Ти) @ verb:rev:impr:s:2
SFX B патися пся ипатися # сипатися сипся (Ти) @ verb:rev:impr:s:2
SFX B ати ись орати # орати орись (Ти) @ verb:rev:impr:s:2
SFX B атися ись оратися # оратися орись (Ти) @ verb:rev:impr:s:2
SFX B ати ися орати # орати орися (Ти) @ verb:rev:impr:s:2
SFX B атися ися оратися # оратися орися (Ти) @ verb:rev:impr:s:2
SFX B рати ерись [бдп]рати # брати берись (Ти) @ verb:rev:impr:s:2
SFX B ратися ерись [бдп]ратися # братися берись (Ти) @ verb:rev:impr:s:2
SFX B рати ерися [бдп]рати # брати берися (Ти) @ verb:rev:impr:s:2
SFX B ратися ерися [бдп]ратися # братися берися (Ти) @ verb:rev:impr:s:2
SFX B ати імось [днжщ]ати # блищати блищімось (Ми) @ verb:rev:impr:p:1
SFX B атися імось [днжщ]атися # блищатися блищімось (Ми) @ verb:rev:impr:p:1
SFX B ати імося [днжщ]ати # блищати блищімося (Ми) @ verb:rev:impr:p:1
SFX B атися імося [днжщ]атися # блищатися блищімося (Ми) @ verb:rev:impr:p:1
SFX B ати німось [ао]чати # зачати зачнімось (Ми) @ verb:rev:impr:p:1
SFX B атися німось [ао]чатися # зачатися зачнімось (Ми) @ verb:rev:impr:p:1
SFX B ати німося [ао]чати # зачати зачнімося (Ми) @ verb:rev:impr:p:1
SFX B атися німося [ао]чатися # зачатися зачнімося (Ми) @ verb:rev:impr:p:1
SFX B ати імось [^оа]чати # бряжчати бряжчімось (Ми) @ verb:rev:impr:p:1
SFX B атися імось [^оа]чатися # бряжчатися бряжчімось (Ми) @ verb:rev:impr:p:1
SFX B ати імося [^оа]чати # бряжчати бряжчімося (Ми) @ verb:rev:impr:p:1
SFX B атися імося [^оа]чатися # бряжчатися бряжчімося (Ми) @ verb:rev:impr:p:1
SFX B тати чімось [^с]тати # шептати шепчімось (Ми) @ verb:rev:impr:p:1
SFX B татися чімось [^с]татися # шептатися шепчімось (Ми) @ verb:rev:impr:p:1
SFX B тати чімося [^с]тати # шептати шепчімося (Ми) @ verb:rev:impr:p:1
SFX B татися чімося [^с]татися # шептатися шепчімося (Ми) @ verb:rev:impr:p:1
SFX B сати шімось сати # писати пишімось (Ми) @ verb:rev:impr:p:1
SFX B сатися шімось сатися # писатися пишімось (Ми) @ verb:rev:impr:p:1
SFX B сати шімося сати # писати пишімося (Ми) @ verb:rev:impr:p:1
SFX B сатися шімося сатися # писатися пишімося (Ми) @ verb:rev:impr:p:1
SFX B хати шімось хати # писати пишімось (Ми) @ verb:rev:impr:p:1
SFX B хатися шімось хатися # писатися пишімось (Ми) @ verb:rev:impr:p:1
SFX B хати шімося хати # писати пишімося (Ми) @ verb:rev:impr:p:1
SFX B хатися шімося хатися # писатися пишімося (Ми) @ verb:rev:impr:p:1
SFX B кати чмось лакати # плакати плачмось (Ми) @ verb:rev:impr:p:1
SFX B катися чмось лакатися # плакатися плачмось (Ми) @ verb:rev:impr:p:1
SFX B кати чмося лакати # плакати плачмося (Ми) @ verb:rev:impr:p:1
SFX B катися чмося лакатися # плакатися плачмося (Ми) @ verb:rev:impr:p:1
SFX B кати чімось какати # ткати тчімось (Ми) @ verb:rev:impr:p:1
SFX B катися чімось какатися # ткатися тчімось (Ми) @ verb:rev:impr:p:1
SFX B кати чімося какати # ткати тчімося (Ми) @ verb:rev:impr:p:1
SFX B катися чімося какатися # ткатися тчімося (Ми) @ verb:rev:impr:p:1
SFX B кати чімось ткати # ткати тчімось (Ми) @ verb:rev:impr:p:1
SFX B катися чімось ткатися # ткатися тчімось (Ми) @ verb:rev:impr:p:1
SFX B кати чімося ткати # ткати тчімося (Ми) @ verb:rev:impr:p:1
SFX B катися чімося ткатися # ткатися тчімося (Ми) @ verb:rev:impr:p:1
SFX B кати чмось икати # кликати кличмось (Ми) @ verb:rev:impr:p:1
SFX B катися чмось икатися # кликатися кличмось (Ми) @ verb:rev:impr:p:1
SFX B кати чмося икати # кликати кличмося (Ми) @ verb:rev:impr:p:1
SFX B катися чмося икатися # кликатися кличмося (Ми) @ verb:rev:impr:p:1
SFX B скати щімось скати # плескати плещімось (Ми) @ verb:rev:impr:p:1
SFX B скатися щімось скатися # плескатися плещімось (Ми) @ verb:rev:impr:p:1
SFX B скати щімося скати # плескати плещімося (Ми) @ verb:rev:impr:p:1
SFX B скатися щімося скатися # плескатися плещімося (Ми) @ verb:rev:impr:p:1
SFX B стати щімось стати # свистати свищімось (Ми) @ verb:rev:impr:p:1
SFX B статися щімось статися # свистатися свищімось (Ми) @ verb:rev:impr:p:1
SFX B стати щімося стати # свистати свищімося (Ми) @ verb:rev:impr:p:1
SFX B статися щімося статися # свистатися свищімося (Ми) @ verb:rev:impr:p:1
SFX B слати шлімось слати # послати пошлімось (Ми) @ verb:rev:impr:p:1
SFX B слатися шлімось слатися # послатися пошлімось (Ми) @ verb:rev:impr:p:1
SFX B слати шлімося слати # послати пошлімося (Ми) @ verb:rev:impr:p:1
SFX B слатися шлімося слатися # послатися пошлімося (Ми) @ verb:rev:impr:p:1
SFX B ати мось ипати # сипати сипмось (Ми) @ verb:rev:impr:p:1
SFX B атися мось ипатися # сипатися сипмось (Ми) @ verb:rev:impr:p:1
SFX B ати мося ипати # сипати сипмося (Ми) @ verb:rev:impr:p:1
SFX B атися мося ипатися # сипатися сипмося (Ми) @ verb:rev:impr:p:1
SFX B ати імось спати # спати спімось (Ми) @ verb:rev:impr:p:1
SFX B атися імось спатися # спатися спімось (Ми) @ verb:rev:impr:p:1
SFX B ати імося спати # спати спімося (Ми) @ verb:rev:impr:p:1
SFX B атися імося спатися # спатися спімося (Ми) @ verb:rev:impr:p:1
SFX B ати імось орати # орати орімось (Ми) @ verb:rev:impr:p:1
SFX B атися імось оратися # оратися орімось (Ми) @ verb:rev:impr:p:1
SFX B ати імося орати # орати орімося (Ми) @ verb:rev:impr:p:1
SFX B атися імося оратися # оратися орімося (Ми) @ verb:rev:impr:p:1
SFX B рати ерімось [бдп]рати # брати берімось (Ми) @ verb:rev:impr:p:1
SFX B ратися ерімось [бдп]ратися # братися берімось (Ми) @ verb:rev:impr:p:1
SFX B рати ерімося [бдп]рати # брати берімося (Ми) @ verb:rev:impr:p:1
SFX B ратися ерімося [бдп]ратися # братися берімося (Ми) @ verb:rev:impr:p:1
SFX B ати іться [джщн]ати # блищати блищіться (Ви) @ verb:rev:impr:p:2
SFX B атися іться [джщн]атися # блищатися блищіться (Ви) @ verb:rev:impr:p:2
SFX B ати ніться [ао]чати # зачати зачніться (Ви) @ verb:rev:impr:p:2
SFX B атися ніться [ао]чатися # зачатися зачніться (Ви) @ verb:rev:impr:p:2
SFX B чати чіться [^оа]чати # бряжчати бряжчіться (Ви) @ verb:rev:impr:p:2
SFX B чатися чіться [^оа]чатися # бряжчатися бряжчіться (Ви) @ verb:rev:impr:p:2
SFX B тати чіться [^с]тати # шептати шепчіться (Ви) @ verb:rev:impr:p:2
SFX B татися чіться [^с]татися # шептатися шепчіться (Ви) @ verb:rev:impr:p:2
SFX B сати шіться сати # писати пишіться (Ви) @ verb:rev:impr:p:2
SFX B сатися шіться сатися # писатися пишіться (Ви) @ verb:rev:impr:p:2
SFX B хати шіться хати # писати пишіться (Ви) @ verb:rev:impr:p:2
SFX B хатися шіться хатися # писатися пишіться (Ви) @ verb:rev:impr:p:2
SFX B кати чтесь лакати # плакати плачтесь (Ви) @ verb:rev:impr:p:2
SFX B катися чтесь лакатися # плакатися плачтесь (Ви) @ verb:rev:impr:p:2
SFX B кати чтеся лакати # плакати плачтеся (Ви) @ verb:rev:impr:p:2
SFX B катися чтеся лакатися # плакатися плачтеся (Ви) @ verb:rev:impr:p:2
SFX B кати чіться какати # скакати скачіться (Ви) @ verb:rev:impr:p:2
SFX B катися чіться какатися # скакатися скачіться (Ви) @ verb:rev:impr:p:2
SFX B кати чіться ткати # ткати тчіться (Ви) @ verb:rev:impr:p:2
SFX B катися чіться ткатися # ткатися тчіться (Ви) @ verb:rev:impr:p:2
SFX B стати щіться стати # свистати свищіться (Ви) @ verb:rev:impr:p:2
SFX B статися щіться статися # свистатися свищіться (Ви) @ verb:rev:impr:p:2
SFX B скати щіться скати # плескати плещіться (Ви) @ verb:rev:impr:p:2
SFX B скатися щіться скатися # плескатися плещіться (Ви) @ verb:rev:impr:p:2
SFX B слати шліться слати # послати пошліться (Ви) @ verb:rev:impr:p:2
SFX B слатися шліться слатися # послатися пошліться (Ви) @ verb:rev:impr:p:2
SFX B кати чтесь [^ста]кати # плакати плачтесь (Ви) @ verb:rev:impr:p:2
SFX B катися чтесь [^ста]катися # плакатися плачтесь (Ви) @ verb:rev:impr:p:2
SFX B кати чтеся [^ста]кати # плакати плачтеся (Ви) @ verb:rev:impr:p:2
SFX B катися чтеся [^ста]катися # плакатися плачтеся (Ви) @ verb:rev:impr:p:2
SFX B ати тесь ипати # сипати сиптесь (Ви) @ verb:rev:impr:p:2
SFX B атися тесь ипатися # сипатися сиптесь (Ви) @ verb:rev:impr:p:2
SFX B ати теся ипати # сипати сиптеся (Ви) @ verb:rev:impr:p:2
SFX B атися теся ипатися # сипатися сиптеся (Ви) @ verb:rev:impr:p:2
SFX B ати іться спати # спати спіться (Ви) @ verb:rev:impr:p:2
SFX B атися іться спатися # спатися спіться (Ви) @ verb:rev:impr:p:2
SFX B ати іться орати # орати оріться (Ви) @ verb:rev:impr:p:2
SFX B атися іться оратися # оратися оріться (Ви) @ verb:rev:impr:p:2
SFX B рати еріться [бдп]рати # брати беріться (Ви) @ verb:rev:impr:p:2
SFX B ратися еріться [бдп]ратися # братися беріться (Ви) @ verb:rev:impr:p:2
SFX B ити жусь [^з]дити # входити входжусь (Я) @ verb:rev:pres:s:1
SFX B итися жусь [^з]дитися # входитися входжусь (Я) @ verb:rev:pres:s:1
SFX B ити жуся [^з]дити # входити входжуся (Я) @ verb:rev:pres:s:1
SFX B итися жуся [^з]дитися # входитися входжуся (Я) @ verb:rev:pres:s:1
SFX B здити жджусь здити # їздити їжджусь (Я) @ verb:rev:pres:s:1
SFX B здитися жджусь здитися # їздитися їжджусь (Я) @ verb:rev:pres:s:1
SFX B здити жджуся здити # їздити їжджуся (Я) @ verb:rev:pres:s:1
SFX B здитися жджуся здитися # їздитися їжджуся (Я) @ verb:rev:pres:s:1
SFX B зити жусь зити # возити вожусь (Я) @ verb:rev:pres:s:1
SFX B зитися жусь зитися # возитися вожусь (Я) @ verb:rev:pres:s:1
SFX B зити жуся зити # возити вожуся (Я) @ verb:rev:pres:s:1
SFX B зитися жуся зитися # возитися вожуся (Я) @ verb:rev:pres:s:1
SFX B ити усь [жчшщ]ити # бентежити бентежусь (Я) @ verb:rev:pres:s:1
SFX B итися усь [жчшщ]итися # бентежитися бентежусь (Я) @ verb:rev:pres:s:1
SFX B ити уся [жчшщ]ити # бентежити бентежуся (Я) @ verb:rev:pres:s:1
SFX B итися уся [жчшщ]итися # бентежитися бентежуся (Я) @ verb:rev:pres:s:1
SFX B сити шусь сити # місити мішусь (Я) @ verb:rev:pres:s:1
SFX B ситися шусь ситися # міситися мішусь (Я) @ verb:rev:pres:s:1
SFX B сити шуся сити # місити мішуся (Я) @ verb:rev:pres:s:1
SFX B ситися шуся ситися # міситися мішуся (Я) @ verb:rev:pres:s:1
SFX B тити чусь [^с]тити # тратити трачусь (Я) @ verb:rev:pres:s:1
SFX B титися чусь [^с]титися # тратитися трачусь (Я) @ verb:rev:pres:s:1
SFX B тити чуся [^с]тити # тратити трачуся (Я) @ verb:rev:pres:s:1
SFX B титися чуся [^с]титися # тратитися трачуся (Я) @ verb:rev:pres:s:1
SFX B стити щусь стити # мостити мощусь (Я) @ verb:rev:pres:s:1
SFX B ститися щусь ститися # моститися мощусь (Я) @ verb:rev:pres:s:1
SFX B стити щуся стити # мостити мощуся (Я) @ verb:rev:pres:s:1
SFX B ститися щуся ститися # моститися мощуся (Я) @ verb:rev:pres:s:1
SFX B ити люсь [бвмпф]ити # вимовити вимовлюсь (Я) @ verb:rev:pres:s:1
SFX B итися люсь [бвмпф]итися # вимовитися вимовлюсь (Я) @ verb:rev:pres:s:1
SFX B ити люся [бвмпф]ити # вимовити вимовлюся (Я) @ verb:rev:pres:s:1
SFX B итися люся [бвмпф]итися # вимовитися вимовлюся (Я) @ verb:rev:pres:s:1
SFX B ити юсь [лнр]ити # творити творюсь (Я) @ verb:rev:pres:s:1
SFX B итися юсь [лнр]итися # творитися творюсь (Я) @ verb:rev:pres:s:1
SFX B ити юся [лнр]ити # творити творюся (Я) @ verb:rev:pres:s:1
SFX B итися юся [лнр]итися # творитися творюся (Я) @ verb:rev:pres:s:1
SFX B ти шся ити # бентежити бентежишся (Ти) @ verb:rev:pres:s:2
SFX B тися шся итися # бентежитися бентежишся (Ти) @ verb:rev:pres:s:2
SFX B и ься ити # бентежити бентежиться (Він) @ verb:rev:pres:s:3
SFX B ися ься итися # бентежитися бентежиться (Він) @ verb:rev:pres:s:3
SFX B ти мось ити # бентежити бентежимось (Ми) @ verb:rev:pres:p:1
SFX B тися мось итися # бентежитися бентежимось (Ми) @ verb:rev:pres:p:1
SFX B ти мося ити # бентежити бентежимося (Ми) @ verb:rev:pres:p:1
SFX B тися мося итися # бентежитися бентежимося (Ми) @ verb:rev:pres:p:1
SFX B ити итесь ити # бентежити бентежитесь (Ви) @ verb:rev:pres:p:2
SFX B итися итесь итися # бентежитися бентежитесь (Ви) @ verb:rev:pres:p:2
SFX B ити итеся ити # бентежити бентежитеся (Ви) @ verb:rev:pres:p:2
SFX B итися итеся итися # бентежитися бентежитеся (Ви) @ verb:rev:pres:p:2
SFX B ити аться [жчшщ]ити # бентежити бентежаться (Вони) @ verb:rev:pres:p:3
SFX B итися аться [жчшщ]итися # бентежитися бентежаться (Вони) @ verb:rev:pres:p:3
SFX B ити ляться [бвмпф]ити # вимовити вимовляться (Вони) @ verb:rev:pres:p:3
SFX B итися ляться [бвмпф]итися # вимовитися вимовляться (Вони) @ verb:rev:pres:p:3
SFX B ити яться [дзлнрст]ити # входити входяться (Вони) @ verb:rev:pres:p:3
SFX B итися яться [дзлнрст]итися # входитися входяться (Вони) @ verb:rev:pres:p:3
SFX B іти жусь діти # смердіти смерджусь (Я) @ verb:rev:pres:s:1
SFX B ітися жусь дітися # смердітися смерджусь (Я) @ verb:rev:pres:s:1
SFX B іти жуся діти # смердіти смерджуся (Я) @ verb:rev:pres:s:1
SFX B ітися жуся дітися # смердітися смерджуся (Я) @ verb:rev:pres:s:1
SFX B іти усь [шж]іти # кишіти кишусь (Я) @ verb:rev:pres:s:1
SFX B ітися усь [шж]ітися # кишітися кишусь (Я) @ verb:rev:pres:s:1
SFX B іти уся [шж]іти # кишіти кишуся (Я) @ verb:rev:pres:s:1
SFX B ітися уся [шж]ітися # кишітися кишуся (Я) @ verb:rev:pres:s:1
SFX B сіти шусь сіти # висіти вишусь (Я) @ verb:rev:pres:s:1
SFX B сітися шусь сітися # висітися вишусь (Я) @ verb:rev:pres:s:1
SFX B сіти шуся сіти # висіти вишуся (Я) @ verb:rev:pres:s:1
SFX B сітися шуся сітися # висітися вишуся (Я) @ verb:rev:pres:s:1
SFX B тіти чусь [^с]тіти # летіти лечусь (Я) @ verb:rev:pres:s:1
SFX B тітися чусь [^с]тітися # летітися лечусь (Я) @ verb:rev:pres:s:1
SFX B тіти чуся [^с]тіти # летіти лечуся (Я) @ verb:rev:pres:s:1
SFX B тітися чуся [^с]тітися # летітися лечуся (Я) @ verb:rev:pres:s:1
SFX B стіти щусь стіти # шелестіти шелещусь (Я) @ verb:rev:pres:s:1
SFX B стітися щусь стітися # шелестітися шелещусь (Я) @ verb:rev:pres:s:1
SFX B стіти щуся стіти # шелестіти шелещуся (Я) @ verb:rev:pres:s:1
SFX B стітися щуся стітися # шелестітися шелещуся (Я) @ verb:rev:pres:s:1
SFX B іти люсь [бвмп]іти # шуміти шумлюсь (Я) @ verb:rev:pres:s:1
SFX B ітися люсь [бвмп]ітися # шумітися шумлюсь (Я) @ verb:rev:pres:s:1
SFX B іти люся [бвмп]іти # шуміти шумлюся (Я) @ verb:rev:pres:s:1
SFX B ітися люся [бвмп]ітися # шумітися шумлюся (Я) @ verb:rev:pres:s:1
SFX B іти юсь [нлр]іти # веліти велюсь (Я) @ verb:rev:pres:s:1
SFX B ітися юсь [нлр]ітися # велітися велюсь (Я) @ verb:rev:pres:s:1
SFX B іти юся [нлр]іти # веліти велюся (Я) @ verb:rev:pres:s:1
SFX B ітися юся [нлр]ітися # велітися велюся (Я) @ verb:rev:pres:s:1
SFX B іти ишся іти # шепотіти шепотишся (Ти) @ verb:rev:pres:s:2
SFX B ітися ишся ітися # шепотітися шепотишся (Ти) @ verb:rev:pres:s:2
SFX B іти иться іти # шепотіти шепотиться (Він) @ verb:rev:pres:s:3
SFX B ітися иться ітися # шепотітися шепотиться (Він) @ verb:rev:pres:s:3
SFX B іти имось іти # шепотіти шепотимось (Ми) @ verb:rev:pres:p:1
SFX B ітися имось ітися # шепотітися шепотимось (Ми) @ verb:rev:pres:p:1
SFX B іти имося іти # шепотіти шепотимося (Ми) @ verb:rev:pres:p:1
SFX B ітися имося ітися # шепотітися шепотимося (Ми) @ verb:rev:pres:p:1
SFX B іти итесь іти # шепотіти шепотитесь (Ви) @ verb:rev:pres:p:2
SFX B ітися итесь ітися # шепотітися шепотитесь (Ви) @ verb:rev:pres:p:2
SFX B іти итеся іти # шепотіти шепотитеся (Ви) @ verb:rev:pres:p:2
SFX B ітися итеся ітися # шепотітися шепотитеся (Ви) @ verb:rev:pres:p:2
SFX B іти аться шіти # кишіти кишаться (Вони) @ verb:rev:pres:p:3
SFX B ітися аться шітися # кишітися кишаться (Вони) @ verb:rev:pres:p:3
SFX B іти ляться [бвмп]іти # шуміти шумляться (Вони) @ verb:rev:pres:p:3
SFX B ітися ляться [бвмп]ітися # шумітися шумляться (Вони) @ verb:rev:pres:p:3
SFX B іти яться [^бвмпш]іти # входити входяться (Вони) @ verb:rev:pres:p:3
SFX B ітися яться [^бвмпш]ітися # входитися входяться (Вони) @ verb:rev:pres:p:3
SFX B іти ись [^д]іти # скрипіти скрипись (Ти) @ verb:rev:impr:s:2
SFX B ітися ись [^д]ітися # скрипітися скрипись (Ти) @ verb:rev:impr:s:2
SFX B іти ися [^д]іти # скрипіти скрипися (Ти) @ verb:rev:impr:s:2
SFX B ітися ися [^д]ітися # скрипітися скрипися (Ти) @ verb:rev:impr:s:2
SFX B іти імось [^д]іти # скрипіти скрипімось (Ми) @ verb:rev:impr:p:1
SFX B ітися імось [^д]ітися # скрипітися скрипімось (Ми) @ verb:rev:impr:p:1
SFX B іти імося [^д]іти # скрипіти скрипімося (Ми) @ verb:rev:impr:p:1
SFX B ітися імося [^д]ітися # скрипітися скрипімося (Ми) @ verb:rev:impr:p:1
SFX B іти іться [^д]іти # скрипіти скрипіться (Ви) @ verb:rev:impr:s:2
SFX B ітися іться [^д]ітися # скрипітися скрипіться (Ви) @ verb:rev:impr:s:2
SFX B ути усь нути # тягнути тягнусь (Я) @ verb:rev:pres:s:1
SFX B утися усь нутися # тягнутися тягнусь (Я) @ verb:rev:pres:s:1
SFX B ути уся нути # тягнути тягнуся (Я) @ verb:rev:pres:s:1
SFX B утися уся нутися # тягнутися тягнуся (Я) @ verb:rev:pres:s:1
SFX B ути ешся нути # тягнути тягнешся (Ти) @ verb:rev:pres:s:2
SFX B утися ешся нутися # тягнутися тягнешся (Ти) @ verb:rev:pres:s:2
SFX B ути еться нути # тягнути тягнеться (Він) @ verb:rev:pres:s:3
SFX B утися еться нутися # тягнутися тягнеться (Він) @ verb:rev:pres:s:3
SFX B ути емось нути # тягнути тягнемось (Ми) @ verb:rev:pres:p:1
SFX B утися емось нутися # тягнутися тягнемось (Ми) @ verb:rev:pres:p:1
SFX B ути емося нути # тягнути тягнемося (Ми) @ verb:rev:pres:p:1
SFX B утися емося нутися # тягнутися тягнемося (Ми) @ verb:rev:pres:p:1
SFX B ути етесь нути # тягнути тягнетесь (Ви) @ verb:rev:pres:p:2
SFX B утися етесь нутися # тягнутися тягнетесь (Ви) @ verb:rev:pres:p:2
SFX B ути етеся нути # тягнути тягнетеся (Ви) @ verb:rev:pres:p:2
SFX B утися етеся нутися # тягнутися тягнетеся (Ви) @ verb:rev:pres:p:2
SFX B ути уться нути # тягнути тягнуться (Вони) @ verb:rev:pres:p:3
SFX B утися уться нутися # тягнутися тягнуться (Вони) @ verb:rev:pres:p:3
SFX B ти дусь бути # забути забудусь (Я) @ verb:rev:pres:s:1
SFX B тися дусь бутися # забутися забудусь (Я) @ verb:rev:pres:s:1
SFX B ти дуся бути # забути забудуся (Я) @ verb:rev:pres:s:1
SFX B тися дуся бутися # забутися забудуся (Я) @ verb:rev:pres:s:1
SFX B ти дешся бути # забути забудешся (Ти) @ verb:rev:pres:s:2
SFX B тися дешся бутися # забутися забудешся (Ти) @ verb:rev:pres:s:2
SFX B ти деться бути # забути забудеться (Він) @ verb:rev:pres:s:3
SFX B тися деться бутися # забутися забудеться (Він) @ verb:rev:pres:s:3
SFX B ти демось бути # забути забудемось (Ми) @ verb:rev:pres:p:1
SFX B тися демось бутися # забутися забудемось (Ми) @ verb:rev:pres:p:1
SFX B ти демося бути # забути забудемося (Ми) @ verb:rev:pres:p:1
SFX B тися демося бутися # забутися забудемося (Ми) @ verb:rev:pres:p:1
SFX B ти детесь бути # забути забудетесь (Ви) @ verb:rev:pres:p:2
SFX B тися детесь бутися # забутися забудетесь (Ви) @ verb:rev:pres:p:2
SFX B ти детеся бути # забути забудетеся (Ви) @ verb:rev:pres:p:2
SFX B тися детеся бутися # забутися забудетеся (Ви) @ verb:rev:pres:p:2
SFX B ти дуться бути # забути забудуться (Вони) @ verb:rev:pres:p:3
SFX B тися дуться бутися # забутися забудуться (Вони) @ verb:rev:pres:p:3
SFX B ти дься бути # забути забудься (Ти) @ verb:rev:impr:s:2
SFX B тися дься бутися # забутися забудься (Ти) @ verb:rev:impr:s:2
SFX B ти дьмось бути # забути забудьмось (Ми) @ verb:rev:impr:p:1
SFX B тися дьмось бутися # забутися забудьмось (Ми) @ verb:rev:impr:p:1
SFX B ти дьмося бути # забути забудьмося (Ми) @ verb:rev:impr:p:1
SFX B тися дьмося бутися # забутися забудьмося (Ми) @ verb:rev:impr:p:1
SFX B ти дьтесь бути # забути забудьтесь (Ви) @ verb:rev:impr:p:2
SFX B тися дьтесь бутися # забутися забудьтесь (Ви) @ verb:rev:impr:p:2
SFX B ти дьтеся бути # забути забудьтеся (Ви) @ verb:rev:impr:p:2
SFX B тися дьтеся бутися # забутися забудьтеся (Ви) @ verb:rev:impr:p:2
SFX B оти юсь оти # бороти борюсь (Я) @ verb:rev:pres:s:1
SFX B отися юсь отися # боротися борюсь (Я) @ verb:rev:pres:s:1
SFX B оти юся оти # бороти борюся (Я) @ verb:rev:pres:s:1
SFX B отися юся отися # боротися борюся (Я) @ verb:rev:pres:s:1
SFX B оти ешся оти # бороти борешся (Ти) @ verb:rev:pres:s:2
SFX B отися ешся отися # боротися борешся (Ти) @ verb:rev:pres:s:2
SFX B оти еться оти # бороти бореться (Він) @ verb:rev:pres:s:3
SFX B отися еться отися # боротися бореться (Він) @ verb:rev:pres:s:3
SFX B оти емось оти # бороти боремось (Ми) @ verb:rev:pres:p:1
SFX B отися емось отися # боротися боремось (Ми) @ verb:rev:pres:p:1
SFX B оти емося оти # бороти боремося (Ми) @ verb:rev:pres:p:1
SFX B отися емося отися # боротися боремося (Ми) @ verb:rev:pres:p:1
SFX B оти етесь оти # бороти боретесь (Ви) @ verb:rev:pres:p:2
SFX B отися етесь отися # боротися боретесь (Ви) @ verb:rev:pres:p:2
SFX B оти етеся оти # бороти боретеся (Ви) @ verb:rev:pres:p:2
SFX B отися етеся отися # боротися боретеся (Ви) @ verb:rev:pres:p:2
SFX B оти ються оти # бороти борються (Вони) @ verb:rev:pres:p:3
SFX B отися ються отися # боротися борються (Вони) @ verb:rev:pres:p:3
SFX B оти ись оти # бороти борись (Ти) @ verb:rev:impr:s:2
SFX B отися ись отися # боротися борись (Ти) @ verb:rev:impr:s:2
SFX B оти ися оти # бороти борися (Ти) @ verb:rev:impr:s:2
SFX B отися ися отися # боротися борися (Ти) @ verb:rev:impr:s:2
SFX B оти імось оти # бороти борімось (Ми) @ verb:rev:impr:p:1
SFX B отися імось отися # боротися борімось (Ми) @ verb:rev:impr:p:1
SFX B оти імося оти # бороти борімося (Ми) @ verb:rev:impr:p:1
SFX B отися імося отися # боротися борімося (Ми) @ verb:rev:impr:p:1
SFX B оти іться оти # бороти боріться (Ви) @ verb:rev:impr:p:2
SFX B отися іться отися # боротися боріться (Ви) @ verb:rev:impr:p:2
SFX B їти юсь їти # клеїти клеюсь (Я) @ verb:rev:pres:s:1
SFX B їтися юсь їтися # клеїтися клеюсь (Я) @ verb:rev:pres:s:1
SFX B їти юся їти # клеїти клеюся (Я) @ verb:rev:pres:s:1
SFX B їтися юся їтися # клеїтися клеюся (Я) @ verb:rev:pres:s:1
SFX B ти шся їти # клеїти клеїшся (Ти) @ verb:rev:pres:s:2
SFX B тися шся їтися # клеїтися клеїшся (Ти) @ verb:rev:pres:s:2
SFX B ти ться їти # клеїти клеїться (Він) @ verb:rev:pres:s:3
SFX B тися ться їтися # клеїтися клеїться (Він) @ verb:rev:pres:s:3
SFX B ти мось їти # клеїти клеїмось (Ми) @ verb:rev:pres:p:1
SFX B тися мось їтися # клеїтися клеїмось (Ми) @ verb:rev:pres:p:1
SFX B ти мося їти # клеїти клеїмося (Ми) @ verb:rev:pres:p:1
SFX B тися мося їтися # клеїтися клеїмося (Ми) @ verb:rev:pres:p:1
SFX B ти тесь їти # клеїти клеїтесь (Ви) @ verb:rev:pres:p:2
SFX B тися тесь їтися # клеїтися клеїтесь (Ви) @ verb:rev:pres:p:2
SFX B ти теся їти # клеїти клеїтеся (Ви) @ verb:rev:pres:p:2
SFX B тися теся їтися # клеїтися клеїтеся (Ви) @ verb:rev:pres:p:2
SFX B їти яться їти # клеїти клеяться (Вони) @ verb:rev:pres:p:3
SFX B їтися яться їтися # клеїтися клеяться (Вони) @ verb:rev:pres:p:3
SFX B ти усь [збв]ти # везти везусь (Я) @ verb:rev:pres:s:1
SFX B тися усь [збв]тися # везтися везусь (Я) @ verb:rev:pres:s:1
SFX B ти уся [збв]ти # везти везуся (Я) @ verb:rev:pres:s:1
SFX B тися уся [збв]тися # везтися везуся (Я) @ verb:rev:pres:s:1
SFX B ти ешся [збв]ти # везти везешся (Ти) @ verb:rev:pres:s:2
SFX B тися ешся [збв]тися # везтися везешся (Ти) @ verb:rev:pres:s:2
SFX B ти еться [збв]ти # везти везеться (Він) @ verb:rev:pres:s:3
SFX B тися еться [збв]тися # везтися везеться (Він) @ verb:rev:pres:s:3
SFX B ти емось [збв]ти # везти веземось (Ми) @ verb:rev:pres:p:1
SFX B тися емось [збв]тися # везтися веземось (Ми) @ verb:rev:pres:p:1
SFX B ти емося [збв]ти # везти веземося (Ми) @ verb:rev:pres:p:1
SFX B тися емося [збв]тися # везтися веземося (Ми) @ verb:rev:pres:p:1
SFX B ти етесь [збв]ти # везти везетесь (Ви) @ verb:rev:pres:p:2
SFX B тися етесь [збв]тися # везтися везетесь (Ви) @ verb:rev:pres:p:2
SFX B ти етеся [збв]ти # везти везетеся (Ви) @ verb:rev:pres:p:2
SFX B тися етеся [збв]тися # везтися везетеся (Ви) @ verb:rev:pres:p:2
SFX B ти уться [збв]ти # везти везуться (Вони) @ verb:rev:pres:p:3
SFX B тися уться [збв]тися # везтися везуться (Вони) @ verb:rev:pres:p:3
SFX B ебти ібся ебти # гребти грібся (Я, Ти, Він) @ verb:rev:past:m
SFX B ебтися ібся ебтися # гребтися грібся (Я, Ти, Він) @ verb:rev:past:m
SFX B езти ізся езти # везти візся (Я, Ти, Він) @ verb:rev:past:m
SFX B езтися ізся езтися # везтися візся (Я, Ти, Він) @ verb:rev:past:m
SFX B зти зся [^е]зти # гризти гризся (Я, Ти, Він) @ verb:rev:past:m
SFX B зтися зся [^е]зтися # гризтися гризся (Я, Ти, Він) @ verb:rev:past:m
SFX B ти івсь евти # ревти ревівсь (Я, Ти, Він) @ verb:rev:past:m
SFX B тися івсь евтися # ревтися ревівсь (Я, Ти, Він) @ verb:rev:past:m
SFX B ти івся евти # ревти ревівся (Я, Ти, Він) @ verb:rev:past:m
SFX B тися івся евтися # ревтися ревівся (Я, Ти, Він) @ verb:rev:past:m
SFX B вти всь [^е]вти # пливти пливсь (Я, Ти, Він) @ verb:rev:past:m
SFX B втися всь [^е]втися # пливтися пливсь (Я, Ти, Він) @ verb:rev:past:m
SFX B вти вся [^е]вти # пливти плився (Я, Ти, Він) @ verb:rev:past:m
SFX B втися вся [^е]втися # пливтися плився (Я, Ти, Він) @ verb:rev:past:m
SFX B бти бся убти # скубти скубся (Я, Ти, Він) @ verb:rev:past:m
SFX B бтися бся убтися # скубтися скубся (Я, Ти, Він) @ verb:rev:past:m
SFX B ти ься ізти # лізти лізься (Ти) @ verb:rev:impr:s:2
SFX B тися ься ізтися # лізтися лізься (Ти) @ verb:rev:impr:s:2
SFX B ти ись [^і]зти # везти везись (Ти) @ verb:rev:impr:s:2
SFX B тися ись [^і]зтися # везтися везись (Ти) @ verb:rev:impr:s:2
SFX B ти ися [^і]зти # везти везися (Ти) @ verb:rev:impr:s:2
SFX B тися ися [^і]зтися # везтися везися (Ти) @ verb:rev:impr:s:2
SFX B ти ись [бв]ти # везти везись (Ти) @ verb:rev:impr:s:2
SFX B тися ись [бв]тися # везтися везись (Ти) @ verb:rev:impr:s:2
SFX B ти ися [бв]ти # везти везися (Ти) @ verb:rev:impr:s:2
SFX B тися ися [бв]тися # везтися везися (Ти) @ verb:rev:impr:s:2
SFX B ти ьмось ізти # лізти лізьмось (Ми) @ verb:rev:impr:p:1
SFX B тися ьмось ізтися # лізтися лізьмось (Ми) @ verb:rev:impr:p:1
SFX B ти ьмося ізти # лізти лізьмося (Ми) @ verb:rev:impr:p:1
SFX B тися ьмося ізтися # лізтися лізьмося (Ми) @ verb:rev:impr:p:1
SFX B ти імось [^і]зти # везти везімось (Ми) @ verb:rev:impr:p:1
SFX B тися імось [^і]зтися # везтися везімось (Ми) @ verb:rev:impr:p:1
SFX B ти імося [^і]зти # везти везімося (Ми) @ verb:rev:impr:p:1
SFX B тися імося [^і]зтися # везтися везімося (Ми) @ verb:rev:impr:p:1
SFX B ти імось [бв]ти # везти везімось (Ми) @ verb:rev:impr:p:1
SFX B тися імось [бв]тися # везтися везімось (Ми) @ verb:rev:impr:p:1
SFX B ти імося [бв]ти # везти везімося (Ми) @ verb:rev:impr:p:1
SFX B тися імося [бв]тися # везтися везімося (Ми) @ verb:rev:impr:p:1
SFX B ти ьтесь ізти # лізти лізьтесь (Ви) @ verb:rev:impr:p:2
SFX B тися ьтесь ізтися # лізтися лізьтесь (Ви) @ verb:rev:impr:p:2
SFX B ти ьтеся ізти # лізти лізьтеся (Ви) @ verb:rev:impr:p:2
SFX B тися ьтеся ізтися # лізтися лізьтеся (Ви) @ verb:rev:impr:p:2
SFX B ти іться [^і]зти # везти везіться (Ви) @ verb:rev:impr:p:2
SFX B тися іться [^і]зтися # везтися везіться (Ви) @ verb:rev:impr:p:2
SFX B ти іться [бв]ти # везти везіться (Ви) @ verb:rev:impr:p:2
SFX B тися іться [бв]тися # везтися везіться (Ви) @ verb:rev:impr:p:2
SFX B сти лась [^о]сти # плести плелась (Вона) @ verb:rev:past:f
SFX B стися лась [^о]стися # плестися плелась (Вона) @ verb:rev:past:f
SFX B сти лася [^о]сти # плести плелася (Вона) @ verb:rev:past:f
SFX B стися лася [^о]стися # плестися плелася (Вона) @ verb:rev:past:f
SFX B сти лось [^о]сти # плести плелось (Воно) @ verb:rev:past:n
SFX B стися лось [^о]стися # плестися плелось (Воно) @ verb:rev:past:n
SFX B сти лося [^о]сти # плести плелося (Воно) @ verb:rev:past:n
SFX B стися лося [^о]стися # плестися плелося (Воно) @ verb:rev:past:n
SFX B сти лись [^о]сти # плести плелись (Вони) @ verb:rev:past:p:3
SFX B стися лись [^о]стися # плестися плелись (Вони) @ verb:rev:past:p:3
SFX B сти лися [^о]сти # плести плелися (Вони) @ verb:rev:past:p:3
SFX B стися лися [^о]стися # плестися плелися (Вони) @ verb:rev:past:p:3
SFX B сти слась ости # рости рослась (Вона) @ verb:rev:past:f
SFX B стися слась остися # ростися рослась (Вона) @ verb:rev:past:f
SFX B сти слася ости # рости рослася (Вона) @ verb:rev:past:f
SFX B стися слася остися # ростися рослася (Вона) @ verb:rev:past:f
SFX B сти слось ости # рости рослось (Воно) @ verb:rev:past:n
SFX B стися слось остися # ростися рослось (Воно) @ verb:rev:past:n
SFX B сти слося ости # рости рослося (Воно) @ verb:rev:past:n
SFX B стися слося остися # ростися рослося (Воно) @ verb:rev:past:n
SFX B сти слись ости # рости рослись (Вони) @ verb:rev:past:p:3
SFX B стися слись остися # ростися рослись (Вони) @ verb:rev:past:p:3
SFX B сти слися ости # рости рослися (Вони) @ verb:rev:past:p:3
SFX B стися слися остися # ростися рослися (Вони) @ verb:rev:past:p:3
SFX B ти тусь ости # рости ростусь (Я) @ verb:rev:pres:s:1
SFX B тися тусь остися # ростися ростусь (Я) @ verb:rev:pres:s:1
SFX B ти туся ости # рости ростуся (Я) @ verb:rev:pres:s:1
SFX B тися туся остися # ростися ростуся (Я) @ verb:rev:pres:s:1
SFX B ти тешся ости # рости ростешся (Ти) @ verb:rev:pres:s:2
SFX B тися тешся остися # ростися ростешся (Ти) @ verb:rev:pres:s:2
SFX B ти теться ости # рости ростеться (Він) @ verb:rev:pres:s:3
SFX B тися теться остися # ростися ростеться (Він) @ verb:rev:pres:s:3
SFX B ти темось ости # рости ростемось (Ми) @ verb:rev:pres:p:1
SFX B тися темось остися # ростися ростемось (Ми) @ verb:rev:pres:p:1
SFX B ти темося ости # рости ростемося (Ми) @ verb:rev:pres:p:1
SFX B тися темося остися # ростися ростемося (Ми) @ verb:rev:pres:p:1
SFX B ти тетесь ости # рости ростетесь (Ви) @ verb:rev:pres:p:2
SFX B тися тетесь остися # ростися ростетесь (Ви) @ verb:rev:pres:p:2
SFX B ти тетеся ости # рости ростетеся (Ви) @ verb:rev:pres:p:2
SFX B тися тетеся остися # ростися ростетеся (Ви) @ verb:rev:pres:p:2
SFX B ти туться ости # рости ростуться (Вони) @ verb:rev:pres:p:3
SFX B тися туться остися # ростися ростуться (Вони) @ verb:rev:pres:p:3
SFX B сти тусь [еі]сти # цвісти цвітусь (Я) @ verb:rev:pres:s:1
SFX B стися тусь [еі]стися # цвістися цвітусь (Я) @ verb:rev:pres:s:1
SFX B сти туся [еі]сти # цвісти цвітуся (Я) @ verb:rev:pres:s:1
SFX B стися туся [еі]стися # цвістися цвітуся (Я) @ verb:rev:pres:s:1
SFX B сти тешся [еі]сти # цвісти цвітешся (Ти) @ verb:rev:pres:s:2
SFX B стися тешся [еі]стися # цвістися цвітешся (Ти) @ verb:rev:pres:s:2
SFX B сти теться [еі]сти # цвісти цвітеться (Він) @ verb:rev:pres:s:3
SFX B стися теться [еі]стися # цвістися цвітеться (Він) @ verb:rev:pres:s:3
SFX B сти темось [еі]сти # цвісти цвітемось (Ми) @ verb:rev:pres:p:1
SFX B стися темось [еі]стися # цвістися цвітемось (Ми) @ verb:rev:pres:p:1
SFX B сти темося [еі]сти # цвісти цвітемося (Ми) @ verb:rev:pres:p:1
SFX B стися темося [еі]стися # цвістися цвітемося (Ми) @ verb:rev:pres:p:1
SFX B сти тетесь [еі]сти # цвісти цвітетесь (Ви) @ verb:rev:pres:p:2
SFX B стися тетесь [еі]стися # цвістися цвітетесь (Ви) @ verb:rev:pres:p:2
SFX B сти тетеся [еі]сти # цвісти цвітетеся (Ви) @ verb:rev:pres:p:2
SFX B стися тетеся [еі]стися # цвістися цвітетеся (Ви) @ verb:rev:pres:p:2
SFX B сти туться [еі]сти # цвісти цвітуться (Вони) @ verb:rev:pres:p:3
SFX B стися туться [еі]стися # цвістися цвітуться (Вони) @ verb:rev:pres:p:3
SFX B сти нусь лясти # клясти клянусь (Я) @ verb:rev:pres:s:1
SFX B стися нусь лястися # клястися клянусь (Я) @ verb:rev:pres:s:1
SFX B сти нуся лясти # клясти клянуся (Я) @ verb:rev:pres:s:1
SFX B стися нуся лястися # клястися клянуся (Я) @ verb:rev:pres:s:1
SFX B сти нешся лясти # клясти клянешся (Ти) @ verb:rev:pres:s:2
SFX B стися нешся лястися # клястися клянешся (Ти) @ verb:rev:pres:s:2
SFX B сти неться лясти # клясти клянеться (Він) @ verb:rev:pres:s:3
SFX B стися неться лястися # клястися клянеться (Він) @ verb:rev:pres:s:3
SFX B сти немось лясти # клясти клянемось (Ми) @ verb:rev:pres:p:1
SFX B стися немось лястися # клястися клянемось (Ми) @ verb:rev:pres:p:1
SFX B сти немося лясти # клясти клянемося (Ми) @ verb:rev:pres:p:1
SFX B стися немося лястися # клястися клянемося (Ми) @ verb:rev:pres:p:1
SFX B сти нетесь лясти # клясти клянетесь (Ви) @ verb:rev:pres:p:2
SFX B стися нетесь лястися # клястися клянетесь (Ви) @ verb:rev:pres:p:2
SFX B сти нетеся лясти # клясти клянетеся (Ви) @ verb:rev:pres:p:2
SFX B стися нетеся лястися # клястися клянетеся (Ви) @ verb:rev:pres:p:2
SFX B сти нуться лясти # клясти клянуться (Вони) @ verb:rev:pres:p:3
SFX B стися нуться лястися # клястися клянуться (Вони) @ verb:rev:pres:p:3
SFX B сти всь [іяа]сти # прясти прявсь (Я, Ти, Він) @ verb:rev:past:m
SFX B стися всь [іяа]стися # прястися прявсь (Я, Ти, Він) @ verb:rev:past:m
SFX B сти вся [іяа]сти # прясти прявся (Я, Ти, Він) @ verb:rev:past:m
SFX B стися вся [іяа]стися # прястися прявся (Я, Ти, Він) @ verb:rev:past:m
SFX B ести івсь ести # плести плівсь (Я, Ти, Він) @ verb:rev:past:m
SFX B естися івсь естися # плестися плівсь (Я, Ти, Він) @ verb:rev:past:m
SFX B ести івся ести # плести плівся (Я, Ти, Він) @ verb:rev:past:m
SFX B естися івся естися # плестися плівся (Я, Ти, Він) @ verb:rev:past:m
SFX B ости ісся ости # рости рісся (Я, Ти, Він) @ verb:rev:past:m
SFX B остися ісся остися # ростися рісся (Я, Ти, Він) @ verb:rev:past:m
SFX B сти вшись [ія]сти # прясти прявшись @ adp:rev:perf
SFX B стися вшись [ія]стися # прястися прявшись @ adp:rev:perf
SFX B сти тись [еі]сти # плести плетись (Ти) @ verb:rev:impr:s:2
SFX B стися тись [еі]стися # плестися плетись (Ти) @ verb:rev:impr:s:2
SFX B сти тися [еі]сти # плести плетися (Ти) @ verb:rev:impr:s:2
SFX B стися тися [еі]стися # плестися плетися (Ти) @ verb:rev:impr:s:2
SFX B сти тімось [еі]сти # плести плетімось (Ми) @ verb:rev:impr:p:1
SFX B стися тімось [еі]стися # плестися плетімось (Ми) @ verb:rev:impr:p:1
SFX B сти тімося [еі]сти # плести плетімося (Ми) @ verb:rev:impr:p:1
SFX B стися тімося [еі]стися # плестися плетімося (Ми) @ verb:rev:impr:p:1
SFX B сти тіться [еі]сти # плести плетіться (Ви) @ verb:rev:impr:p:2
SFX B стися тіться [еі]стися # плестися плетіться (Ви) @ verb:rev:impr:p:2
SFX B сти нись лясти # клясти клянись (Ти) @ verb:rev:impr:s:2
SFX B стися нись лястися # клястися клянись (Ти) @ verb:rev:impr:s:2
SFX B сти нися лясти # клясти клянися (Ти) @ verb:rev:impr:s:2
SFX B стися нися лястися # клястися клянися (Ти) @ verb:rev:impr:s:2
SFX B сти німось лясти # клясти клянімось (Ми) @ verb:rev:impr:p:1
SFX B стися німось лястися # клястися клянімось (Ми) @ verb:rev:impr:p:1
SFX B сти німося лясти # клясти клянімося (Ми) @ verb:rev:impr:p:1
SFX B стися німося лястися # клястися клянімося (Ми) @ verb:rev:impr:p:1
SFX B сти ніться лясти # клясти кляніться (Ви) @ verb:rev:impr:p:2
SFX B стися ніться лястися # клястися кляніться (Ви) @ verb:rev:impr:p:2
SFX B и імось ости # рости ростімось (Ми) @ verb:rev:impr:p:1
SFX B ися імось остися # ростися ростімось (Ми) @ verb:rev:impr:p:1
SFX B и імося ости # рости ростімося (Ми) @ verb:rev:impr:p:1
SFX B ися імося остися # ростися ростімося (Ми) @ verb:rev:impr:p:1
SFX B и іться ости # рости ростіться (Ви) @ verb:rev:impr:p:2
SFX B ися іться остися # ростися ростіться (Ви) @ verb:rev:impr:p:2
SFX B кти чусь кти # текти течусь (Я) @ verb:rev:pres:s:1
SFX B ктися чусь ктися # тектися течусь (Я) @ verb:rev:pres:s:1
SFX B кти чуся кти # текти течуся (Я) @ verb:rev:pres:s:1
SFX B ктися чуся ктися # тектися течуся (Я) @ verb:rev:pres:s:1
SFX B кти чешся кти # текти течешся (Ти) @ verb:rev:pres:s:2
SFX B ктися чешся ктися # тектися течешся (Ти) @ verb:rev:pres:s:2
SFX B кти четься кти # текти течеться (Він) @ verb:rev:pres:s:3
SFX B ктися четься ктися # тектися течеться (Він) @ verb:rev:pres:s:3
SFX B кти чемось кти # текти течемось (Ми) @ verb:rev:pres:p:1
SFX B ктися чемось ктися # тектися течемось (Ми) @ verb:rev:pres:p:1
SFX B кти чемося кти # текти течемося (Ми) @ verb:rev:pres:p:1
SFX B ктися чемося ктися # тектися течемося (Ми) @ verb:rev:pres:p:1
SFX B кти четесь кти # текти течетесь (Ви) @ verb:rev:pres:p:2
SFX B ктися четесь ктися # тектися течетесь (Ви) @ verb:rev:pres:p:2
SFX B кти четеся кти # текти течетеся (Ви) @ verb:rev:pres:p:2
SFX B ктися четеся ктися # тектися течетеся (Ви) @ verb:rev:pres:p:2
SFX B кти чуться кти # текти течуться (Вони) @ verb:rev:pres:p:3
SFX B ктися чуться ктися # тектися течуться (Вони) @ verb:rev:pres:p:3
SFX B екти ікся екти # текти тікся (Я, Ти, Він) @ verb:rev:past:m
SFX B ектися ікся ектися # тектися тікся (Я, Ти, Він) @ verb:rev:past:m
SFX B ікти ікся ікти # одсікти одсікся (Я, Ти, Він) @ verb:rev:past:m
SFX B іктися ікся іктися # одсіктися одсікся (Я, Ти, Він) @ verb:rev:past:m
SFX B окти ікся окти # волокти волікся (Я, Ти, Він) @ verb:rev:past:m
SFX B октися ікся октися # волоктися волікся (Я, Ти, Він) @ verb:rev:past:m
SFX B вкти вкся вкти # товкти товкся (Я, Ти, Він) @ verb:rev:past:m
SFX B вктися вкся вктися # товктися товкся (Я, Ти, Він) @ verb:rev:past:m
SFX B екти ікшись екти # текти тіксь @ adp:rev:perf
SFX B ектися ікшись ектися # тектися тіксь @ adp:rev:perf
SFX B ікти ікшись ікти # одсікти одсіксь @ adp:rev:perf
SFX B іктися ікшись іктися # одсіктися одсіксь @ adp:rev:perf
SFX B окти ікшись окти # волокти воліксь @ adp:rev:perf
SFX B октися ікшись октися # волоктися воліксь @ adp:rev:perf
SFX B вкти вкшись вкти # товкти товксь @ adp:rev:perf
SFX B вктися вкшись вктися # товктися товксь @ adp:rev:perf
SFX B кти чись кти # текти течись (Ти) @ verb:rev:impr:s:2
SFX B ктися чись ктися # тектися течись (Ти) @ verb:rev:impr:s:2
SFX B кти чися кти # текти течися (Ти) @ verb:rev:impr:s:2
SFX B ктися чися ктися # тектися течися (Ти) @ verb:rev:impr:s:2
SFX B кти чімось кти # текти течімось (Ми) @ verb:rev:impr:p:1
SFX B ктися чімось ктися # тектися течімось (Ми) @ verb:rev:impr:p:1
SFX B кти чімося кти # текти течімося (Ми) @ verb:rev:impr:p:1
SFX B ктися чімося ктися # тектися течімося (Ми) @ verb:rev:impr:p:1
SFX B кти чіться кти # текти течіться (Ви) @ verb:rev:impr:p:2
SFX B ктися чіться ктися # тектися течіться (Ви) @ verb:rev:impr:p:2
SFX B гти жусь [еоиія]гти # допомогти допоможусь (Я) @ verb:rev:pres:s:1
SFX B гтися жусь [еоиія]гтися # допомогтися допоможусь (Я) @ verb:rev:pres:s:1
SFX B гти жуся [еоиія]гти # допомогти допоможуся (Я) @ verb:rev:pres:s:1
SFX B гтися жуся [еоиія]гтися # допомогтися допоможуся (Я) @ verb:rev:pres:s:1
SFX B гти жишся ігти # бігти біжишся (Ти) @ verb:rev:pres:s:2
SFX B гтися жишся ігтися # бігтися біжишся (Ти) @ verb:rev:pres:s:2
SFX B гти жешся [еоия]гти # допомогти допоможешся (Ти) @ verb:rev:pres:s:2
SFX B гтися жешся [еоия]гтися # допомогтися допоможешся (Ти) @ verb:rev:pres:s:2
SFX B гти житься ігти # бігти біжиться (Він) @ verb:rev:pres:s:3
SFX B гтися житься ігтися # бігтися біжиться (Він) @ verb:rev:pres:s:3
SFX B гти жеться [еоия]гти # допомогти допоможеться (Він) @ verb:rev:pres:s:3
SFX B гтися жеться [еоия]гтися # допомогтися допоможеться (Він) @ verb:rev:pres:s:3
SFX B гти жимось ігти # бігти біжимось (Ми) @ verb:rev:pres:p:1
SFX B гтися жимось ігтися # бігтися біжимось (Ми) @ verb:rev:pres:p:1
SFX B гти жимося ігти # бігти біжимося (Ми) @ verb:rev:pres:p:1
SFX B гтися жимося ігтися # бігтися біжимося (Ми) @ verb:rev:pres:p:1
SFX B гти жемось [еоия]гти # допомогти допоможемось (Ми) @ verb:rev:pres:p:1
SFX B гтися жемось [еоия]гтися # допомогтися допоможемось (Ми) @ verb:rev:pres:p:1
SFX B гти жемося [еоия]гти # допомогти допоможемося (Ми) @ verb:rev:pres:p:1
SFX B гтися жемося [еоия]гтися # допомогтися допоможемося (Ми) @ verb:rev:pres:p:1
SFX B гти житесь ігти # бігти біжитесь (Ви) @ verb:rev:pres:p:2
SFX B гтися житесь ігтися # бігтися біжитесь (Ви) @ verb:rev:pres:p:2
SFX B гти житеся ігти # бігти біжитеся (Ви) @ verb:rev:pres:p:2
SFX B гтися житеся ігтися # бігтися біжитеся (Ви) @ verb:rev:pres:p:2
SFX B гти жетесь [еоия]гти # допомогти допоможетесь (Ви) @ verb:rev:pres:p:2
SFX B гтися жетесь [еоия]гтися # допомогтися допоможетесь (Ви) @ verb:rev:pres:p:2
SFX B гти жетеся [еоия]гти # допомогти допоможетеся (Ви) @ verb:rev:pres:p:2
SFX B гтися жетеся [еоия]гтися # допомогтися допоможетеся (Ви) @ verb:rev:pres:p:2
SFX B гти жаться ігти # бігти біжаться (Вони) @ verb:rev:pres:p:3
SFX B гтися жаться ігтися # бігтися біжаться (Вони) @ verb:rev:pres:p:3
SFX B гти жуться [еоия]гти # допомогти допоможуться (Вони) @ verb:rev:pres:p:3
SFX B гтися жуться [еоия]гтися # допомогтися допоможуться (Вони) @ verb:rev:pres:p:3
SFX B егти ігся егти # зберегти зберігся (Я, Ти, Він) @ verb:rev:past:m
SFX B егтися ігся егтися # зберегтися зберігся (Я, Ти, Він) @ verb:rev:past:m
SFX B огти ігся огти # допомогти допомігся (Я, Ти, Він) @ verb:rev:past:m
SFX B огтися ігся огтися # допомогтися допомігся (Я, Ти, Він) @ verb:rev:past:m
SFX B ягти ігся ягти # лягти лігся (Я, Ти, Він) @ verb:rev:past:m
SFX B ягтися ігся ягтися # лягтися лігся (Я, Ти, Він) @ verb:rev:past:m
SFX B гти гся [иі]гти # стригти стригся біг (Я, Ти, Він) @ verb:rev:past:m
SFX B гтися гся [иі]гтися # стригтися стригся біг (Я, Ти, Він) @ verb:rev:past:m
SFX B егти ігшись егти # зберегти зберігшись @ adp:rev:perf
SFX B егтися ігшись егтися # зберегтися зберігшись @ adp:rev:perf
SFX B огти ігшись огти # допомогти допомігшись @ adp:rev:perf
SFX B огтися ігшись огтися # допомогтися допомігшись @ adp:rev:perf
SFX B ягти ігшись ягти # лягти лігшись @ adp:rev:perf
SFX B ягтися ігшись ягтися # лягтися лігшись @ adp:rev:perf
SFX B гти гшись [иі]гти # стригти стригшись @ adp:rev:perf
SFX B гтися гшись [иі]гтися # стригтися стригшись @ adp:rev:perf
SFX B гти жся лягти # лягти ляжся (Ти) @ verb:rev:impr:s:2
SFX B гтися жся лягтися # лягтися ляжся (Ти) @ verb:rev:impr:s:2
SFX B гти жись рягти # запрягти запряжись (Ти) @ verb:rev:impr:s:2
SFX B гтися жись рягтися # запрягтися запряжись (Ти) @ verb:rev:impr:s:2
SFX B гти жися рягти # запрягти запряжися (Ти) @ verb:rev:impr:s:2
SFX B гтися жися рягтися # запрягтися запряжися (Ти) @ verb:rev:impr:s:2
SFX B гти жись [еоіи]гти # допомогти допоможись (Ти) @ verb:rev:impr:s:2
SFX B гтися жись [еоіи]гтися # допомогтися допоможись (Ти) @ verb:rev:impr:s:2
SFX B гти жися [еоіи]гти # допомогти допоможися (Ти) @ verb:rev:impr:s:2
SFX B гтися жися [еоіи]гтися # допомогтися допоможися (Ти) @ verb:rev:impr:s:2
SFX B гти жмось лягти # лягти ляжмось (Ми) @ verb:rev:impr:p:1
SFX B гтися жмось лягтися # лягтися ляжмось (Ми) @ verb:rev:impr:p:1
SFX B гти жмося лягти # лягти ляжмося (Ми) @ verb:rev:impr:p:1
SFX B гтися жмося лягтися # лягтися ляжмося (Ми) @ verb:rev:impr:p:1
SFX B гти жімось рягти # запрягти запряжімось (Ми) @ verb:rev:impr:p:1
SFX B гтися жімось рягтися # запрягтися запряжімось (Ми) @ verb:rev:impr:p:1
SFX B гти жімося рягти # запрягти запряжімося (Ми) @ verb:rev:impr:p:1
SFX B гтися жімося рягтися # запрягтися запряжімося (Ми) @ verb:rev:impr:p:1
SFX B гти жімось [еоіи]гти # допомогти допоможімось (Ми) @ verb:rev:impr:p:1
SFX B гтися жімось [еоіи]гтися # допомогтися допоможімось (Ми) @ verb:rev:impr:p:1
SFX B гти жімося [еоіи]гти # допомогти допоможімося (Ми) @ verb:rev:impr:p:1
SFX B гтися жімося [еоіи]гтися # допомогтися допоможімося (Ми) @ verb:rev:impr:p:1
SFX B гти жтесь лягти # лягти ляжтесь (Ви) @ verb:rev:impr:p:2
SFX B гтися жтесь лягтися # лягтися ляжтесь (Ви) @ verb:rev:impr:p:2
SFX B гти жтеся лягти # лягти ляжтеся (Ви) @ verb:rev:impr:p:2
SFX B гтися жтеся лягтися # лягтися ляжтеся (Ви) @ verb:rev:impr:p:2
SFX B гти жіться рягти # запрягти запряжіться (Ви) @ verb:rev:impr:p:2
SFX B гтися жіться рягтися # запрягтися запряжіться (Ви) @ verb:rev:impr:p:2
SFX B гти жіться [еоіи]гти # допомогти допоможіться (Ви) @ verb:rev:impr:p:2
SFX B гтися жіться [еоіи]гтися # допомогтися допоможіться (Ви) @ verb:rev:impr:p:2
SFX B ерти русь [^дж]ерти # терти трусь (Я) @ verb:rev:pres:s:1
SFX B ертися русь [^дж]ертися # тертися трусь (Я) @ verb:rev:pres:s:1
SFX B ерти руся [^дж]ерти # терти труся (Я) @ verb:rev:pres:s:1
SFX B ертися руся [^дж]ертися # тертися труся (Я) @ verb:rev:pres:s:1
SFX B ерти решся [^дж]ерти # терти трешся (Ти) @ verb:rev:pres:s:2
SFX B ертися решся [^дж]ертися # тертися трешся (Ти) @ verb:rev:pres:s:2
SFX B ерти реться [^дж]ерти # терти треться (Він) @ verb:rev:pres:s:3
SFX B ертися реться [^дж]ертися # тертися треться (Він) @ verb:rev:pres:s:3
SFX B ерти ремось [^дж]ерти # терти тремось (Ми) @ verb:rev:pres:p:1
SFX B ертися ремось [^дж]ертися # тертися тремось (Ми) @ verb:rev:pres:p:1
SFX B ерти ремося [^дж]ерти # терти тремося (Ми) @ verb:rev:pres:p:1
SFX B ертися ремося [^дж]ертися # тертися тремося (Ми) @ verb:rev:pres:p:1
SFX B ерти ретесь [^дж]ерти # терти третесь (Ви) @ verb:rev:pres:p:2
SFX B ертися ретесь [^дж]ертися # тертися третесь (Ви) @ verb:rev:pres:p:2
SFX B ерти ретеся [^дж]ерти # терти третеся (Ви) @ verb:rev:pres:p:2
SFX B ертися ретеся [^дж]ертися # тертися третеся (Ви) @ verb:rev:pres:p:2
SFX B ерти руться [^дж]ерти # терти труться (Вони) @ verb:rev:pres:p:3
SFX B ертися руться [^дж]ертися # тертися труться (Вони) @ verb:rev:pres:p:3
SFX B рти рся рти # терти терся (Я, Ти, Він) @ verb:rev:past:m
SFX B ртися рся ртися # тертися терся (Я, Ти, Він) @ verb:rev:past:m
SFX B рти ршись рти # терти тершись @ adp:rev:perf
SFX B ртися ршись ртися # тертися тершись @ adp:rev:perf
SFX B ерти рись [^дж]ерти # терти трись (Ти) @ verb:rev:impr:s:2
SFX B ертися рись [^дж]ертися # тертися трись (Ти) @ verb:rev:impr:s:2
SFX B ерти рися [^дж]ерти # терти трися (Ти) @ verb:rev:impr:s:2
SFX B ертися рися [^дж]ертися # тертися трися (Ти) @ verb:rev:impr:s:2
SFX B ерти рімось [^дж]ерти # терти трімось (Ми) @ verb:rev:impr:p:1
SFX B ертися рімось [^дж]ертися # тертися трімось (Ми) @ verb:rev:impr:p:1
SFX B ерти рімося [^дж]ерти # терти трімося (Ми) @ verb:rev:impr:p:1
SFX B ертися рімося [^дж]ертися # тертися трімося (Ми) @ verb:rev:impr:p:1
SFX B ерти ріться [^дж]ерти # терти тріться (Ви) @ verb:rev:impr:p:2
SFX B ертися ріться [^дж]ертися # тертися тріться (Ви) @ verb:rev:impr:p:2
SFX B рти русь [дж]ерти # жерти жерусь (Я) @ verb:rev:pres:s:1
SFX B ртися русь [дж]ертися # жертися жерусь (Я) @ verb:rev:pres:s:1
SFX B рти руся [дж]ерти # жерти жеруся (Я) @ verb:rev:pres:s:1
SFX B ртися руся [дж]ертися # жертися жеруся (Я) @ verb:rev:pres:s:1
SFX B рти решся [дж]ерти # жерти жерешся (Ти) @ verb:rev:pres:s:2
SFX B ртися решся [дж]ертися # жертися жерешся (Ти) @ verb:rev:pres:s:2
SFX B рти реться [дж]ерти # жерти жереться (Він) @ verb:rev:pres:s:3
SFX B ртися реться [дж]ертися # жертися жереться (Він) @ verb:rev:pres:s:3
SFX B рти ремось [дж]ерти # жерти жеремось (Ми) @ verb:rev:pres:p:1
SFX B ртися ремось [дж]ертися # жертися жеремось (Ми) @ verb:rev:pres:p:1
SFX B рти ремося [дж]ерти # жерти жеремося (Ми) @ verb:rev:pres:p:1
SFX B ртися ремося [дж]ертися # жертися жеремося (Ми) @ verb:rev:pres:p:1
SFX B рти ретесь [дж]ерти # жерти жеретесь (Ви) @ verb:rev:pres:p:2
SFX B ртися ретесь [дж]ертися # жертися жеретесь (Ви) @ verb:rev:pres:p:2
SFX B рти ретеся [дж]ерти # жерти жеретеся (Ви) @ verb:rev:pres:p:2
SFX B ртися ретеся [дж]ертися # жертися жеретеся (Ви) @ verb:rev:pres:p:2
SFX B рти руться [дж]ерти # жерти жеруться (Вони) @ verb:rev:pres:p:3
SFX B ртися руться [дж]ертися # жертися жеруться (Вони) @ verb:rev:pres:p:3
SFX B ти ись [дж]ерти # жерти жерись (Ти) @ verb:rev:impr:s:2
SFX B тися ись [дж]ертися # жертися жерись (Ти) @ verb:rev:impr:s:2
SFX B ти ися [дж]ерти # жерти жерися (Ти) @ verb:rev:impr:s:2
SFX B тися ися [дж]ертися # жертися жерися (Ти) @ verb:rev:impr:s:2
SFX B ти імось [дж]ерти # жерти жерімось (Ми) @ verb:rev:impr:p:1
SFX B тися імось [дж]ертися # жертися жерімось (Ми) @ verb:rev:impr:p:1
SFX B ти імося [дж]ерти # жерти жерімося (Ми) @ verb:rev:impr:p:1
SFX B тися імося [дж]ертися # жертися жерімося (Ми) @ verb:rev:impr:p:1
SFX B ти іться [дж]ерти # жерти жеріться (Ви) @ verb:rev:impr:p:2
SFX B тися іться [дж]ертися # жертися жеріться (Ви) @ verb:rev:impr:p:2
SFX B ти юсь [аі]яти # паяти паяюсь (Я) @ verb:rev:pres:s:1
SFX B тися юсь [аі]ятися # паятися паяюсь (Я) @ verb:rev:pres:s:1
SFX B ти юся [аі]яти # паяти паяюся (Я) @ verb:rev:pres:s:1
SFX B тися юся [аі]ятися # паятися паяюся (Я) @ verb:rev:pres:s:1
SFX B ти єшся [аі]яти # паяти паяєшся (Ти) @ verb:rev:pres:s:2
SFX B тися єшся [аі]ятися # паятися паяєшся (Ти) @ verb:rev:pres:s:2
SFX B ти ється [аі]яти # паяти паяється (Він) @ verb:rev:pres:s:3
SFX B тися ється [аі]ятися # паятися паяється (Він) @ verb:rev:pres:s:3
SFX B ти ємось [аі]яти # паяти паяємось (Ми) @ verb:rev:pres:p:1
SFX B тися ємось [аі]ятися # паятися паяємось (Ми) @ verb:rev:pres:p:1
SFX B ти ємося [аі]яти # паяти паяємося (Ми) @ verb:rev:pres:p:1
SFX B тися ємося [аі]ятися # паятися паяємося (Ми) @ verb:rev:pres:p:1
SFX B ти єтесь [аі]яти # паяти паяєтесь (Ви) @ verb:rev:pres:p:2
SFX B тися єтесь [аі]ятися # паятися паяєтесь (Ви) @ verb:rev:pres:p:2
SFX B ти єтеся [аі]яти # паяти паяєтеся (Ви) @ verb:rev:pres:p:2
SFX B тися єтеся [аі]ятися # паятися паяєтеся (Ви) @ verb:rev:pres:p:2
SFX B ти ються [аі]яти # паяти паяються (Вони) @ verb:rev:pres:p:3
SFX B тися ються [аі]ятися # паятися паяються (Вони) @ verb:rev:pres:p:3
SFX B ти йсь [аі]яти # паяти паяйсь (Ти) @ verb:rev:impr:s:2
SFX B тися йсь [аі]ятися # паятися паяйсь (Ти) @ verb:rev:impr:s:2
SFX B ти йся [аі]яти # паяти паяйся (Ти) @ verb:rev:impr:s:2
SFX B тися йся [аі]ятися # паятися паяйся (Ти) @ verb:rev:impr:s:2
SFX B ти ймось [аі]яти # паяти паяймось (Ми) @ verb:rev:impr:p:1
SFX B тися ймось [аі]ятися # паятися паяймось (Ми) @ verb:rev:impr:p:1
SFX B ти ймося [аі]яти # паяти паяймося (Ми) @ verb:rev:impr:p:1
SFX B тися ймося [аі]ятися # паятися паяймося (Ми) @ verb:rev:impr:p:1
SFX B ти йтесь [аі]яти # паяти паяйтесь (Ви) @ verb:rev:impr:p:2
SFX B тися йтесь [аі]ятися # паятися паяйтесь (Ви) @ verb:rev:impr:p:2
SFX B ти йтеся [аі]яти # паяти паяйтеся (Ви) @ verb:rev:impr:p:2
SFX B тися йтеся [аі]ятися # паятися паяйтеся (Ви) @ verb:rev:impr:p:2
SFX B зяти ізьмусь взяти # взяти візьмусь (Я) @ verb:rev:futr:s:1
SFX B зятися ізьмусь взятися # взятися візьмусь (Я) @ verb:rev:futr:s:1
SFX B зяти ізьмуся взяти # взяти візьмуся (Я) @ verb:rev:futr:s:1
SFX B зятися ізьмуся взятися # взятися візьмуся (Я) @ verb:rev:futr:s:1
SFX B зяти ізьмешся взяти # взяти візьмешся (Ти) @ verb:rev:futr:s:2
SFX B зятися ізьмешся взятися # взятися візьмешся (Ти) @ verb:rev:futr:s:2
SFX B зяти ізьметься взяти # взяти візьметься (Він) @ verb:rev:futr:s:3
SFX B зятися ізьметься взятися # взятися візьметься (Він) @ verb:rev:futr:s:3
SFX B зяти ізьмемось взяти # взяти візьмемось (Ми) @ verb:rev:futr:p:1
SFX B зятися ізьмемось взятися # взятися візьмемось (Ми) @ verb:rev:futr:p:1
SFX B зяти ізьмемося взяти # взяти візьмемося (Ми) @ verb:rev:futr:p:1
SFX B зятися ізьмемося взятися # взятися візьмемося (Ми) @ verb:rev:futr:p:1
SFX B зяти ізьметесь взяти # взяти візьметесь (Ви) @ verb:rev:futr:p:2
SFX B зятися ізьметесь взятися # взятися візьметесь (Ви) @ verb:rev:futr:p:2
SFX B зяти ізьметеся взяти # взяти візьметеся (Ви) @ verb:rev:futr:p:2
SFX B зятися ізьметеся взятися # взятися візьметеся (Ви) @ verb:rev:futr:p:2
SFX B зяти ізьмуться взяти # взяти візьмуться (Вони) @ verb:rev:futr:p:3
SFX B зятися ізьмуться взятися # взятися візьмуться (Вони) @ verb:rev:futr:p:3
SFX B зяти ізьмись взяти # взяти візьмись (Ти) @ verb:rev:impr:s:2
SFX B зятися ізьмись взятися # взятися візьмись (Ти) @ verb:rev:impr:s:2
SFX B зяти ізьмися взяти # взяти візьмися (Ти) @ verb:rev:impr:s:2
SFX B зятися ізьмися взятися # взятися візьмися (Ти) @ verb:rev:impr:s:2
SFX B зяти ізьмімось взяти # взяти візьмімось (Ми) @ verb:rev:impr:p:1
SFX B зятися ізьмімось взятися # взятися візьмімось (Ми) @ verb:rev:impr:p:1
SFX B зяти ізьмімося взяти # взяти візьмімося (Ми) @ verb:rev:impr:p:1
SFX B зятися ізьмімося взятися # взятися візьмімося (Ми) @ verb:rev:impr:p:1
SFX B зяти ізьміться взяти # взяти візьміться (Ви) @ verb:rev:impr:p:2
SFX B зятися ізьміться взятися # взятися візьміться (Ви) @ verb:rev:impr:p:2
SFX B няти мусь йняти # зайняти займусь (Я) @ verb:rev:futr:s:1
SFX B нятися мусь йнятися # зайнятися займусь (Я) @ verb:rev:futr:s:1
SFX B няти муся йняти # зайняти займуся (Я) @ verb:rev:futr:s:1
SFX B нятися муся йнятися # зайнятися займуся (Я) @ verb:rev:futr:s:1
SFX B няти мешся йняти # зайняти займешся (Ти) @ verb:rev:futr:s:2
SFX B нятися мешся йнятися # зайнятися займешся (Ти) @ verb:rev:futr:s:2
SFX B няти меться йняти # зайняти займеться (Він) @ verb:rev:futr:s:3
SFX B нятися меться йнятися # зайнятися займеться (Він) @ verb:rev:futr:s:3
SFX B няти мемось йняти # зайняти займемось (Ми) @ verb:rev:futr:p:1
SFX B нятися мемось йнятися # зайнятися займемось (Ми) @ verb:rev:futr:p:1
SFX B няти мемося йняти # зайняти займемося (Ми) @ verb:rev:futr:p:1
SFX B нятися мемося йнятися # зайнятися займемося (Ми) @ verb:rev:futr:p:1
SFX B няти метесь йняти # зайняти займетесь (Ви) @ verb:rev:futr:p:2
SFX B нятися метесь йнятися # зайнятися займетесь (Ви) @ verb:rev:futr:p:2
SFX B няти метеся йняти # зайняти займетеся (Ви) @ verb:rev:futr:p:2
SFX B нятися метеся йнятися # зайнятися займетеся (Ви) @ verb:rev:futr:p:2
SFX B няти муться йняти # зайняти займуться (Вони) @ verb:rev:futr:p:3
SFX B нятися муться йнятися # зайнятися займуться (Вони) @ verb:rev:futr:p:3
SFX B няти мись йняти # зайняти займись (Ти) @ verb:rev:impr:s:2
SFX B нятися мись йнятися # зайнятися займись (Ти) @ verb:rev:impr:s:2
SFX B няти мися йняти # зайняти займися (Ти) @ verb:rev:impr:s:2
SFX B нятися мися йнятися # зайнятися займися (Ти) @ verb:rev:impr:s:2
SFX B няти мімось йняти # зайняти займімось (Ми) @ verb:rev:impr:p:1
SFX B нятися мімось йнятися # зайнятися займімось (Ми) @ verb:rev:impr:p:1
SFX B няти мімося йняти # зайняти займімося (Ми) @ verb:rev:impr:p:1
SFX B нятися мімося йнятися # зайнятися займімося (Ми) @ verb:rev:impr:p:1
SFX B няти міться йняти # зайняти займіться (Ви) @ verb:rev:impr:p:2
SFX B нятися міться йнятися # зайнятися займіться (Ви) @ verb:rev:impr:p:2
SFX B яти імусь [здб]няти # підняти піднімусь (Я) @ verb:rev:futr:s:1
SFX B ятися імусь [здб]нятися # піднятися піднімусь (Я) @ verb:rev:futr:s:1
SFX B яти імуся [здб]няти # підняти піднімуся (Я) @ verb:rev:futr:s:1
SFX B ятися імуся [здб]нятися # піднятися піднімуся (Я) @ verb:rev:futr:s:1
SFX B яти імешся [здб]няти # підняти піднімешся (Ти) @ verb:rev:futr:s:2
SFX B ятися імешся [здб]нятися # піднятися піднімешся (Ти) @ verb:rev:futr:s:2
SFX B яти іметься [здб]няти # підняти підніметься (Він) @ verb:rev:futr:s:3
SFX B ятися іметься [здб]нятися # піднятися підніметься (Він) @ verb:rev:futr:s:3
SFX B яти імемось [здб]няти # підняти піднімемось (Ми) @ verb:rev:futr:p:1
SFX B ятися імемось [здб]нятися # піднятися піднімемось (Ми) @ verb:rev:futr:p:1
SFX B яти імемося [здб]няти # підняти піднімемося (Ми) @ verb:rev:futr:p:1
SFX B ятися імемося [здб]нятися # піднятися піднімемося (Ми) @ verb:rev:futr:p:1
SFX B яти іметесь [здб]няти # підняти підніметесь (Ви) @ verb:rev:futr:p:2
SFX B ятися іметесь [здб]нятися # піднятися підніметесь (Ви) @ verb:rev:futr:p:2
SFX B яти іметеся [здб]няти # підняти підніметеся (Ви) @ verb:rev:futr:p:2
SFX B ятися іметеся [здб]нятися # піднятися підніметеся (Ви) @ verb:rev:futr:p:2
SFX B яти імуться [здб]няти # підняти піднімуться (Вони) @ verb:rev:futr:p:3
SFX B ятися імуться [здб]нятися # піднятися піднімуться (Вони) @ verb:rev:futr:p:3
SFX B яти імись [здб]няти # підняти піднімись (Ти) @ verb:rev:impr:s:2
SFX B ятися імись [здб]нятися # піднятися піднімись (Ти) @ verb:rev:impr:s:2
SFX B яти імися [здб]няти # підняти піднімися (Ти) @ verb:rev:impr:s:2
SFX B ятися імися [здб]нятися # піднятися піднімися (Ти) @ verb:rev:impr:s:2
SFX B яти імімось [здб]няти # підняти піднімімось (Ми) @ verb:rev:impr:p:1
SFX B ятися імімось [здб]нятися # піднятися піднімімось (Ми) @ verb:rev:impr:p:1
SFX B яти імімося [здб]няти # підняти піднімімося (Ми) @ verb:rev:impr:p:1
SFX B ятися імімося [здб]нятися # піднятися піднімімося (Ми) @ verb:rev:impr:p:1
SFX B яти іміться [здб]няти # підняти підніміться (Ви) @ verb:rev:impr:p:2
SFX B ятися іміться [здб]нятися # піднятися підніміться (Ви) @ verb:rev:impr:p:2
SFX B 'яти нусь 'яти # зім'яти зімнусь (Я) @ verb:rev:futr:s:1
SFX B 'ятися нусь 'ятися # зім'ятися зімнусь (Я) @ verb:rev:futr:s:1
SFX B 'яти нуся 'яти # зім'яти зімнуся (Я) @ verb:rev:futr:s:1
SFX B 'ятися нуся 'ятися # зім'ятися зімнуся (Я) @ verb:rev:futr:s:1
SFX B 'яти нешся 'яти # зім'яти зімнешся (Ти) @ verb:rev:futr:s:2
SFX B 'ятися нешся 'ятися # зім'ятися зімнешся (Ти) @ verb:rev:futr:s:2
SFX B 'яти неться 'яти # зім'яти зімнеться (Він) @ verb:rev:futr:s:3
SFX B 'ятися неться 'ятися # зім'ятися зімнеться (Він) @ verb:rev:futr:s:3
SFX B 'яти немось 'яти # зім'яти зімнемось (Ми) @ verb:rev:futr:p:1
SFX B 'ятися немось 'ятися # зім'ятися зімнемось (Ми) @ verb:rev:futr:p:1
SFX B 'яти немося 'яти # зім'яти зімнемося (Ми) @ verb:rev:futr:p:1
SFX B 'ятися немося 'ятися # зім'ятися зімнемося (Ми) @ verb:rev:futr:p:1
SFX B 'яти нетесь 'яти # зім'яти зімнетесь (Ви) @ verb:rev:futr:p:2
SFX B 'ятися нетесь 'ятися # зім'ятися зімнетесь (Ви) @ verb:rev:futr:p:2
SFX B 'яти нетеся 'яти # зім'яти зімнетеся (Ви) @ verb:rev:futr:p:2
SFX B 'ятися нетеся 'ятися # зім'ятися зімнетеся (Ви) @ verb:rev:futr:p:2
SFX B 'яти нуться 'яти # зім'яти зімнуться (Вони) @ verb:rev:futr:p:3
SFX B 'ятися нуться 'ятися # зім'ятися зімнуться (Вони) @ verb:rev:futr:p:3
SFX B 'яти нись 'яти # зім'яти зімнись (Ти) @ verb:rev:impr:s:2
SFX B 'ятися нись 'ятися # зім'ятися зімнись (Ти) @ verb:rev:impr:s:2
SFX B 'яти нися 'яти # зім'яти зімнися (Ти) @ verb:rev:impr:s:2
SFX B 'ятися нися 'ятися # зім'ятися зімнися (Ти) @ verb:rev:impr:s:2
SFX B 'яти німось 'яти # зім'яти зімнімось (Ми) @ verb:rev:impr:p:1
SFX B 'ятися німось 'ятися # зім'ятися зімнімось (Ми) @ verb:rev:impr:p:1
SFX B 'яти німося 'яти # зім'яти зімнімося (Ми) @ verb:rev:impr:p:1
SFX B 'ятися німося 'ятися # зім'ятися зімнімося (Ми) @ verb:rev:impr:p:1
SFX B 'яти ніться 'яти # зім'яти зімніться (Ви) @ verb:rev:impr:p:2
SFX B 'ятися ніться 'ятися # зім'ятися зімніться (Ви) @ verb:rev:impr:p:2
SFX D Y 52
SFX D ити ся [вжчшщбмпр]ити # бентежити бентежся (Ти) @ verb:rev:impr:s:2
SFX D итися ся [вжчшщбмпр]итися # бентежитися бентежся (Ти) @ verb:rev:impr:s:2
SFX D ити ься [дтзснл]ити # заходити заходься (Ти) @ verb:rev:impr:s:2
SFX D итися ься [дтзснл]итися # заходитися заходься (Ти) @ verb:rev:impr:s:2
SFX D ити мось [вжчшщбмпр]ити # бентежити бентежмось (Ми) @ verb:rev:impr:p:1
SFX D итися мось [вжчшщбмпр]итися # бентежитися бентежмось (Ми) @ verb:rev:impr:p:1
SFX D ити мося [вжчшщбмпр]ити # бентежити бентежмося (Ми) @ verb:rev:impr:p:1
SFX D итися мося [вжчшщбмпр]итися # бентежитися бентежмося (Ми) @ verb:rev:impr:p:1
SFX D ити ьмось [дтзснл]ити # заходити заходьмось (Ми) @ verb:rev:impr:p:1
SFX D итися ьмось [дтзснл]итися # заходитися заходьмось (Ми) @ verb:rev:impr:p:1
SFX D ити ьмося [дтзснл]ити # заходити заходьмося (Ми) @ verb:rev:impr:p:1
SFX D итися ьмося [дтзснл]итися # заходитися заходьмося (Ми) @ verb:rev:impr:p:1
SFX D ити тесь [вжчшщбмпр]ити # бентежити бентежтесь (Ви) @ verb:rev:impr:p:2
SFX D итися тесь [вжчшщбмпр]итися # бентежитися бентежтесь (Ви) @ verb:rev:impr:p:2
SFX D ити теся [вжчшщбмпр]ити # бентежити бентежтеся (Ви) @ verb:rev:impr:p:2
SFX D итися теся [вжчшщбмпр]итися # бентежитися бентежтеся (Ви) @ verb:rev:impr:p:2
SFX D ити ьтесь [дтзснл]ити # проводити проводьтесь (Ви) @ verb:rev:impr:p:2
SFX D итися ьтесь [дтзснл]итися # проводитися проводьтесь (Ви) @ verb:rev:impr:p:2
SFX D ити ьтеся [дтзснл]ити # проводити проводьтеся (Ви) @ verb:rev:impr:p:2
SFX D итися ьтеся [дтзснл]итися # проводитися проводьтеся (Ви) @ verb:rev:impr:p:2
SFX D іти ься діти # посидіти посидься (Ти) @ verb:rev:impr:s:2
SFX D ітися ься дітися # посидітися посидься (Ти) @ verb:rev:impr:s:2
SFX D іти ьмось діти # посидіти посидьмось (Ми) @ verb:rev:impr:p:1
SFX D ітися ьмось дітися # посидітися посидьмось (Ми) @ verb:rev:impr:p:1
SFX D іти ьмося діти # посидіти посидьмося (Ми) @ verb:rev:impr:p:1
SFX D ітися ьмося дітися # посидітися посидьмося (Ми) @ verb:rev:impr:p:1
SFX D іти ьтесь діти # посидіти посидьтесь (Ви) @ verb:rev:impr:p:2
SFX D ітися ьтесь дітися # посидітися посидьтесь (Ви) @ verb:rev:impr:p:2
SFX D іти ьтеся діти # посидіти посидьтеся (Ви) @ verb:rev:impr:p:2
SFX D ітися ьтеся дітися # посидітися посидьтеся (Ви) @ verb:rev:impr:p:2
SFX D ути ься нути # кинути кинься (Ти) @ verb:rev:impr:s:2
SFX D утися ься нутися # кинутися кинься (Ти) @ verb:rev:impr:s:2
SFX D ути ьмось нути # кинути киньмось (Ми) @ verb:rev:impr:p:1
SFX D утися ьмось нутися # кинутися киньмось (Ми) @ verb:rev:impr:p:1
SFX D ути ьмося нути # кинути киньмося (Ми) @ verb:rev:impr:p:1
SFX D утися ьмося нутися # кинутися киньмося (Ми) @ verb:rev:impr:p:1
SFX D ути ьтесь нути # кинути киньтесь (Ви) @ verb:rev:impr:p:2
SFX D утися ьтесь нутися # кинутися киньтесь (Ви) @ verb:rev:impr:p:2
SFX D ути ьтеся нути # кинути киньтеся (Ви) @ verb:rev:impr:p:2
SFX D утися ьтеся нутися # кинутися киньтеся (Ви) @ verb:rev:impr:p:2
SFX D їти йсь їти # клеїти клейсь (Ти) @ verb:rev:impr:s:2
SFX D їтися йсь їтися # клеїтися клейсь (Ти) @ verb:rev:impr:s:2
SFX D їти йся їти # клеїти клейся (Ти) @ verb:rev:impr:s:2
SFX D їтися йся їтися # клеїтися клейся (Ти) @ verb:rev:impr:s:2
SFX D їти ймось їти # клеїти клеймось (Ми) @ verb:rev:impr:p:1
SFX D їтися ймось їтися # клеїтися клеймось (Ми) @ verb:rev:impr:p:1
SFX D їти ймося їти # клеїти клеймося (Ми) @ verb:rev:impr:p:1
SFX D їтися ймося їтися # клеїтися клеймося (Ми) @ verb:rev:impr:p:1
SFX D їти йтесь їти # клеїти клейтесь (Ви) @ verb:rev:impr:p:2
SFX D їтися йтесь їтися # клеїтися клейтесь (Ви) @ verb:rev:impr:p:2
SFX D їти йтеся їти # клеїти клейтеся (Ви) @ verb:rev:impr:p:2
SFX D їтися йтеся їтися # клеїтися клейтеся (Ви) @ verb:rev:impr:p:2
SFX F Y 40
SFX F ити ись ити # учити учись (Ти) @ verb:rev:impr:s:2
SFX F итися ись итися # учитися учись (Ти) @ verb:rev:impr:s:2
SFX F ити ися ити # учити учися (Ти) @ verb:rev:impr:s:2
SFX F итися ися итися # учитися учися (Ти) @ verb:rev:impr:s:2
SFX F ити імось ити # учити учімось (Ми) @ verb:rev:impr:p:1
SFX F итися імось итися # учитися учімось (Ми) @ verb:rev:impr:p:1
SFX F ити імося ити # учити учімося (Ми) @ verb:rev:impr:p:1
SFX F итися імося итися # учитися учімося (Ми) @ verb:rev:impr:p:1
SFX F ити іться ити # учити учіться (Ви) @ verb:rev:impr:p:2
SFX F итися іться итися # учитися учіться (Ви) @ verb:rev:impr:p:2
SFX F іти ись діти # сидіти сидись (Ти) @ verb:rev:impr:s:2
SFX F ітися ись дітися # сидітися сидись (Ти) @ verb:rev:impr:s:2
SFX F іти ися діти # сидіти сидися (Ти) @ verb:rev:impr:s:2
SFX F ітися ися дітися # сидітися сидися (Ти) @ verb:rev:impr:s:2
SFX F іти імось діти # сидіти сидімось (Ми) @ verb:rev:impr:p:1
SFX F ітися імось дітися # сидітися сидімось (Ми) @ verb:rev:impr:p:1
SFX F іти імося діти # сидіти сидімося (Ми) @ verb:rev:impr:p:1
SFX F ітися імося дітися # сидітися сидімося (Ми) @ verb:rev:impr:p:1
SFX F іти іться діти # сидіти сидіться (Ви) @ verb:rev:impr:p:2
SFX F ітися іться дітися # сидітися сидіться (Ви) @ verb:rev:impr:p:2
SFX F ути ись нути # кашлянути кашлянись (Ти) @ verb:rev:impr:s:2
SFX F утися ись нутися # кашлянутися кашлянись (Ти) @ verb:rev:impr:s:2
SFX F ути ися нути # кашлянути кашлянися (Ти) @ verb:rev:impr:s:2
SFX F утися ися нутися # кашлянутися кашлянися (Ти) @ verb:rev:impr:s:2
SFX F ути імось нути # кашлянути кашлянімось (Ми) @ verb:rev:impr:p:1
SFX F утися імось нутися # кашлянутися кашлянімось (Ми) @ verb:rev:impr:p:1
SFX F ути імося нути # кашлянути кашлянімося (Ми) @ verb:rev:impr:p:1
SFX F утися імося нутися # кашлянутися кашлянімося (Ми) @ verb:rev:impr:p:1
SFX F ути іться нути # кашлянути кашляніться (Ви) @ verb:rev:impr:p:2
SFX F утися іться нутися # кашлянутися кашляніться (Ви) @ verb:rev:impr:p:2
SFX F їти їсь їти # напоїти напоїсь (Ти) @ verb:rev:impr:s:2
SFX F їтися їсь їтися # напоїтися напоїсь (Ти) @ verb:rev:impr:s:2
SFX F їти їся їти # напоїти напоїся (Ти) @ verb:rev:impr:s:2
SFX F їтися їся їтися # напоїтися напоїся (Ти) @ verb:rev:impr:s:2
SFX F їти їмось їти # напоїти напоїмось (Ми) @ verb:rev:impr:p:1
SFX F їтися їмось їтися # напоїтися напоїмось (Ми) @ verb:rev:impr:p:1
SFX F їти їмося їти # напоїти напоїмося (Ми) @ verb:rev:impr:p:1
SFX F їтися їмося їтися # напоїтися напоїмося (Ми) @ verb:rev:impr:p:1
SFX F їти їться їти # напоїти напоїться (Ви) @ verb:rev:impr:p:2
SFX F їтися їться їтися # напоїтися напоїться (Ви) @ verb:rev:impr:p:2
SFX H Y 18
SFX H 0 мусь ти # абонувати абонуватимусь (Я) @ verb:rev:futr:s:1
SFX H ся мусь тися # абонуватися абонуватимусь (Я) @ verb:rev:futr:s:1
SFX H 0 муся ти # абонувати абонуватимуся (Я) @ verb:rev:futr:s:1
SFX H ся муся тися # абонуватися абонуватимуся (Я) @ verb:rev:futr:s:1
SFX H 0 мешся ти # абонувати абонуватимешся (Ти) @ verb:rev:futr:s:2
SFX H ся мешся тися # абонуватися абонуватимешся (Ти) @ verb:rev:futr:s:2
SFX H 0 меться ти # абонувати абонуватиметься (Він) @ verb:rev:futr:s:3
SFX H ся меться тися # абонуватися абонуватиметься (Він) @ verb:rev:futr:s:3
SFX H 0 мемось ти # абонувати абонуватимемось (Ми) @ verb:rev:futr:p:1
SFX H ся мемось тися # абонуватися абонуватимемось (Ми) @ verb:rev:futr:p:1
SFX H 0 мемося ти # абонувати абонуватимемося (Ми) @ verb:rev:futr:p:1
SFX H ся мемося тися # абонуватися абонуватимемося (Ми) @ verb:rev:futr:p:1
SFX H 0 метесь ти # абонувати абонуватиметесь (Ви) @ verb:rev:futr:p:2
SFX H ся метесь тися # абонуватися абонуватиметесь (Ви) @ verb:rev:futr:p:2
SFX H 0 метеся ти # абонувати абонуватиметеся (Ви) @ verb:rev:futr:p:2
SFX H ся метеся тися # абонуватися абонуватиметеся (Ви) @ verb:rev:futr:p:2
SFX H 0 муться ти # абонувати абонуватимуться (Вони) @ verb:rev:futr:p:3
SFX H ся муться тися # абонуватися абонуватимуться (Вони) @ verb:rev:futr:p:3
SFX J Y 267
SFX J 0 ся ти # ~ти ~тися @ verb:inf:rev
SFX J 0 сь ти # ~ти ~ись @ verb:inf:rev
SFX J ся сь тися # ~тися ~ись @ verb:inf:rev
SFX J ти лась ти # вбивати вбивалась (Вона) @ verb:rev:past:f
SFX J тися лась тися # вбиватися вбивалась (Вона) @ verb:rev:past:f
SFX J ти лася ти # вбивати вбивалася (Вона) @ verb:rev:past:f
SFX J тися лася тися # вбиватися вбивалася (Вона) @ verb:rev:past:f
SFX J ти лось ти # вбивати вбивалось (Воно) @ verb:rev:past:n
SFX J тися лось тися # вбиватися вбивалось (Воно) @ verb:rev:past:n
SFX J ти лося ти # вбивати вбивалося (Воно) @ verb:rev:past:n
SFX J тися лося тися # вбиватися вбивалося (Воно) @ verb:rev:past:n
SFX J ти лись ти # вбивати вбивались (Вони) @ verb:rev:past:p
SFX J тися лись тися # вбиватися вбивались (Вони) @ verb:rev:past:p
SFX J ти лися ти # вбивати вбивалися (Вони) @ verb:rev:past:p
SFX J тися лися тися # вбиватися вбивалися (Вони) @ verb:rev:past:p
SFX J ти всь [аиіуя]ти # вбивати вбивавсь (Я, Ти, Він) @ verb:rev:past:m
SFX J тися всь [аиіуя]тися # вбиватися вбивавсь (Я, Ти, Він) @ verb:rev:past:m
SFX J ти вся [аиіуя]ти # вбивати вбивався (Я, Ти, Він) @ verb:rev:past:m
SFX J тися вся [аиіуя]тися # вбиватися вбивався (Я, Ти, Він) @ verb:rev:past:m
SFX J ти вшись [аиіуя]ти # вбивати вбивавшись @ adp:rev:perf
SFX J тися вшись [аиіуя]тися # вбиватися вбивавшись @ adp:rev:perf
SFX J яти юсь [аяіо]яти # віяти віюсь (Я) @ verb:rev:pres:s:1
SFX J ятися юсь [аяіо]ятися # віятися віюсь (Я) @ verb:rev:pres:s:1
SFX J яти юся [аяіо]яти # віяти віюся (Я) @ verb:rev:pres:s:1
SFX J ятися юся [аяіо]ятися # віятися віюся (Я) @ verb:rev:pres:s:1
SFX J ти юсь [илнрцд]яти # ганяти ганяюсь (Я) @ verb:rev:pres:s:1
SFX J тися юсь [илнрцд]ятися # ганятися ганяюсь (Я) @ verb:rev:pres:s:1
SFX J ти юся [илнрцд]яти # ганяти ганяюся (Я) @ verb:rev:pres:s:1
SFX J тися юся [илнрцд]ятися # ганятися ганяюся (Я) @ verb:rev:pres:s:1
SFX J ти юсь [аіу]ти # вбивати вбиваюсь (Я) @ verb:rev:pres:s:1
SFX J тися юсь [аіу]тися # вбиватися вбиваюсь (Я) @ verb:rev:pres:s:1
SFX J ти юся [аіу]ти # вбивати вбиваюся (Я) @ verb:rev:pres:s:1
SFX J тися юся [аіу]тися # вбиватися вбиваюся (Я) @ verb:rev:pres:s:1
SFX J яти їшся ояти # стояти стоїшся (Ти) @ verb:rev:pres:s:2
SFX J ятися їшся оятися # стоятися стоїшся (Ти) @ verb:rev:pres:s:2
SFX J яти єшся [аяі]яти # віяти вієшся (Ти) @ verb:rev:pres:s:2
SFX J ятися єшся [аяі]ятися # віятися вієшся (Ти) @ verb:rev:pres:s:2
SFX J ти єшся [илнрцд]яти # ганяти ганяєшся (Ти) @ verb:rev:pres:s:2
SFX J тися єшся [илнрцд]ятися # ганятися ганяєшся (Ти) @ verb:rev:pres:s:2
SFX J ти єшся [аіу]ти # вбивати вбиваєшся (Ти) @ verb:rev:pres:s:2
SFX J тися єшся [аіу]тися # вбиватися вбиваєшся (Ти) @ verb:rev:pres:s:2
SFX J яти їться ояти # стояти стоїться (Він) @ verb:rev:pres:s:3
SFX J ятися їться оятися # стоятися стоїться (Він) @ verb:rev:pres:s:3
SFX J яти ється [аяі]яти # віяти віється (Він) @ verb:rev:pres:s:3
SFX J ятися ється [аяі]ятися # віятися віється (Він) @ verb:rev:pres:s:3
SFX J ти ється [илнрцд]яти # ганяти ганяється (Він) @ verb:rev:pres:s:3
SFX J тися ється [илнрцд]ятися # ганятися ганяється (Він) @ verb:rev:pres:s:3
SFX J ти ється [аіу]ти # вбивати вбивається (Він) @ verb:rev:pres:s:3
SFX J тися ється [аіу]тися # вбиватися вбивається (Він) @ verb:rev:pres:s:3
SFX J яти їмось ояти # стояти стоїмось (Ми) @ verb:rev:pres:p:1
SFX J ятися їмось оятися # стоятися стоїмось (Ми) @ verb:rev:pres:p:1
SFX J яти їмося ояти # стояти стоїмося (Ми) @ verb:rev:pres:p:1
SFX J ятися їмося оятися # стоятися стоїмося (Ми) @ verb:rev:pres:p:1
SFX J яти ємось [аяі]яти # віяти віємось (Ми) @ verb:rev:pres:p:1
SFX J ятися ємось [аяі]ятися # віятися віємось (Ми) @ verb:rev:pres:p:1
SFX J яти ємося [аяі]яти # віяти віємося (Ми) @ verb:rev:pres:p:1
SFX J ятися ємося [аяі]ятися # віятися віємося (Ми) @ verb:rev:pres:p:1
SFX J ти ємось [илнрцд]яти # ганяти ганяємось (Ми) @ verb:rev:pres:p:1
SFX J тися ємось [илнрцд]ятися # ганятися ганяємось (Ми) @ verb:rev:pres:p:1
SFX J ти ємося [илнрцд]яти # ганяти ганяємося (Ми) @ verb:rev:pres:p:1
SFX J тися ємося [илнрцд]ятися # ганятися ганяємося (Ми) @ verb:rev:pres:p:1
SFX J ти ємось [аіу]ти # вбивати вбиваємось (Ми) @ verb:rev:pres:p:1
SFX J тися ємось [аіу]тися # вбиватися вбиваємось (Ми) @ verb:rev:pres:p:1
SFX J ти ємося [аіу]ти # вбивати вбиваємося (Ми) @ verb:rev:pres:p:1
SFX J тися ємося [аіу]тися # вбиватися вбиваємося (Ми) @ verb:rev:pres:p:1
SFX J яти їтесь ояти # стояти стоїтесь (Ви) @ verb:rev:pres:p:2
SFX J ятися їтесь оятися # стоятися стоїтесь (Ви) @ verb:rev:pres:p:2
SFX J яти їтеся ояти # стояти стоїтеся (Ви) @ verb:rev:pres:p:2
SFX J ятися їтеся оятися # стоятися стоїтеся (Ви) @ verb:rev:pres:p:2
SFX J яти єтесь [аяі]яти # віяти вієтесь (Ви) @ verb:rev:pres:p:2
SFX J ятися єтесь [аяі]ятися # віятися вієтесь (Ви) @ verb:rev:pres:p:2
SFX J яти єтеся [аяі]яти # віяти вієтеся (Ви) @ verb:rev:pres:p:2
SFX J ятися єтеся [аяі]ятися # віятися вієтеся (Ви) @ verb:rev:pres:p:2
SFX J ти єтесь [илнрцд]яти # ганяти ганяєтесь (Ви) @ verb:rev:pres:p:2
SFX J тися єтесь [илнрцд]ятися # ганятися ганяєтесь (Ви) @ verb:rev:pres:p:2
SFX J ти єтеся [илнрцд]яти # ганяти ганяєтеся (Ви) @ verb:rev:pres:p:2
SFX J тися єтеся [илнрцд]ятися # ганятися ганяєтеся (Ви) @ verb:rev:pres:p:2
SFX J ти єтесь [аіу]ти # вбивати вбиваєтесь (Ви) @ verb:rev:pres:p:2
SFX J тися єтесь [аіу]тися # вбиватися вбиваєтесь (Ви) @ verb:rev:pres:p:2
SFX J ти єтеся [аіу]ти # вбивати вбиваєтеся (Ви) @ verb:rev:pres:p:2
SFX J тися єтеся [аіу]тися # вбиватися вбиваєтеся (Ви) @ verb:rev:pres:p:2
SFX J яти яться ояти # стояти стояться (Вони) @ verb:rev:pres:p:3
SFX J ятися яться оятися # стоятися стояться (Вони) @ verb:rev:pres:p:3
SFX J яти ються [аяі]яти # віяти віються (Вони) @ verb:rev:pres:p:3
SFX J ятися ються [аяі]ятися # віятися віються (Вони) @ verb:rev:pres:p:3
SFX J ти ються [илнрцд]яти # ганяти ганяються (Вони) @ verb:rev:pres:p:3
SFX J тися ються [илнрцд]ятися # ганятися ганяються (Вони) @ verb:rev:pres:p:3
SFX J ти ються [аіу]ти # вбивати вбиваються (Вони) @ verb:rev:pres:p:3
SFX J тися ються [аіу]тися # вбиватися вбиваються (Вони) @ verb:rev:pres:p:3
SFX J ояти ійсь ояти # стояти стійсь (Ти) @ verb:rev:impr:s:2
SFX J оятися ійсь оятися # стоятися стійсь (Ти) @ verb:rev:impr:s:2
SFX J ояти ійся ояти # стояти стійся (Ти) @ verb:rev:impr:s:2
SFX J оятися ійся оятися # стоятися стійся (Ти) @ verb:rev:impr:s:2
SFX J яти йсь [аяі]яти # віяти війсь (Ти) @ verb:rev:impr:s:2
SFX J ятися йсь [аяі]ятися # віятися війсь (Ти) @ verb:rev:impr:s:2
SFX J яти йся [аяі]яти # віяти війся (Ти) @ verb:rev:impr:s:2
SFX J ятися йся [аяі]ятися # віятися війся (Ти) @ verb:rev:impr:s:2
SFX J ти йсь [илнрцд]яти # ганяти ганяйсь (Ти) @ verb:rev:impr:s:2
SFX J тися йсь [илнрцд]ятися # ганятися ганяйсь (Ти) @ verb:rev:impr:s:2
SFX J ти йся [илнрцд]яти # ганяти ганяйся (Ти) @ verb:rev:impr:s:2
SFX J тися йся [илнрцд]ятися # ганятися ганяйся (Ти) @ verb:rev:impr:s:2
SFX J ти йсь [аіу]ти # вбивати вбивайсь (Ти) @ verb:rev:impr:s:2
SFX J тися йсь [аіу]тися # вбиватися вбивайсь (Ти) @ verb:rev:impr:s:2
SFX J ти йся [аіу]ти # вбивати вбивайся (Ти) @ verb:rev:impr:s:2
SFX J тися йся [аіу]тися # вбиватися вбивайся (Ти) @ verb:rev:impr:s:2
SFX J ояти іймось ояти # стояти стіймось (Ми) @ verb:rev:impr:p:1
SFX J оятися іймось оятися # стоятися стіймось (Ми) @ verb:rev:impr:p:1
SFX J ояти іймося ояти # стояти стіймося (Ми) @ verb:rev:impr:p:1
SFX J оятися іймося оятися # стоятися стіймося (Ми) @ verb:rev:impr:p:1
SFX J яти ймось [аяі]яти # віяти віймось (Ми) @ verb:rev:impr:p:1
SFX J ятися ймось [аяі]ятися # віятися віймось (Ми) @ verb:rev:impr:p:1
SFX J яти ймося [аяі]яти # віяти віймося (Ми) @ verb:rev:impr:p:1
SFX J ятися ймося [аяі]ятися # віятися віймося (Ми) @ verb:rev:impr:p:1
SFX J ти ймось [илнрцд]яти # ганяти ганяймось (Ми) @ verb:rev:impr:p:1
SFX J тися ймось [илнрцд]ятися # ганятися ганяймось (Ми) @ verb:rev:impr:p:1
SFX J ти ймося [илнрцд]яти # ганяти ганяймося (Ми) @ verb:rev:impr:p:1
SFX J тися ймося [илнрцд]ятися # ганятися ганяймося (Ми) @ verb:rev:impr:p:1
SFX J ти ймось [аіу]ти # вбивати вбиваймось (Ми) @ verb:rev:impr:p:1
SFX J тися ймось [аіу]тися # вбиватися вбиваймось (Ми) @ verb:rev:impr:p:1
SFX J ти ймося [аіу]ти # вбивати вбиваймося (Ми) @ verb:rev:impr:p:1
SFX J тися ймося [аіу]тися # вбиватися вбиваймося (Ми) @ verb:rev:impr:p:1
SFX J ояти ійтесь ояти # стояти стійтесь (Ви) @ verb:rev:impr:p:2
SFX J оятися ійтесь оятися # стоятися стійтесь (Ви) @ verb:rev:impr:p:2
SFX J ояти ійтеся ояти # стояти стійтеся (Ви) @ verb:rev:impr:p:2
SFX J оятися ійтеся оятися # стоятися стійтеся (Ви) @ verb:rev:impr:p:2
SFX J яти йтесь [аяі]яти # віяти війтесь (Ви) @ verb:rev:impr:p:2
SFX J ятися йтесь [аяі]ятися # віятися війтесь (Ви) @ verb:rev:impr:p:2
SFX J яти йтеся [аяі]яти # віяти війтеся (Ви) @ verb:rev:impr:p:2
SFX J ятися йтеся [аяі]ятися # віятися війтеся (Ви) @ verb:rev:impr:p:2
SFX J ти йтесь [илнрцд]яти # ганяти ганяйтесь (Ви) @ verb:rev:impr:p:2
SFX J тися йтесь [илнрцд]ятися # ганятися ганяйтесь (Ви) @ verb:rev:impr:p:2
SFX J ти йтеся [илнрцд]яти # ганяти ганяйтеся (Ви) @ verb:rev:impr:p:2
SFX J тися йтеся [илнрцд]ятися # ганятися ганяйтеся (Ви) @ verb:rev:impr:p:2
SFX J ти йтесь [аіу]ти # вбивати вбивайтесь (Ви) @ verb:rev:impr:p:2
SFX J тися йтесь [аіу]тися # вбиватися вбивайтесь (Ви) @ verb:rev:impr:p:2
SFX J ти йтеся [аіу]ти # вбивати вбивайтеся (Ви) @ verb:rev:impr:p:2
SFX J тися йтеся [аіу]тися # вбиватися вбивайтеся (Ви) @ verb:rev:impr:p:2
SFX J ити 'юсь [бвп]ити # бити б'юсь (Я) @ verb:rev:pres:s:1
SFX J итися 'юсь [бвп]итися # битися б'юсь (Я) @ verb:rev:pres:s:1
SFX J ити 'юся [бвп]ити # бити б'юся (Я) @ verb:rev:pres:s:1
SFX J итися 'юся [бвп]итися # битися б'юся (Я) @ verb:rev:pres:s:1
SFX J ити 'єшся [бвп]ити # бити б'єшся (Ти) @ verb:rev:pres:s:2
SFX J итися 'єшся [бвп]итися # битися б'єшся (Ти) @ verb:rev:pres:s:2
SFX J ити 'ється [бвп]ити # бити б'ється (Він) @ verb:rev:pres:s:3
SFX J итися 'ється [бвп]итися # битися б'ється (Він) @ verb:rev:pres:s:3
SFX J ити 'ємось [бвп]ити # бити б'ємось (Ми) @ verb:rev:pres:p:1
SFX J итися 'ємось [бвп]итися # битися б'ємось (Ми) @ verb:rev:pres:p:1
SFX J ити 'ємося [бвп]ити # бити б'ємося (Ми) @ verb:rev:pres:p:1
SFX J итися 'ємося [бвп]итися # битися б'ємося (Ми) @ verb:rev:pres:p:1
SFX J ити 'єтесь [бвп]ити # бити б'єтесь (Ви) @ verb:rev:pres:p:2
SFX J итися 'єтесь [бвп]итися # битися б'єтесь (Ви) @ verb:rev:pres:p:2
SFX J ити 'єтеся [бвп]ити # бити б'єтеся (Ви) @ verb:rev:pres:p:2
SFX J итися 'єтеся [бвп]итися # битися б'єтеся (Ви) @ verb:rev:pres:p:2
SFX J ити 'ються [бвп]ити # бити б'ються (Вони) @ verb:rev:pres:p:3
SFX J итися 'ються [бвп]итися # битися б'ються (Вони) @ verb:rev:pres:p:3
SFX J ити июсь [врмнш]ити # рити риюсь (Я) @ verb:rev:pres:s:1
SFX J итися июсь [врмнш]итися # ритися риюсь (Я) @ verb:rev:pres:s:1
SFX J ити июся [врмнш]ити # рити риюся (Я) @ verb:rev:pres:s:1
SFX J итися июся [врмнш]итися # ритися риюся (Я) @ verb:rev:pres:s:1
SFX J ити иєшся [врмнш]ити # рити риєшся (Ти) @ verb:rev:pres:s:2
SFX J итися иєшся [врмнш]итися # ритися риєшся (Ти) @ verb:rev:pres:s:2
SFX J ити иється [врмнш]ити # рити риється (Він) @ verb:rev:pres:s:3
SFX J итися иється [врмнш]итися # ритися риється (Він) @ verb:rev:pres:s:3
SFX J ити иємось [врмнш]ити # рити риємось (Ми) @ verb:rev:pres:p:1
SFX J итися иємось [врмнш]итися # ритися риємось (Ми) @ verb:rev:pres:p:1
SFX J ити иємося [врмнш]ити # рити риємося (Ми) @ verb:rev:pres:p:1
SFX J итися иємося [врмнш]итися # ритися риємося (Ми) @ verb:rev:pres:p:1
SFX J ити иєтесь [врмнш]ити # рити риєтесь (Ви) @ verb:rev:pres:p:2
SFX J итися иєтесь [врмнш]итися # ритися риєтесь (Ви) @ verb:rev:pres:p:2
SFX J ити иєтеся [врмнш]ити # рити риєтеся (Ви) @ verb:rev:pres:p:2
SFX J итися иєтеся [врмнш]итися # ритися риєтеся (Ви) @ verb:rev:pres:p:2
SFX J ити иються [врмнш]ити # рити риються (Вони) @ verb:rev:pres:p:3
SFX J итися иються [врмнш]итися # ритися риються (Вони) @ verb:rev:pres:p:3
SFX J ити люсь лити # лити ллюсь (Я) @ verb:rev:pres:s:1
SFX J итися люсь литися # литися ллюсь (Я) @ verb:rev:pres:s:1
SFX J ити люся лити # лити ллюся (Я) @ verb:rev:pres:s:1
SFX J итися люся литися # литися ллюся (Я) @ verb:rev:pres:s:1
SFX J ити лєшся лити # лити ллєшся (Ти) @ verb:rev:pres:s:2
SFX J итися лєшся литися # литися ллєшся (Ти) @ verb:rev:pres:s:2
SFX J ити лється лити # лити ллється (Він) @ verb:rev:pres:s:3
SFX J итися лється литися # литися ллється (Він) @ verb:rev:pres:s:3
SFX J ити лємось лити # лити ллємось (Ми) @ verb:rev:pres:p:1
SFX J итися лємось литися # литися ллємось (Ми) @ verb:rev:pres:p:1
SFX J ити лємося лити # лити ллємося (Ми) @ verb:rev:pres:p:1
SFX J итися лємося литися # литися ллємося (Ми) @ verb:rev:pres:p:1
SFX J ити лєтесь лити # лити ллєтесь (Ви) @ verb:rev:pres:p:2
SFX J итися лєтесь литися # литися ллєтесь (Ви) @ verb:rev:pres:p:2
SFX J ити лєтеся лити # лити ллєтеся (Ви) @ verb:rev:pres:p:2
SFX J итися лєтеся литися # литися ллєтеся (Ви) @ verb:rev:pres:p:2
SFX J ити лються лити # лити ллються (Вони) @ verb:rev:pres:p:3
SFX J итися лються литися # литися ллються (Вони) @ verb:rev:pres:p:3
SFX J ти вусь жити # жити живусь (Я) @ verb:rev:pres:s:1
SFX J тися вусь житися # житися живусь (Я) @ verb:rev:pres:s:1
SFX J ти вуся жити # жити живуся (Я) @ verb:rev:pres:s:1
SFX J тися вуся житися # житися живуся (Я) @ verb:rev:pres:s:1
SFX J ти вешся жити # жити живешся (Ти) @ verb:rev:pres:s:2
SFX J тися вешся житися # житися живешся (Ти) @ verb:rev:pres:s:2
SFX J ти веться жити # жити живеться (Він) @ verb:rev:pres:s:3
SFX J тися веться житися # житися живеться (Він) @ verb:rev:pres:s:3
SFX J ти вемось жити # жити живемось (Ми) @ verb:rev:pres:p:1
SFX J тися вемось житися # житися живемось (Ми) @ verb:rev:pres:p:1
SFX J ти вемося жити # жити живемося (Ми) @ verb:rev:pres:p:1
SFX J тися вемося житися # житися живемося (Ми) @ verb:rev:pres:p:1
SFX J ти ветесь жити # жити живетесь (Ви) @ verb:rev:pres:p:2
SFX J тися ветесь житися # житися живетесь (Ви) @ verb:rev:pres:p:2
SFX J ти ветеся жити # жити живетеся (Ви) @ verb:rev:pres:p:2
SFX J тися ветеся житися # житися живетеся (Ви) @ verb:rev:pres:p:2
SFX J ти вуться жити # жити живуться (Вони) @ verb:rev:pres:p:3
SFX J тися вуться житися # житися живуться (Вони) @ verb:rev:pres:p:3
SFX J ти йсь [^ж]ити # бити бийсь (Ти) @ verb:rev:impr:s:2
SFX J тися йсь [^ж]итися # битися бийсь (Ти) @ verb:rev:impr:s:2
SFX J ти йся [^ж]ити # бити бийся (Ти) @ verb:rev:impr:s:2
SFX J тися йся [^ж]итися # битися бийся (Ти) @ verb:rev:impr:s:2
SFX J ти ймось [^ж]ити # бити биймось (Ми) @ verb:rev:impr:p:1
SFX J тися ймось [^ж]итися # битися биймось (Ми) @ verb:rev:impr:p:1
SFX J ти ймося [^ж]ити # бити биймося (Ми) @ verb:rev:impr:p:1
SFX J тися ймося [^ж]итися # битися биймося (Ми) @ verb:rev:impr:p:1
SFX J ти йтесь [^ж]ити # бити бийтесь (Ви) @ verb:rev:impr:p:2
SFX J тися йтесь [^ж]итися # битися бийтесь (Ви) @ verb:rev:impr:p:2
SFX J ти йтеся [^ж]ити # бити бийтеся (Ви) @ verb:rev:impr:p:2
SFX J тися йтеся [^ж]итися # битися бийтеся (Ви) @ verb:rev:impr:p:2
SFX J ти вись [ж]ити # жити живись (Ти) @ verb:rev:impr:s:2
SFX J тися вись [ж]итися # житися живись (Ти) @ verb:rev:impr:s:2
SFX J ти вися [ж]ити # жити живися (Ти) @ verb:rev:impr:s:2
SFX J тися вися [ж]итися # житися живися (Ти) @ verb:rev:impr:s:2
SFX J ти вімось [ж]ити # жити живімось (Ми) @ verb:rev:impr:p:1
SFX J тися вімось [ж]итися # житися живімось (Ми) @ verb:rev:impr:p:1
SFX J ти вімося [ж]ити # жити живімося (Ми) @ verb:rev:impr:p:1
SFX J тися вімося [ж]итися # житися живімося (Ми) @ verb:rev:impr:p:1
SFX J ти віться [ж]ити # жити живіться (Ви) @ verb:rev:impr:p:2
SFX J тися віться [ж]итися # житися живіться (Ви) @ verb:rev:impr:p:2
SFX J ти усь сти # пасти пасусь (Я) @ verb:rev:pres:s:1
SFX J тися усь стися # пастися пасусь (Я) @ verb:rev:pres:s:1
SFX J ти уся сти # пасти пасуся (Я) @ verb:rev:pres:s:1
SFX J тися уся стися # пастися пасуся (Я) @ verb:rev:pres:s:1
SFX J ти ешся сти # пасти пасешся (Ти) @ verb:rev:pres:s:2
SFX J тися ешся стися # пастися пасешся (Ти) @ verb:rev:pres:s:2
SFX J ти еться сти # пасти пасеться (Він) @ verb:rev:pres:s:3
SFX J тися еться стися # пастися пасеться (Він) @ verb:rev:pres:s:3
SFX J ти емось сти # пасти пасемось (Ми) @ verb:rev:pres:p:1
SFX J тися емось стися # пастися пасемось (Ми) @ verb:rev:pres:p:1
SFX J ти емося сти # пасти пасемося (Ми) @ verb:rev:pres:p:1
SFX J тися емося стися # пастися пасемося (Ми) @ verb:rev:pres:p:1
SFX J ти етесь сти # пасти пасетесь (Ви) @ verb:rev:pres:p:2
SFX J тися етесь стися # пастися пасетесь (Ви) @ verb:rev:pres:p:2
SFX J ти етеся сти # пасти пасетеся (Ви) @ verb:rev:pres:p:2
SFX J тися етеся стися # пастися пасетеся (Ви) @ verb:rev:pres:p:2
SFX J ти уться сти # пасти пасуться (Вони) @ verb:rev:pres:p:3
SFX J тися уться стися # пастися пасуться (Вони) @ verb:rev:pres:p:3
SFX J сти сся [ая]сти # пасти пасся (Я, Ти, Він) @ verb:rev:past:m
SFX J стися сся [ая]стися # пастися пасся (Я, Ти, Він) @ verb:rev:past:m
SFX J ести ісся ести # нести нісся (Я, Ти, Він) @ verb:rev:past:m
SFX J естися ісся естися # нестися нісся (Я, Ти, Він) @ verb:rev:past:m
SFX J сти сшись [ая]сти # пасти пасшись @ adp:rev:perf
SFX J стися сшись [ая]стися # пастися пасшись @ adp:rev:perf
SFX J ести ісшись ести # нести нісшись @ adp:rev:perf
SFX J естися ісшись естися # нестися нісшись @ adp:rev:perf
SFX J ти ись сти # пасти пасись (Ти) @ verb:rev:impr:s:2
SFX J тися ись стися # пастися пасись (Ти) @ verb:rev:impr:s:2
SFX J ти ися сти # пасти пасися (Ти) @ verb:rev:impr:s:2
SFX J тися ися стися # пастися пасися (Ти) @ verb:rev:impr:s:2
SFX J ти імось сти # пасти пасімось (Ми) @ verb:rev:impr:p:1
SFX J тися імось стися # пастися пасімось (Ми) @ verb:rev:impr:p:1
SFX J ти імося сти # пасти пасімося (Ми) @ verb:rev:impr:p:1
SFX J тися імося стися # пастися пасімося (Ми) @ verb:rev:impr:p:1
SFX J ти іться сти # пасти пасітеся (Ви) @ verb:rev:impr:p:2
SFX J тися іться стися # пастися пасітеся (Ви) @ verb:rev:impr:p:2
SFX L Y 517
SFX L 0 ся ти # ~ти ~тися @ verb:inf:rev
SFX L 0 сь ти # ~ти ~ись @ verb:inf:rev
SFX L ся сь тися # ~тися ~ись @ verb:inf:rev
SFX L ти лась [^ус]ти # гнати гналась (Вона) @ verb:rev:past:s:f
SFX L тися лась [^ус]тися # гнатися гналась (Вона) @ verb:rev:past:s:f
SFX L ти лася [^ус]ти # гнати гналася (Вона) @ verb:rev:past:s:f
SFX L тися лася [^ус]тися # гнатися гналася (Вона) @ verb:rev:past:s:f
SFX L ти лось [^ус]ти # гнати гналось (Воно) @ verb:rev:past:s:n
SFX L тися лось [^ус]тися # гнатися гналось (Воно) @ verb:rev:past:s:n
SFX L ти лося [^ус]ти # гнати гналося (Воно) @ verb:rev:past:s:n
SFX L тися лося [^ус]тися # гнатися гналося (Воно) @ verb:rev:past:s:n
SFX L ти лись [^ус]ти # гнати гнались (Вони) @ verb:rev:past:p
SFX L тися лись [^ус]тися # гнатися гнались (Вони) @ verb:rev:past:p
SFX L ти лися [^ус]ти # гнати гналися (Вони) @ verb:rev:past:p
SFX L тися лися [^ус]тися # гнатися гналися (Вони) @ verb:rev:past:p
SFX L ти всь [аеиіоя]ти # зігнати зігнавсь (Я, Ти, Він) @ verb:rev:past:m
SFX L тися всь [аеиіоя]тися # зігнатися зігнавсь (Я, Ти, Він) @ verb:rev:past:m
SFX L ти вся [аеиіоя]ти # зігнати зігнався (Я, Ти, Він) @ verb:rev:past:m
SFX L тися вся [аеиіоя]тися # зігнатися зігнався (Я, Ти, Він) @ verb:rev:past:m
SFX L ти вшись [аеиіоя]ти # зігнати зігнавшись @ adp:rev:perf
SFX L тися вшись [аеиіоя]тися # зігнатися зігнавшись @ adp:rev:perf
SFX L нути лась нути # змерзнути змерзлась (Вона) @ verb:rev:past:f
SFX L нутися лась нутися # змерзнутися змерзлась (Вона) @ verb:rev:past:f
SFX L нути лася нути # змерзнути змерзлася (Вона) @ verb:rev:past:f
SFX L нутися лася нутися # змерзнутися змерзлася (Вона) @ verb:rev:past:f
SFX L нути лось нути # змерзнути змерзлось (Воно) @ verb:rev:past:n
SFX L нутися лось нутися # змерзнутися змерзлось (Воно) @ verb:rev:past:n
SFX L нути лося нути # змерзнути змерзлося (Воно) @ verb:rev:past:n
SFX L нутися лося нутися # змерзнутися змерзлося (Воно) @ verb:rev:past:n
SFX L нути лись нути # змерзнути змерзлись (Вони) @ verb:rev:past:p
SFX L нутися лись нутися # змерзнутися змерзлись (Вони) @ verb:rev:past:p
SFX L нути лися нути # змерзнути змерзлися (Вони) @ verb:rev:past:p
SFX L нутися лися нутися # змерзнутися змерзлися (Вони) @ verb:rev:past:p
SFX L нути ся [^о]нути # змерзнути змерзся (Я, Ти, Він) @ verb:rev:past:m
SFX L нутися ся [^о]нутися # змерзнутися змерзся (Я, Ти, Він) @ verb:rev:past:m
SFX L нути всь онути # прохолонути прохоловсь (Я, Ти, Він) @ verb:rev:past:m
SFX L нутися всь онутися # прохолонутися прохоловсь (Я, Ти, Він) @ verb:rev:past:m
SFX L нути вся онути # прохолонути прохоловся (Я, Ти, Він) @ verb:rev:past:m
SFX L нутися вся онутися # прохолонутися прохоловся (Я, Ти, Він) @ verb:rev:past:m
SFX L нути нувсь онути # прохолонути прохолонувсь (Я, Ти, Він) @ verb:rev:past:m
SFX L нутися нувсь онутися # прохолонутися прохолонувсь (Я, Ти, Він) @ verb:rev:past:m
SFX L нути нувся онути # прохолонути прохолонувся (Я, Ти, Він) @ verb:rev:past:m
SFX L нутися нувся онутися # прохолонутися прохолонувся (Я, Ти, Він) @ verb:rev:past:m
SFX L нути шись [^о]нути # змерзнути змерзшись @ adp:rev:perf
SFX L нутися шись [^о]нутися # змерзнутися змерзшись @ adp:rev:perf
SFX L нути вшись онути # охолонути охоловшись @ adp:rev:perf
SFX L нутися вшись онутися # охолонутися охоловшись @ adp:rev:perf
SFX L нути нувшись онути # охолонути охолонувшись @ adp:rev:perf
SFX L нутися нувшись онутися # охолонутися охолонувшись @ adp:rev:perf
SFX L нути нулась нути # змерзнути змерзнулась (Вона) @ verb:rev:past:f
SFX L нутися нулась нутися # змерзнутися змерзнулась (Вона) @ verb:rev:past:f
SFX L нути нулася нути # змерзнути змерзнулася (Вона) @ verb:rev:past:f
SFX L нутися нулася нутися # змерзнутися змерзнулася (Вона) @ verb:rev:past:f
SFX L нути нулось нути # змерзнути змерзнулось (Воно) @ verb:rev:past:n
SFX L нутися нулось нутися # змерзнутися змерзнулось (Воно) @ verb:rev:past:n
SFX L нути нулося нути # змерзнути змерзнулося (Воно) @ verb:rev:past:n
SFX L нутися нулося нутися # змерзнутися змерзнулося (Воно) @ verb:rev:past:n
SFX L нути нулись нути # змерзнути змерзнулись (Вони) @ verb:rev:past:p
SFX L нутися нулись нутися # змерзнутися змерзнулись (Вони) @ verb:rev:past:p
SFX L нути нулися нути # змерзнути змерзнулися (Вони) @ verb:rev:past:p
SFX L нутися нулися нутися # змерзнутися змерзнулися (Вони) @ verb:rev:past:p
SFX L нути нувсь нути # змерзнути змерзнувсь (Я, Ти, Він) @ verb:rev:past:m
SFX L нутися нувсь нутися # змерзнутися змерзнувсь (Я, Ти, Він) @ verb:rev:past:m
SFX L нути нувся нути # змерзнути змерзнувся (Я, Ти, Він) @ verb:rev:past:m
SFX L нутися нувся нутися # змерзнутися змерзнувся (Я, Ти, Він) @ verb:rev:past:m
SFX L нути нувшись нути # змерзнути змерзнувшись @ adp:rev:perf
SFX L нутися нувшись нутися # змерзнутися змерзнувшись @ adp:rev:perf
SFX L ути усь нути # змерзнути змерзнусь (Я) @ verb:rev:futr:s:1
SFX L утися усь нутися # змерзнутися змерзнусь (Я) @ verb:rev:futr:s:1
SFX L ути уся нути # змерзнути змерзнуся (Я) @ verb:rev:futr:s:1
SFX L утися уся нутися # змерзнутися змерзнуся (Я) @ verb:rev:futr:s:1
SFX L ути ешся нути # змерзнути змерзнешся (Ти) @ verb:rev:futr:s:2
SFX L утися ешся нутися # змерзнутися змерзнешся (Ти) @ verb:rev:futr:s:2
SFX L ути еться нути # змерзнути змерзнеться (Він) @ verb:rev:futr:s:3
SFX L утися еться нутися # змерзнутися змерзнеться (Він) @ verb:rev:futr:s:3
SFX L ути емось нути # змерзнути змерзнемось (Ми) @ verb:rev:futr:p:1
SFX L утися емось нутися # змерзнутися змерзнемось (Ми) @ verb:rev:futr:p:1
SFX L ути емося нути # змерзнути змерзнемося (Ми) @ verb:rev:futr:p:1
SFX L утися емося нутися # змерзнутися змерзнемося (Ми) @ verb:rev:futr:p:1
SFX L ути етесь нути # змерзнути змерзнетесь (Ви) @ verb:rev:futr:p:2
SFX L утися етесь нутися # змерзнутися змерзнетесь (Ви) @ verb:rev:futr:p:2
SFX L ути етеся нути # змерзнути змерзнетеся (Ви) @ verb:rev:futr:p:2
SFX L утися етеся нутися # змерзнутися змерзнетеся (Ви) @ verb:rev:futr:p:2
SFX L ути уться нути # змерзнути змерзнуться (Вони) @ verb:rev:futr:p:3
SFX L утися уться нутися # змерзнутися змерзнуться (Вони) @ verb:rev:futr:p:3
SFX L ти нусь чити # відпочити відпочинусь (Я) @ verb:rev:futr:s:1
SFX L тися нусь читися # відпочитися відпочинусь (Я) @ verb:rev:futr:s:1
SFX L ти нуся чити # відпочити відпочинуся (Я) @ verb:rev:futr:s:1
SFX L тися нуся читися # відпочитися відпочинуся (Я) @ verb:rev:futr:s:1
SFX L ти нешся чити # відпочити відпочинешся (Ти) @ verb:rev:futr:s:2
SFX L тися нешся читися # відпочитися відпочинешся (Ти) @ verb:rev:futr:s:2
SFX L ти неться чити # відпочити відпочинеться (Він) @ verb:rev:futr:s:3
SFX L тися неться читися # відпочитися відпочинеться (Він) @ verb:rev:futr:s:3
SFX L ти немось чити # відпочити відпочинемось (Ми) @ verb:rev:futr:p:1
SFX L тися немось читися # відпочитися відпочинемось (Ми) @ verb:rev:futr:p:1
SFX L ти немося чити # відпочити відпочинемося (Ми) @ verb:rev:futr:p:1
SFX L тися немося читися # відпочитися відпочинемося (Ми) @ verb:rev:futr:p:1
SFX L ти нетесь чити # відпочити відпочинетесь (Ви) @ verb:rev:futr:p:2
SFX L тися нетесь читися # відпочитися відпочинетесь (Ви) @ verb:rev:futr:p:2
SFX L ти нетеся чити # відпочити відпочинетеся (Ви) @ verb:rev:futr:p:2
SFX L тися нетеся читися # відпочитися відпочинетеся (Ви) @ verb:rev:futr:p:2
SFX L ти нуться чити # відпочити відпочинуться (Вони) @ verb:rev:futr:p:3
SFX L тися нуться читися # відпочитися відпочинуться (Вони) @ verb:rev:futr:p:3
SFX L ігнати женусь ігнати # відігнати відженусь (Я) @ verb:rev:futr:s:1
SFX L ігнатися женусь ігнатися # відігнатися відженусь (Я) @ verb:rev:futr:s:1
SFX L ігнати женуся ігнати # відігнати відженуся (Я) @ verb:rev:futr:s:1
SFX L ігнатися женуся ігнатися # відігнатися відженуся (Я) @ verb:rev:futr:s:1
SFX L ігнати женешся ігнати # відігнати відженешся (Ти) @ verb:rev:futr:s:2
SFX L ігнатися женешся ігнатися # відігнатися відженешся (Ти) @ verb:rev:futr:s:2
SFX L ігнати женеться ігнати # відігнати відженеться (Він) @ verb:rev:futr:s:3
SFX L ігнатися женеться ігнатися # відігнатися відженеться (Він) @ verb:rev:futr:s:3
SFX L ігнати женемось ігнати # відігнати відженемось (Ми) @ verb:rev:futr:p:1
SFX L ігнатися женемось ігнатися # відігнатися відженемось (Ми) @ verb:rev:futr:p:1
SFX L ігнати женемося ігнати # відігнати відженемося (Ми) @ verb:rev:futr:p:1
SFX L ігнатися женемося ігнатися # відігнатися відженемося (Ми) @ verb:rev:futr:p:1
SFX L ігнати женетесь ігнати # відігнати відженетесь (Ви) @ verb:rev:futr:p:2
SFX L ігнатися женетесь ігнатися # відігнатися відженетесь (Ви) @ verb:rev:futr:p:2
SFX L ігнати женетеся ігнати # відігнати відженетеся (Ви) @ verb:rev:futr:p:2
SFX L ігнатися женетеся ігнатися # відігнатися відженетеся (Ви) @ verb:rev:futr:p:2
SFX L ігнати женуться ігнати # відігнати відженуться (Вони) @ verb:rev:futr:p:3
SFX L ігнатися женуться ігнатися # відігнатися відженуться (Вони) @ verb:rev:futr:p:3
SFX L ігнати женись ігнати # відігнати відженись (Ти) @ verb:rev:impr:s:2
SFX L ігнатися женись ігнатися # відігнатися відженись (Ти) @ verb:rev:impr:s:2
SFX L ігнати женися ігнати # відігнати відженися (Ти) @ verb:rev:impr:s:2
SFX L ігнатися женися ігнатися # відігнатися відженися (Ти) @ verb:rev:impr:s:2
SFX L ігнати женімось ігнати # відігнати відженімось (Ми) @ verb:rev:impr:p:1
SFX L ігнатися женімось ігнатися # відігнатися відженімось (Ми) @ verb:rev:impr:p:1
SFX L ігнати женімося ігнати # відігнати відженімося (Ми) @ verb:rev:impr:p:1
SFX L ігнатися женімося ігнатися # відігнатися відженімося (Ми) @ verb:rev:impr:p:1
SFX L ігнати женіться ігнати # відігнати відженіться (Ви) @ verb:rev:impr:p:2
SFX L ігнатися женіться ігнатися # відігнатися відженіться (Ви) @ verb:rev:impr:p:2
SFX L іпрати перусь іпрати # відіпрати відперусь (Я) @ verb:rev:futr:s:1
SFX L іпратися перусь іпратися # відіпратися відперусь (Я) @ verb:rev:futr:s:1
SFX L іпрати перуся іпрати # відіпрати відперуся (Я) @ verb:rev:futr:s:1
SFX L іпратися перуся іпратися # відіпратися відперуся (Я) @ verb:rev:futr:s:1
SFX L іпрати перешся іпрати # відіпрати відперешся (Ти) @ verb:rev:futr:s:2
SFX L іпратися перешся іпратися # відіпратися відперешся (Ти) @ verb:rev:futr:s:2
SFX L іпрати переться іпрати # відіпрати відпереться (Він) @ verb:rev:futr:s:3
SFX L іпратися переться іпратися # відіпратися відпереться (Він) @ verb:rev:futr:s:3
SFX L іпрати перемось іпрати # відіпрати відперемось (Ми) @ verb:rev:futr:p:1
SFX L іпратися перемось іпратися # відіпратися відперемось (Ми) @ verb:rev:futr:p:1
SFX L іпрати перемося іпрати # відіпрати відперемося (Ми) @ verb:rev:futr:p:1
SFX L іпратися перемося іпратися # відіпратися відперемося (Ми) @ verb:rev:futr:p:1
SFX L іпрати перетесь іпрати # відіпрати відперетесь (Ви) @ verb:rev:futr:p:2
SFX L іпратися перетесь іпратися # відіпратися відперетесь (Ви) @ verb:rev:futr:p:2
SFX L іпрати перетеся іпрати # відіпрати відперетеся (Ви) @ verb:rev:futr:p:2
SFX L іпратися перетеся іпратися # відіпратися відперетеся (Ви) @ verb:rev:futr:p:2
SFX L іпрати перуться іпрати # відіпрати відперуться (Вони) @ verb:rev:futr:p:3
SFX L іпратися перуться іпратися # відіпратися відперуться (Вони) @ verb:rev:futr:p:3
SFX L іпрати перись іпрати # відіпрати відперись (Ти) @ verb:rev:impr:s:2
SFX L іпратися перись іпратися # відіпратися відперись (Ти) @ verb:rev:impr:s:2
SFX L іпрати перися іпрати # відіпрати відперися (Ти) @ verb:rev:impr:s:2
SFX L іпратися перися іпратися # відіпратися відперися (Ти) @ verb:rev:impr:s:2
SFX L іпрати перімось іпрати # відіпрати відперімось (Ми) @ verb:rev:impr:p:1
SFX L іпратися перімось іпратися # відіпратися відперімось (Ми) @ verb:rev:impr:p:1
SFX L іпрати перімося іпрати # відіпрати відперімося (Ми) @ verb:rev:impr:p:1
SFX L іпратися перімося іпратися # відіпратися відперімося (Ми) @ verb:rev:impr:p:1
SFX L іпрати періться іпрати # відіпрати відперіться (Ви) @ verb:rev:impr:p:2
SFX L іпратися періться іпратися # відіпратися відперіться (Ви) @ verb:rev:impr:p:2
SFX L ібрати берусь ібрати # відібрати відберусь (Я) @ verb:rev:futr:s:1
SFX L ібратися берусь ібратися # відібратися відберусь (Я) @ verb:rev:futr:s:1
SFX L ібрати беруся ібрати # відібрати відберуся (Я) @ verb:rev:futr:s:1
SFX L ібратися беруся ібратися # відібратися відберуся (Я) @ verb:rev:futr:s:1
SFX L ібрати берешся ібрати # відібрати відберешся (Ти) @ verb:rev:futr:s:2
SFX L ібратися берешся ібратися # відібратися відберешся (Ти) @ verb:rev:futr:s:2
SFX L ібрати береться ібрати # відібрати відбереться (Він) @ verb:rev:futr:s:3
SFX L ібратися береться ібратися # відібратися відбереться (Він) @ verb:rev:futr:s:3
SFX L ібрати беремось ібрати # відібрати відберемось (Ми) @ verb:rev:futr:p:1
SFX L ібратися беремось ібратися # відібратися відберемось (Ми) @ verb:rev:futr:p:1
SFX L ібрати беремося ібрати # відібрати відберемося (Ми) @ verb:rev:futr:p:1
SFX L ібратися беремося ібратися # відібратися відберемося (Ми) @ verb:rev:futr:p:1
SFX L ібрати беретесь ібрати # відібрати відберетесь (Ви) @ verb:rev:futr:p:2
SFX L ібратися беретесь ібратися # відібратися відберетесь (Ви) @ verb:rev:futr:p:2
SFX L ібрати беретеся ібрати # відібрати відберетеся (Ви) @ verb:rev:futr:p:2
SFX L ібратися беретеся ібратися # відібратися відберетеся (Ви) @ verb:rev:futr:p:2
SFX L ібрати беруться ібрати # відібрати відберуться (Вони) @ verb:rev:futr:p:3
SFX L ібратися беруться ібратися # відібратися відберуться (Вони) @ verb:rev:futr:p:3
SFX L ібрати берись ібрати # відібрати відберись (Ти) @ verb:rev:impr:s:2
SFX L ібратися берись ібратися # відібратися відберись (Ти) @ verb:rev:impr:s:2
SFX L ібрати берися ібрати # відібрати відберися (Ти) @ verb:rev:impr:s:2
SFX L ібратися берися ібратися # відібратися відберися (Ти) @ verb:rev:impr:s:2
SFX L ібрати берімось ібрати # відібрати відберімось (Ми) @ verb:rev:impr:p:1
SFX L ібратися берімось ібратися # відібратися відберімось (Ми) @ verb:rev:impr:p:1
SFX L ібрати берімося ібрати # відібрати відберімося (Ми) @ verb:rev:impr:p:1
SFX L ібратися берімося ібратися # відібратися відберімося (Ми) @ verb:rev:impr:p:1
SFX L ібрати беріться ібрати # відібрати відберіться (Ви) @ verb:rev:impr:p:2
SFX L ібратися беріться ібратися # відібратися відберіться (Ви) @ verb:rev:impr:p:2
SFX L ідрати дерусь ідрати # відідрати віддерусь (Я) @ verb:rev:futr:s:1
SFX L ідратися дерусь ідратися # відідратися віддерусь (Я) @ verb:rev:futr:s:1
SFX L ідрати деруся ідрати # відідрати віддеруся (Я) @ verb:rev:futr:s:1
SFX L ідратися деруся ідратися # відідратися віддеруся (Я) @ verb:rev:futr:s:1
SFX L ідрати дерешся ідрати # відідрати віддерешся (Ти) @ verb:rev:futr:s:2
SFX L ідратися дерешся ідратися # відідратися віддерешся (Ти) @ verb:rev:futr:s:2
SFX L ідрати дереться ідрати # відідрати віддереться (Він) @ verb:rev:futr:s:3
SFX L ідратися дереться ідратися # відідратися віддереться (Він) @ verb:rev:futr:s:3
SFX L ідрати деремось ідрати # відідрати віддеремось (Ми) @ verb:rev:futr:p:1
SFX L ідратися деремось ідратися # відідратися віддеремось (Ми) @ verb:rev:futr:p:1
SFX L ідрати деремося ідрати # відідрати віддеремося (Ми) @ verb:rev:futr:p:1
SFX L ідратися деремося ідратися # відідратися віддеремося (Ми) @ verb:rev:futr:p:1
SFX L ідрати деретесь ідрати # відідрати віддеретесь (Ви) @ verb:rev:futr:p:2
SFX L ідратися деретесь ідратися # відідратися віддеретесь (Ви) @ verb:rev:futr:p:2
SFX L ідрати деретеся ідрати # відідрати віддеретеся (Ви) @ verb:rev:futr:p:2
SFX L ідратися деретеся ідратися # відідратися віддеретеся (Ви) @ verb:rev:futr:p:2
SFX L ідрати деруться ідрати # відідрати віддеруться (Вони) @ verb:rev:futr:p:3
SFX L ідратися деруться ідратися # відідратися віддеруться (Вони) @ verb:rev:futr:p:3
SFX L ідрати дерись ідрати # відідрати віддерись (Ти) @ verb:rev:impr:s:2
SFX L ідратися дерись ідратися # відідратися віддерись (Ти) @ verb:rev:impr:s:2
SFX L ідрати дерися ідрати # відідрати віддерися (Ти) @ verb:rev:impr:s:2
SFX L ідратися дерися ідратися # відідратися віддерися (Ти) @ verb:rev:impr:s:2
SFX L ідрати дерімось ідрати # відідрати віддерімось (Ми) @ verb:rev:impr:p:1
SFX L ідратися дерімось ідратися # відідратися віддерімось (Ми) @ verb:rev:impr:p:1
SFX L ідрати дерімося ідрати # відідрати віддерімося (Ми) @ verb:rev:impr:p:1
SFX L ідратися дерімося ідратися # відідратися віддерімося (Ми) @ verb:rev:impr:p:1
SFX L ідрати деріться ідрати # відідрати віддеріться (Ви) @ verb:rev:impr:p:2
SFX L ідратися деріться ідратися # відідратися віддеріться (Ви) @ verb:rev:impr:p:2
SFX L бити іб'юсь [бдз]бити # надбити надіб'юсь (Я) @ verb:rev:futr:s:1
SFX L битися іб'юсь [бдз]битися # надбитися надіб'юсь (Я) @ verb:rev:futr:s:1
SFX L бити іб'юся [бдз]бити # надбити надіб'юся (Я) @ verb:rev:futr:s:1
SFX L битися іб'юся [бдз]битися # надбитися надіб'юся (Я) @ verb:rev:futr:s:1
SFX L бити іб'єшся [бдз]бити # надбити надіб'єшся (Ти) @ verb:rev:futr:s:2
SFX L битися іб'єшся [бдз]битися # надбитися надіб'єшся (Ти) @ verb:rev:futr:s:2
SFX L бити іб'ється [бдз]бити # надбити надіб'ється (Він) @ verb:rev:futr:s:3
SFX L битися іб'ється [бдз]битися # надбитися надіб'ється (Він) @ verb:rev:futr:s:3
SFX L бити іб'ємось [бдз]бити # надбити надіб'ємось (Ми) @ verb:rev:futr:p:1
SFX L битися іб'ємось [бдз]битися # надбитися надіб'ємось (Ми) @ verb:rev:futr:p:1
SFX L бити іб'ємося [бдз]бити # надбити надіб'ємося (Ми) @ verb:rev:futr:p:1
SFX L битися іб'ємося [бдз]битися # надбитися надіб'ємося (Ми) @ verb:rev:futr:p:1
SFX L бити іб'єтесь [бдз]бити # надбити надіб'єтесь (Ви) @ verb:rev:futr:p:2
SFX L битися іб'єтесь [бдз]битися # надбитися надіб'єтесь (Ви) @ verb:rev:futr:p:2
SFX L бити іб'єтеся [бдз]бити # надбити надіб'єтеся (Ви) @ verb:rev:futr:p:2
SFX L битися іб'єтеся [бдз]битися # надбитися надіб'єтеся (Ви) @ verb:rev:futr:p:2
SFX L бити іб'ються [бдз]бити # надбити надіб'ються (Вони) @ verb:rev:futr:p:3
SFX L битися іб'ються [бдз]битися # надбитися надіб'ються (Вони) @ verb:rev:futr:p:3
SFX L вити ів'юсь [бдз]вити # надвити надів'юсь (Я) @ verb:rev:futr:s:1
SFX L витися ів'юсь [бдз]витися # надвитися надів'юсь (Я) @ verb:rev:futr:s:1
SFX L вити ів'юся [бдз]вити # надвити надів'юся (Я) @ verb:rev:futr:s:1
SFX L витися ів'юся [бдз]витися # надвитися надів'юся (Я) @ verb:rev:futr:s:1
SFX L вити ів'єшся [бдз]вити # надвити надів'єшся (Ти) @ verb:rev:futr:s:2
SFX L витися ів'єшся [бдз]витися # надвитися надів'єшся (Ти) @ verb:rev:futr:s:2
SFX L вити ів'ється [бдз]вити # надвити надів'ється (Він) @ verb:rev:futr:s:3
SFX L витися ів'ється [бдз]витися # надвитися надів'ється (Він) @ verb:rev:futr:s:3
SFX L вити ів'ємось [бдз]вити # надвити надів'ємось (Ми) @ verb:rev:futr:p:1
SFX L витися ів'ємось [бдз]витися # надвитися надів'ємось (Ми) @ verb:rev:futr:p:1
SFX L вити ів'ємося [бдз]вити # надвити надів'ємося (Ми) @ verb:rev:futr:p:1
SFX L витися ів'ємося [бдз]витися # надвитися надів'ємося (Ми) @ verb:rev:futr:p:1
SFX L вити ів'єтесь [бдз]вити # надвити надів'єтесь (Ви) @ verb:rev:futr:p:2
SFX L витися ів'єтесь [бдз]витися # надвитися надів'єтесь (Ви) @ verb:rev:futr:p:2
SFX L вити ів'єтеся [бдз]вити # надвити надів'єтеся (Ви) @ verb:rev:futr:p:2
SFX L витися ів'єтеся [бдз]витися # надвитися надів'єтеся (Ви) @ verb:rev:futr:p:2
SFX L вити ів'ються [бдз]вити # надвити надів'ються (Вони) @ verb:rev:futr:p:3
SFX L витися ів'ються [бдз]витися # надвитися надів'ються (Вони) @ verb:rev:futr:p:3
SFX L пити іп'юсь [бдз]пити # надпити надіп'юсь (Я) @ verb:rev:futr:s:1
SFX L питися іп'юсь [бдз]питися # надпитися надіп'юсь (Я) @ verb:rev:futr:s:1
SFX L пити іп'юся [бдз]пити # надпити надіп'юся (Я) @ verb:rev:futr:s:1
SFX L питися іп'юся [бдз]питися # надпитися надіп'юся (Я) @ verb:rev:futr:s:1
SFX L пити іп'єшся [бдз]пити # надпити надіп'єшся (Ти) @ verb:rev:futr:s:2
SFX L питися іп'єшся [бдз]питися # надпитися надіп'єшся (Ти) @ verb:rev:futr:s:2
SFX L пити іп'ється [бдз]пити # надпити надіп'ється (Він) @ verb:rev:futr:s:3
SFX L питися іп'ється [бдз]питися # надпитися надіп'ється (Він) @ verb:rev:futr:s:3
SFX L пити іп'ємось [бдз]пити # надпити надіп'ємось (Ми) @ verb:rev:futr:p:1
SFX L питися іп'ємось [бдз]питися # надпитися надіп'ємось (Ми) @ verb:rev:futr:p:1
SFX L пити іп'ємося [бдз]пити # надпити надіп'ємося (Ми) @ verb:rev:futr:p:1
SFX L питися іп'ємося [бдз]питися # надпитися надіп'ємося (Ми) @ verb:rev:futr:p:1
SFX L пити іп'єтесь [бдз]пити # надпити надіп'єтесь (Ви) @ verb:rev:futr:p:2
SFX L питися іп'єтесь [бдз]питися # надпитися надіп'єтесь (Ви) @ verb:rev:futr:p:2
SFX L пити іп'єтеся [бдз]пити # надпити надіп'єтеся (Ви) @ verb:rev:futr:p:2
SFX L питися іп'єтеся [бдз]питися # надпитися надіп'єтеся (Ви) @ verb:rev:futr:p:2
SFX L пити іп'ються [бдз]пити # надпити надіп'ються (Вони) @ verb:rev:futr:p:3
SFX L питися іп'ються [бдз]питися # надпитися надіп'ються (Вони) @ verb:rev:futr:p:3
SFX L лити іллюсь [бвдз]лити # надлити наділлюсь (Я) @ verb:rev:futr:s:1
SFX L литися іллюсь [бвдз]литися # надлитися наділлюсь (Я) @ verb:rev:futr:s:1
SFX L лити іллюся [бвдз]лити # надлити наділлюся (Я) @ verb:rev:futr:s:1
SFX L литися іллюся [бвдз]литися # надлитися наділлюся (Я) @ verb:rev:futr:s:1
SFX L лити іллєшся [бвдз]лити # надлити наділлєшся (Ти) @ verb:rev:futr:s:2
SFX L литися іллєшся [бвдз]литися # надлитися наділлєшся (Ти) @ verb:rev:futr:s:2
SFX L лити іллється [бвдз]лити # надлити наділлється (Він) @ verb:rev:futr:s:3
SFX L литися іллється [бвдз]литися # надлитися наділлється (Він) @ verb:rev:futr:s:3
SFX L лити іллємось [бвдз]лити # надлити наділлємось (Ми) @ verb:rev:futr:p:1
SFX L литися іллємось [бвдз]литися # надлитися наділлємось (Ми) @ verb:rev:futr:p:1
SFX L лити іллємося [бвдз]лити # надлити наділлємося (Ми) @ verb:rev:futr:p:1
SFX L литися іллємося [бвдз]литися # надлитися наділлємося (Ми) @ verb:rev:futr:p:1
SFX L лити іллєтесь [бвдз]лити # надлити наділлєтесь (Ви) @ verb:rev:futr:p:2
SFX L литися іллєтесь [бвдз]литися # надлитися наділлєтесь (Ви) @ verb:rev:futr:p:2
SFX L лити іллєтеся [бвдз]лити # надлити наділлєтеся (Ви) @ verb:rev:futr:p:2
SFX L литися іллєтеся [бвдз]литися # надлитися наділлєтеся (Ви) @ verb:rev:futr:p:2
SFX L лити іллються [бвдз]лити # надлити наділлються (Вони) @ verb:rev:futr:p:3
SFX L литися іллються [бвдз]литися # надлитися наділлються (Вони) @ verb:rev:futr:p:3
SFX L ти йсь [бвлп]ити # надбити надбийсь (Ти) @ verb:rev:impr:s:2
SFX L тися йсь [бвлп]итися # надбитися надбийсь (Ти) @ verb:rev:impr:s:2
SFX L ти йся [бвлп]ити # надбити надбийся (Ти) @ verb:rev:impr:s:2
SFX L тися йся [бвлп]итися # надбитися надбийся (Ти) @ verb:rev:impr:s:2
SFX L ти ймось [бвлп]ити # надбити надбиймось (Ми) @ verb:rev:impr:p:1
SFX L тися ймось [бвлп]итися # надбитися надбиймось (Ми) @ verb:rev:impr:p:1
SFX L ти ймося [бвлп]ити # надбити надбиймося (Ми) @ verb:rev:impr:p:1
SFX L тися ймося [бвлп]итися # надбитися надбиймося (Ми) @ verb:rev:impr:p:1
SFX L ти йтесь [бвлп]ити # надбити надбийтесь (Ви) @ verb:rev:impr:p:2
SFX L тися йтесь [бвлп]итися # надбитися надбийтесь (Ви) @ verb:rev:impr:p:2
SFX L ти йтеся [бвлп]ити # надбити надбийтеся (Ви) @ verb:rev:impr:p:2
SFX L тися йтеся [бвлп]итися # надбитися надбийтеся (Ви) @ verb:rev:impr:p:2
SFX L ти мся дати # дати дамся (Я) @ verb:rev:futr:s:1
SFX L тися мся датися # датися дамся (Я) @ verb:rev:futr:s:1
SFX L ти сись дати # дати дасись (Ти) @ verb:rev:futr:s:2
SFX L тися сись датися # датися дасись (Ти) @ verb:rev:futr:s:2
SFX L ти сися дати # дати дасися (Ти) @ verb:rev:futr:s:2
SFX L тися сися датися # датися дасися (Ти) @ verb:rev:futr:s:2
SFX L ти сться дати # дати дасться (Він) @ verb:rev:futr:s:3
SFX L тися сться датися # датися дасться (Він) @ verb:rev:futr:s:3
SFX L ти мось дати # дати дамось (Ми) @ verb:rev:futr:p:1
SFX L тися мось датися # датися дамось (Ми) @ verb:rev:futr:p:1
SFX L ти мося дати # дати дамося (Ми) @ verb:rev:futr:p:1
SFX L тися мося датися # датися дамося (Ми) @ verb:rev:futr:p:1
SFX L ти стесь дати # дати дастесь (Ви) @ verb:rev:futr:p:2
SFX L тися стесь датися # датися дастесь (Ви) @ verb:rev:futr:p:2
SFX L ти стеся дати # дати дастеся (Ви) @ verb:rev:futr:p:2
SFX L тися стеся датися # датися дастеся (Ви) @ verb:rev:futr:p:2
SFX L ти дуться дати # дати дадуться (Вони) @ verb:rev:futr:p:3
SFX L тися дуться датися # датися дадуться (Вони) @ verb:rev:futr:p:3
SFX L ти йсь дати # дати дайсь (Ти) @ verb:rev:impr:s:2
SFX L тися йсь датися # датися дайсь (Ти) @ verb:rev:impr:s:2
SFX L ти йся дати # дати дайся (Ти) @ verb:rev:impr:s:2
SFX L тися йся датися # датися дайся (Ти) @ verb:rev:impr:s:2
SFX L ти ймось дати # дати даймось (Ми) @ verb:rev:impr:p:1
SFX L тися ймось датися # датися даймось (Ми) @ verb:rev:impr:p:1
SFX L ти ймося дати # дати даймося (Ми) @ verb:rev:impr:p:1
SFX L тися ймося датися # датися даймося (Ми) @ verb:rev:impr:p:1
SFX L ти йтесь дати # дати дайтесь (Ви) @ verb:rev:impr:p:2
SFX L тися йтесь датися # датися дайтесь (Ви) @ verb:rev:impr:p:2
SFX L ти йтеся дати # дати дайтеся (Ви) @ verb:rev:impr:p:2
SFX L тися йтеся датися # датися дайтеся (Ви) @ verb:rev:impr:p:2
SFX L ати мусь жати # жати жмусь (Я) @ verb:rev:pres:s:1
SFX L атися мусь жатися # жатися жмусь (Я) @ verb:rev:pres:s:1
SFX L ати муся жати # жати жмуся (Я) @ verb:rev:pres:s:1
SFX L атися муся жатися # жатися жмуся (Я) @ verb:rev:pres:s:1
SFX L ати мешся жати # жати жмешся (Ти) @ verb:rev:pres:s:2
SFX L атися мешся жатися # жатися жмешся (Ти) @ verb:rev:pres:s:2
SFX L ати меться жати # жати жметься (Він) @ verb:rev:pres:s:3
SFX L атися меться жатися # жатися жметься (Він) @ verb:rev:pres:s:3
SFX L ати мемось жати # жати жмемось (Ми) @ verb:rev:pres:p:1
SFX L атися мемось жатися # жатися жмемось (Ми) @ verb:rev:pres:p:1
SFX L ати мемося жати # жати жмемося (Ми) @ verb:rev:pres:p:1
SFX L атися мемося жатися # жатися жмемося (Ми) @ verb:rev:pres:p:1
SFX L ати метесь жати # жати жметесь (Ви) @ verb:rev:pres:p:2
SFX L атися метесь жатися # жатися жметесь (Ви) @ verb:rev:pres:p:2
SFX L ати метеся жати # жати жметеся (Ви) @ verb:rev:pres:p:2
SFX L атися метеся жатися # жатися жметеся (Ви) @ verb:rev:pres:p:2
SFX L ати муться жати # жати жмуться (Вони) @ verb:rev:pres:p:3
SFX L атися муться жатися # жатися жмуться (Вони) @ verb:rev:pres:p:3
SFX L ати мись жати # жати жмись (Ти) @ verb:rev:impr:s:2
SFX L атися мись жатися # жатися жмись (Ти) @ verb:rev:impr:s:2
SFX L ати мися жати # жати жмися (Ти) @ verb:rev:impr:s:2
SFX L атися мися жатися # жатися жмися (Ти) @ verb:rev:impr:s:2
SFX L ати мімось жати # жати жмімось (Ми) @ verb:rev:impr:p:1
SFX L атися мімось жатися # жатися жмімось (Ми) @ verb:rev:impr:p:1
SFX L ати мімося жати # жати жмімося (Ми) @ verb:rev:impr:p:1
SFX L атися мімося жатися # жатися жмімося (Ми) @ verb:rev:impr:p:1
SFX L ати міться жати # жати жміться (Ви) @ verb:rev:impr:p:2
SFX L атися міться жатися # жатися жміться (Ви) @ verb:rev:impr:p:2
SFX L ти нусь діти # подіти подінусь (Я) @ verb:rev:futr:s:1
SFX L тися нусь дітися # подітися подінусь (Я) @ verb:rev:futr:s:1
SFX L ти нуся діти # подіти подінуся (Я) @ verb:rev:futr:s:1
SFX L тися нуся дітися # подітися подінуся (Я) @ verb:rev:futr:s:1
SFX L ти нешся діти # подіти подінешся (Ти) @ verb:rev:futr:s:2
SFX L тися нешся дітися # подітися подінешся (Ти) @ verb:rev:futr:s:2
SFX L ти неться діти # подіти подінеться (Він) @ verb:rev:futr:s:3
SFX L тися неться дітися # подітися подінеться (Він) @ verb:rev:futr:s:3
SFX L ти немось діти # подіти подінемось (Ми) @ verb:rev:futr:p:1
SFX L тися немось дітися # подітися подінемось (Ми) @ verb:rev:futr:p:1
SFX L ти немося діти # подіти подінемося (Ми) @ verb:rev:futr:p:1
SFX L тися немося дітися # подітися подінемося (Ми) @ verb:rev:futr:p:1
SFX L ти нетесь діти # подіти подінетесь (Ви) @ verb:rev:futr:p:2
SFX L тися нетесь дітися # подітися подінетесь (Ви) @ verb:rev:futr:p:2
SFX L ти нетеся діти # подіти подінетеся (Ви) @ verb:rev:futr:p:2
SFX L тися нетеся дітися # подітися подінетеся (Ви) @ verb:rev:futr:p:2
SFX L ти нуться діти # подіти подінуться (Вони) @ verb:rev:futr:p:3
SFX L тися нуться дітися # подітися подінуться (Вони) @ verb:rev:futr:p:3
SFX L ти нься .діти # одіти одінься (Ти) @ verb:rev:impr:s:2
SFX L тися нься .дітися # одітися одінься (Ти) @ verb:rev:impr:s:2
SFX L ти ньмось .діти # одіти одіньмось (Ми) @ verb:rev:impr:p:1
SFX L тися ньмось .дітися # одітися одіньмось (Ми) @ verb:rev:impr:p:1
SFX L ти ньмося .діти # одіти одіньмося (Ми) @ verb:rev:impr:p:1
SFX L тися ньмося .дітися # одітися одіньмося (Ми) @ verb:rev:impr:p:1
SFX L ти ньтесь .діти # одіти одіньтесь (Ви) @ verb:rev:impr:p:2
SFX L тися ньтесь .дітися # одітися одіньтесь (Ви) @ verb:rev:impr:p:2
SFX L ти ньтеся .діти # одіти одіньтеся (Ви) @ verb:rev:impr:p:2
SFX L тися ньтеся .дітися # одітися одіньтеся (Ви) @ verb:rev:impr:p:2
SFX L ти нусь стати # постати постанусь (Я) @ verb:rev:futr:s:1
SFX L тися нусь статися # постатися постанусь (Я) @ verb:rev:futr:s:1
SFX L ти нуся стати # постати постануся (Я) @ verb:rev:futr:s:1
SFX L тися нуся статися # постатися постануся (Я) @ verb:rev:futr:s:1
SFX L ти нешся стати # постати постанешся (Ти) @ verb:rev:futr:s:2
SFX L тися нешся статися # постатися постанешся (Ти) @ verb:rev:futr:s:2
SFX L ти неться стати # постати постанеться (Він) @ verb:rev:futr:s:3
SFX L тися неться статися # постатися постанеться (Він) @ verb:rev:futr:s:3
SFX L ти немось стати # постати постанемось (Ми) @ verb:rev:futr:p:1
SFX L тися немось статися # постатися постанемось (Ми) @ verb:rev:futr:p:1
SFX L ти немося стати # постати постанемося (Ми) @ verb:rev:futr:p:1
SFX L тися немося статися # постатися постанемося (Ми) @ verb:rev:futr:p:1
SFX L ти нетесь стати # постати постанетесь (Ви) @ verb:rev:futr:p:2
SFX L тися нетесь статися # постатися постанетесь (Ви) @ verb:rev:futr:p:2
SFX L ти нетеся стати # постати постанетеся (Ви) @ verb:rev:futr:p:2
SFX L тися нетеся статися # постатися постанетеся (Ви) @ verb:rev:futr:p:2
SFX L ти нуться стати # постати постануться (Вони) @ verb:rev:futr:p:3
SFX L тися нуться статися # постатися постануться (Вони) @ verb:rev:futr:p:3
SFX L ти нься стати # постати постанься (Ти) @ verb:rev:impr:s:2
SFX L тися нься статися # постатися постанься (Ти) @ verb:rev:impr:s:2
SFX L ти ньмось стати # постати постаньмось (Ми) @ verb:rev:impr:p:1
SFX L тися ньмось статися # постатися постаньмось (Ми) @ verb:rev:impr:p:1
SFX L ти ньмося стати # постати постаньмося (Ми) @ verb:rev:impr:p:1
SFX L тися ньмося статися # постатися постаньмося (Ми) @ verb:rev:impr:p:1
SFX L ти ньтесь стати # постати постаньтесь (Ви) @ verb:rev:impr:p:2
SFX L тися ньтесь статися # постатися постаньтесь (Ви) @ verb:rev:impr:p:2
SFX L ти ньтеся стати # постати постаньтеся (Ви) @ verb:rev:impr:p:2
SFX L тися ньтеся статися # постатися постаньтеся (Ви) @ verb:rev:impr:p:2
SFX L ати усь ссати # поссати поссусь (Я) @ verb:rev:futr:s:1
SFX L атися усь ссатися # поссатися поссусь (Я) @ verb:rev:futr:s:1
SFX L ати уся ссати # поссати поссуся (Я) @ verb:rev:futr:s:1
SFX L атися уся ссатися # поссатися поссуся (Я) @ verb:rev:futr:s:1
SFX L ати ешся ссати # поссати поссешся (Ти) @ verb:rev:futr:s:2
SFX L атися ешся ссатися # поссатися поссешся (Ти) @ verb:rev:futr:s:2
SFX L ати еться ссати # поссати поссеться (Він) @ verb:rev:futr:s:3
SFX L атися еться ссатися # поссатися поссеться (Він) @ verb:rev:futr:s:3
SFX L ати емось ссати # поссати поссемось (Ми) @ verb:rev:futr:p:1
SFX L атися емось ссатися # поссатися поссемось (Ми) @ verb:rev:futr:p:1
SFX L ати емося ссати # поссати поссемося (Ми) @ verb:rev:futr:p:1
SFX L атися емося ссатися # поссатися поссемося (Ми) @ verb:rev:futr:p:1
SFX L ати етесь ссати # поссати поссетесь (Ви) @ verb:rev:futr:p:2
SFX L атися етесь ссатися # поссатися поссетесь (Ви) @ verb:rev:futr:p:2
SFX L ати етеся ссати # поссати поссетеся (Ви) @ verb:rev:futr:p:2
SFX L атися етеся ссатися # поссатися поссетеся (Ви) @ verb:rev:futr:p:2
SFX L ати уться ссати # поссати поссуться (Вони) @ verb:rev:futr:p:3
SFX L атися уться ссатися # поссатися поссуться (Вони) @ verb:rev:futr:p:3
SFX L ати ись ссати # поссати поссись (Ти) @ verb:rev:impr:s:2
SFX L атися ись ссатися # поссатися поссись (Ти) @ verb:rev:impr:s:2
SFX L ати ися ссати # поссати поссися (Ти) @ verb:rev:impr:s:2
SFX L атися ися ссатися # поссатися поссися (Ти) @ verb:rev:impr:s:2
SFX L ати імось ссати # поссати поссімось (Ми) @ verb:rev:impr:p:1
SFX L атися імось ссатися # поссатися поссімось (Ми) @ verb:rev:impr:p:1
SFX L ати імося ссати # поссати поссімося (Ми) @ verb:rev:impr:p:1
SFX L атися імося ссатися # поссатися поссімося (Ми) @ verb:rev:impr:p:1
SFX L ати імся ссати # поссати поссімся (Ми) @ verb:rev:impr:p:1
SFX L атися імся ссатися # поссатися поссімся (Ми) @ verb:rev:impr:p:1
SFX L ати іться ссати # поссати поссіться (Ви) @ verb:rev:impr:p:2
SFX L атися іться ссатися # поссатися поссіться (Ви) @ verb:rev:impr:p:2
SFX L олоти елюсь олоти # молоти мелюсь (Я) @ verb:rev:pres:s:1
SFX L олотися елюсь олотися # молотися мелюсь (Я) @ verb:rev:pres:s:1
SFX L олоти елюся олоти # молоти мелюся (Я) @ verb:rev:pres:s:1
SFX L олотися елюся олотися # молотися мелюся (Я) @ verb:rev:pres:s:1
SFX L олоти елешся олоти # молоти мелешся (Ти) @ verb:rev:pres:s:2
SFX L олотися елешся олотися # молотися мелешся (Ти) @ verb:rev:pres:s:2
SFX L олоти елеться олоти # молоти мелеться (Він) @ verb:rev:pres:s:3
SFX L олотися елеться олотися # молотися мелеться (Він) @ verb:rev:pres:s:3
SFX L олоти елемось олоти # молоти мелемось (Ми) @ verb:rev:pres:p:1
SFX L олотися елемось олотися # молотися мелемось (Ми) @ verb:rev:pres:p:1
SFX L олоти елемося олоти # молоти мелемося (Ми) @ verb:rev:pres:p:1
SFX L олотися елемося олотися # молотися мелемося (Ми) @ verb:rev:pres:p:1
SFX L олоти елетесь олоти # молоти мелетесь (Ви) @ verb:rev:pres:p:2
SFX L олотися елетесь олотися # молотися мелетесь (Ви) @ verb:rev:pres:p:2
SFX L олоти елетеся олоти # молоти мелетеся (Ви) @ verb:rev:pres:p:2
SFX L олотися елетеся олотися # молотися мелетеся (Ви) @ verb:rev:pres:p:2
SFX L олоти елються олоти # молоти мелються (Вони) @ verb:rev:pres:p:3
SFX L олотися елються олотися # молотися мелються (Вони) @ verb:rev:pres:p:3
SFX L олоти елись олоти # молоти мелись (Ти) @ verb:rev:impr:s:2
SFX L олотися елись олотися # молотися мелись (Ти) @ verb:rev:impr:s:2
SFX L олоти елися олоти # молоти мелися (Ти) @ verb:rev:impr:s:2
SFX L олотися елися олотися # молотися мелися (Ти) @ verb:rev:impr:s:2
SFX L олоти елімось олоти # молоти мелімось (Ми) @ verb:rev:impr:p:1
SFX L олотися елімось олотися # молотися мелімось (Ми) @ verb:rev:impr:p:1
SFX L олоти елімося олоти # молоти мелімося (Ми) @ verb:rev:impr:p:1
SFX L олотися елімося олотися # молотися мелімося (Ми) @ verb:rev:impr:p:1
SFX L олоти еліться олоти # молоти меліться (Ви) @ verb:rev:impr:p:2
SFX L олотися еліться олотися # молотися меліться (Ви) @ verb:rev:impr:p:2
SFX L істи ядусь істи # сісти сядусь (Я) @ verb:rev:futr:s:1
SFX L істися ядусь істися # сістися сядусь (Я) @ verb:rev:futr:s:1
SFX L істи ядуся істи # сісти сядуся (Я) @ verb:rev:futr:s:1
SFX L істися ядуся істися # сістися сядуся (Я) @ verb:rev:futr:s:1
SFX L істи ядешся істи # сісти сядешся (Ти) @ verb:rev:futr:s:2
SFX L істися ядешся істися # сістися сядешся (Ти) @ verb:rev:futr:s:2
SFX L істи ядеться істи # сісти сядеться (Він) @ verb:rev:futr:s:3
SFX L істися ядеться істися # сістися сядеться (Він) @ verb:rev:futr:s:3
SFX L істи ядемось істи # сісти сядемось (Ми) @ verb:rev:futr:p:1
SFX L істися ядемось істися # сістися сядемось (Ми) @ verb:rev:futr:p:1
SFX L істи ядемося істи # сісти сядемося (Ми) @ verb:rev:futr:p:1
SFX L істися ядемося істися # сістися сядемося (Ми) @ verb:rev:futr:p:1
SFX L істи ядетесь істи # сісти сядетесь (Ви) @ verb:rev:futr:p:2
SFX L істися ядетесь істися # сістися сядетесь (Ви) @ verb:rev:futr:p:2
SFX L істи ядетеся істи # сісти сядетеся (Ви) @ verb:rev:futr:p:2
SFX L істися ядетеся істися # сістися сядетеся (Ви) @ verb:rev:futr:p:2
SFX L істи ядуться істи # сісти сядуться (Вони) @ verb:rev:futr:p:3
SFX L істися ядуться істися # сістися сядуться (Вони) @ verb:rev:futr:p:3
SFX L сти лась істи # сісти сілась (Вона) @ verb:rev:past:p:f
SFX L стися лась істися # сістися сілась (Вона) @ verb:rev:past:p:f
SFX L сти лася істи # сісти сілася (Вона) @ verb:rev:past:p:f
SFX L стися лася істися # сістися сілася (Вона) @ verb:rev:past:p:f
SFX L сти лось істи # сісти сілось (Воно) @ verb:rev:past:p:n
SFX L стися лось істися # сістися сілось (Воно) @ verb:rev:past:p:n
SFX L сти лося істи # сісти сілося (Воно) @ verb:rev:past:p:n
SFX L стися лося істися # сістися сілося (Воно) @ verb:rev:past:p:n
SFX L сти лись істи # сісти сілись (Вони) @ verb:rev:past:p:p
SFX L стися лись істися # сістися сілись (Вони) @ verb:rev:past:p:p
SFX L сти лися істи # сісти сілися (Вони) @ verb:rev:past:p:p
SFX L стися лися істися # сістися сілися (Вони) @ verb:rev:past:p:p
SFX L сти всь істи # сісти сівсь (Він) @ verb:rev:past:p:m
SFX L стися всь істися # сістися сівсь (Він) @ verb:rev:past:p:m
SFX L сти вся істи # сісти сівся (Він) @ verb:rev:past:p:m
SFX L стися вся істися # сістися сівся (Він) @ verb:rev:past:p:m
SFX L сти вшись істи # сісти сівшись @ adp:rev:perf
SFX L стися вшись істися # сістися сівшись @ adp:rev:perf
SFX L істи ядься істи # сісти сядься (Ти) @ verb:rev:impr:s:2
SFX L істися ядься істися # сістися сядься (Ти) @ verb:rev:impr:s:2
SFX L істи ядьмось істи # сісти сядьмось (Ми) @ verb:rev:impr:p:1
SFX L істися ядьмось істися # сістися сядьмось (Ми) @ verb:rev:impr:p:1
SFX L істи ядьмося істи # сісти сядьмося (Ми) @ verb:rev:impr:p:1
SFX L істися ядьмося істися # сістися сядьмося (Ми) @ verb:rev:impr:p:1
SFX L істи ядьтесь істи # сісти сядьтесь (Ви) @ verb:rev:impr:p:2
SFX L істися ядьтесь істися # сістися сядьтесь (Ви) @ verb:rev:impr:p:2
SFX L істи ядьтеся істи # сісти сядьтеся (Ви) @ verb:rev:impr:p:2
SFX L істися ядьтеся істися # сістися сядьтеся (Ви) @ verb:rev:impr:p:2
SFX N Y 291
SFX N 0 ся ти # ~ти ~тися @ verb:inf:rev
SFX N 0 сь ти # ~ти ~ись @ verb:inf:rev
SFX N ся сь тися # ~тися ~ись @ verb:inf:rev
SFX N ти лась [^йіс]ти # ржати ржалась (Вона) @ verb:rev:past:f
SFX N тися лась [^йіс]тися # ржатися ржалась (Вона) @ verb:rev:past:f
SFX N ти лася [^йіс]ти # ржати ржалася (Вона) @ verb:rev:past:f
SFX N тися лася [^йіс]тися # ржатися ржалася (Вона) @ verb:rev:past:f
SFX N ти лось [^йіс]ти # ржати ржалось (Воно) @ verb:rev:past:n
SFX N тися лось [^йіс]тися # ржатися ржалось (Воно) @ verb:rev:past:n
SFX N ти лося [^йіс]ти # ржати ржалося (Воно) @ verb:rev:past:n
SFX N тися лося [^йіс]тися # ржатися ржалося (Воно) @ verb:rev:past:n
SFX N ти лись [^йіс]ти # ржати ржались (Вони) @ verb:rev:past:m
SFX N тися лись [^йіс]тися # ржатися ржались (Вони) @ verb:rev:past:m
SFX N ти лися [^йіс]ти # ржати ржалися (Вони) @ verb:rev:past:m
SFX N тися лися [^йіс]тися # ржатися ржалися (Вони) @ verb:rev:past:m
SFX N ти всь [аеиоу]ти # ржати ржавсь (Я, Ти, Він) @ verb:rev:past:m
SFX N тися всь [аеиоу]тися # ржатися ржавсь (Я, Ти, Він) @ verb:rev:past:m
SFX N ти вся [аеиоу]ти # ржати ржався (Я, Ти, Він) @ verb:rev:past:m
SFX N тися вся [аеиоу]тися # ржатися ржався (Я, Ти, Він) @ verb:rev:past:m
SFX N ти вшись [аеиоу]ти # ржати ржавшись @ adp:rev:perf
SFX N тися вшись [аеиоу]тися # ржатися ржавшись @ adp:rev:perf
SFX N ати усь ржати # ржати ржусь (Я) @ verb:rev:pres:s:1
SFX N атися усь ржатися # ржатися ржусь (Я) @ verb:rev:pres:s:1
SFX N ати уся ржати # ржати ржуся (Я) @ verb:rev:pres:s:1
SFX N атися уся ржатися # ржатися ржуся (Я) @ verb:rev:pres:s:1
SFX N ати ешся ржати # ржати ржешся (Ти) @ verb:rev:pres:s:2
SFX N атися ешся ржатися # ржатися ржешся (Ти) @ verb:rev:pres:s:2
SFX N ати еться ржати # ржати ржеться (Він) @ verb:rev:pres:s:3
SFX N атися еться ржатися # ржатися ржеться (Він) @ verb:rev:pres:s:3
SFX N ати емось ржати # ржати ржемось (Ми) @ verb:rev:pres:p:1
SFX N атися емось ржатися # ржатися ржемось (Ми) @ verb:rev:pres:p:1
SFX N ати емося ржати # ржати ржемося (Ми) @ verb:rev:pres:p:1
SFX N атися емося ржатися # ржатися ржемося (Ми) @ verb:rev:pres:p:1
SFX N ати етесь ржати # ржати ржетесь (Ви) @ verb:rev:pres:p:2
SFX N атися етесь ржатися # ржатися ржетесь (Ви) @ verb:rev:pres:p:2
SFX N ати етеся ржати # ржати ржетеся (Ви) @ verb:rev:pres:p:2
SFX N атися етеся ржатися # ржатися ржетеся (Ви) @ verb:rev:pres:p:2
SFX N ати уться ржати # ржати ржуться (Вони) @ verb:rev:pres:p:3
SFX N атися уться ржатися # ржатися ржуться (Вони) @ verb:rev:pres:p:3
SFX N ати ись ржати # ржати ржись (Ти) @ verb:rev:impr:s:2
SFX N атися ись ржатися # ржатися ржись (Ти) @ verb:rev:impr:s:2
SFX N ати ися ржати # ржати ржися (Ти) @ verb:rev:impr:s:2
SFX N атися ися ржатися # ржатися ржися (Ти) @ verb:rev:impr:s:2
SFX N ати імось ржати # ржати ржімось (Ми) @ verb:rev:impr:p:1
SFX N атися імось ржатися # ржатися ржімось (Ми) @ verb:rev:impr:p:1
SFX N ати імося ржати # ржати ржімося (Ми) @ verb:rev:impr:p:1
SFX N атися імося ржатися # ржатися ржімося (Ми) @ verb:rev:impr:p:1
SFX N ати іться ржати # ржати ржіться (Ви) @ verb:rev:impr:p:2
SFX N атися іться ржатися # ржатися ржіться (Ви) @ verb:rev:impr:p:2
SFX N жати іжмусь [^р]жати # жати жмусь (Я) @ verb:rev:futr:s:1
SFX N жатися іжмусь [^р]жатися # жатися жмусь (Я) @ verb:rev:futr:s:1
SFX N жати іжмуся [^р]жати # жати жмуся (Я) @ verb:rev:futr:s:1
SFX N жатися іжмуся [^р]жатися # жатися жмуся (Я) @ verb:rev:futr:s:1
SFX N жати іжмешся [^р]жати # жати жмешся (Ти) @ verb:rev:futr:s:2
SFX N жатися іжмешся [^р]жатися # жатися жмешся (Ти) @ verb:rev:futr:s:2
SFX N жати іжметься [^р]жати # жати жметься (Він) @ verb:rev:futr:s:3
SFX N жатися іжметься [^р]жатися # жатися жметься (Він) @ verb:rev:futr:s:3
SFX N жати іжмемось [^р]жати # жати жмемось (Ми) @ verb:rev:futr:p:1
SFX N жатися іжмемось [^р]жатися # жатися жмемось (Ми) @ verb:rev:futr:p:1
SFX N жати іжмемося [^р]жати # жати жмемося (Ми) @ verb:rev:futr:p:1
SFX N жатися іжмемося [^р]жатися # жатися жмемося (Ми) @ verb:rev:futr:p:1
SFX N жати іжметесь [^р]жати # жати жметесь (Ви) @ verb:rev:futr:p:2
SFX N жатися іжметесь [^р]жатися # жатися жметесь (Ви) @ verb:rev:futr:p:2
SFX N жати іжметеся [^р]жати # жати жметеся (Ви) @ verb:rev:futr:p:2
SFX N жатися іжметеся [^р]жатися # жатися жметеся (Ви) @ verb:rev:futr:p:2
SFX N жати іжмуться [^р]жати # жати жмуться (Вони) @ verb:rev:futr:p:3
SFX N жатися іжмуться [^р]жатися # жатися жмуться (Вони) @ verb:rev:futr:p:3
SFX N жати іжмись [^р]жати # жати жмись (Ти) @ verb:rev:impr:s:2
SFX N жатися іжмись [^р]жатися # жатися жмись (Ти) @ verb:rev:impr:s:2
SFX N жати іжмися [^р]жати # жати жмися (Ти) @ verb:rev:impr:s:2
SFX N жатися іжмися [^р]жатися # жатися жмися (Ти) @ verb:rev:impr:s:2
SFX N жати іжмімось [^р]жати # жати жмімось (Ми) @ verb:rev:impr:p:1
SFX N жатися іжмімось [^р]жатися # жатися жмімось (Ми) @ verb:rev:impr:p:1
SFX N жати іжмімося [^р]жати # жати жмімося (Ми) @ verb:rev:impr:p:1
SFX N жатися іжмімося [^р]жатися # жатися жмімося (Ми) @ verb:rev:impr:p:1
SFX N жати іжміться [^р]жати # жати жміться (Ви) @ verb:rev:impr:p:2
SFX N жатися іжміться [^р]жатися # жатися жміться (Ви) @ verb:rev:impr:p:2
SFX N ти дусь [йі]ти # йти йдусь (Я) @ verb:rev:pres:s:1
SFX N тися дусь [йі]тися # йтися йдусь (Я) @ verb:rev:pres:s:1
SFX N ти дуся [йі]ти # йти йдуся (Я) @ verb:rev:pres:s:1
SFX N тися дуся [йі]тися # йтися йдуся (Я) @ verb:rev:pres:s:1
SFX N ти дешся [йі]ти # йти йдешся (Ти) @ verb:rev:pres:s:2
SFX N тися дешся [йі]тися # йтися йдешся (Ти) @ verb:rev:pres:s:2
SFX N ти деться [йі]ти # йти йдеться (Він) @ verb:rev:pres:s:3
SFX N тися деться [йі]тися # йтися йдеться (Він) @ verb:rev:pres:s:3
SFX N ти демось [йі]ти # йти йдемось (Ми) @ verb:rev:pres:p:1
SFX N тися демось [йі]тися # йтися йдемось (Ми) @ verb:rev:pres:p:1
SFX N ти демося [йі]ти # йти йдемося (Ми) @ verb:rev:pres:p:1
SFX N тися демося [йі]тися # йтися йдемося (Ми) @ verb:rev:pres:p:1
SFX N ти детесь [йі]ти # йти йдетесь (Ви) @ verb:rev:pres:p:2
SFX N тися детесь [йі]тися # йтися йдетесь (Ви) @ verb:rev:pres:p:2
SFX N ти детеся [йі]ти # йти йдетеся (Ви) @ verb:rev:pres:p:2
SFX N тися детеся [йі]тися # йтися йдетеся (Ви) @ verb:rev:pres:p:2
SFX N ти дуться [йі]ти # йти йдуться (Вони) @ verb:rev:pres:p:3
SFX N тися дуться [йі]тися # йтися йдуться (Вони) @ verb:rev:pres:p:3
SFX N ти шовсь [йі]ти # йти йшовсь (Я, Ти, Він) @ verb:rev:past:m
SFX N тися шовсь [йі]тися # йтися йшовсь (Я, Ти, Він) @ verb:rev:past:m
SFX N ти шовся [йі]ти # йти йшовся (Я, Ти, Він) @ verb:rev:past:m
SFX N тися шовся [йі]тися # йтися йшовся (Я, Ти, Він) @ verb:rev:past:m
SFX N ти шлась [йі]ти # йти йшлась (Вона) @ verb:rev:past:f
SFX N тися шлась [йі]тися # йтися йшлась (Вона) @ verb:rev:past:f
SFX N ти шлася [йі]ти # йти йшлася (Вона) @ verb:rev:past:f
SFX N тися шлася [йі]тися # йтися йшлася (Вона) @ verb:rev:past:f
SFX N ти шлось [йі]ти # йти йшлось (Воно) @ verb:rev:past:n
SFX N тися шлось [йі]тися # йтися йшлось (Воно) @ verb:rev:past:n
SFX N ти шлося [йі]ти # йти йшлося (Воно) @ verb:rev:past:n
SFX N тися шлося [йі]тися # йтися йшлося (Воно) @ verb:rev:past:n
SFX N ти шлись [йі]ти # йти йшлись (Вони) @ verb:rev:past:p
SFX N тися шлись [йі]тися # йтися йшлись (Вони) @ verb:rev:past:p
SFX N ти шлися [йі]ти # йти йшлися (Вони) @ verb:rev:past:p
SFX N тися шлися [йі]тися # йтися йшлися (Вони) @ verb:rev:past:p
SFX N ти шовшись [йі]ти # йти йшовшись @ adp:rev:perf
SFX N тися шовшись [йі]тися # йтися йшовшись @ adp:rev:perf
SFX N ти дись [йі]ти # йти йдись (Ти) @ verb:rev:impr:s:2
SFX N тися дись [йі]тися # йтися йдись (Ти) @ verb:rev:impr:s:2
SFX N ти дися [йі]ти # йти йдися (Ти) @ verb:rev:impr:s:2
SFX N тися дися [йі]тися # йтися йдися (Ти) @ verb:rev:impr:s:2
SFX N ти дімось [йі]ти # йти йдімось (Ми) @ verb:rev:impr:p:1
SFX N тися дімось [йі]тися # йтися йдімось (Ми) @ verb:rev:impr:p:1
SFX N ти дімося [йі]ти # йти йдімося (Ми) @ verb:rev:impr:p:1
SFX N тися дімося [йі]тися # йтися йдімося (Ми) @ verb:rev:impr:p:1
SFX N ти діться [йі]ти # йти йдіться (Ви) @ verb:rev:impr:p:2
SFX N тися діться [йі]тися # йтися йдіться (Ви) @ verb:rev:impr:p:2
SFX N хати дусь хати # їхати їдусь (Я) @ verb:rev:pres:s:1
SFX N хатися дусь хатися # їхатися їдусь (Я) @ verb:rev:pres:s:1
SFX N хати дуся хати # їхати їдуся (Я) @ verb:rev:pres:s:1
SFX N хатися дуся хатися # їхатися їдуся (Я) @ verb:rev:pres:s:1
SFX N хати дешся хати # їхати їдешся (Ти) @ verb:rev:pres:s:2
SFX N хатися дешся хатися # їхатися їдешся (Ти) @ verb:rev:pres:s:2
SFX N хати деться хати # їхати їдеться (Він) @ verb:rev:pres:s:3
SFX N хатися деться хатися # їхатися їдеться (Він) @ verb:rev:pres:s:3
SFX N хати демось хати # їхати їдемось (Ми) @ verb:rev:pres:p:1
SFX N хатися демось хатися # їхатися їдемось (Ми) @ verb:rev:pres:p:1
SFX N хати демося хати # їхати їдемося (Ми) @ verb:rev:pres:p:1
SFX N хатися демося хатися # їхатися їдемося (Ми) @ verb:rev:pres:p:1
SFX N хати детесь хати # їхати їдетесь (Ви) @ verb:rev:pres:p:2
SFX N хатися детесь хатися # їхатися їдетесь (Ви) @ verb:rev:pres:p:2
SFX N хати детеся хати # їхати їдетеся (Ви) @ verb:rev:pres:p:2
SFX N хатися детеся хатися # їхатися їдетеся (Ви) @ verb:rev:pres:p:2
SFX N хати дуться хати # їхати їдуться (Вони) @ verb:rev:pres:p:3
SFX N хатися дуться хатися # їхатися їдуться (Вони) @ verb:rev:pres:p:3
SFX N хати дься хати # їхати їдься (Ти) @ verb:rev:impr:s:2
SFX N хатися дься хатися # їхатися їдься (Ти) @ verb:rev:impr:s:2
SFX N хати дьмось хати # їхати їдьмось (Ми) @ verb:rev:impr:p:1
SFX N хатися дьмось хатися # їхатися їдьмось (Ми) @ verb:rev:impr:p:1
SFX N хати дьмося хати # їхати їдьмося (Ми) @ verb:rev:impr:p:1
SFX N хатися дьмося хатися # їхатися їдьмося (Ми) @ verb:rev:impr:p:1
SFX N хати дьтесь хати # їхати їдьтесь (Ви) @ verb:rev:impr:p:2
SFX N хатися дьтесь хатися # їхатися їдьтесь (Ви) @ verb:rev:impr:p:2
SFX N хати дьтеся хати # їхати їдьтеся (Ви) @ verb:rev:impr:p:2
SFX N хатися дьтеся хатися # їхатися їдьтеся (Ви) @ verb:rev:impr:p:2
SFX N гнати женусь гнати # гнати женусь (Я) @ verb:rev:pres:s:1
SFX N гнатися женусь гнатися # гнатися женусь (Я) @ verb:rev:pres:s:1
SFX N гнати женуся гнати # гнати женуся (Я) @ verb:rev:pres:s:1
SFX N гнатися женуся гнатися # гнатися женуся (Я) @ verb:rev:pres:s:1
SFX N гнати женешся гнати # гнати женешся (Ти) @ verb:rev:pres:s:2
SFX N гнатися женешся гнатися # гнатися женешся (Ти) @ verb:rev:pres:s:2
SFX N гнати женеться гнати # гнати женеться (Він) @ verb:rev:pres:s:3
SFX N гнатися женеться гнатися # гнатися женеться (Він) @ verb:rev:pres:s:3
SFX N гнати женемось гнати # гнати женемось (Ми) @ verb:rev:pres:p:1
SFX N гнатися женемось гнатися # гнатися женемось (Ми) @ verb:rev:pres:p:1
SFX N гнати женемося гнати # гнати женемося (Ми) @ verb:rev:pres:p:1
SFX N гнатися женемося гнатися # гнатися женемося (Ми) @ verb:rev:pres:p:1
SFX N гнати женетесь гнати # гнати женетесь (Ви) @ verb:rev:pres:p:2
SFX N гнатися женетесь гнатися # гнатися женетесь (Ви) @ verb:rev:pres:p:2
SFX N гнати женетеся гнати # гнати женетеся (Ви) @ verb:rev:pres:p:2
SFX N гнатися женетеся гнатися # гнатися женетеся (Ви) @ verb:rev:pres:p:2
SFX N гнати женуться гнати # гнати женуться (Вони) @ verb:rev:pres:p:3
SFX N гнатися женуться гнатися # гнатися женуться (Вони) @ verb:rev:pres:p:3
SFX N гнати женись гнати # гнати женись (Ти) @ verb:rev:impr:s:2
SFX N гнатися женись гнатися # гнатися женись (Ти) @ verb:rev:impr:s:2
SFX N гнати женися гнати # гнати женися (Ти) @ verb:rev:impr:s:2
SFX N гнатися женися гнатися # гнатися женися (Ти) @ verb:rev:impr:s:2
SFX N гнати женімось гнати # гнати женімось (Ми) @ verb:rev:impr:p:1
SFX N гнатися женімось гнатися # гнатися женімось (Ми) @ verb:rev:impr:p:1
SFX N гнати женімося гнати # гнати женімося (Ми) @ verb:rev:impr:p:1
SFX N гнатися женімося гнатися # гнатися женімося (Ми) @ verb:rev:impr:p:1
SFX N гнати женіться гнати # гнати женіться (Ви) @ verb:rev:impr:p:2
SFX N гнатися женіться гнатися # гнатися женіться (Ви) @ verb:rev:impr:p:2
SFX N ти нусь [ия]гти # одягти одягнусь (Я) @ verb:rev:futr:s:1
SFX N тися нусь [ия]гтися # одягтися одягнусь (Я) @ verb:rev:futr:s:1
SFX N ти нуся [ия]гти # одягти одягнуся (Я) @ verb:rev:futr:s:1
SFX N тися нуся [ия]гтися # одягтися одягнуся (Я) @ verb:rev:futr:s:1
SFX N ти нешся [ия]гти # одягти одягнешся (Ти) @ verb:rev:futr:s:2
SFX N тися нешся [ия]гтися # одягтися одягнешся (Ти) @ verb:rev:futr:s:2
SFX N ти неться [ия]гти # одягти одягнеться (Він) @ verb:rev:futr:s:3
SFX N тися неться [ия]гтися # одягтися одягнеться (Він) @ verb:rev:futr:s:3
SFX N ти немось [ия]гти # одягти одягнемось (Ми) @ verb:rev:futr:p:1
SFX N тися немось [ия]гтися # одягтися одягнемось (Ми) @ verb:rev:futr:p:1
SFX N ти немося [ия]гти # одягти одягнемося (Ми) @ verb:rev:futr:p:1
SFX N тися немося [ия]гтися # одягтися одягнемося (Ми) @ verb:rev:futr:p:1
SFX N ти нетесь [ия]гти # одягти одягнетесь (Ви) @ verb:rev:futr:p:2
SFX N тися нетесь [ия]гтися # одягтися одягнетесь (Ви) @ verb:rev:futr:p:2
SFX N ти нетеся [ия]гти # одягти одягнетеся (Ви) @ verb:rev:futr:p:2
SFX N тися нетеся [ия]гтися # одягтися одягнетеся (Ви) @ verb:rev:futr:p:2
SFX N ти нуться [ия]гти # одягти одягнуться (Вони) @ verb:rev:futr:p:3
SFX N тися нуться [ия]гтися # одягтися одягнуться (Вони) @ verb:rev:futr:p:3
SFX N гти гся [ия]гти # одягти одягся (Я, Ти, Він) @ verb:rev:past:s:m
SFX N гтися гся [ия]гтися # одягтися одягся (Я, Ти, Він) @ verb:rev:past:s:m
SFX N гти гшись [ия]гти # одягти одягшись @ adp:rev:perf
SFX N гтися гшись [ия]гтися # одягтися одягшись @ adp:rev:perf
SFX N ти нись ягти # одягти одягнись (Ти) @ verb:rev:impr:s:2
SFX N тися нись ягтися # одягтися одягнись (Ти) @ verb:rev:impr:s:2
SFX N ти нися ягти # одягти одягнися (Ти) @ verb:rev:impr:s:2
SFX N тися нися ягтися # одягтися одягнися (Ти) @ verb:rev:impr:s:2
SFX N ти німось ягти # одягти одягнімось (Ми) @ verb:rev:impr:p:1
SFX N тися німось ягтися # одягтися одягнімось (Ми) @ verb:rev:impr:p:1
SFX N ти німося ягти # одягти одягнімося (Ми) @ verb:rev:impr:p:1
SFX N тися німося ягтися # одягтися одягнімося (Ми) @ verb:rev:impr:p:1
SFX N ти ніться ягти # одягти одягніться (Ви) @ verb:rev:impr:p:2
SFX N тися ніться ягтися # одягтися одягніться (Ви) @ verb:rev:impr:p:2
SFX N сти дусь [еая]сти # вести ведусь (Я) @ verb:rev:pres:s:1
SFX N стися дусь [еая]стися # вестися ведусь (Я) @ verb:rev:pres:s:1
SFX N сти дуся [еая]сти # вести ведуся (Я) @ verb:rev:pres:s:1
SFX N стися дуся [еая]стися # вестися ведуся (Я) @ verb:rev:pres:s:1
SFX N сти дешся [еая]сти # вести ведешся (Ти) @ verb:rev:pres:s:2
SFX N стися дешся [еая]стися # вестися ведешся (Ти) @ verb:rev:pres:s:2
SFX N сти деться [еая]сти # вести ведеться (Він) @ verb:rev:pres:s:3
SFX N стися деться [еая]стися # вестися ведеться (Він) @ verb:rev:pres:s:3
SFX N сти демось [еая]сти # вести ведемось (Ми) @ verb:rev:pres:p:1
SFX N стися демось [еая]стися # вестися ведемось (Ми) @ verb:rev:pres:p:1
SFX N сти демося [еая]сти # вести ведемося (Ми) @ verb:rev:pres:p:1
SFX N стися демося [еая]стися # вестися ведемося (Ми) @ verb:rev:pres:p:1
SFX N сти детесь [еая]сти # вести ведетесь (Ви) @ verb:rev:pres:p:2
SFX N стися детесь [еая]стися # вестися ведетесь (Ви) @ verb:rev:pres:p:2
SFX N сти детеся [еая]сти # вести ведетеся (Ви) @ verb:rev:pres:p:2
SFX N стися детеся [еая]стися # вестися ведетеся (Ви) @ verb:rev:pres:p:2
SFX N сти дуться [еая]сти # вести ведуться (Вони) @ verb:rev:pres:p:3
SFX N стися дуться [еая]стися # вестися ведуться (Вони) @ verb:rev:pres:p:3
SFX N сти лась сти # їсти їлась (Вона) @ verb:rev:past:s:f
SFX N стися лась стися # їстися їлась (Вона) @ verb:rev:past:s:f
SFX N сти лася сти # їсти їлася (Вона) @ verb:rev:past:s:f
SFX N стися лася стися # їстися їлася (Вона) @ verb:rev:past:s:f
SFX N сти лось сти # їсти їлось (Воно) @ verb:rev:past:s:n
SFX N стися лось стися # їстися їлось (Воно) @ verb:rev:past:s:n
SFX N сти лося сти # їсти їлося (Воно) @ verb:rev:past:s:n
SFX N стися лося стися # їстися їлося (Воно) @ verb:rev:past:s:n
SFX N сти лись сти # їсти їлись (Вони) @ verb:rev:past:s:p
SFX N стися лись стися # їстися їлись (Вони) @ verb:rev:past:s:p
SFX N сти лися сти # їсти їлися (Вони) @ verb:rev:past:s:p
SFX N стися лися стися # їстися їлися (Вони) @ verb:rev:past:s:p
SFX N сти всь [^е]сти # їсти ївсь (Він) @ verb:rev:past:s:m
SFX N стися всь [^е]стися # їстися ївсь (Він) @ verb:rev:past:s:m
SFX N сти вся [^е]сти # їсти ївся (Він) @ verb:rev:past:s:m
SFX N стися вся [^е]стися # їстися ївся (Він) @ verb:rev:past:s:m
SFX N ести івсь ести # вести вівсь (Я, Ти, Він) @ verb:rev:past:s:m
SFX N естися івсь естися # вестися вівсь (Я, Ти, Він) @ verb:rev:past:s:m
SFX N ести івся ести # вести вівся (Я, Ти, Він) @ verb:rev:past:s:m
SFX N естися івся естися # вестися вівся (Я, Ти, Він) @ verb:rev:past:s:m
SFX N ести івшись ести # вести вівшись @ adp:rev:perf
SFX N естися івшись естися # вестися вівшись @ adp:rev:perf
SFX N сти вшись [^е]сти # їсти ївшись @ adp:rev:perf
SFX N стися вшись [^е]стися # їстися ївшись @ adp:rev:perf
SFX N сти дись [еая]сти # вести ведись (Ти) @ verb:rev:impr:s:2
SFX N стися дись [еая]стися # вестися ведись (Ти) @ verb:rev:impr:s:2
SFX N сти дися [еая]сти # вести ведися (Ти) @ verb:rev:impr:s:2
SFX N стися дися [еая]стися # вестися ведися (Ти) @ verb:rev:impr:s:2
SFX N сти дімось [еая]сти # вести веімось (Ми) @ verb:rev:impr:p:1
SFX N стися дімось [еая]стися # вестися веімось (Ми) @ verb:rev:impr:p:1
SFX N сти дімося [еая]сти # вести веімося (Ми) @ verb:rev:impr:p:1
SFX N стися дімося [еая]стися # вестися веімося (Ми) @ verb:rev:impr:p:1
SFX N сти діться [еая]сти # вести ведіться (Ви) @ verb:rev:impr:p:2
SFX N стися діться [еая]стися # вестися ведіться (Ви) @ verb:rev:impr:p:2
SFX N сти мся [ії]сти # їсти їмся (Я) @ verb:rev:pres:s:1
SFX N стися мся [ії]стися # їстися їмся (Я) @ verb:rev:pres:s:1
SFX N ти ись [ії]сти # їсти їсись (Ти) @ verb:rev:pres:s:2
SFX N тися ись [ії]стися # їстися їсись (Ти) @ verb:rev:pres:s:2
SFX N ти ися [ії]сти # їсти їсися (Ти) @ verb:rev:pres:s:2
SFX N тися ися [ії]стися # їстися їсися (Ти) @ verb:rev:pres:s:2
SFX N ти ться [ії]сти # їсти їсться (Він) @ verb:rev:pres:s:3
SFX N тися ться [ії]стися # їстися їсться (Він) @ verb:rev:pres:s:3
SFX N сти мось [ії]сти # їсти їмось (Ми) @ verb:rev:pres:p:1
SFX N стися мось [ії]стися # їстися їмось (Ми) @ verb:rev:pres:p:1
SFX N сти мося [ії]сти # їсти їмося (Ми) @ verb:rev:pres:p:1
SFX N стися мося [ії]стися # їстися їмося (Ми) @ verb:rev:pres:p:1
SFX N ти тесь [ії]сти # їсти їстесь (Ви) @ verb:rev:pres:p:2
SFX N тися тесь [ії]стися # їстися їстесь (Ви) @ verb:rev:pres:p:2
SFX N ти теся [ії]сти # їсти їстеся (Ви) @ verb:rev:pres:p:2
SFX N тися теся [ії]стися # їстися їстеся (Ви) @ verb:rev:pres:p:2
SFX N сти дяться їсти # їсти їдяться (Вони) @ verb:rev:pres:p:3
SFX N стися дяться їстися # їстися їдяться (Вони) @ verb:rev:pres:p:3
SFX N сти жся їсти # їсти їжся (Ти) @ verb:rev:impr:s:2
SFX N стися жся їстися # їстися їжся (Ти) @ verb:rev:impr:s:2
SFX N сти жмось їсти # їсти їжмось (Ми) @ verb:rev:impr:p:1
SFX N стися жмось їстися # їстися їжмось (Ми) @ verb:rev:impr:p:1
SFX N сти жмося їсти # їсти їжмося (Ми) @ verb:rev:impr:p:1
SFX N стися жмося їстися # їстися їжмося (Ми) @ verb:rev:impr:p:1
SFX N сти жтесь їсти # їсти їжтесь (Ви) @ verb:rev:impr:p:2
SFX N стися жтесь їстися # їстися їжтесь (Ви) @ verb:rev:impr:p:2
SFX N сти жтеся їсти # їсти їжтеся (Ви) @ verb:rev:impr:p:2
SFX N стися жтеся їстися # їстися їжтеся (Ви) @ verb:rev:impr:p:2
SFX O Y 42
SFX O вати ючи [ауюя]вати # абонувати абонуючи @ adp:imperf
SFX O ати учи [рз]вати # рвати рвучи @ adp:imperf
SFX O зати жучи зати # казати кажучи @ adp:imperf
SFX O ати ачи [днжщ]ати # блищати блищучи @ adp:imperf
SFX O ати ачи [^ао]чати # деренчати деренчачи @ adp:imperf
SFX O тати чучи [^с]тати # шептати шепчучи @ adp:imperf
SFX O кати чучи [^с]кати # плакати плачучи @ adp:imperf
SFX O сати шучи сати # писати пишучи @ adp:imperf
SFX O хати шучи хати # брехати брешучи @ adp:imperf
SFX O стати щучи стати # свистати свищучи @ adp:imperf
SFX O скати щучи скати # плескати плещучи @ adp:imperf
SFX O слати шлючи слати # послати пошлючи @ adp:imperf
SFX O ати лючи пати # сипати сиплючи @ adp:imperf
SFX O ати ючи орати # орати орючи @ adp:imperf
SFX O рати еручи [бдп]рати # брати беручи @ adp:imperf
SFX O ити ячи дити # винаходити винаходячи @ adp:imperf
SFX O здити жджучи здити # їздити їжджучи @ adp:imperf
SFX O ити ячи [^о]зити # возити вожучи @ adp:imperf
SFX O ити ячи озити # возити вожучи @ adp:imperf
SFX O ити ачи [жчшщ]ити # бентежити бентежучи @ adp:imperf
SFX O ити ячи [лснр]ити # місити місячи @ adp:imperf
SFX O ити лячи [бвмпф]ити # робити роблячи @ adp:imperf
SFX O тити чучи [^с]тити # тратити трачучи @ adp:imperf
SFX O ити ячи тити # мостити мостячи @ adp:imperf
SFX O іти ячи діти # смердіти смердячи @ adp:imperf
SFX O іти ачи [шж]іти # кишіти кишучи @ adp:imperf
SFX O іти ячи [нлртс]іти # летіти летячи @ adp:imperf
SFX O іти учи діти # гудіти гудучи @ adp:imperf
SFX O іти лячи [бвмпф]іти # шуміти шумлячи @ adp:imperf
SFX O ути учи нути # тягнути тягнучи @ adp:imperf
SFX O ти дучи бути # забути забудучи @ adp:imperf
SFX O оти ючи оти # бороти борючи @ adp:imperf
SFX O їти ячи їти # клеїти клеючи @ adp:imperf
SFX O ти учи [збв]ти # везти везучи @ adp:imperf
SFX O ти тучи ости # рости ростучи @ adp:imperf
SFX O сти тучи [еі]сти # цвісти цвітучи @ adp:imperf
SFX O сти нучи лясти # клясти клянучи @ adp:imperf
SFX O кти чучи кти # текти течучи @ adp:imperf
SFX O гти жучи [еоиія]гти # допомогти допоможучи @ adp:imperf
SFX O ерти ручи [^дж]ерти # терти тручи @ adp:imperf
SFX O рти ручи [дж]ерти # жерти жеручи @ adp:imperf
SFX O ти ючи [аі]яти # паяти паяючи @ adp:imperf
SFX Q Y 8
SFX Q яти ючи [аяіо]яти # віяти віючи @ adp:imperf
SFX Q ти ючи [илнрцд]яти # ганяти ганяючи @ adp:imperf
SFX Q ти ючи [аіу]ти # вбивати вбиваючи @ adp:imperf
SFX Q ити 'ючи [бвп]ити # бити б'ючи @ adp:imperf
SFX Q ити иючи [врмнш]ити # рити риючи @ adp:imperf
SFX Q ити лючи лити # лити ллючи @ adp:imperf
SFX Q ти вучи жити # жити живучи @ adp:imperf
SFX Q ти учи сти # пасти пасучи @ adp:imperf
SFX P Y 84
SFX P вати ючись [ауюя]вати # абонувати абонуючись @ adp:rev:imperf
SFX P ватися ючись [ауюя]ватися # абонуватися абонуючись @ adp:rev:imperf
SFX P ати учись [рз]вати # рвати рвучись @ adp:rev:imperf
SFX P атися учись [рз]ватися # рватися рвучись @ adp:rev:imperf
SFX P зати жучись зати # казати кажучись @ adp:rev:imperf
SFX P затися жучись затися # казатися кажучись @ adp:rev:imperf
SFX P ати ачись [днжщ]ати # блищати блищучись @ adp:rev:imperf
SFX P атися ачись [днжщ]атися # блищатися блищучись @ adp:rev:imperf
SFX P ати ачись [^ао]чати # деренчати деренчачись @ adp:rev:imperf
SFX P атися ачись [^ао]чатися # деренчатися деренчачись @ adp:rev:imperf
SFX P тати чучись [^с]тати # шептати шепчучись @ adp:rev:imperf
SFX P татися чучись [^с]татися # шептатися шепчучись @ adp:rev:imperf
SFX P кати чучись [^с]кати # плакати плачучись @ adp:rev:imperf
SFX P катися чучись [^с]катися # плакатися плачучись @ adp:rev:imperf
SFX P сати шучись сати # писати пишучись @ adp:rev:imperf
SFX P сатися шучись сатися # писатися пишучись @ adp:rev:imperf
SFX P хати шучись хати # брехати брешучись @ adp:rev:imperf
SFX P хатися шучись хатися # брехатися брешучись @ adp:rev:imperf
SFX P стати щучись стати # свистати свищучись @ adp:rev:imperf
SFX P статися щучись статися # свистатися свищучись @ adp:rev:imperf
SFX P скати щучись скати # плескати плещучись @ adp:rev:imperf
SFX P скатися щучись скатися # плескатися плещучись @ adp:rev:imperf
SFX P слати шлючись слати # послати пошлючись @ adp:rev:imperf
SFX P слатися шлючись слатися # послатися пошлючись @ adp:rev:imperf
SFX P ати лючись пати # сипати сиплючись @ adp:rev:imperf
SFX P атися лючись патися # сипатися сиплючись @ adp:rev:imperf
SFX P ати ючись орати # орати орючись @ adp:rev:imperf
SFX P атися ючись оратися # оратися орючись @ adp:rev:imperf
SFX P рати еручись [бдп]рати # брати беручись @ adp:rev:imperf
SFX P ратися еручись [бдп]ратися # братися беручись @ adp:rev:imperf
SFX P ити ячись дити # винаходити винаходячись @ adp:rev:imperf
SFX P итися ячись дитися # винаходитися винаходячись @ adp:rev:imperf
SFX P здити жджучись здити # їздити їжджучись @ adp:rev:imperf
SFX P здитися жджучись здитися # їздитися їжджучись @ adp:rev:imperf
SFX P ити ячись [^о]зити # возити вожучись @ adp:rev:imperf
SFX P итися ячись [^о]зитися # возитися вожучись @ adp:rev:imperf
SFX P ити ячись озити # возити вожучись @ adp:rev:imperf
SFX P итися ячись озитися # возитися вожучись @ adp:rev:imperf
SFX P ити ачись [жчшщ]ити # бентежити бентежучись @ adp:rev:imperf
SFX P итися ачись [жчшщ]итися # бентежитися бентежучись @ adp:rev:imperf
SFX P ити ячись [лснр]ити # місити місячись @ adp:rev:imperf
SFX P итися ячись [лснр]итися # міситися місячись @ adp:rev:imperf
SFX P ити лячись [бвмпф]ити # робити роблячись @ adp:rev:imperf
SFX P итися лячись [бвмпф]итися # робитися роблячись @ adp:rev:imperf
SFX P тити чучись [^с]тити # тратити трачучись @ adp:rev:imperf
SFX P титися чучись [^с]титися # тратитися трачучись @ adp:rev:imperf
SFX P ити ячись тити # мостити мостячись @ adp:rev:imperf
SFX P итися ячись титися # моститися мостячись @ adp:rev:imperf
SFX P іти ячись діти # смердіти смердячись @ adp:rev:imperf
SFX P ітися ячись дітися # смердітися смердячись @ adp:rev:imperf
SFX P іти ачись [шж]іти # кишіти кишучись @ adp:rev:imperf
SFX P ітися ачись [шж]ітися # кишітися кишучись @ adp:rev:imperf
SFX P іти ячись [нлртс]іти # летіти летячись @ adp:rev:imperf
SFX P ітися ячись [нлртс]ітися # летітися летячись @ adp:rev:imperf
SFX P іти учись діти # гудіти гудучись @ adp:rev:imperf
SFX P ітися учись дітися # гудітися гудучись @ adp:rev:imperf
SFX P іти лячись [бвмпф]іти # шуміти шумлячись @ adp:rev:imperf
SFX P ітися лячись [бвмпф]ітися # шумітися шумлячись @ adp:rev:imperf
SFX P ути учись нути # тягнути тягнучись @ adp:rev:imperf
SFX P утися учись нутися # тягнутися тягнучись @ adp:rev:imperf
SFX P ти дучись бути # забути забудучись @ adp:rev:imperf
SFX P тися дучись бутися # забутися забудучись @ adp:rev:imperf
SFX P оти ючись оти # бороти борючись @ adp:rev:imperf
SFX P отися ючись отися # боротися борючись @ adp:rev:imperf
SFX P їти ячись їти # клеїти клеючись @ adp:rev:imperf
SFX P їтися ячись їтися # клеїтися клеючись @ adp:rev:imperf
SFX P ти учись [збв]ти # везти везучись @ adp:rev:imperf
SFX P тися учись [збв]тися # везтися везучись @ adp:rev:imperf
SFX P ти тучись ости # рости ростучись @ adp:rev:imperf
SFX P тися тучись остися # ростися ростучись @ adp:rev:imperf
SFX P сти тучись [еі]сти # цвісти цвітучись @ adp:rev:imperf
SFX P стися тучись [еі]стися # цвістися цвітучись @ adp:rev:imperf
SFX P сти нучись лясти # клясти клянучись @ adp:rev:imperf
SFX P стися нучись лястися # клястися клянучись @ adp:rev:imperf
SFX P кти чучись кти # текти течучись @ adp:rev:imperf
SFX P ктися чучись ктися # тектися течучись @ adp:rev:imperf
SFX P гти жучись [еоиія]гти # допомогти допоможучись @ adp:rev:imperf
SFX P гтися жучись [еоиія]гтися # допомогтися допоможучись @ adp:rev:imperf
SFX P ерти ручись [^дж]ерти # терти тручись @ adp:rev:imperf
SFX P ертися ручись [^дж]ертися # тертися тручись @ adp:rev:imperf
SFX P рти ручись [дж]ерти # жерти жеручись @ adp:rev:imperf
SFX P ртися ручись [дж]ертися # жертися жеручись @ adp:rev:imperf
SFX P ти ючись [аі]яти # паяти паяючись @ adp:rev:imperf
SFX P тися ючись [аі]ятися # паятися паяючись @ adp:rev:imperf
SFX R Y 16
SFX R яти ючись [аяіо]яти # віяти віючись @ adp:rev:imperf
SFX R ятися ючись [аяіо]ятися # віятися віючись @ adp:rev:imperf
SFX R ти ючись [илнрцд]яти # ганяти ганяючись @ adp:rev:imperf
SFX R тися ючись [илнрцд]ятися # ганятися ганяючись @ adp:rev:imperf
SFX R ти ючись [аіу]ти # вбивати вбиваючись @ adp:rev:imperf
SFX R тися ючись [аіу]тися # вбиватися вбиваючись @ adp:rev:imperf
SFX R ити 'ючись [бвп]ити # бити б'ючись @ adp:rev:imperf
SFX R итися 'ючись [бвп]итися # битися б'ючись @ adp:rev:imperf
SFX R ити иючись [врмнш]ити # рити риючись @ adp:rev:imperf
SFX R итися иючись [врмнш]итися # ритися риючись @ adp:rev:imperf
SFX R ити лючись лити # лити ллючись @ adp:rev:imperf
SFX R итися лючись литися # литися ллючись @ adp:rev:imperf
SFX R ти вучись жити # жити живучись @ adp:rev:imperf
SFX R тися вучись житися # житися живучись @ adp:rev:imperf
SFX R ти учись сти # пасти пасучись @ adp:rev:imperf
SFX R тися учись стися # пастися пасучись @ adp:rev:imperf
PFX Z Y 1
PFX Z 0 не . # голосний > неголосний
PFX Y Y 1
PFX Y 0 най . # голосніший > найголосніший
PFX X Y 2
PFX X в у в[^'] # вбити > убити
PFX X в' у в' # в'їзд > уїзд
